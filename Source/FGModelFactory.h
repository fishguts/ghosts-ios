//
//  FGGhostFactory.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGMessageVO;
@class FGProfileVO;
@class FGProfileVO;
@class FGExpirationVO;


@interface FGModelFactory : FGStaticClass

+ (FGMessageVO*)createMessage:(NSString*)text expiration:(FGExpirationVO*)expiration;
+ (void)updateMessage:(FGMessageVO*)message text:(NSString*)text expiration:(FGExpirationVO*)expiration;

+ (FGProfileVO*)createLogin:(NSString*)name password:(NSString*)password;
+ (FGProfileVO*)createRegistration:(NSString*)name email:(NSString*)email password:(NSString*)password;
@end
