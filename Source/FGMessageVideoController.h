//
//  FGMessageXRayController.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/13/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGLocationFilterBase.h"


@interface FGMessageVideoController : UIViewController <FGModalDelegate, FGLocationFilterDelegate>

@end
