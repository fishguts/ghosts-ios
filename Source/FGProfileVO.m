//
//  FGProfileVO.m
//  Goggles
//
//  Created by Curtis Elsasser on 11/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGProfileVO.h"
#import "FGMessageVO.h"


@implementation FGProfileVO

@dynamic created;
@dynamic email;
@dynamic imageUrlFull;
@dynamic imageUrlThumb;
@dynamic loggedIn;
@dynamic login;
@dynamic password;
@dynamic status;
@dynamic username;
@dynamic messages;

@end
