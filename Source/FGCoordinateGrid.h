//
//  FGCoordinateGrid.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSObject.h>
#import <CoreLocation/CoreLocation.h>


@interface FGCoordinateGrid : NSObject
/**** Public Properties ****/
@property (readonly, nonatomic) CLLocationDegrees left;
@property (readonly, nonatomic) CLLocationDegrees right;
@property (readonly, nonatomic) CLLocationDegrees top;
@property (readonly, nonatomic) CLLocationDegrees bottom;
@property (readonly, nonatomic) CLLocationDegrees width;
@property (readonly, nonatomic) CLLocationDegrees height;
@property (readonly, nonatomic) CLLocationDegrees horizontalCenter;
@property (readonly, nonatomic) CLLocationDegrees verticalCenter;

/**** Public Interface ****/
- (void)reset;
- (BOOL)isEmpty;

- (void)addCoordinate:(CLLocationCoordinate2D)coordinate;
- (BOOL)contains:(CLLocationCoordinate2D)coordinate;

@end
