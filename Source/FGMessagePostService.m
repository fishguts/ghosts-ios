//
//  FGPostMessageService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessagePostService.h"
#import "FGModel.h"
#import "FGStatusVO.h"
#import "FGMessageVO.h"
#import "FGModelSerializer.h"
#import "FGMessageProxy.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGMessagePostService
#pragma mark - Public Interface
- (id)initWithMessage:(FGMessageVO *)message
{
	self=[super init];
	self->_message=message;
	// #34 - we take him out of his current status immediately so that he does not not get
	// reposted (as error, for example) between the time he was queued and actually processed
	[FGMessageProxy setMessage:self->_message status:FGMessageStatusPost];

	return self;
}

- (void)send
{
	// message status - a bit redundant but makes things bullet-proof.
	[FGMessageProxy setMessage:self->_message status:FGMessageStatusPost];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagePosting object:self->_message];
	@try
	{
		NSString *url=[FGSettings getUrlPostMessage];
		NSData *data=[FGModelSerializer buildPostMessageRequest:self->_message pretty:NO];

		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		[FGModelSerializer parsePostResponse:data intoMessage:self->_message];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagePostSucceeded object:self->_message];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to post message failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Post message failed: %@", [response description]];
	
	[FGMessageProxy setMessage:self->_message status:FGMessageStatusError errorText:response.text];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationMessagePostFailed object:self->_message text:errorTextFriendly status:response]];
}


@end
