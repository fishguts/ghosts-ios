//
//  FGMessageVO.h
//  Goggles
//
//  Created by Curtis Elsasser on 1/14/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGProfileVO;

/**
 * Note: we have taken this guy out of automated management.
 * Changes to the model should be manually synced
 */
@interface FGMessageVO : NSManagedObject

@property (nonatomic, retain) NSNumber * allowFeedback;
@property (nonatomic, retain) NSNumber * altitude;
@property (nonatomic, retain) NSNumber * color;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * expiration;
@property (nonatomic, retain) NSNumber * flaggedUser;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * imageUrlFull;
@property (nonatomic, retain) NSString * imageUrlThumb;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * ratingDislikes;
@property (nonatomic, retain) NSNumber * ratingLikes;
@property (nonatomic, retain) NSNumber * ratingSum;
@property (nonatomic, retain) NSNumber * ratingUser;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * sortPrefix;
@property (nonatomic, retain) FGProfileVO *owner;

@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * error;

- (BOOL)isAllowFeedback;
- (BOOL)isUserRatingLike;
- (BOOL)isUserRatingDislike;

@end
