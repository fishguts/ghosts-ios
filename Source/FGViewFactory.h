//
//  FGViewFactory.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGMessageVO;

/**
 * collection of little buddies that build and configure views that do not come directly out
 * of our storyboard.
**/
@interface FGViewFactory : FGStaticClass

/***** public interface ****/
+ (UIView*)createMessageTextureView:(FGMessageVO*)message;

@end
