//
//  FGNetworkService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"
#import "FGStatusVO.h"
#import "FGLoginProxy.h"
#import "FGModelSerializer.h"
#import "FGConstants.h"
#import "NSException+FG.h"


/**** local defines and constants ****/
#define NETDump NSDebug


@interface FGNetworkService()

/**** private interface ****/
- (NSMutableURLRequest*)buildUrlRequest:(NSString*)url;
- (NSMutableURLRequest*)buildUrlRequest:(NSString*)url body:(NSData*)body;
- (void)sendUrlRequest:(NSMutableURLRequest*)request;
@end


@implementation FGNetworkService
#pragma mark - IO
- (void)send
{
	@throw [NSException abstractViolation:@"send"];
}

- (void)cancel
{
	// if this becomes essential then we will have to allocate a connection and manage it.  Opting for simple for now.
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSendCanceled object:self];
}

- (void)sendRequestToURL:(NSString*)url
{
	[self sendUrlRequest:[self buildUrlRequest:url]];
}

- (void)postRequestToURL:(NSString*)url data:(NSData*)json
{
	[self sendUrlRequest:[self buildUrlRequest:url body:json]];
}

#pragma mark - Private interface
- (NSMutableURLRequest*)buildUrlRequest:(NSString*)url
{
	return [self buildUrlRequest:url body:nil];
}

- (NSMutableURLRequest*)buildUrlRequest:(NSString*)url body:(NSData*)body
{
	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];

	[request setTimeoutInterval:30];
	[request setHTTPShouldHandleCookies:YES];
	if(body==nil)
	{
		[request setHTTPMethod:@"GET"];
	}
	else
	{
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:body];
		[request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	}
	
	return request;
}

- (void)sendUrlRequest:(NSMutableURLRequest*)request
{
	// keep response on the main queue so that we don't have to worry about threading
	NSOperationQueue *queue=[NSOperationQueue mainQueue];
	
	NETDump(@"Sending: %@\n%@", [request URL], (([request HTTPBody]!=nil) ? [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding] : @"nil"));
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSending object:self];
	[NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
	 {
		 if(data!=nil)
		 {
			 @try 
			 {
				 NETDump(@"Response: %@\n%@", [request URL], [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
				 
				 FGStatusVO *response=[FGModelSerializer parseResponseStatus:data];
				 if([response.code isEqualToString:FGResponseStatusOkay])
				 {
					 [self handleSuccess:data];
				 }
				 else
				 {
					 // if this is an auth issue then allow our implementation to preprocess before dispatching failure
					 if([response.code isEqualToString:FGResponseStatusAuth])
					 {
						 [self preprocessAuthFailure:response];
					 }
					 [self handleFailure:response];
				 }
				 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSendSucceeded object:self];
			 }
			 @catch (NSException *exception) 
			 {
				 [self handleFailure:[FGStatusVO statusFromText:[exception description]]];
				 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSendFailed object:self];
			 }
		 }
		 else
		 {
			 if(error!=nil)
			 {
				 [self handleFailure:[FGStatusVO statusFromText:[error localizedDescription]]];
			 }
			 else
			 {
				 [self handleFailure:[FGStatusVO statusFromText:@"Reason unknown"]];
			 }
			 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSendFailed object:self];
		 }
	 }];
}


#pragma mark - Abstract handlers
- (void)handleSuccess:(NSData*)data
{
	@throw [NSException abstractViolation:@"handleSuccess"];
}

- (void)handleFailure:(FGStatusVO*)response
{
	@throw [NSException abstractViolation:@"handleFailure"];
}

- (void)preprocessAuthFailure:(FGStatusVO*)response
{
	[FGLoginProxy logoutIfPossible];
}


@end
