//
//  FGSettingsProxy.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"
#import "FGTypes.h"


/**
 * Proxies FGSettings.  All settings that should have a notification should be managed through this proxy
 */
@interface FGSettingsProxy : FGStaticClass
/* Looks for persisted settings and if found then installs them in FGSettings */
+ (void)installSettings;

+ (FGListSortType)getListSortMethod;
+ (void)setListSortMethod:(FGListSortType)sortMethod;

@end
