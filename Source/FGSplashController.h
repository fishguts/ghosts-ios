//
//  FGSplashController.h
//  Gears
//
//  Created by Curtis Elsasser on 5/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FGSplashController : UIViewController
{
	NSTimer *_timer;
	BOOL _elapsed;
	BOOL _dismissed;
}

@property (nonatomic, weak) id <FGModalDelegate> delegate;

// issue once dismissal from within is ok
- (void)allowDismissal;

@end
