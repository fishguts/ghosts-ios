//
//  FGReferenceCount.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGReferenceCount : NSObject
{
	int _count;
}

/**** properties ****/
@property (nonatomic, readonly) int count;

/**** public interface ****/
- (int)addReference;
- (int)releaseReference;

@end
