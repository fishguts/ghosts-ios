//
//  FGGetMessagesService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessagesGetService.h"
#import "FGModel.h"
#import "FGModelSerializer.h"
#import "FGModelQueries.h"
#import "FGModelOperations.h"
#import "FGMotionProxy.h"
#import "FGMessageVO.h"
#import "FGStatusVO.h"
#import "FGSettings.h"
#import "FGSession.h"
#import "FGConstants.h"



@implementation FGMessagesGetService

#pragma mark - Public Interface
- (id)initWithLocation:(CLLocation*)location replaceMessages:(BOOL)replace welcomeState:(BOOL)welcome
{
	self=[super init];
	self->_location=location;
	self->_replaceMessages=replace;
	self->_welcomeState=welcome;
	
	return self;
}


- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagesGetting object:self->_location];
	@try 
	{
		// protected against no services
		if(self->_location==nil)
		{
			@throw [NSException exceptionWithName:@"MessageServiceException" reason:@"No location specified" userInfo:nil];
		}
		else
		{
			NSString *url=[FGSettings getUrlGetMessages];
			NSData *data=[FGModelSerializer buildGetMessagesRequest:self->_location welcomeState:self->_welcomeState pretty:NO];

			[self postRequestToURL:url data:data];
		}
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		NSArray *messagesOld=[FGModelQueries fetchMessages];
		NSArray *messagesNew=[FGModelSerializer parseMessagesResponse:data];
		
		if(self->_replaceMessages)
		{
			for(FGMessageVO *messageOld in messagesOld)
			{
				// note: we don't remove messages that are either in a posting or error status
				if([messageOld.status isEqualToString:FGMessageStatusPosted])
				{
					// Keep an eye on this: NSManagedObject does not overload isEqual, nonetheless the references appear to be the same
					if([messagesNew indexOfObject:messageOld]==NSNotFound)
					{
						[[[FGModel instance] context] deleteObject:messageOld];
					}
				}
			}
			[FGModelOperations removeUnreferencedProfiles];
		}

		[[FGSession instance] setLocationLastRetrieve:self->_location];
		// note: this will not include local messages either posting or in error. You will most likely be interested in [FGMessageProxy getMessages]
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagesGetSucceeded object:messagesNew];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to get messages failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Get messages failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationMessagesGetFailed object:self->_location text:errorTextFriendly status:response]];
}

@end
