//
//  NSUserDefaults+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 1/1/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSUserDefaults+FG.h"

@implementation NSUserDefaults (FG)
- (NSString*)stringForKey:(NSString *)defaultName defaultValue:(NSString*)defaultValue
{
	NSString *result=[self stringForKey:defaultName];
	return (result) ? result : defaultValue;
}

- (BOOL)boolForKey:(NSString*)defaultName defaultValue:(BOOL)defaultValue
{
	id exists=[self objectForKey:defaultName];
	return (exists!=nil) ? [self boolForKey:defaultName] : defaultValue;
}

- (NSInteger)integerForKey:(NSString*)defaultName defaultValue:(NSInteger)defaultValue
{
	id exists=[self objectForKey:defaultName];
	return (exists!=nil) ? [self integerForKey:defaultName] : defaultValue;
}

@end
