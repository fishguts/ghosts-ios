//
//  GLCMath.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#ifndef Ghosts_GLCMath_h
#define Ghosts_GLCMath_h

#include <GLKit/GLKMath.h>

/**** declarations ****/
/*
 * Determines rotation around X axis in 180 degrees: -90 -> 90.
 */
GLfloat glcYZtoXRotation180(GLfloat deltaY, GLfloat deltaZ);
/*
 * Determines rotation around X axis in 360 degrees 
 */
GLfloat glcYZtoXRotation360(GLfloat deltaY, GLfloat deltaZ);
/*
 * Determines rotation around Y axis in 360 degrees
 */
GLfloat glcXZtoYRotation360(GLfloat deltaX, GLfloat deltaZ);


/**** inline definitions ****/
inline GLfloat glcYZtoXRotation180(GLfloat deltaY, GLfloat deltaZ)
{
	const GLfloat hypotenuse=sqrtf(deltaY*deltaY + deltaZ*deltaZ);
	if(hypotenuse>0)
	{
		return GLKMathRadiansToDegrees(asinf(deltaY/hypotenuse));
	}
	return 0;
}

inline GLfloat glcYZtoXRotation360(GLfloat deltaY, GLfloat deltaZ)
{
	const GLfloat hypotenuse=sqrtf(deltaY*deltaY + deltaZ*deltaZ);
	if(hypotenuse>0)
	{
		GLfloat result=GLKMathRadiansToDegrees(asinf(deltaY/hypotenuse));
		if(deltaZ>0)
		{
			result=180-result;
		}
		else if(deltaY<0)
		{
			result=360+result;
		}
		return result;
	}
	return 0;
}

inline GLfloat glcXZtoYRotation360(GLfloat deltaX, GLfloat deltaZ)
{
	const GLfloat hypotenuse=sqrtf(deltaX*deltaX + deltaZ*deltaZ);
	if(hypotenuse>0)
	{
		GLfloat result=GLKMathRadiansToDegrees(asinf(deltaX/hypotenuse));
		// if the message is behind the camera
		if(deltaZ>0)
		{
			result=180+result;
		}
		else if(deltaX<0)
		{
			result=0-result;
		}
		else
		{
			result=360-result;
		}
		return result;
	}
	return 0;
}

#endif
