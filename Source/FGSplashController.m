//
//  FGSplashController.m
//  Gears
//
//  Created by Curtis Elsasser on 5/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSplashController.h"
#import "FGConstants.h"

@interface FGSplashController()
/**** private properties ****/
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;

/**** private interface ****/
- (void)processState;
@end


@implementation FGSplashController
@synthesize delegate;

/**** view lifetime ****/
- (void)viewDidLoad
{
	NSLog(@"Splash: %f seconds", FGDefaultSplashDuration);
	UIScreen *screen=[UIScreen mainScreen];

    [super viewDidLoad];

	// ios framework doesn't seem to do automatic name handling of 5g images
	if((screen.scale==2.0)
	   && (screen.bounds.size.height==568.0))
	{
		[self.imageBackground setImage:[UIImage imageNamed:@"Default-568h@2x"]];
	}

	self->_timer=[NSTimer scheduledTimerWithTimeInterval:FGDefaultSplashDuration target:self selector:@selector(handleTick:) userInfo:nil repeats:NO];

}

/**** public interface ****/
- (void)allowDismissal
{
	self->_dismissed=YES;
	[self processState];
}

/**** private interface ****/
- (void)processState
{
	if(self->_elapsed && self->_dismissed)
	{
		if(self.delegate)
		{
			[self.delegate modalComplete:self];
		}
		else
		{
			[self smartDismissViewControllerAnimated:NO];
		}
	}
}

/**** observer handlers ****/
- (void)handleTick:(NSTimer*)timer
{
	[self->_timer invalidate];
	self->_timer=nil;
	self->_elapsed=YES;
	[self processState];
}

- (void)viewDidUnload {
	[self setImageBackground:nil];
	[super viewDidUnload];
}
@end
