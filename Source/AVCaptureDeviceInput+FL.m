//
//  AVCaptureDeviceInput+FL.m
//  TestVideo
//
//  Created by Curtis Elsasser on 8/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "AVCaptureDeviceInput+FL.h"

@implementation AVCaptureDeviceInput (FL)

+ (AVCaptureDeviceInput *)defaultDeviceInputWithMediaType:(NSString *)mediaType
{
	AVCaptureDevice *device=[AVCaptureDevice defaultDeviceWithMediaType:mediaType];
	if(device!=nil)
	{
		NSError *error;
		AVCaptureDeviceInput *input=[[AVCaptureDeviceInput alloc] initWithDevice:device error:&error];
		if(input!=nil)
		{
			return input;
		}
	}
	@throw [NSException exceptionWithName:@"AVCaptureDeviceInputException" reason:@"Device not available" userInfo:nil];
}

@end
