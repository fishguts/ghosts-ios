//
//  FGConversionGrid.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/3/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//
// todo: To be pure this guy should have an independent height radius. For now a sphere will do.


/* FGConversionSphere - based on a center coordinate. All coordinates are converted to meter distances in relation to the "origin"
 * coordinate.  The radius is only significant if you want to test for containment.  The sphere is latitude sensitive when it comes
 * to conversions so keep this in mind for long distance calculations and motion
 *
 * Note: Coordinates are treated and converted with openGL in mind:
 *	latitude = 0-z  (depth) See note below concerning sign reversal
 *	longitude = x (left and right)
 *	altitude = y (up and down)
 *
 * **Depth: the reason we reverse the sign is because opengl's z axis is backwards. It's negative axis is away from the origin (towards user).
 */
@interface FGConversionSphere : NSObject
{
	CLLocation *_center;
	float _radiusMeters;
}

/**** initialization ****/
- (id)initWithCoordinate:(CLLocation*)center radiusInMeters:(float)radius;

/**** interface ****/
- (BOOL)isCoordinateInGrid:(CLLocation*)location;
- (BOOL)isCoordinateInGrid:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude altitude:(CLLocationDistance)altitude;

- (GLKVector3)coordinateToVector3:(CLLocation*)location;
- (GLKVector3)coordinateToVector3:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude altitude:(CLLocationDistance)altitude;

@end
