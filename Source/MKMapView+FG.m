//
//  MKMapView+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "MKMapView+FG.h"

@implementation MKMapView (FG)

- (void)removeAllAnnotations
{
	[self removeAnnotations:[self annotations]];
}

- (void)removeCustomAnnotations
{
	// leave all but the user pin
	for(int index=[[self annotations] count]-1; index>-1; index--)
	{
		id <MKAnnotation> annotation=[self.annotations objectAtIndex:index];
		if([annotation isKindOfClass:[MKUserLocation class]]==NO)
		{
			[self removeAnnotation:annotation];
		}
	}
	
}

@end
