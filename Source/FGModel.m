//
//  FGModel.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import "FGModel.h"
#import "FGConstants.h"


static FGModel *instance;

/**** private api ****/
@interface FGModel()
@property (nonatomic, strong) UIManagedDocument *document;
@end


/**** implementation ****/
@implementation FGModel

#pragma mark - Properties
@synthesize document=_document;


#pragma mark - Properties
- (NSManagedObjectContext*)context
{
	return self.document.managedObjectContext;
}

- (UIManagedDocument*)document
{
	if(_document==nil)
	{
		NSURL *url=[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
		url=[url URLByAppendingPathComponent:@"database"];
		self->_document=[[UIManagedDocument alloc] initWithFileURL:url];

		NSAssertWarn(self->_document!=nil, @"Cound not allocate DB document");
	}
	return _document;
}


#pragma mark - Singleton and initialization
+ (void)initialize
{
	NSAssert(instance==nil, @"Should be nil");
	instance=[[FGModel alloc] init];
}

+ (FGModel*)instance
{
	NSAssert(instance!=nil, @"Should not be nil");
	return instance;
}

- (void)startup
{
	[self open];
}

- (void)shutdown
{
	// note: close will save
	[self close];
}

#pragma mark - DB io
- (BOOL)isOpen
{
	return (self.document.documentState==UIDocumentStateNormal);
}

- (void)open
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBOpening object:self];
	if(self.document.documentState==UIDocumentStateNormal)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBOpenSucceeded object:self];
	}
	else
	{
		NSFileManager *manager=[NSFileManager defaultManager];
		if([manager fileExistsAtPath:[self.document.fileURL path]]==NO)
		{
			[self.document saveToURL:self.document.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success)
			 {
				 if(success)
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBOpenSucceeded object:self];
				 }
				 else
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBOpenFailed object:self];
					 [[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Failed to open database"]];
				 }
				 
			 }];
		}
		else
		{
			[self.document openWithCompletionHandler:^(BOOL success)
			 {
				 if(success)
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBOpenSucceeded object:self];
				 }
				 else
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBOpenFailed object:self];
					 [[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Failed to open database"]];
				 }
			 }];
		}
	}
}
	 
- (void)close
{
	// note: close will save
	[self.document closeWithCompletionHandler:^(BOOL success)
	 {
		 if(success)
		 {
			 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBCloseSucceeded object:self];
		 }
		 else
		 {
			 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBCloseFailed object:self];
			 NSAssertWarn(success, @"Close failed");
		 }
	 }];
}

- (void)save
{
	NSAssertWarn([[NSFileManager defaultManager] fileExistsAtPath:[self.document.fileURL path]], @"File '%@' should exist", self.document.fileURL);

	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBSaving object:self];
	[self.document saveToURL:self.document.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success)
	 {
		 if(success)
		 {
			 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBSaveSucceeded object:self];
		 }
		 else
		 {
			 [[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBSaveFailed object:self];
			 [[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Failed to save database"]];
		 }
	 }];

}
@end
