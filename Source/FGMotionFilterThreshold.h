//
//  FGMotionFilterThreshold.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/30/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMotionFilterBase.h"

@interface FGMotionFilterThreshold : FGMotionFilterBase
/**** Properties ****/
@property (nonatomic) GLfloat thresholdX;
@property (nonatomic) GLfloat thresholdY;
@property (nonatomic) GLfloat thresholdZ;

/**** Interface ****/
- (id)initWithThreshold:(GLfloat)threshold;
- (id)initWithThresholdX:(GLfloat)x thresholdY:(GLfloat)y thresholdZ:(GLfloat)z;

@end
