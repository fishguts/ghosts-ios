//
//  FGLocationServer.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGLocationFilterBase.h"
#import "FGMotionFilterBase.h"

@class FGVector3;


/**
 * Encompasses all location and motion monitoring functionality.  Use notifications to initiate and release 
 * monitoring: FGNotificationLocationRetain, FGNotificationLocationRelease, FGNotificationOrientationRetain...
 **/
@interface FGMotionProxy : NSObject <FGSingleton, CLLocationManagerDelegate, FGLocationFilterDelegate, FGMotionFilterDelegate>

/**** properties ****/
@property (nonatomic, readonly) CLLocation *currentLocation;
// Rotation in degrees. Origin (0,0,0) - device points up y axis and faces the user (+ z axis). All rotation uses right hand rule.
@property (nonatomic, readonly) FGVector3 *currentOrientation;

/**** public interface ****/
+ (FGMotionProxy*)instance;
- (BOOL)isLocationServicesEnabled;

@end
