//
//  UIDevice+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 2/1/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (FG)

/**** Public Interface ****/
- (NSString*)encoded;
@end
