//
//  FGValueWrappers.m
//  Gears
//
//  Created by Curtis Elsasser on 6/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGValueWrappers.h"

@implementation FGPoint
- (id)initWithValue:(CGPoint)object
{
	self=[super init];
	self->value=object;
	return self;
}

@end

@implementation FGSize
- (id)initWithValue:(CGSize)object
{
	self=[super init];
	self->value=object;
	return self;
}

@end

@implementation FGRect
- (id)initWithValue:(CGRect)object
{
	self=[super init];
	self->value=object;
	return self;
}

@end

@implementation FGVector3
- (id)initWithValue:(GLKVector3)object
{
	self=[super init];
	self->value=object;
	return self;
}

@end