//
//  FGLocationServer.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMotionProxy.h"
#import "FGConstants.h"
#import "FGSettings.h"
#import "FGReferenceCount.h"
#import "FGValueWrappers.h"
#import "FGLocationFilterThreshold.h"
#import "FGMotionFilterConditioner.h"
#import "FGMotionFilterThreshold.h"
#import "CLLocation+FG.h"


/**** local defines and constants ****/
#define CLDump NSDebug
#define CMDump NSDebug
#define SMOOTH_MOTION 0


@interface FGMotionProxy()
{
	CLLocationManager *_locationManager;
	CMMotionManager *_motionManager;
	NSTimer *_motionTimer;
	FGLocationFilterThreshold *_filterLocation;
	FGMotionFilterConditioner *_filterMotion;

	CLLocation *_currentLocation;
	CLHeading *_currentHeading;
	FGVector3 *_currentOrientation;

	FGReferenceCount *_referenceCountLocation;
	FGReferenceCount *_referenceCountMotion;

	/* "constants" for dispatching major/minor location change updates */
	NSDictionary *_userInfoMinor;
	NSDictionary *_userInfoMajor;
}

/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

- (float)smoothDegreeTransition:(float)valueFrom valueTo:(float)valueTo;

/**** observer handlers ****/
- (void)handleMotionTimer:(NSTimer*)timer;
- (void)handleLocationRequest:(NSNotification*)notification;
- (void)handleMotionRequest:(NSNotification*)notification;

@end


@implementation FGMotionProxy
static FGMotionProxy *instance;

/**** properties ****/
- (CLLocation*)currentLocation
{
	return self->_currentLocation;
}
- (FGVector3*)currentOrientation
{
	return self->_currentOrientation;
}

#pragma mark - Initialization
+ (void)initialize
{
	NSAssert(instance==nil, @"Should be nil");
	instance=[[FGMotionProxy alloc] init];
}

+ (FGMotionProxy*)instance
{
	NSAssert(instance!=nil, @"Should not be nil");
	return instance;
}

- (id)init
{
	self=[super init];
	self->_referenceCountLocation=[[FGReferenceCount alloc] init];
	self->_referenceCountMotion=[[FGReferenceCount alloc] init];
	self->_userInfoMinor=[NSDictionary dictionaryWithObjectsAndKeys:
						  ARC_STRING_YES, FGUserKeyLocationMinor,
						  nil];
	// note: all major shifts include a minor shift
	self->_userInfoMajor=[NSDictionary dictionaryWithObjectsAndKeys:
						  ARC_STRING_YES, FGUserKeyLocationMinor,
						  ARC_STRING_YES, FGUserKeyLocationMajor,
						  nil];
	self->_filterLocation=[[FGLocationFilterThreshold alloc] initWithMethod:FGLocationFilterThresholdMeasure
														horizontalThreshold:[FGSettings getRadiusHorizontal]
														  verticalThreshold:[FGSettings getRadiusVertical]];
	[self->_filterLocation setDelegate:self];
	self->_filterMotion=(FGMotionFilterConditioner*)[FGMotionFilterBase createFilterChainWithDelegates:
													 [NSArray arrayWithObjects:
													  [[FGMotionFilterConditioner alloc] init],
													  [[FGMotionFilterThreshold alloc] initWithThresholdX:0.5 thresholdY:0.2 thresholdZ:0.5],
													  self,
													  nil]];

	return self;
}

- (void)dealloc
{
	[self shutdown];
}

#pragma mark - FGSingleton
/**
 * startup puts us in a ready state but does not start anything. That is left up to the [function]_RETAIN and [function]_RELEASE notifications.
 **/
- (void)startup
{
	if(self->_locationManager==nil)
	{
		@try 
		{
			NSLog(@"Location services %@", ([CLLocationManager locationServicesEnabled] ? @"ENABLED" : @"DISABLED"));
			
			self->_locationManager=[[CLLocationManager alloc] init];
			self->_locationManager.desiredAccuracy=kCLLocationAccuracyBest;
			self->_locationManager.delegate=self;
		}
		@catch (NSException *exception) 
		{
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Location Services are not enabled"]];
		}
	}
	
	if(self->_motionManager==nil)
	{
		@try 
		{
			self->_motionManager=[[CMMotionManager alloc] init];
			NSLog(@"Motion services %@", ([self->_motionManager isDeviceMotionAvailable] ? @"ENABLED" : @"DISABLED"));
		}
		@catch (NSException *exception) 
		{
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Motion Services are not enabled"]];
		}
	}
	[self addObservers];
}

- (void)shutdown
{
	[self removeObservers];
	[self->_locationManager stopUpdatingLocation];
	[self->_locationManager stopUpdatingHeading];
	self->_locationManager=nil;

	[self->_motionManager stopDeviceMotionUpdates];
	self->_motionManager=nil;

	[self->_motionTimer invalidate];
	self->_motionTimer=nil;

	[self->_filterLocation dispose];
	self->_filterLocation=nil;
	[self->_filterMotion dispose];
	self->_filterMotion=nil;


	self->_currentHeading=nil;
	self->_currentOrientation=nil;
}

#pragma mark - Public interface
- (BOOL)isLocationServicesEnabled
{
	return [CLLocationManager locationServicesEnabled];
}

#pragma mark - Private interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	[center addObserver:self selector:@selector(handleLocationRequest:) name:FGNotificationLocationRetain object:nil];
	[center addObserver:self selector:@selector(handleLocationRequest:) name:FGNotificationLocationRelease object:nil];
	[center addObserver:self selector:@selector(handleMotionRequest:) name:FGNotificationOrientationRetain object:nil];
	[center addObserver:self selector:@selector(handleMotionRequest:) name:FGNotificationOrientationRelease object:nil];
	[center addObserver:self selector:@selector(handleSettingsGetSucceeded:) name:FGNotificationSettingsGetSucceeded object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (float)smoothDegreeTransition:(float)valueFrom valueTo:(float)valueTo
{
	const float delta=valueTo-valueFrom;
	if(fabs(delta)<180)
	{
		return valueFrom+(delta)/3;
	}
	else
	{
		return (delta>0)
			? valueFrom+(360-delta)/3
			: valueFrom+(360+delta)/3;
	}
}

#pragma mark - FGLocationFilterDelegate and FGMotionFilterDelegate
- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	// note: the sdk filter is strange.  It's accuracy can get better and worse while the location does not change
	self->_currentLocation=location;
	if(changeType==FGLocationChangeTypeMajor)
	{
		CLDump(@"Major location change %@", [self->_currentLocation descriptionShort]);
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLocationChanged object:self->_currentLocation userInfo:self->_userInfoMajor];
	}
	else if(changeType==FGLocationChangeTypeMinor)
	{
		CLDump(@"Minor location change: %@", [self->_currentLocation descriptionShort]);
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLocationChanged object:self->_currentLocation userInfo:self->_userInfoMinor];
	}
	else
	{
		NSFailWarn(@"Change type - %d?", changeType);
	}
}

- (void)motionFilter:(FGMotionFilterBase *)filter acceptOrientation:(GLKVector3)orientation
{
	CMDump(@"Orientation change %@", NSStringFromGLKVector3(orientation));
	self->_currentOrientation=[[FGVector3 alloc] initWithValue:orientation];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationOrientationChanged object:self->_currentOrientation];
}


#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	[self->_filterLocation addLocation:newLocation changeType:FGLocationChangeTypeUndetermined];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
	// store it - we'll wait for the next gravity tick to pick it up
	self->_currentHeading=newHeading;
}

#pragma mark - Observer handlers	
- (void)handleMotionTimer:(NSTimer*)timer
{
	if(self->_currentHeading!=nil)
	{
		[self->_filterMotion addDeviceMotion:[self->_motionManager deviceMotion]
								 withHeading:[[self->_locationManager heading] trueHeading]];
	}
}

- (void)handleLocationRequest:(NSNotification*)notification
{
	 if([[notification name] isEqualToString:FGNotificationLocationRetain])
	 {
		 if([self->_referenceCountLocation addReference]==1)
		 {
			 [self->_locationManager startUpdatingLocation];
		 }
	 }
	 else if([[notification name] isEqualToString:FGNotificationLocationRelease])
	 {
		 if([self->_referenceCountLocation releaseReference]==0)
		 {
			 [self->_locationManager stopUpdatingLocation];
			 [self->_filterLocation reset:YES];
		 }
	 }
}
	 
 - (void)handleMotionRequest:(NSNotification*)notification
{
	if([[notification name] isEqualToString:FGNotificationOrientationRetain])
	{
		if([self->_referenceCountMotion addReference]==1)
		{
			[self->_motionManager startDeviceMotionUpdates];
			[self->_locationManager startUpdatingHeading];
			self->_motionTimer=[NSTimer scheduledTimerWithTimeInterval:FGMotionTimerInterval target:self selector:@selector(handleMotionTimer:) userInfo:nil repeats:YES];
		#if TARGET_IPHONE_SIMULATOR
			// no orientation in simulator so setup a default
			self->_currentOrientation=[[FGVector3 alloc] initWithValue:GLKVector3Make(0, 0, 0)];
			[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationOrientationChanged object:self->_currentOrientation];
		#endif
		}
	}
	else if([[notification name] isEqualToString:FGNotificationOrientationRelease])
	{
		if([self->_referenceCountMotion releaseReference]==0)
		{
			[self->_motionManager stopDeviceMotionUpdates];
			[self->_locationManager stopUpdatingHeading];
			[self->_motionTimer invalidate];
			[self->_filterMotion reset:YES];
			self->_motionTimer=nil;
			self->_currentHeading=nil;
			self->_currentOrientation=nil;
		}
	}
}

- (void)handleSettingsGetSucceeded:(NSNotification*)notification
{
	[self->_filterLocation setThresholdHorizontal:[FGSettings getRadiusHorizontal]];
	[self->_filterLocation setThresholdVertical:[FGSettings getRadiusVertical]];
}

@end

