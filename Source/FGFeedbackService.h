//
//  FGFeedbackService.h
//  Goggles
//
//  Created by Curtis Elsasser on 12/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@class FGFeedbackVO;


@interface FGFeedbackService : FGNetworkService
{
	FGFeedbackVO *_feedback;
}
/**** public interface ****/
- (id)initWithFeedback:(FGFeedbackVO*)feedback;

@end
