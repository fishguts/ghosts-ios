//
//  FGMessageVideoLayer.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/3/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageVideoLayer.h"
#import "FGVideoMessageWrapper.h"
#import "FGLocationCompression.h"
#import "FGMessageVO.h"
#import "FGViewFactory.h"
#import "FGSettings.h"
#import "FGValueWrappers.h"
#import "FGConstants.h"
#import "GLCContext.h"
#import "GLCTypes.h"
#import "GLCExt.h"
#import "GLCMath.h"
#import "GLCDrawableCircle.h"
#import "GLCProjectionMatrix.h"
#import "GLKTextureLoader+FG.h"
#import "CLLocation+FG.h"


/*** local defines ***/
#if DEBUG
	#define DumpGL(...)	if(ForceDump) { NSDebug(__VA_ARGS__); } else { NSNoOutput(__VA_ARGS__); }
#else
	#define DumpGL NSNoOutput
#endif
#define DumpPP NSNoOutput
#define DumpHT NSDebug

// COMPRESS_ALTITUDE - altitude is wildly inaccurate on iphone. This guy assumes 0.0 altitude for
//	all locations and compresses message altitudes way down and orients them around 0.0
#define COMPRESS_ALTITUDE 1

/*** local types ****/
typedef struct
{
	GLKVector3 primitive;
	GLshort texture[2];
} GLCTextureVertex;

typedef struct
{
	BOOL draw;
	GLKVector3 location;
	GLKVector3 delta;
	GLKVector2 rotation;
	GLfloat scale;
} GLCMessageDrawParams;

static BOOL ForceDump=NO;

// FOV - following is based on specs online: sensor=4.54 x 3.39mm, focal length=3.85mm. tan=(3.39/2)/3.39 -> 47.5 (not very accurate so opening a little bit)
static const GLfloat DefaultFieldOfView=60;
static const GLfloat MessageWidth=4.4;
static const GLfloat MessageHeight=MessageWidth/2;
static const GLfloat HUDRadarScale=1.0/4.5;


/**** FGMessageVideoLayer - Action ****/
@interface FGMessageVideoLayer()
{
	/* current phone/camera location */
	CLLocation *_cameraLocaton;
	/* offset within the sphere */
	GLKVector3 _cameraOffset;
	GLfloat _clipDistance;
	FGConversionSphere *_conversionSphere;
	FGLocationCompression *_locationCompression;
	NSMutableArray *_wrappedMessages;

	GLCContext *_context;
	GLCFrustumMatrix *_frustumMatrix;
	GLCOrthoMatrix *_orthoMatrix;
	GLCDrawableCircle2D *_hudBackground;
	
	BOOL _stateDirty;
	BOOL _updateSphere;
	BOOL _buildMessages;
	BOOL _sortMessages;
	BOOL _updateMessageLocations;
	BOOL _updateCameraLocation;
	BOOL _updatePerspective;
	
}

/**** private interface ****/
- (void)setupGL;
- (void)configGL;
- (void)tearDownGL;

- (void)invalidateState;
- (void)updateState;
- (void)updateSphere;
- (void)updateCameraLocation;
- (void)updatePerspective;
- (void)updateMessageOrder;
- (void)buildMessages;
- (void)buildMessageWrappers;
- (void)updateMessageLocations;

/**** calculators ****/
- (GLCMessageDrawParams)messageToDrawParams:(FGVideoMessageWrapper*)wrapper;
- (BOOL)messageToViewportCoordinates:(FGVideoMessageWrapper*)message result:(GLCRect2*)rect;

/**** drawing ****/
- (void)drawLayer;
- (void)drawMessages;
- (void)drawMessage:(FGVideoMessageWrapper*)wrapper;
- (void)drawHUDRadar;

@end


@implementation FGMessageVideoLayer
@synthesize orientation=_orientation;
@synthesize messages=_messages;
@synthesize scale=_scale;

#pragma mark - Properties
- (void)setMessages:(NSArray *)messages
{
	self->_messages=messages;
	self->_buildMessages=YES;
	self->_sortMessages=YES;
	self->_updateMessageLocations=YES;
	[self invalidateState];
}

- (void)setLocation:(CLLocation *)location rebuildSphere:(BOOL)rebuild
{
#if COMPRESS_ALTITUDE
	self->_cameraLocaton=[CLLocation clone:location withCoordinate:[location coordinate] altitude:0.0];
#else
	self->_cameraLocaton=location;
#endif
	self->_updateCameraLocation=YES;
	self->_sortMessages=YES;
	if((self->_conversionSphere==nil) || rebuild)
	{
		self->_updateSphere=YES;
		self->_updateMessageLocations=YES;
	}
	[self invalidateState];
}

- (void)setOrientation:(FGVector3 *)orientation
{
	self->_orientation=orientation;
	// we only need to redraw
	[self invalidateState];
}

- (void)setScale:(GLfloat)scale
{
	if(scale!=self->_scale)
	{
		if(scale<=FLT_EPSILON)
		{
			NSFailWarn(@"Scale is out of range");
		}
		else
		{
			self->_scale=scale;
			self->_updatePerspective=YES;
			[self invalidateState];
		}
	}
}

#pragma mark - Public Interface
- (id)initWithFrame:(CGRect)frame
{
	self=[super init];

	[self setFrame:frame];
	[self setOpaque:NO];
	[self setupGL];
	[self configGL];
	[self setupFiltering];
	[self setScale:1.0];

	return self;
}

- (void)dispose
{
	[self tearDownGL];
}

- (FGMessageVO*)hitTestForMessage:(CGPoint)point
{
	GLCRect2 rect2;
	// correct for different y orientation between quartz and opengl
	point.y=self.bounds.size.height-point.y;
	for(FGVideoMessageWrapper *wrapper in self->_wrappedMessages)
	{
		if([self messageToViewportCoordinates:wrapper result:&rect2])
		{
			// real simple rectangular hit testing. Think it will be fine, but if it needs to be more
			// sophisticated it could easily adapt CGPath to create a closed path and look for containment
			if((point.x>=MIN(rect2.tl.x, rect2.bl.x))
				&& (point.x<=MAX(rect2.tr.x, rect2.br.x))
				&& (point.y<=MAX(rect2.tl.y, rect2.tr.y))
				&& (point.y>=MIN(rect2.bl.y, rect2.br.y)))
			{
				return wrapper.message;
			}
		}
	}
	return nil;
}



#if DEBUG
- (void)dump
{
	NSDebugFL(@"Messages=%d:", [self->_wrappedMessages count]);
	// we are going to squeeze it through draw with ForceDump on.
	ForceDump=YES;
	{
		[self drawLayer];
	}
	ForceDump=NO;
}
#endif

#pragma mark - Private Interface
- (void)setupGL
{
	self->_context=[[GLCContext alloc] initWithEAGLDrawable:self width:self.bounds.size.width height:self.bounds.size.height includeDepthBuffer:NO];

	self->_updatePerspective=YES;
	[self invalidateState];
}

/**
 * configGL - setup our one-time objects
 **/
- (void)configGL
{
	// create a projection space equal to the applications message radius. It's okay if messages fall out of this radius (as they will when moving). It
	// will serve as a visual indicator of what is within the retrieve boundary.  The messages outside of the perimiter will disappear on the next retrieve.
	// Note: the slight padding is so that our dots representing messages fall within the outter border.
	self->_orthoMatrix=[[GLCOrthoMatrix alloc] initByAspectRatioWithWidth:[FGSettings getRadiusHorizontal]*2.1 near:0 far:1];
	self->_hudBackground=[[GLCDrawableCircle2D alloc] initWithOrigin:GLKVector2Make(0, 0)
															  radius:[self->_orthoMatrix right]
														   divisions:50 fillColor:&FGHUDBackgroundColor
														 borderColor:&FGHUDBorderColor
														   lineWidth:2.0];
	
	// setup an optimal default state. This state will remain as our base state.
	[self->_context pushClientState];
	[self->_context enableClientState:GL_VERTEX_ARRAY];
	[self->_context disableClientState:GL_COLOR_ARRAY];
}

- (void)setupFiltering
{
#if COMPRESS_ALTITUDE
	// compress things so that the vertical range is squashed and orient around 0.0 on z axis (altitude)
	self->_locationCompression=[[FGLocationCompression alloc] initWithAltitudeCompression:FGMessageVideoAltitudeCompression
																   orientAroundCoordinate:[NSNumber numberWithFloat:0.0]];
#endif

}

- (void)tearDownGL
{
	[self->_context dispose];
	self->_context=nil;
}

/**** state stuff ****/
- (void)invalidateState
{
	if(self->_stateDirty==NO)
	{
		self->_stateDirty=YES;
		[self performSelectorOnMainThread:@selector(updateState) withObject:self waitUntilDone:NO];
	}
}

- (void)updateState
{
	self->_stateDirty=NO;
	if(self->_updateSphere)
	{
		[self updateSphere];
	}
	if(self->_updateCameraLocation)
	{
		[self updateCameraLocation];
	}
	if(self->_updatePerspective)
	{
		[self updatePerspective];
	}
	if(self->_buildMessages)
	{
		[self buildMessages];
	}
	if(self->_updateMessageLocations)
	{
		[self updateMessageLocations];
	}
	if(self->_sortMessages)
	{
		[self updateMessageOrder];
	}
	
	[self drawLayer];
}

- (void)updateSphere
{
	if(self->_cameraLocaton)
	{
		self->_updateSphere=NO;
		self->_conversionSphere=[[FGConversionSphere alloc] initWithCoordinate:self->_cameraLocaton radiusInMeters:[FGSettings getRadiusHorizontal]];
	}
	else
	{
		NSFailWarn(@"updateSphere: no location?");
	}
}

- (void)updateCameraLocation
{
	if(self->_conversionSphere)
	{
		self->_updateCameraLocation=NO;
		self->_cameraOffset=[self->_conversionSphere coordinateToVector3:self->_cameraLocaton];
	}
	else
	{
		NSDebugFL(@"updateCameraLocation: no conversion sphere");
	}
}

/**
 * Note: the idea of this guy being outside of configGL is to support concepts such as zoom.  Not today....
 **/
 - (void)updatePerspective
{
	const GLfloat fovDegrees=DefaultFieldOfView/self->_scale;
	const float fovRadians=GLKMathDegreesToRadians(fovDegrees);
	const GLfloat clipZFar=[FGSettings getRadiusHorizontal]*2;

	self->_updatePerspective=NO;
	// establish a clipping distance that matches the distance at which the message width fills the screen
	self->_clipDistance=(MessageWidth/2.05)/tanh(fovRadians/2);
	self->_frustumMatrix=[[GLCFrustumMatrix alloc] initWithFOV:fovDegrees near:1.0 far:clipZFar];
	[self->_context installProjectionMatrix:self->_frustumMatrix];
	DumpPP(@"updatePerspective: fov=%f, clipDistance=%f", fovDegrees, self->_clipDistance);
}
 
- (void)buildMessages
{
	if(self->_conversionSphere==nil)
	{
		NSDebugFL(@"buildMessages: no conversion sphere");
	}
	else
	{
		self->_buildMessages=NO;
		self->_updateMessageLocations=NO;
		self->_wrappedMessages=[NSMutableArray arrayWithCapacity:[self->_messages count]];
		[self buildMessageWrappers];
	}
}

#if COMPRESS_ALTITUDE
- (void)buildCompressedCoordinates:(GLKVector3[])vectors count:(int)count
{
	NSAssertNotNil(self->_locationCompression);
	NSAssertTrue(count==[self->_messages count]);

	for(int index=0; index<count; index++)
	{
		FGMessageVO *source=[self->_messages objectAtIndex:index];
		vectors[index]=[self->_conversionSphere coordinateToVector3:[source.latitude doubleValue]
														  longitude:[source.longitude doubleValue]
														   altitude:[source.altitude doubleValue]];
	}
	[self->_locationCompression compress:vectors count:count];
}

- (void)buildMessageWrappers
{
	const int count=[self->_messages count];
	GLKVector3 vectors[count];

	[self buildCompressedCoordinates:vectors count:count];
	for(int index=0; index<count; index++)
	{
		@try
		{
			FGMessageVO *source=[self->_messages objectAtIndex:index];
			GLKTextureInfo *texture=[GLKTextureLoader textureWithView:[FGViewFactory createMessageTextureView:source] generateMipmap:YES];
			[self->_wrappedMessages addObject:[[FGVideoMessageWrapper alloc] initWithMessage:source vector:vectors[index] texture:texture]];
		}
		@catch(NSException *exception)
		{
			// should already be logged as a warning.
		}
	}
}

- (void)updateMessageLocations
{
	if(self->_conversionSphere==nil)
	{
		NSDebugFL(@"updateMessageLocations: no conversion sphere");
	}
	else
	{
		const int count=[self->_messages count];
		GLKVector3 vectors[count];

		self->_updateMessageLocations=NO;

		[self buildCompressedCoordinates:vectors count:count];
		for(int index=0; index<count; index++)
		{
			[[self->_wrappedMessages objectAtIndex:index] setVector:vectors[index]];
		}
	}
}

#else

- (void)buildMessageWrappers
{
	for(int index=0; index<[self->_messages count]; index++)
	{
		@try
		{
			FGMessageVO *source=[self->_messages objectAtIndex:index];
			GLKVector3 vector=[self->_conversionSphere coordinateToVector3:[source.latitude doubleValue]
																 longitude:[source.longitude doubleValue]
																  altitude:[source.altitude doubleValue]];
			GLKTextureInfo *texture=[GLKTextureLoader textureWithView:[FGViewFactory createMessageTextureView:source] generateMipmap:YES];
			[self->_wrappedMessages addObject:[[FGVideoMessageWrapper alloc] initWithMessage:source vector:vector texture:texture]];
		}
		@catch(NSException *exception)
		{
			// should already be logged as a warning.
		}
	}
}

- (void)updateMessageLocations
{
	if(self->_conversionSphere==nil)
	{
		NSDebugFL(@"updateMessageLocations: no conversion sphere");
	}
	else
	{
		self->_updateMessageLocations=NO;
		for(FGVideoMessageWrapper *wrapper in self->_wrappedMessages)
		{
			wrapper.vector=[self->_conversionSphere coordinateToVector3:[wrapper.message.latitude doubleValue]
															  longitude:[wrapper.message.longitude doubleValue]
															   altitude:[wrapper.message.altitude doubleValue]];
		}
	}
}

#endif

/**
 * Sort messages from closest to furthest. This is because we are using blending for texture mapping transparency and it is not accounted for (transparency)
 *	in the depth buffer and causes artifacts.  Hence we use distance to determine our draw order.
 */
- (void)updateMessageOrder
{
	self->_sortMessages=NO;
	[self->_wrappedMessages sortWithOptions:NSSortConcurrent usingComparator:^NSComparisonResult(FGVideoMessageWrapper *wrapper1, FGVideoMessageWrapper *wrapper2)
	 {
		 const GLfloat delta1=GLKVector3Distance([wrapper1 vector], self->_cameraOffset);
		 const GLfloat delta2=GLKVector3Distance([wrapper2 vector], self->_cameraOffset);
		 if(delta1<delta2)
		 {
			 return NSOrderedAscending;
		 }
		 else if(delta1>delta2)
		 {
			 return NSOrderedDescending;
		 }
		 return NSOrderedSame;
	 }];
}

#pragma mark - calculators
/**
 * messageToDrawParams - a little note about how we use distance.  We largely remove the vertical component for a couple of reasons.
 *	- it's really innacurate
 *	- the phones sensors flip as the back or front approaches face up or face down
 *	- it's confusing when they are above or below you (continuation on altimeter problems)
 * So horizontal distance is used for almost all threshold testing.
 */
- (GLCMessageDrawParams)messageToDrawParams:(FGVideoMessageWrapper*)wrapper
{
	GLCMessageDrawParams params;
	GLfloat distanceXZ;

	memset(&params, 0, sizeof(params));
	params.location=[wrapper vector];
	params.delta=GLKVector3Subtract(params.location, self->_cameraOffset);
	distanceXZ=sqrtf(params.delta.x*params.delta.x + params.delta.z*params.delta.z);

	// process his distance
	if(distanceXZ>FLT_EPSILON*10)
	{
		params.draw=(distanceXZ>=self->_clipDistance);
		// we will only attempt to create horizontal distance if we are not adhering to our _clipDistance (scale==1.0)
		if((params.draw==NO)
		   && (self->_scale<=1.0))
		{
			const GLfloat scale=self->_clipDistance/distanceXZ;
			params.location.x=self->_cameraOffset.x+params.delta.x*scale;
			params.location.z=self->_cameraOffset.z+params.delta.z*scale;
			params.delta=GLKVector3Subtract(params.location, self->_cameraOffset);
			distanceXZ=sqrtf(params.delta.x*params.delta.x + params.delta.z*params.delta.z);
			params.draw=YES;
		}
	}

	if(params.draw)
	{
		// process y delta so that our x rotation from going bonkers (45 degrees for now)
		if(fabsf(params.delta.y)>distanceXZ)
		{
			params.location.y*=distanceXZ/fabsf(params.delta.y);
			params.delta.y=params.location.y-self->_cameraOffset.y;
		}

		// rotations - we rotate around the x and y axis to get the message to point at the camera.
		params.rotation.x=glcYZtoXRotation180(params.delta.y, distanceXZ);
		params.rotation.y=glcXZtoYRotation360(params.delta.x, params.delta.z);

		// scale - we scale the message width and height to create a "fake" perspective in which our fake vanishing point is farther away.
		// And we use the clip distance which seems a little hairy, but what it prevents is near messages from getting humungous
		// when scaled and keeps other equations, such as clipping distance equation, from getting too messy.
		const GLfloat distanceXYZ=GLKVector3Length(params.delta);
		params.scale=1+((distanceXYZ-self->_clipDistance)/40);
	}

	return params;
}

- (BOOL)messageToViewportCoordinates:(FGVideoMessageWrapper*)wrapper result:(GLCRect2*)rect
{
	BOOL valid=NO;
	if(self->_orientation!=nil)
	{
		const GLCMessageDrawParams params=[self messageToDrawParams:wrapper];
		if(params.draw)
		{
			GLCRect4 rect4;
			GLKMatrix4 matrixModel, matrixProjection;
			const GLfloat widthMessage=MessageWidth*params.scale;
			const GLfloat heightMessage=MessageHeight*params.scale;
			const CGFloat halfWidthLayer=self.bounds.size.width/2;
			const CGFloat halfHeightLayer=self.bounds.size.height/2;

			// 1. get/build projection and model matrices
			glGetFloatv(GL_PROJECTION_MATRIX, matrixProjection.m);

			// reconstruct model matrix with steps we take during drawing to build it
			matrixModel=GLKMatrix4Identity;
			matrixModel=GLKMatrix4RotateZ(matrixModel, GLKMathDegreesToRadians(0-self->_orientation->value.z));
			matrixModel=GLKMatrix4RotateX(matrixModel, GLKMathDegreesToRadians(0-self->_orientation->value.x));
			matrixModel=GLKMatrix4RotateY(matrixModel, GLKMathDegreesToRadians(0-self->_orientation->value.y));
			matrixModel=GLKMatrix4Translate(matrixModel, 0-self->_cameraOffset.x, 0-self->_cameraOffset.y, 0-self->_cameraOffset.z);
			matrixModel=GLKMatrix4Translate(matrixModel, params.location.x, params.location.y, params.location.z);
			matrixModel=GLKMatrix4RotateY(matrixModel, GLKMathDegreesToRadians(params.rotation.y));
			matrixModel=GLKMatrix4RotateX(matrixModel, GLKMathDegreesToRadians(params.rotation.x));
			matrixModel=GLKMatrix4Translate(matrixModel, -params.location.x, -params.location.y, -params.location.z);

			// 2. build rectangle reperesentation of our message
			rect4.tl=GLKVector4Make(params.location.x-widthMessage/2, params.location.y+heightMessage/2, params.location.z, 1);
			rect4.tr=GLKVector4Make(params.location.x+widthMessage/2, params.location.y+heightMessage/2, params.location.z, 1);
			rect4.bl=GLKVector4Make(params.location.x-widthMessage/2, params.location.y-heightMessage/2, params.location.z, 1);
			rect4.br=GLKVector4Make(params.location.x+widthMessage/2, params.location.y-heightMessage/2, params.location.z, 1);

			// 3. convert model coordinates to viewport
			for(int index=0; index<4; index++)
			{
				GLKVector4 v4Edge=rect4.v[index];

				v4Edge=GLKMatrix4MultiplyVector4(matrixModel, v4Edge);
				v4Edge=GLKMatrix4MultiplyVector4(matrixProjection, v4Edge);
				const GLKVector2 v2Normalized=GLKVector2MultiplyScalar(GLKVector2Make(v4Edge.x, v4Edge.y), 1.0f/v4Edge.w);
				rect->v[index]=GLKVector2Make(halfWidthLayer+v2Normalized.x*halfWidthLayer,
											  halfHeightLayer+v2Normalized.y*halfHeightLayer);
				// we make sure that at least one point is in front of the "camera"
				if(v4Edge.z>=0)
				{
					valid=YES;
				}
				DumpHT(@"messageToViewportCoordinates: '%@'-%d, viewport=%@, norm=%@, proj=%@",
					   wrapper.message.text, index, NSStringFromGLKVector2(rect->v[index]), NSStringFromGLKVector2(v2Normalized), NSStringFromGLKVector4(v4Edge));
			}
		}
	}

	return valid;
}


#pragma mark - drawing
- (void)drawLayer
{
#if TARGET_IPHONE_SIMULATOR
	[self->_context prepareRenderbuffer:GLKVector4Make(0.25, 0.25, 0.25, 1)];
#else
	[self->_context prepareRenderbuffer:GLKVector4Make(0, 0, 0, 0)];
#endif

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	[self drawMessages];
	[self drawHUDRadar];
	
	[self->_context presentRenderbuffer:GL_RENDERBUFFER_OES];
}

- (void)drawMessages
{
	if(self->_orientation!=nil)
	{
		[self->_context pushClientState];
		{
			[self->_context enableClientState:GL_VERTEX_ARRAY];
			[self->_context enableClientState:GL_TEXTURE_COORD_ARRAY];
			[self->_context enableState:GL_TEXTURE_2D];
			[self->_context enableState:GL_BLEND];

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			glPushMatrix();
			{
				// camera transformation: This is what we need to do - rotate the world based on our camera orientation
				// which very well may not be our world origin:
				//	- camera orientation is reversed in relation to our world: clockwise rotation around y axis results in counter-clockwise rotation of world.
				//	- camera location is in our coordinates. We want to rotate everything around it. Essentially same as rotation - camera motion is reverse of world motion.
				glRotatef(0-self->_orientation->value.z, 0, 0, 1);
				glRotatef(0-self->_orientation->value.x, 1, 0, 0);
				glRotatef(0-self->_orientation->value.y, 0, 1, 0);
				glTranslatef(0-self->_cameraOffset.x, 0-self->_cameraOffset.y, 0-self->_cameraOffset.z);

				// not totally clear on why our background bleed through our texture. Until then make sure we don't turn blue.
				[self->_context setColor4f:GLKVector4Make(1.0, 1.0, 1.0, 1.0)];

				DumpGL(@"Messages: camera=(%.2f, %.2f, %.2f), rx=%.1f, ry=%.2f, rz=%.2f",
						self->_cameraOffset.x, self->_cameraOffset.y, self->_cameraOffset.z, 
						self->_orientation->value.x, self->_orientation->value.y, self->_orientation->value.z);

				// Note: we are ordered nearest to furthest. Because we are using blending we cannot use cannot use depth buffering so we draw from back to front
				for(int messageIndex=[self->_wrappedMessages count]-1; messageIndex>-1; messageIndex--)
				{
					FGVideoMessageWrapper *wrapper=[self->_wrappedMessages objectAtIndex:messageIndex];
					[self drawMessage:wrapper];
				}
			}
			glPopMatrix();
		}
		[self->_context popClientState];
	}
}

- (void)drawMessage:(FGVideoMessageWrapper*)wrapper
{
	const GLCMessageDrawParams params=[self messageToDrawParams:wrapper];

	// 5/2013 - messages that are on top of the camera can render at crazy angles (with altitude).
	// Make sure there is some horizontal distance between the camera and between the message.
	DumpGL(@"[%s] '%@', @=(%.2f, %.2f, %.2f), Δ=(%.2f, %.2f, %.2f), rx=%.2f, ry=%.2f, sxy=%.2f",
		   (params.draw) ? "draw" : "skip", wrapper.message.text,
		   params.location.x, params.location.y, params.location.z,
		   params.delta.x, params.delta.y, params.delta.z,
		   params.rotation.x, params.rotation.y, params.scale);

	if(params.draw)
	{
		// rotations - we rotate around the x and y axis to get the message to point at the camera.
		const GLfloat widthMessage=MessageWidth*params.scale;
		const GLfloat heightMessage=MessageHeight*params.scale;
		const GLCTextureVertex vertices[]={
			{{ params.location.x-widthMessage/2, params.location.y+heightMessage/2, params.location.z }, { 0, 1 }},
			{{ params.location.x-widthMessage/2, params.location.y-heightMessage/2, params.location.z }, { 0, 0 }},
			{{ params.location.x+widthMessage/2, params.location.y+heightMessage/2, params.location.z }, { 1, 1 }},
			{{ params.location.x+widthMessage/2, params.location.y-heightMessage/2, params.location.z }, { 1, 0 }},
		};

		glPushMatrix();
		{
			// 1. configure our matrix
			glTranslatef(params.location.x, params.location.y, params.location.z);
			glRotatef(params.rotation.y, 0, 1, 0);
			glRotatef(params.rotation.x, 1, 0, 0);
			glTranslatef(-params.location.x, -params.location.y, -params.location.z);

			// 2. setup our primitive
			glVertexPointer(3, GL_FLOAT, sizeof(GLCTextureVertex), &vertices[0].primitive);

			// 3. bind our texture configure how we interpolate our mipmaps
			glBindTexture(GL_TEXTURE_2D, wrapper.texture.name);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexCoordPointer(2, GL_SHORT, sizeof(GLCTextureVertex), &vertices[0].texture);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, sizeof(vertices)/sizeof(vertices[0]));
		}
		glPopMatrix();
	}
}

- (void)drawHUDRadar
{
	[self->_context pushProjectionMatrix:self->_orthoMatrix];
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	{
		// setup the "camera" transform: scale our radar view and move him to the lower left corner
		glTranslatef([self->_orthoMatrix left]*(1.0-HUDRadarScale)+[self->_orthoMatrix pointWidthToGL:8 depth:0],
					 [self->_orthoMatrix bottom]*(1.0-HUDRadarScale)+[self->_orthoMatrix pointHeightToGL:6 depth:0],
					 0);
		glScalef(HUDRadarScale, HUDRadarScale, 1);

		[self->_context pushClientState];
		{
			[self->_context enableState:GL_BLEND];

			// draw background
			[self->_hudBackground draw:self->_context];

			// draw camera
			[GLCExt drawPoint:self->_context origin:GLKVector2Make(0, 0) radiusInPixels:2.0 color:FGHUDCameraColor];

			// draw messages if we've got an orientation
			if(self->_orientation!=nil)
			{
				// birds-eye orthoginal view: the z component is y and remember that z is reversed (-z axis should extend up)
				glRotatef(0-self->_orientation->value.y, 0, 0, 1);
				glTranslatef(0-self->_cameraOffset.x, self->_cameraOffset.z, 0);

				for(FGVideoMessageWrapper *wrapper in self->_wrappedMessages)
				{
					[GLCExt drawPoint:self->_context origin:GLKVector2Make(wrapper.vector.x, 0-wrapper.vector.z) radiusInPixels:2.0 color:FGHUDMessageColor];
				}
			}
		}
		[self->_context popClientState];
	}
	glPopMatrix();
	[self->_context popProjectionMatrix];
}

@end
