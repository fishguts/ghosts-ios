//
//  FGSessionData.m
//  Goggles
//
//  Created by Curtis Elsasser on 6/5/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSession.h"

static FGSession *instance;

@implementation FGSession

+ (void)initialize
{
	NSAssertNil(instance);
	instance=[[FGSession alloc] init];
}

+ (FGSession*)instance
{
	NSAssertNotNil(instance);
	return instance;
}

@end
