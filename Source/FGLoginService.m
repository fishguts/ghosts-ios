//
//  FGLoginService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLoginService.h"
#import "FGProfileVO.h"
#import "FGStatusVO.h"
#import "FGModelSerializer.h"
#import "FGLoginProxy.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGLoginService
#pragma mark - Public Interface
- (id)initWithProfile:(FGProfileVO*)profile
{
	self=[super init];
	self->_profile=profile;
	
	return self;
}

- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLoggingIn object:self->_profile];
	@try 
	{
		NSString *url=[FGSettings getUrlLoginUser];
		NSData *data=[FGModelSerializer buildLoginRequest:self->_profile pretty:NO];

		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		[FGModelSerializer processLoginResponse:data intoProfile:self->_profile];
		// now that he is logged in, make sure he is flagged properly and is our only login.
		[FGLoginProxy setLogin:self->_profile];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLoginSucceeded object:self->_profile];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to login failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Login failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationLoginFailed object:self->_profile text:errorTextFriendly status:response]];
}

@end
