//
//  FGmessageSerializer.h
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGStatusVO;
@class FGProfileVO;
@class FGMessageVO;
@class FGFeedbackVO;
@class CLLocation;

@interface FGModelSerializer : FGStaticClass

/**** public interface: request builders ****/
+ (NSData*)buildGetSettingsRequest:(BOOL)pretty;
+ (NSData*)buildLoginRequest:(FGProfileVO*)profile pretty:(BOOL)pretty;
+ (NSData*)buildLogoutRequest:(FGProfileVO*)profile pretty:(BOOL)pretty;
+ (NSData*)buildRegisterRequest:(FGProfileVO*)profile pretty:(BOOL)pretty;
+ (NSData*)buildSendFeedbackRequest:(FGFeedbackVO*)feedback pretty:(BOOL)pretty;
+ (NSData*)buildGetMessagesRequest:(CLLocation*)location welcomeState:(BOOL)welcomeState pretty:(BOOL)pretty;
+ (NSData*)buildPostMessageRequest:(FGMessageVO*)message pretty:(BOOL)pretty;
+ (NSData*)buildPostMessagesRequest:(NSArray*)messages pretty:(BOOL)pretty;
+ (NSData*)buildRateMessageRequest:(FGMessageVO*)message rating:(NSNumber*)rating pretty:(BOOL)pretty;
+ (NSData*)buildFlagMessageRequest:(FGMessageVO*)message pretty:(BOOL)pretty;
+ (NSData*)buildDeleteMessageRequest:(FGMessageVO*)message pretty:(BOOL)pretty;

/**** public interface: response parser ****/
+ (FGStatusVO*)parseResponseStatus:(NSData*)jsonData;
+ (void)processLoginResponse:(NSData*)jsonData intoProfile:(FGProfileVO*)profile;
+ (void)processLogoutResponse:(NSData*)jsonData;
+ (void)processRegisterResponse:(NSData*)jsonData;
+ (NSArray*)parseSettingsResponse:(NSData*)jsonData;
+ (void)parsePostResponse:(NSData*)jsonData intoMessage:(FGMessageVO*)message;
+ (NSArray*)parseMessagesResponse:(NSData*)jsonData;

@end
