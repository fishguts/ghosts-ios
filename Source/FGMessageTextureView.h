//
//  FGMessageTextureView.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGMessageTextureView : UIView
/**** public interface ****/
- (void)setUserText:(NSString*)text;
- (void)setMessageText:(NSString*)text;

@end
