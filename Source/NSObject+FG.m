//
//  NSObject+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 12/10/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSObject+FG.h"

@implementation NSObject (FG)

+ (BOOL)isEqual:(NSObject*)obj1 obj2:(NSObject*)obj2
{
	if(obj1==obj2)
	{
		return YES;
	}
	else if(obj1==nil)
	{
		return NO;
	}
	else if(obj2==nil)
	{
		return NO;
	}
	return [obj1 isEqual:obj2];
}

@end
