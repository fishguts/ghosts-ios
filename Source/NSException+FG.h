//
//  NSException+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSException (FG)

+ (NSException*)abstractViolation:(NSString*)method;

@end
