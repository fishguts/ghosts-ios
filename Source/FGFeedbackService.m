//
//  FGFeedbackService.m
//  Goggles
//
//  Created by Curtis Elsasser on 12/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGFeedbackService.h"
#import "FGFeedbackVO.h"
#import "FGStatusVO.h"
#import "FGModelSerializer.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGFeedbackService
#pragma mark - Public interface
- (id)initWithFeedback:(FGFeedbackVO*)feedback
{
	self=[super init];
	self->_feedback=feedback;
	return self;
}

- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationFeedbackPosting object:self->_feedback];
	@try
	{
		NSString *url=[FGSettings getUrlSendFeedback];
		NSData *data=[FGModelSerializer buildSendFeedbackRequest:self->_feedback pretty:NO];
		
		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception)
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationFeedbackPostSucceeded object:self->_feedback];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationUser object:self->_feedback title:@"Thanks" message:@"We appreciate feedback"]];
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to send feedback failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Send feedback failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationFeedbackPostFailed object:self->_feedback text:errorTextFriendly status:response]];
}

@end
