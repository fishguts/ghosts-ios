//
//  NSNotification+FGFactory.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGStatusVO;


@interface NSNotification (FG)
+ (id)notificationWithName:(NSString*)name object:(id)object text:(NSString*)text;
+ (id)notificationWithName:(NSString*)name object:(id)object text:(NSString*)text status:(FGStatusVO*)status;
+ (id)notificationWithName:(NSString*)name object:(id)object value:(id)value text:(NSString*)text status:(FGStatusVO*)status;
+ (id)notificationWithName:(NSString*)name object:(id)object title:(NSString*)title message:(NSString*)message;
+ (id)notificationWithName:(NSString*)name object:(id)object value:(id)value;

+ (id)debugNotificationWithObject:(id)object text:(NSString*)text;
+ (id)infoNotificationWithObject:(id)object text:(NSString*)text;
+ (id)warnNotificationWithObject:(id)object text:(NSString*)text;
+ (id)errorNotificationWithObject:(id)object text:(NSString*)text;

- (NSString*)text;
- (NSString*)title;
- (NSString*)message;
- (FGStatusVO*)status;
- (NSDate*)timestamp;
- (id)value;
@end

