//
//  FGGhostFactory.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGModelFactory.h"
#import "FGModel.h"
#import "FGModelQueries.h"
#import "FGProfileVO.h"
#import "FGMessageVO.h"
#import "FGExpirationVO.h"
#import "FGLoginProxy.h"
#import "FGMotionProxy.h"
#import "FGConstants.h"

@implementation FGModelFactory

+ (FGMessageVO*)createMessage:(NSString*)text expiration:(FGExpirationVO*)expiration
{
	FGProfileVO *login=[FGLoginProxy getLogin];
	if(login==nil)
	{
		@throw [NSException exceptionWithName:@"LoginException" reason:@"Login required to post messages" userInfo:nil];
	}
	
	CLLocation *location=[[FGMotionProxy instance] currentLocation];
#if !defined(TEST)
	if(location==nil)
	{
		@throw [NSException exceptionWithName:@"LocationException" reason:@"Current location cannot be detected" userInfo:nil];
	}
#endif

	NSManagedObjectContext *context=[[FGModel instance] context];
	FGMessageVO *message=[NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];

	message.text=text;
	message.type=FGMessageTypeUser;
	message.status=FGMessageStatusPost;
	message.created=[NSDate date];
	message.expiration=[expiration getExpirationDate:[message created]];
	message.latitude=[NSNumber numberWithDouble:location.coordinate.latitude];
	message.longitude=[NSNumber numberWithDouble:location.coordinate.longitude];
	message.altitude=[NSNumber numberWithDouble:location.altitude];
	[login addMessagesObject:message];
	
	return message;
}

+ (void)updateMessage:(FGMessageVO*)message text:(NSString*)text expiration:(FGExpirationVO*)expiration
{
	message.text=text;
	message.expiration=[expiration getExpirationDate:[message created]];
	message.status=FGMessageStatusPost;
}

+ (FGProfileVO*)createLogin:(NSString*)name password:(NSString*)password
{
	// it's possible that this guy already exists as an existing profile in which case we escalate his status to login
	FGProfileVO *login=[FGModelQueries fetchProfileByName:name];
	if(login==nil)
	{
		NSManagedObjectContext *context=[[FGModel instance] context];
		login=[NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
		login.username=name;
		login.created=[NSDate date];
	}
	
	login.password=password;
	// set him as our exclusive, current login
	[FGLoginProxy setLogin:login];
	
	return login;
}

+ (FGProfileVO*)createRegistration:(NSString*)name email:(NSString*)email password:(NSString*)password
{
	// note: we probably should not allow registration if it already exists locally. Will not cause visual issues.
	FGProfileVO *profile=[FGModelFactory createLogin:name password:password];
	profile.email=email;
	
	return profile;
}


@end
