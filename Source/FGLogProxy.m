//
//  FGLogProxy.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLogProxy.h"
#import "FGConstants.h"
#import "NSString+FG.h"
#import "NSDateFormatter+FG.h"


@interface FGLogProxy()
/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

/**** observers ****/
- (void)handleAlertNotification:(NSNotification*)notification;

@end


@implementation FGLogProxy
static FGLogProxy *instance;

#pragma mark - Initialization
+ (void)initialize
{
	NSAssert(instance==nil, @"Should be nil");
	instance=[[FGLogProxy alloc] init];
}

+ (FGLogProxy*)instance
{
	NSAssert(instance!=nil, @"Should not be nil");
	return instance;
}

#pragma mark - Public interface
- (NSArray*)getAlerts
{
#if defined(LOG)
	// return non-mutable instance that won't change
	return [NSArray arrayWithArray:self->_alerts];
#else
	return nil;
#endif
}

- (void)clearAlerts
{
#if defined(LOG)
	[self->_alerts removeAllObjects];
#endif
}

- (void)saveAlerts
{
#if defined(LOG)
	@try
	{
		// build buffer with all of our notifications
		NSMutableData *buffer=[[NSMutableData alloc] init];
		for(NSNotification *notification in self->_alerts)
		{
			[buffer appendData:[[NSString formatAlert:notification includeTimestamp:YES suffix:@"\n"] dataUsingEncoding:NSUTF8StringEncoding]];
		}

		// get our path if we don't have it already (same for each session)
		if(self->_fileUrl==nil)
		{
			NSURL *url=[[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil]
						URLByAppendingPathComponent:@"logs" isDirectory:YES];
			NSAssertWarn([[NSFileManager defaultManager] createDirectoryAtURL:url withIntermediateDirectories:YES attributes:nil error:nil], 
						 @"Unable to create directory: %@", [url path]);
			for(int sessionIndex=1; YES; sessionIndex++)
			{
				self->_fileUrl=[url URLByAppendingPathComponent:[NSString stringWithFormat:@"%@-%d.log", 
																 [NSDateFormatter stringFromDate:[NSDate date] withFormat:@"YYYY'-'MM'-'dd'"],
																 sessionIndex]];
				if([[NSFileManager defaultManager] fileExistsAtPath:[self->_fileUrl path]]==NO)
				{
					break;
				}
			}
		}

		// write
		NSAssertWarn([[NSFileManager defaultManager] createFileAtPath:[self->_fileUrl path] contents:buffer attributes:nil], 
					 @"Unable to save log: %@", [self->_fileUrl path]);
	}
	@catch (NSException *exception) 
	{
		NSAssertWarn(NO, @"Exception thrown while saving log: %@", [exception description]);
	}
#endif
}

#pragma mark - FGSingleton and Private interface
- (void)startup
{
#if defined(LOG)
	self->_alerts=[[NSMutableArray alloc] init];
#endif
	[self addObservers];
}

- (void)shutdown
{
	self->_alerts=nil;
	[self removeObservers];
}

- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	// alerts: error, warn, info, debug
	[center addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationError object:nil];
	[center addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationWarn object:nil];
	[center addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationInfo object:nil];
	[center addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationDebug object:nil];
	
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Observers
- (void)handleAlertNotification:(NSNotification*)notification
{
	[self->_alerts addObject:notification];
	NSLog(@"%@", [NSString formatAlert:notification includeTimestamp:NO]);
}

@end
