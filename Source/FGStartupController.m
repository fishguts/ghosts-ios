//
//  FGStartupViewController.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStartupController.h"
#import "FGSplashController.h"
#import "FGIntroductionController.h"
#import "FGSettings.h"
#import "FGConstants.h"

@interface FGStartupController()
{
	FGSplashController *_splashController;
	FGIntroductionController *_introductionController;
}

/**** private workers ****/
- (void)showSplash;
- (void)showIntroduction;

- (void)processDismissal:(UIViewController*)controller;

/**** observers ****/
- (void)handleStartupConfigurationSucceeded:(NSNotification*)notification;
@end


@implementation FGStartupController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self showSplash];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleStartupConfigurationSucceeded:) name:FGNotificationStartupConfigurationSucceeded object:nil];
}

#pragma mark - FGModalDelegate
- (BOOL)acceptModalOwnership:(id)modal
{
	return (modal==self->_splashController);
}

- (void)modalCanceled:(id)modal
{
	[self processDismissal:modal];
}

- (void)modalComplete:(id)modal
{
	[self processDismissal:modal];
}


#pragma mark - Private interface
- (void)showSplash
{
	if(FGDefaultSplashDuration>0)
	{
		UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Storyboard-iPhone" bundle:nil];
		self->_splashController=[storyboard instantiateViewControllerWithIdentifier:@"SplashController"];
		[self->_splashController setDelegate:self];
		[self presentViewController:self->_splashController animated:NO completion:NULL];
	}
}

- (void)showIntroduction
{
	UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Storyboard-iPhone" bundle:nil];
	self->_introductionController=[storyboard instantiateViewControllerWithIdentifier:@"IntroductionController"];
	[self->_introductionController setDelegate:self];
	[self presentViewController:self->_introductionController animated:NO completion:NULL];
}

- (void)processDismissal:(UIViewController*)controller
{
	if(controller==self->_introductionController)
	{
		[self dismissViewControllerAnimated:YES completion:nil];
		self->_introductionController=nil;
	}
	else if(controller==self->_splashController)
	{
		if([FGSettings getShowIntroduction])
		{
			[self dismissViewControllerAnimated:NO completion:^{
				[self showIntroduction];
			}];
		}
		else
		{
			[self dismissViewControllerAnimated:YES completion:nil];
		}
		self->_splashController=nil;
	}
	else
	{
		NSFailWarn(@"Who are you?");
	}
}

#pragma mark - Observer handlers
- (void)handleStartupConfigurationSucceeded:(NSNotification*)notification
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:FGNotificationStartupConfigurationSucceeded object:nil];
	[self->_splashController allowDismissal];
}



@end
