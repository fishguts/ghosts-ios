//
//  FGLocationCompression.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/20/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLocationCompression.h"

@interface FGLocationCompression()
{
	float _compressionAlitude;
	NSNumber *_orientationAltitude;
}
@end


@implementation FGLocationCompression
- (id)init
{
	// amounts to a pass through
	return [self initWithAltitudeCompression:1.0 orientAroundCoordinate:nil];
}

- (id)initWithAltitudeCompression:(float)altitude
{
	return [self initWithAltitudeCompression:altitude orientAroundCoordinate:nil];
}

- (id)initWithAltitudeCompression:(float)altitude orientAroundCoordinate:(NSNumber*)coordinate
{
	self=[super init];
	self->_compressionAlitude=altitude;
	self->_orientationAltitude=coordinate;
	return self;
}

- (void)compress:(GLKVector3[])vectors count:(int)count
{
	if(count>0)
	{
		// 1. get sum of coordinates
		float sumAltitude=0.0;
		for(int index=0; index<count; index++)
		{
			sumAltitude+=vectors[index].y;
		}

		// 2. average
		const float avgAltitude=sumAltitude/count;

		// 3. offset
		for(int index=0; index<count; index++)
		{
			const float altitudeOffset=(vectors[index].y-avgAltitude)*self->_compressionAlitude;
			if(self->_orientationAltitude)
			{
				vectors[index].y=([self->_orientationAltitude floatValue]+altitudeOffset);
			}
			else
			{
				vectors[index].y=(avgAltitude+altitudeOffset);
			}
		}
	}
}

@end
