//
//  FGExpirationVO.m
//  Goggles
//
//  Created by Curtis Elsasser on 4/5/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGExpirationVO.h"

@interface FGExpirationVO()
{
	NSDate *_deadline;
	NSNumber *_duration;
}

@end


@implementation FGExpirationVO

#pragma mark - Public Interface
+ (FGExpirationVO*)expirationWithDuration:(NSTimeInterval)totalSeconds id:(NSString*)id text:(NSString*)text
{
	FGExpirationVO *result=[[FGExpirationVO alloc] init];
	result->_duration=[NSNumber numberWithInt:totalSeconds];
	result->_id=id;
	result->_text=text;
	return result;
}

+ (FGExpirationVO*)expirationWithDeadline:(NSDate*)deadline id:(NSString*)id text:(NSString*)text
{
	FGExpirationVO *result=[[FGExpirationVO alloc] init];
	result->_deadline=deadline;
	result->_id=id;
	result->_text=text;
	return result;
}

- (NSDate*)getExpirationDate
{
	return [self getExpirationDate:[NSDate date]];
}

- (NSDate*)getExpirationDate:(NSDate*)fromTime
{
	NSAssertTrue(self->_duration!=nil || self->_deadline!=nil);
	if(self->_deadline)
	{
		return self->_deadline;
	}
	if([self->_duration intValue]>0)
	{
		return [fromTime dateByAddingTimeInterval:[self->_duration intValue]];
	}
	return nil;
}


@end
