//
//  NSInvocation+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSInvocation (FG)

+ (NSInvocation*)invocationForNotificationPost:(NSNotificationCenter*)center notification:(NSNotification*)notification;
@end
