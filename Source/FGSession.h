//
//  FGSessionData.h
//  Goggles
//
//  Created by Curtis Elsasser on 6/5/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSObject.h>

@class CLLocation;

/**
 * Session data that is not managed as part of our model.
 */
@interface FGSession : NSObject
/**** Singleton ****/
+ (FGSession*)instance;

/**** Public Properties ****/
@property (nonatomic, strong) CLLocation *locationLastRetrieve;

@end
