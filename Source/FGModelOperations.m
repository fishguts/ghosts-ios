//
//  FGModelOperations.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/12/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGModelOperations.h"
#import "FGModelQueries.h"
#import "FGProfileVO.h"
#import "FGModel.h"


@implementation FGModelOperations
#pragma mark - Public Interface
+ (void)removeUnreferencedProfiles
{
	NSArray *profiles=[FGModelQueries fetchProfiles];

	for(FGProfileVO *profile in profiles)
	{
		// note: 'login' can be nil or no.  Use positive to confirm.
		if([NSNumber isEqualNumber:profile.login toNumber:[NSNumber staticBoolYES]]==NO)
		{
			if([profile.messages count]==0)
			{
				[[[FGModel instance] context] deleteObject:profile];
			}
		}
	}
}

+ (void)clearDatabase
{
	NSManagedObject *object;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSArray *messages=[FGModelQueries fetchMessages];
	NSArray *profiles=[FGModelQueries fetchProfiles];
	NSArray *settings=[FGModelQueries fetchSettings];

	for(object in messages)
	{
		[context deleteObject:object];
	}
	for(object in profiles)
	{
		[context deleteObject:object];
	}
	for(object in settings)
	{
		[context deleteObject:object];
	}
}

@end
