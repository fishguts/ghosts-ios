//
//  GLCContext.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "GLCContext.h"
#import <vector>


/**** local definitions ****/
typedef void (*restoreFn)(GLenum);
struct GLCEnableState
{
	restoreFn fn;	/* restore state function */
	GLenum arg;		/* state/array to restore */
	BOOL value;		/* enabled state as a result of this push  */
	GLCEnableState(restoreFn _fn, GLenum _arg, BOOL _value)
	{
		this->fn=_fn;
		this->arg=_arg;
		this->value=_value;
	}
};

typedef std::vector<GLCEnableState> ClientStateVector;
typedef std::vector<ClientStateVector> ClientStateStack;
typedef std::vector< id<GLCProjectionMatrixProtocol> > ProjectionMatrixStack;

@interface GLCContext()
{
	EAGLContext *_context;
	GLuint _resolveFrameBuffer;
	GLuint _resolveColorBuffer;
	GLuint _sampleFrameBuffer;
	GLuint _sampleColorBuffer;
	GLuint _sampleDepthBuffer;
	
	GLKVector4 _clearColor;
	GLKVector4 _drawColor;
	GLfloat _pointSize;
	GLfloat _lineWidth;
	ClientStateStack _clientStateStack;
	ProjectionMatrixStack _projectionMatrixStack;
}

/**** Private Interface ****/
-(BOOL)getEnabledState:(GLenum)state;
@end


@implementation GLCContext
#pragma mark - Public Interface
- (id)initWithEAGLDrawable:(id<EAGLDrawable>)drawable width:(GLfloat)width height:(GLfloat)height
{
	return [self initWithEAGLDrawable:drawable width:width height:height includeDepthBuffer:YES];
}

- (id)initWithEAGLDrawable:(id<EAGLDrawable>)drawable width:(GLfloat)width height:(GLfloat)height includeDepthBuffer:(BOOL)depthBuffer
{
	self=[super init];

	// init our 'state' instances
	self->_lineWidth=self->_pointSize=-1.0;
	self->_clearColor=self->_drawColor=GLKVector4Make(-1.0, -1.0, -1.0, -1.0);

	// setup our gl context with depth testing and mutlisample antialiasing
	self->_context=[[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
	if([EAGLContext setCurrentContext:self->_context]==NO)
	{
		@throw [NSException exceptionWithName:@"OpenGLException" reason:@"Cannot find GL support" userInfo:nil];
	}

	// resolve frame buffer
	glGenFramebuffersOES(1, &self->_resolveFrameBuffer);
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, self->_resolveFrameBuffer);

	// resolve color buffer
	glGenRenderbuffersOES(1, &self->_resolveColorBuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, self->_resolveColorBuffer);
	[self->_context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:drawable];
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES,
								 GL_COLOR_ATTACHMENT0_OES,
								 GL_RENDERBUFFER_OES,
								 self->_resolveColorBuffer);

	glGenFramebuffersOES(1, &self->_sampleFrameBuffer);
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, self->_sampleFrameBuffer);

	// sample color buffer
	glGenRenderbuffersOES(1, &self->_sampleColorBuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, self->_sampleColorBuffer);
	glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER_OES, 4, GL_RGBA8_OES, width, height);
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES,
								 GL_COLOR_ATTACHMENT0_OES,
								 GL_RENDERBUFFER_OES,
								 self->_sampleColorBuffer);

	// depth buffer
	if(depthBuffer)
	{
		glGenRenderbuffersOES(1, &self->_sampleDepthBuffer);
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, self->_sampleDepthBuffer);
		glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER_OES, 4, GL_DEPTH_COMPONENT16_OES, width, height);
		glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES,
									 GL_DEPTH_ATTACHMENT_OES,
									 GL_RENDERBUFFER_OES,
									 self->_sampleDepthBuffer);
	}

	glViewport(0, 0, width, height);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// optimization - we are managing a "hidden" client state to which we are going to write default state
	[self pushClientState];

	return self;
}

- (void)dispose
{
	self->_clientStateStack.clear();
	if(self->_context!=nil)
	{
		glDeleteRenderbuffersOES(1, &self->_resolveColorBuffer);
		glDeleteRenderbuffersOES(1, &self->_sampleColorBuffer);
		if(self->_sampleDepthBuffer)
		{
			glDeleteRenderbuffersOES(1, &self->_sampleDepthBuffer);
		}
		glDeleteFramebuffersOES(1, &self->_resolveFrameBuffer);
		glDeleteFramebuffersOES(1, &self->_sampleFrameBuffer);
		self->_resolveColorBuffer=0;
		self->_resolveFrameBuffer=0;
		self->_sampleColorBuffer=0;
		self->_sampleDepthBuffer=0;
		self->_sampleFrameBuffer=0;
		
		if([EAGLContext currentContext]==self->_context)
		{
			[EAGLContext setCurrentContext:nil];
		}
		self->_context=nil;
	}
}

/**** Public Render Buffer Interface ****/
- (void)prepareRenderbuffer:(GLKVector4)clearColor
{
	if(GLKVector4AllEqualToVector4(clearColor, self->_clearColor)==false)
	{
		self->_clearColor=clearColor;
		glClearColor(self->_clearColor.r, self->_clearColor.g, self->_clearColor.b, self->_clearColor.a);
	}

	// move our multisample antialiasing frame buffer into place
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, self->_sampleFrameBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

- (BOOL)presentRenderbuffer:(NSUInteger)target
{
	// render antialiased buffer into our resolve color buffer
	glBindFramebufferOES(GL_DRAW_FRAMEBUFFER_APPLE, self->_resolveFrameBuffer);
	glBindFramebufferOES(GL_READ_FRAMEBUFFER_APPLE, self->_sampleFrameBuffer);
	glResolveMultisampleFramebufferAPPLE();
	
	// note: our sampleFrameBuffer is still the currently bounds framebuffer. We are trusting that it's discarding associated attachments
	const GLenum discardBuffers[]  = { GL_COLOR_ATTACHMENT0_OES, GL_DEPTH_ATTACHMENT_OES };
	glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 2, discardBuffers);
	
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, self->_resolveColorBuffer);
	return [self->_context presentRenderbuffer:target];
}


/**** Public Projection Matrix Interface ****/
- (void)installProjectionMatrix:(id<GLCProjectionMatrixProtocol>)matrix
{
	self->_projectionMatrixStack.clear();
	[self pushProjectionMatrix:matrix];
}

- (void)pushProjectionMatrix:(id<GLCProjectionMatrixProtocol>)matrix
{
	self->projectionMatrix=matrix;
	self->_projectionMatrixStack.push_back(self->projectionMatrix);
	[self->projectionMatrix installMatrix];
}

- (void)popProjectionMatrix
{
	NSAssert(self->_projectionMatrixStack.size()>1, @"Not balanced");
	self->_projectionMatrixStack.pop_back();
	self->projectionMatrix=self->_projectionMatrixStack.back();
	[self->projectionMatrix installMatrix];
}

/**** Public Client State Interface ****/
- (void)pushClientState
{
	self->_clientStateStack.push_back(ClientStateVector());
}

- (void)popClientState
{
	NSAssert(self->_clientStateStack.size()>1, @"Not balanced (0 is our internal state)");
	ClientStateVector &stack=self->_clientStateStack.back();
	for(ClientStateVector::reverse_iterator it=stack.rbegin(); it!=stack.rend(); ++it)
	{
		it->fn(it->arg);
	}
	self->_clientStateStack.pop_back();
}

- (void)enableState:(GLenum)state
{
	if([self getEnabledState:state]==NO)
	{
		if(self->_clientStateStack.size()>1)
		{
			self->_clientStateStack.back().push_back(GLCEnableState(glDisable, state, YES));
		}
		glEnable(state);
	}
}

- (void)disableState:(GLenum)state
{
	if([self getEnabledState:state]==YES)
	{
		if(self->_clientStateStack.size()>1)
		{
			self->_clientStateStack.back().push_back(GLCEnableState(glEnable, state, NO));
		}
		glDisable(state);
	}
}

- (void)enableClientState:(GLenum)state
{
	if([self getEnabledState:state]==NO)
	{
		if(self->_clientStateStack.size()>1)
		{
			self->_clientStateStack.back().push_back(GLCEnableState(glDisableClientState, state, YES));
		}
		glEnableClientState(state);
	}
}

- (void)disableClientState:(GLenum)state
{
	if([self getEnabledState:state]==YES)
	{
		if(self->_clientStateStack.size()>1)
		{
			self->_clientStateStack.back().push_back(GLCEnableState(glEnableClientState, state, NO));
		}
		glDisableClientState(state);
	}
}

- (void)setColor4f:(GLKVector4)color
{
	if(GLKVector4AllEqualToVector4(color, self->_drawColor)==false)
	{
		self->_drawColor=color;
		glColor4f(self->_drawColor.r, self->_drawColor.g, self->_drawColor.b, self->_drawColor.a);
	}
}

- (void)setPointSize:(GLfloat)size
{
	if(size!=self->_pointSize)
	{
		self->_pointSize=size;
		glPointSize(self->_pointSize);
	}
}

- (void)setLineWidth:(GLfloat)width
{
	if(width!=self->_lineWidth)
	{
		self->_lineWidth=width;
		glLineWidth(self->_lineWidth);
	}
}


#pragma mark - Private Interface
/**
 * The profiler makes the opengl state machine out to be kind of a sissy.  We'll try to ease his pain as best we can
 **/
-(BOOL)getEnabledState:(GLenum)state
{
	for(ClientStateStack::const_reverse_iterator itStack=self->_clientStateStack.rbegin(); itStack!=self->_clientStateStack.rend(); itStack++)
	{
		for(ClientStateVector::const_reverse_iterator itState=itStack->rbegin(); itState!=itStack->rend(); itState++)
		{
			if(itState->arg==state)
			{
				return itState->value;
			}
		}
	}

	// we haven't encountered this state yet so we are going to save it in our secret state
	const BOOL enabled=glIsEnabled(state);
	self->_clientStateStack.front().push_back(GLCEnableState(nil, state, enabled));

	return enabled;
}

@end
