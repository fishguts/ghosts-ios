//
//  FGIntroductionController.m
//  Goggles
//
//  Created by Curtis Elsasser on 1/12/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGIntroductionController.h"
#import "FGSettings.h"

@interface FGIntroductionController()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end


@implementation FGIntroductionController
#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.scrollView setContentSize:self.imageView.bounds.size];
}

#pragma mark - Observer handlers
- (IBAction)handleKeepItComing:(id)sender
{
	[FGSettings setShowIntroduction:YES];
	if(self.delegate)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

- (IBAction)handleNeverAgain:(id)sender
{
	[FGSettings setShowIntroduction:NO];
	if(self.delegate)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

- (void)viewDidUnload {
	[self setScrollView:nil];
	[self setImageView:nil];
	[super viewDidUnload];
}
@end
