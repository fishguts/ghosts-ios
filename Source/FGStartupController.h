//
//  FGStartupViewController.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//
// Completes the tiny core startup framework: Splash, startup completion monitoring and splash dismissal. 

#import <UIKit/UIKit.h>
#import "FGTypes.h"


// Change the base class to whatever type it is that you need (table, tab, etc) per project.
@interface FGStartupController : UITableViewController <FGModalDelegate>
/**** Public Interface ****/
- (BOOL)acceptModalOwnership:(id)modal;

@end
