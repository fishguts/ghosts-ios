//
//  FGSettingsController.m
//  Goggles
//
//  Created by Curtis Elsasser on 6/7/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSettingsController.h"
#import "FGSettingsProxy.h"
#import "UIViewController+FG.h"


/**** Local Types and Defines ****/
typedef enum
{
	SECTION_LIST,
	SECTION_UNKNOWN,
} SECTION;

typedef enum
{
	CELL_LIST_DATE,
	CELL_LIST_RANK,
	CELL_UNKNOWN,
} CELL;


@interface FGSettingsController ()
{
	UITableViewCell *_cellListSortSelection;
}

/**** Private Interface ****/
- (CELL)indexPathToCell:(NSIndexPath *)indexPath;

/**** Message List Interface ****/
- (void)listSortToUI;
- (void)uiToListSort;
- (CELL)cellForListSortType:(FGListSortType)sortType;
- (FGListSortType)listSortTypeForCell:(CELL)cell;

@end


@implementation FGSettingsController
#pragma mark - Component lifecycle
/**
 * Note: using viewWillAppear over viewDidLoad because getting really erratic results when loading
 *	table rows from their indexes from viewDidLoad
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	[self listSortToUI];
}

- (CELL)indexPathToCell:(NSIndexPath *)indexPath
{
	if([indexPath section]==SECTION_LIST)
	{
		if([indexPath row]==0)
		{
			return CELL_LIST_DATE;
		}
		else if([indexPath row]==1)
		{
			return CELL_LIST_RANK;
		}
	}
	NSFailWarn(@"Cell?");
	return CELL_UNKNOWN;
}


#pragma mark - Message List Interface
- (void)listSortToUI
{
	const CELL cell=[self cellForListSortType:[FGSettingsProxy getListSortMethod]];
	NSIndexPath *indexPath=[NSIndexPath indexPathForRow:cell inSection:SECTION_LIST];

	self->_cellListSortSelection=[self.tableView cellForRowAtIndexPath:indexPath];
	[self->_cellListSortSelection setAccessoryType:UITableViewCellAccessoryCheckmark];
	NSLog(@"%d %@", [self.tableView numberOfRowsInSection:SECTION_LIST], self->_cellListSortSelection);
}

- (void)uiToListSort
{
	NSIndexPath *path=[self.tableView indexPathForCell:self->_cellListSortSelection];
	const CELL cell=[self indexPathToCell:path];
	const FGListSortType sort=[self listSortTypeForCell:cell];

	[FGSettingsProxy setListSortMethod:sort];
}

- (CELL)cellForListSortType:(FGListSortType)sortType
{
	if(sortType==FGListSortChronological)
	{
		return CELL_LIST_DATE;
	}
	else
	{
		NSAssertWarn(sortType==FGListSortRank, @"?");
		return CELL_LIST_RANK;
	}
}

- (FGListSortType)listSortTypeForCell:(CELL)cell
{
	if(cell==CELL_LIST_DATE)
	{
		return FGListSortChronological;
	}
	else
	{
		NSAssertWarn(cell==CELL_LIST_RANK, @"?");
		return FGListSortRank;
	}
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:indexPath];
	if([indexPath section]==SECTION_LIST)
	{
		if(cell!=self->_cellListSortSelection)
		{
			[self->_cellListSortSelection setAccessoryType:UITableViewCellAccessoryNone];
			self->_cellListSortSelection=cell;
			[self->_cellListSortSelection setAccessoryType:UITableViewCellAccessoryCheckmark];
		}
	}
}

#pragma mark - Observer handlers
- (IBAction)handleDone:(UIBarButtonItem *)sender
{
	[self uiToListSort];
	if(self.delegate)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

@end
