//
//  FGMessageXRayController.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/13/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "AVCaptureDeviceInput+FL.h"
#import "UIViewController+MessageEditor.h"
#import "FGMessageVideoController.h"
#import "FGMessageVideoLayer.h"
#import "FGMessageProxy.h"
#import "FGMotionProxy.h"
#import "FGMessageProxy.h"
#import "FGLoginProxy.h"
#import "FGProfileVO.h"
#import "FGMessageVO.h"
#import "FGBrain.h"
#import "FGLocationFilterThreshold.h"
#import "FGLocationFilterAnimate.h"
#import "FGLocationFilterAccuracy.h"
#import "FGConstants.h"
#import "NSString+FG.h"
#import "NSNotification+FG.h"
#import "UIPinchGestureRecognizer+FG.h"


/**** Local Types and defines ****/
#define DumpCL	NSNoOutput
#define DumpSL	NSNoOutput
#if DEBUG
	#define SUSPEND_FILTERING 0
#else
	#define SUSPEND_FILTERING 0
#endif


@interface FGMessageVideoController()
{
	BOOL _activeViewController;
	NSString *_pendingSegueId;
	
	AVCaptureSession *_session;
	AVCaptureDeviceInput *_input;
	AVCaptureVideoPreviewLayer *_layerVideo;
	FGMessageVideoLayer *_layerMessages;

	FGLocationFilterBase *_filterLocationHead;
	FGLocationFilterAccuracy *_filterLocationAccuracy;
	FGLocationFilterThreshold *_filterLocationThreshold;
	FGLocationFilterAnimate *_filterLocationAnimate;

	BOOL _videoLayerSetup;
	BOOL _messageLayerSetup;
	BOOL _rebuildSphere;
}

/**** properties ****/
@property (weak, nonatomic) IBOutlet UIView *viewVideo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSnap;

/**** private workers ****/
- (void)addObservers;
- (void)removeObservers;

- (void)setupVideoLayer;
- (void)setupMessageLayer;
- (void)updateMessageLayer;
- (void)startVideo;
- (void)stopVideo;
- (void)setScale:(float)scale;

- (void)configureMotionFilters;

- (void)performSegueSequence:(NSString*)segue1 completionSegue:(NSString*)segue2;

/**** observer handlers ****/
- (void)handleApplicationSuspending:(NSNotification*)notification;
- (void)handleMessageGetSucceeded:(NSNotification*)notification;
- (void)handleLocationChange:(NSNotification*)notification;
- (void)handleOrientationChange:(NSNotification*)notification;
- (void)handleAVCaptureSessionRuntimeError:(NSNotification*)notification;

@end


@implementation FGMessageVideoController
@synthesize viewVideo=_viewVideo;
@synthesize buttonSnap = _buttonSnap;

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self configureMotionFilters];

	// note: deferring layer creation due to view size issues. See viewWillAppear

#if !DEBUG
	// if we are release then remove this guy - it's going to be confusing
	// for users to make sense of....and makes things overly complex.
	[self.navigationItem setLeftBarButtonItem:nil];
#endif
}

- (void)viewDidUnload 
{
	[self->_layerMessages dispose];
	[super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	// default processing
	[super viewWillAppear:animated];

	// these are here to just ensure that we haven't gotten
	// this far without our views having been layed out.
	[self setupVideoLayer];
	[self setupMessageLayer];

	if(self->_activeViewController==NO)
	{
		self->_activeViewController=YES;
		[self setScale:1.0];
		[self addObservers];
	}
	else
	{
		[[FGBrain instance] wakeup:YES];
	}
	[self startVideo];
	[self updateMessageLayer];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLocationRetain object:self];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationOrientationRetain object:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
	// our processing
	self->_activeViewController=[self.navigationController isSelectedViewController];
	if(self->_activeViewController)
	{
		[[FGBrain instance] sleep];
	}
	else
	{
		// set us up for our re-entry: update our bearings and don't animate crazy leaps
		self->_rebuildSphere=YES;
		[self->_filterLocationHead reset:YES];
		[self removeObservers];
	}
	[self stopVideo];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLocationRelease object:self];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationOrientationRelease object:self];
	
	// default processing
	[super viewDidDisappear:animated];
}

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	
	// We should be properly sized now.
	// Make sure our fellas have been created.
	[self setupVideoLayer];
	[self setupMessageLayer];
}

#pragma mark - Private Interface: Configuration and Overrides
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	[center addObserver:self selector:@selector(handleApplicationSuspending:) name:FGNotificationApplicationSuspending object:nil];
	[center addObserver:self selector:@selector(handleMessageGetSucceeded:) name:FGNotificationMessagesGetSucceeded object:nil];
	[center addObserver:self selector:@selector(handleLocationChange:) name:FGNotificationLocationChanged object:nil];
	[center addObserver:self selector:@selector(handleOrientationChange:) name:FGNotificationOrientationChanged object:nil];
	[center addObserver:self selector:@selector(handleAVCaptureSessionRuntimeError:) name:AVCaptureSessionRuntimeErrorNotification object:self->_session];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	[super prepareForSegue:segue sender:sender];

	// prevent traffic while we are modal
	id destinationViewController=[segue.destinationViewController activeViewController];
	if([destinationViewController respondsToSelector:@selector(setDelegate:)])
	{
		[destinationViewController setDelegate:self];
	}
	if([destinationViewController respondsToSelector:@selector(setMessage:)])
	{
		if([sender isKindOfClass:[FGMessageVO class]])
		{
			[destinationViewController setMessage:sender];
		}
	}
}

- (void)performSegueSequence:(NSString*)segue1 completionSegue:(NSString*)segue2
{
	self->_pendingSegueId=segue2;
	[self performSegueWithIdentifier:segue1 sender:self];
}


#pragma mark - Private Interface: Video
- (void)setupVideoLayer
{
	// due to sizing issues we may be called more than once, but we only want to act once.
	if(self->_videoLayerSetup==NO)
	{
		@try
		{
			self->_videoLayerSetup=YES;
			self->_input=[AVCaptureDeviceInput defaultDeviceInputWithMediaType:AVMediaTypeVideo];
			if([self->_input.device lockForConfiguration:nil])
			{
				@try
				{
					if([self->_input.device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
					{
						[self->_input.device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
					}
					if([self->_input.device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
					{
						[self->_input.device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
					}
				}
				@catch (NSException *exception)
				{
					NSFailWarn(@"Configuring input device failed: %@", [exception description]);
				}
				@finally
				{
					[self->_input.device unlockForConfiguration];
				}
			}
			self->_session=[[AVCaptureSession alloc] init];
			self->_layerVideo=[AVCaptureVideoPreviewLayer layerWithSession:self->_session];

			[self->_session addInput:self->_input];
			[self->_session setSessionPreset:AVCaptureSessionPresetHigh];
			[self->_layerVideo setOrientation:AVCaptureVideoOrientationPortrait];
			[self->_layerVideo setVideoGravity:AVLayerVideoGravityResizeAspectFill];
			[self->_layerVideo setFrame:self->_viewVideo.bounds];
			[[self->_viewVideo layer] addSublayer:self->_layerVideo];

			NSLog(@"%f", self->_layerVideo.connection.videoMaxScaleAndCropFactor);
		}
		@catch (NSException *exception) 
		{
			NSString *errorText=[NSString stringWithFormat:@"Unable to capture video: %@", [exception description]];
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:errorText]];
		}
	}
}

- (void)startVideo
{
	[self->_session startRunning];
}

- (void)stopVideo
{
	[self->_session stopRunning];
}

- (void)setScale:(float)scale
{
	[self->_layerMessages setScale:scale];
	// a. the connection on our video layer does not support videoScaleAndCropFactor (max=1.0).
	// b. this works but is pretty screwy. It has something to do with the message layer. When I turn it off
	//	this guy operates smoothyly.  Not sure what is going on.  Turning off for now.
	//	Actually, I don't like the zoomed video look so not opposed to a z order parallax effect...
	// [self->_layerVideo setAffineTransform:CGAffineTransformMakeScale(scale, scale)];
}


#pragma mark - Private Interface: Message layer
- (void)setupMessageLayer
{
	// due to sizing issues we may be called more than once, but we only want to act once.
	if(self->_messageLayerSetup==NO)
	{
		@try
		{
			self->_messageLayerSetup=YES;
			self->_layerMessages=[[FGMessageVideoLayer alloc] initWithFrame:self->_viewVideo.bounds];
			[[self->_viewVideo layer] addSublayer:self->_layerMessages];
		}
		@catch (NSException *exception) 
		{
			NSString *errorText=[NSString stringWithFormat:@"Unable to show messages: %@", [exception description]];
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:errorText]];
		}
	}
}

- (void)updateMessageLayer
{
	[self->_layerMessages setMessages:[FGMessageProxy getMessages]];
	[self->_layerMessages setOrientation:[[FGMotionProxy instance] currentOrientation]];
	if([[FGMotionProxy instance] currentLocation])
	{
		[self->_filterLocationHead addLocation:[[FGMotionProxy instance] currentLocation] changeType:FGLocationChangeTypeUndetermined];
	}
}


#pragma mark - Private Interface: Location management and filtering
- (void)configureMotionFilters
{
	self->_filterLocationAccuracy=[[FGLocationFilterAccuracy alloc] initWithThreshold:100.0];
	self->_filterLocationThreshold=[[FGLocationFilterThreshold alloc] initWithMethod:FGLocationFilterThresholdFilter horizontalThreshold:20.0 verticalThreshold:20.0];
	self->_filterLocationAnimate=[[FGLocationFilterAnimate alloc] initWithAnimationIntervals:50 overDuration:2.0];

#if SUSPEND_FILTERING
	NSArray *delegates=[NSArray arrayWithObjects:self->_filterLocationAnimate, self, nil];
#else
	// Filter is as follows:
	//	1. Accuracy - filter out the crazy location changes
	//	2. Threshold - because iOS accuracy is so crazy, make sure we exceed a threshold before we consider ourselves moved.
	//	3. Animate - when we have exceeded a threshold we animate it to make it appear smoother.
	NSArray *delegates=[NSArray arrayWithObjects:self->_filterLocationAccuracy, self->_filterLocationThreshold, self->_filterLocationAnimate, self, nil];
#endif

	self->_filterLocationHead=[FGLocationFilterBase createFilterChainWithDelegates:delegates];
}


#pragma mark - FGModalDelegate
- (void)modalCanceled:(id)modal
{
	[self dismissViewControllerAnimated:YES completion:nil];
	// and reset our lad
	self->_pendingSegueId=nil;
}

- (void)modalComplete:(id)modal
{
	[modal dismissViewControllerAnimated:YES completion:^(void)
	 {
		 if(self->_pendingSegueId!=nil)
		 {
			 [self performSegueSequence:self->_pendingSegueId completionSegue:nil];
		 }
	 }];
}

#pragma mark - FGLocationFilterDelegate
- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	DumpCL(@"[Video controller] Filtered location: %@", [location description]);
	[self->_layerMessages setLocation:location rebuildSphere:self->_rebuildSphere];
	self->_rebuildSphere=NO;
}


#pragma mark - Action handlers
- (IBAction)handlePostMessage:(id)sender 
{
	if([FGLoginProxy isLoggedIn])
	{
		[self performSegueSequence:@"SeguePost" completionSegue:nil];
	}
	else
	{
		[self performSegueSequence:@"SegueLogin" completionSegue:@"SeguePost"];
	}
}

#pragma mark - Observer handlers
- (void)handleApplicationSuspending:(NSNotification*)notification
{
	// set us up for our re-entry: update our bearings and don't animate crazy leaps
	self->_rebuildSphere=YES;
	[self->_filterLocationHead reset:YES];
}

- (void)handleMessageGetSucceeded:(NSNotification*)notification
{
	[self->_layerMessages setMessages:[FGMessageProxy getMessages]];
}

- (void)handleLocationChange:(NSNotification*)notification
{
	[self->_filterLocationHead addLocation:[notification object] changeType:FGLocationChangeTypeUndetermined];
}

- (void)handleOrientationChange:(NSNotification*)notification
{
	[self->_layerMessages setOrientation:[notification object]];
}

- (void)handleAVCaptureSessionRuntimeError:(NSNotification*)notification
{
	NSError *error=[[notification userInfo] valueForKey:AVCaptureSessionErrorKey];
	NSString *errorText=[NSString stringWithFormat:@"Unable to capture video: %@", [error description]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:errorText]];
}

#pragma mark - Action handlers
- (IBAction)handleSnap:(id)sender
{
#if DEBUG
	[self->_layerMessages dump];
#endif
}

- (IBAction)handleTapGesture:(UITapGestureRecognizer*)sender
{
	const CGPoint location=[sender locationInView:self->_viewVideo];
	FGMessageVO *message=[self->_layerMessages hitTestForMessage:location];
	if(message!=nil)
	{
		NSString *segue=[self messageSelectionToSegue:message];
		[self performSegueWithIdentifier:segue sender:message];
	}
}

- (IBAction)handlePinchGesture:(UIPinchGestureRecognizer*)sender
{
	if((sender.state==UIGestureRecognizerStateChanged)
	   ||(sender.state==UIGestureRecognizerStateEnded))
	{
		// temper scale so that users don't fly off the charts too quickly
		const float scaleTamed=[sender scaleWithFactor:0.75 minScale:0.92 maxScale:1.07];
		const float scaleLayer=MAX(FGMessageVideoScaleMin, MIN(FGMessageVideoScaleMax, self->_layerMessages.scale*scaleTamed));
		DumpSL(@"handlePinchGesture: gesture=%.3f, tamed=%.3f, layer=%.2f", sender.scale, scaleTamed, scaleLayer);
		
		[self setScale:scaleLayer];
	}
	else if(sender.state==UIGestureRecognizerStateCancelled)
	{
		[self setScale:1.0];
	}

	// reset so that we are relative
	[sender setScale:1.0];
}


@end
