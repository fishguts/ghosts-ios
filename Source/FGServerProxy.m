//
//  FGServerProxy.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGServerProxy.h"
#import "FGMotionProxy.h"
#import "FGLoginService.h"
#import "FGLogoutService.h"
#import "FGRegisterService.h"
#import "FGFeedbackService.h"
#import "FGSettingsGetService.h"
#import "FGMessagesGetService.h"
#import "FGMessagePostService.h"
#import "FGMessageRateService.h"
#import "FGMessageFlagService.h"
#import "FGMessageDeleteService.h"
#import "FGSettings.h"
#import "FGConstants.h"


#define SYNCHRONOUS_QUEUE 1

static FGServerProxy *instance;

@interface FGServerProxy()
{
	NSMutableArray *_services;
#if SYNCHRONOUS_QUEUE
	FGNetworkService *_service;
#endif
}

/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

- (void)addService:(FGNetworkService*)service;
- (void)removeService:(FGNetworkService*)service;

/**** observers ****/
- (void)handleNetworkSendResult:(NSNotification*)notification;

- (void)handleLoginRequest:(NSNotification*)notification;
- (void)handleLogoutRequest:(NSNotification*)notification;
- (void)handleRegisterRequest:(NSNotification*)notification;
- (void)handleGetSettingsRequest:(NSNotification*)notification;
- (void)handleSendFeedbackRequest:(NSNotification*)notification;
- (void)handleGetMessagesRequest:(NSNotification*)notification;
- (void)handlePostMessageRequest:(NSNotification*)notification;
- (void)handleRateMessageRequest:(NSNotification*)notification;
- (void)handleFlagMessageRequest:(NSNotification*)notification;
- (void)handleDeleteMessageRequest:(NSNotification*)notification;

@end


@implementation FGServerProxy

#pragma mark - Singleton and initialization
+ (void)initialize
{
	NSAssert(instance==nil, @"should be nil");
	instance=[[FGServerProxy alloc] init];

	[instance addObservers];
}

+ (FGServerProxy*)instance
{
	NSAssert(instance!=nil, @"should not be nil");
	return instance;
}

- (id)init
{
	self=[super init];
	self->_services=[NSMutableArray array];
	
	return self;
}

- (void)dealloc
{
	[self shutdown];
	[self removeObservers];
}


#pragma mark - FGSingleton
- (void)startup
{
}

- (void)shutdown
{
	while([self->_services count]>0)
	{
		FGNetworkService *service=[self->_services lastObject];
		[self removeService:service];
		// now that we are no longer watching do our best to get him to stop.
		[service cancel];
	}
}

#pragma mark - Private workers
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	[center addObserver:self selector:@selector(handleLoginRequest:) name:FGNotificationLogin object:nil];
	[center addObserver:self selector:@selector(handleLogoutRequest:) name:FGNotificationLogout object:nil];
	[center addObserver:self selector:@selector(handleRegisterRequest:) name:FGNotificationRegister object:nil];
	[center addObserver:self selector:@selector(handleGetSettingsRequest:) name:FGNotificationSettingsGet object:nil];
	[center addObserver:self selector:@selector(handleSendFeedbackRequest:) name:FGNotificationFeedbackPost object:nil];
	[center addObserver:self selector:@selector(handleGetMessagesRequest:) name:FGNotificationMessagesGet object:nil];
	[center addObserver:self selector:@selector(handlePostMessageRequest:) name:FGNotificationMessagePost object:nil];
	[center addObserver:self selector:@selector(handlePostMessageRequest:) name:FGNotificationMessagePostRetry object:nil];
	[center addObserver:self selector:@selector(handleRateMessageRequest:) name:FGNotificationMessageRate object:nil];
	[center addObserver:self selector:@selector(handleFlagMessageRequest:) name:FGNotificationMessageFlag object:nil];
	[center addObserver:self selector:@selector(handleDeleteMessageRequest:) name:FGNotificationMessageDelete object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)addService:(FGNetworkService*)service
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	// note: if we ever need to schedule services, such as not having overlapping message request and post requests then this will be the place to do it:
	//	- create 2 different queues: processing and pending.  Examine the service types and place in one or the other depending on status
	//	- attach a status to the service.  
	// For now we are going to keep it simple.
	[center addObserver:self selector:@selector(handleNetworkSendResult:) name:FGNotificationNetworkSendSucceeded object:service];
	[center addObserver:self selector:@selector(handleNetworkSendResult:) name:FGNotificationNetworkSendFailed object:service];
	[center addObserver:self selector:@selector(handleNetworkSendResult:) name:FGNotificationNetworkSendCanceled object:service];
	[self->_services addObject:service];
#if SYNCHRONOUS_QUEUE
	if(self->_service==nil)
	{
		self->_service=service;
		[service send];
	}
#else
	[service send];
#endif
}

- (void)removeService:(FGNetworkService*)service
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	NSAssert(service==self->_service, @"Services mismatched");
	[center removeObserver:self name:FGNotificationNetworkSendSucceeded object:service];
	[center removeObserver:self name:FGNotificationNetworkSendFailed object:service];
	[center removeObserver:self name:FGNotificationNetworkSendCanceled object:service];
	[self->_services removeObject:service];
#if SYNCHRONOUS_QUEUE
	NSAssert(service==self->_service, @"Services out of sync");
	// if there is a chap at the head of our queue then let him out of the bag
	if((self->_service=[self->_services firstObject])!=nil)
	{
		[self->_service send];
	}
#endif
}


#pragma mark - Observers
- (void)handleNetworkSendResult:(NSNotification*)notification
{
	[self removeService:[notification object]];
}

- (void)handleLoginRequest:(NSNotification*)notification
{
	FGProfileVO *profile=[notification object];
	[self addService:[[FGLoginService alloc] initWithProfile:profile]];
}

- (void)handleLogoutRequest:(NSNotification*)notification
{
	FGProfileVO *profile=[notification object];
	[self addService:[[FGLogoutService alloc] initWithProfile:profile]];
}

- (void)handleRegisterRequest:(NSNotification*)notification
{
	FGProfileVO *profile=[notification object];
	[self addService:[[FGRegisterService alloc] initWithProfile:profile]];
}

- (void)handleGetSettingsRequest:(NSNotification*)notification
{
	[self addService:[[FGSettingsGetService alloc] init]];
}

- (void)handleSendFeedbackRequest:(NSNotification *)notification
{
	FGFeedbackVO *feedback=[notification object];
	[self addService:[[FGFeedbackService alloc] initWithFeedback:feedback]];
}

- (void)handleGetMessagesRequest:(NSNotification*)notification
{
	const BOOL welcomeState=[FGSettings getRetrieveWelcomeMessages];
	CLLocation *location=([notification object]!=nil) ? [notification object] : [[FGMotionProxy instance] currentLocation];
	[self addService:[[FGMessagesGetService alloc] initWithLocation:location replaceMessages:YES welcomeState:welcomeState]];
}

- (void)handlePostMessageRequest:(NSNotification*)notification
{
	FGMessageVO *message=[notification object];
	[self addService:[[FGMessagePostService alloc] initWithMessage:message]];
}

- (void)handleRateMessageRequest:(NSNotification*)notification
{
	FGMessageVO *message=[notification object];
	NSNumber *rating=[notification value];
	[self addService:[[FGMessageRateService alloc] initWithMessage:message rating:rating]];
}

- (void)handleFlagMessageRequest:(NSNotification*)notification
{
	FGMessageVO *message=[notification object];
	[self addService:[[FGMessageFlagService alloc] initWithMessage:message]];
}

- (void)handleDeleteMessageRequest:(NSNotification*)notification
{
	FGMessageVO *message=[notification object];
	[self addService:[[FGMessageDeleteService alloc] initWithMessage:message]];
}

@end
