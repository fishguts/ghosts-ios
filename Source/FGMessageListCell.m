//
//  FGMessageListCell.m
//  Goggles
//
//  Created by Curtis Elsasser on 6/9/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageListCell.h"

#define OPTIMIZE_SPACE	1

@interface FGMessageListCell ()
/**** Public Properties ****/
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDetail;
@property (weak, nonatomic) IBOutlet UILabel *labelLike;
@property (weak, nonatomic) IBOutlet UILabel *labelDislike;
@property (weak, nonatomic) IBOutlet UIImageView *imageLike;
@property (weak, nonatomic) IBOutlet UIImageView *imageDislike;

@end

@implementation FGMessageListCell
#pragma mark - Public Interface
- (void)setTitleText:(NSString*)text
{
	[self.labelTitle setText:text];
}

- (void)setDetailText:(NSString*)text
{
	[self.labelDetail setText:text];
	[self setNeedsLayout];
}

- (void)setLikeText:(NSString*)text
{
	[self.labelLike setText:text];
	[self setNeedsLayout];
}

- (void)setDislikeText:(NSString*)text
{
	[self.labelDislike setText:text];
	[self setNeedsLayout];
}

- (void)setRatingVisble:(BOOL)visible
{
	[self.labelLike setHidden:!visible];
	[self.labelDislike setHidden:!visible];
	[self.imageLike setHidden:!visible];
	[self.imageDislike setHidden:!visible];
	[self setNeedsLayout];
}

- (void)setRatingEnabled:(BOOL)selectLike selectDislike:(BOOL)selectDislike
{
	NSString *imageNameLike=(selectLike) ? @"icon_rating_like_selected.png" : @"icon_rating_like_enabled.png";
	NSString *imageNameDislike=(selectDislike) ? @"icon_rating_dislike_selected.png" : @"icon_rating_dislike_enabled.png";
	[self.imageLike setImage:[UIImage imageNamed:imageNameLike]];
	[self.imageDislike setImage:[UIImage imageNamed:imageNameDislike]];
}

- (void)setRatingDisabled
{
	[self.imageLike setImage:[UIImage imageNamed:@"icon_rating_like_disabled.png"]];
	[self.imageDislike setImage:[UIImage imageNamed:@"icon_rating_dislike_disabled.png"]];
}


#pragma mark - Overrides
- (void)layoutSubviews
{
	const CGFloat gapFriends=2.0;
	const CGFloat gapEnemies=9.0;
	const CGFloat paddingRight=42.0;
	const CGRect bounds=[self bounds];

	[super layoutSubviews];

	if([self.imageLike isHidden])
	{
		CGRect frame=[self.labelDetail frame];
		frame.size.width=bounds.size.width-frame.origin.x-paddingRight;
		[self.labelDetail setFrame:frame];
	}
	else
	{
		CGRect frame1, frame2;

		// make sure our rating text is optimally sized
		[self.labelLike sizeToFit];
		[self.labelDislike sizeToFit];

		// dislike text: frame1
		frame1=[self.labelDislike frame];
		frame1.origin.x=bounds.size.width-frame1.size.width-paddingRight;
		[self.labelDislike setFrame:frame1];

		// dislike image: frame2
		frame2=[self.imageDislike frame];
		frame2.origin.x=frame1.origin.x-frame2.size.width-gapFriends;
		[self.imageDislike setFrame:frame2];

		// like text: frame1
		frame1=[self.labelLike frame];
		frame1.origin.x=frame2.origin.x-frame1.size.width-gapEnemies;
		[self.labelLike setFrame:frame1];

		// like button: frame2
		frame2=[self.imageLike frame];
		frame2.origin.x=frame1.origin.x-frame2.size.width-gapFriends;
		[self.imageLike setFrame:frame2];

		// detail text: frame1
		frame1=[self.labelDetail frame];
		frame1.size.width=frame2.origin.x-frame1.origin.x-gapFriends;
		[self.labelDetail setFrame:frame1];
	}
}

@end
