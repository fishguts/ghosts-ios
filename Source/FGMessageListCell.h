//
//  FGMessageListCell.h
//  Goggles
//
//  Created by Curtis Elsasser on 6/9/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGMessageListCell : UITableViewCell

/**** Public Interface ****/
- (void)setTitleText:(NSString*)text;
- (void)setDetailText:(NSString*)text;
- (void)setLikeText:(NSString*)text;
- (void)setDislikeText:(NSString*)text;
- (void)setRatingVisble:(BOOL)visible;
- (void)setRatingEnabled:(BOOL)selectLike selectDislike:(BOOL)selectDislike;
- (void)setRatingDisabled;

@end
