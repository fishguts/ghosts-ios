//
//  CLLocation+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (FG)

+ (CLLocation*)clone:(CLLocation*)location withCoordinate:(CLLocationCoordinate2D)coordinate altitude:(CLLocationDistance)altitude;

+ (CLLocationDegrees)metersToLatitudeDelta:(CLLocationDistance)meters;
+ (CLLocationDegrees)metersToLongitudeDelta:(CLLocationDistance)meters atLatitude:(CLLocationDegrees)latitude;

- (CLLocationDistance)distanceFromCoordinate:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude altitude:(CLLocationDistance)altitude;
- (CLLocationDistance)distanceIncludingAltitude:(const CLLocation*)subtrahend;

- (CLLocationDistance)latitudeDeltaInMetersWithLatitude:(CLLocationDegrees)latitudeSubtrahend;
- (CLLocationDistance)latitudeDeltaInMetersWithCoordinate:(const CLLocation*)subtrahend;

- (CLLocationDistance)longitudeDeltaInMetersWithLongitude:(CLLocationDegrees)longitudeSubtrahend latitude:(CLLocationDegrees)latitudeSubtrahend;
- (CLLocationDistance)longitudeDeltaInMetersWithCoordinate:(const CLLocation*)subtrahend;

- (CLLocationDistance)altitudeDeltaInMetersWithAltitude:(CLLocationDegrees)altitudeSubtrahend;
- (CLLocationDistance)altitudeDeltaInMetersWithCoordinate:(const CLLocation*)subtrahend;

- (NSString*)descriptionShort;

@end
