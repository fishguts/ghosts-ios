//
//  FGSettingsGetService.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/31/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@interface FGSettingsGetService : FGNetworkService

@end
