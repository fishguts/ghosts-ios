//
//  FGAccountController.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/14/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLoginController.h"
#import "FGProfileVO.h"
#import "FGLoginProxy.h"
#import "FGModelFactory.h"
#import "FGConstants.h"


@interface FGLoginController()
/**** properties ****/
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *password;

@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonLogin;

/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

- (void)modelToUI:(FGProfileVO*)login;
- (void)updateEnabledStates;

/**** handlers ****/
- (IBAction)handleTextChanged:(id)sender;
- (IBAction)handleLogin:(id)sender;
- (IBAction)handleCancel:(id)sender;
- (void)handleLoginSucceeded:(NSNotification*)notification;
- (void)handleLoginFailed:(NSNotification*)notification;
- (void)handleRegisterSucceeded:(NSNotification*)notification;
@end


@implementation FGLoginController
#pragma mark - Properties
@synthesize delegate;
@synthesize textName;
@synthesize textPassword;
@synthesize buttonLogin;

- (NSString*)name
{
	return [self.textName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString*)password
{
	return [self.textPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self modelToUI:[FGLoginProxy getLogin]];
	[self addObservers];
	[self.textName becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
	if([self isBeingDismissed])
	{
		[self removeObservers];
		[super viewDidDisappear:animated];
	}
}

- (void)viewDidUnload
{
	[self setTextName:nil];
	[self setTextPassword:nil];
	[self setButtonLogin:nil];
    [super viewDidUnload];
}

#pragma mark - Private interface
- (void)addObservers
{
	NSNotificationCenter *notificationCenter=[NSNotificationCenter defaultCenter];
	
	[notificationCenter addObserver:self selector:@selector(handleLoginSucceeded:) name:FGNotificationLoginSucceeded object:nil];
	[notificationCenter addObserver:self selector:@selector(handleLoginFailed:) name:FGNotificationLoginFailed object:nil];
	[notificationCenter addObserver:self selector:@selector(handleRegisterSucceeded:) name:FGNotificationRegisterSucceeded object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)modelToUI:(FGProfileVO*)login
{
	if(login!=nil)
	{
		self.textName.text=login.username;
		self.textPassword.text=login.password;
	}
	[self updateEnabledStates];
}

- (void)updateEnabledStates
{
	BOOL ready=([self.name length]>=FGMinUserNameLength) && ([self.password length]>=FGMinPasswordLength);

	[self.buttonLogin setEnabled:ready];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if(textField==self.textName)
	{
		[self.textPassword becomeFirstResponder];
	}
	else if(textField==self.textPassword)
	{
		if([self.buttonLogin isEnabled])
		{
			[self handleLogin:textField];
		}
	}
	return NO;
}

#pragma mark - Observer handlers
- (IBAction)handleTextChanged:(id)sender 
{
	[self updateEnabledStates];
}

- (IBAction)handleLogin:(id)sender 
{
	FGProfileVO *login=[FGModelFactory createLogin:self.name password:self.password];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLogin object:login];
}

- (IBAction)handleCancel:(id)sender 
{
	if(self.delegate!=nil)
	{
		[self.delegate modalCanceled:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)handleLoginSucceeded:(NSNotification*)notification
{
	if(self.delegate!=nil)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)handleLoginFailed:(NSNotification*)notification
{
	// repost as an error so that it's modally displayed
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:[notification object] text:[notification text]]];
}

- (void)handleRegisterSucceeded:(NSNotification*)notification
{
	// attempt to absorb their login credentions
	[self modelToUI:[FGLoginProxy getLogin]];
}

@end
