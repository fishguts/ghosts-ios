//
//  FGModelOperations.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/12/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@interface FGModelOperations : FGStaticClass
/**** public interface ****/
+ (void)clearDatabase;
+ (void)removeUnreferencedProfiles;

@end
