//
//  FGMotionFilterConditioner.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/29/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>
#import "FGMotionFilterConditioner.h"

#define CMDump NSNoOutput


@implementation FGMotionFilterConditioner

- (void)addDeviceMotion:(CMDeviceMotion *)motion withHeading:(double)heading
{
	/**
	 * This uses a combination of attitude and gravity.  I found that gravity readings for rotation around the x axis (y gravity)
	 * were strongly affected by rotation around y axis.  Using MotionTest application, I sought out a combination that felt the
	 * most accurate.  History has our various iterations should we ever decide to regress
	 **/
	GLKVector3 rotation;

	// X rotation:
	//	pitch: phone is at 0 when face up and perpendicular to gravity. -PI/2 when top down and PI when top up.
	rotation.x=(180*motion.attitude.pitch)/M_PI;
	if(motion.gravity.z>0)
	{
		rotation.x=180-rotation.x;
	}
	// rotate 90 degrees CCW so that 0 is top up and range is -180->180
	rotation.x=rotation.x-90;

	// Y rotation:
	rotation.y=360-heading;

	// Z rotation: gravity adheres to right hand rule. 0 degrees is phone facing up.
	rotation.z=-motion.gravity.x*90;
	if(motion.gravity.x>0)
	{
		// rotating to the right: 0 -> -180.
		if(motion.gravity.y>0)
		{
			// phone is pointing down
			rotation.z=-180-rotation.z;
		}
	}
	else
	{
		// rotating to the left: 0 -> 180
		if(motion.gravity.y>0)
		{
			rotation.z=180-rotation.z;
		}
	}

	CMDump(@"Orientation: {%.1f, %.1f, %.1f} h=%.1f a={%.2f, %.2f, %.2f} g={%.2f, %.2f, %.2f}",
		   rotation.x, rotation.y, rotation.z, heading,
		   motion.attitude.pitch, motion.attitude.yaw, motion.attitude.roll,
		   motion.gravity.x, motion.gravity.y, motion.gravity.z);


	[self.delegate motionFilter:self acceptOrientation:rotation];
}

@end
