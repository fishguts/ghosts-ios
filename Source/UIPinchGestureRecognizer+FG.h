//
//  UIPinchGestureRecognizer+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/28/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIPinchGestureRecognizer.h>

@interface UIPinchGestureRecognizer (FG)

- (CGFloat)scaleWithFactor:(CGFloat)factor minScale:(CGFloat)minScale maxScale:(CGFloat)maxScale;

@end
