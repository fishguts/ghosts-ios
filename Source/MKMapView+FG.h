//
//  MKMapView+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (FG)

- (void)removeAllAnnotations;
- (void)removeCustomAnnotations;
@end
