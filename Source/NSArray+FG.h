//
//  NSArray+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (FG)
/**** public interface ****/
- (id)firstObject;
@end
