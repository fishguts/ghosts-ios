//
//  NSIndexPath+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/28/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSIndexPath+FG.h"

@implementation NSIndexPath (FG)

- (NSString*)descriptionOfIndexes
{
	NSMutableString *buffer=[[NSMutableString alloc] init];
	for(int index=0; index<[self length]; index++)
	{
		if(index>0)
		{
			[buffer appendString:@","];
		}
		[buffer appendFormat:@"%d", [self indexAtPosition:index]];
	}
	return buffer;
}

@end
