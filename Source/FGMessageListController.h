//
//  FGViewController.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/5/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStartupController.h"

@interface FGMessageListController : FGStartupController <NSFetchedResultsControllerDelegate>


@end
