//
//  FGMessageFlagService.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@class FGMessageVO;


@interface FGMessageFlagService : FGNetworkService
{
	FGMessageVO *_message;
}

/**** public interface ****/
- (id)initWithMessage:(FGMessageVO*)message;

@end
