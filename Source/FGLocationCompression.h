//
//  FGLocationCompression.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/20/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSObject.h>
#include <GLKit/GLKMathTypes.h>


@interface FGLocationCompression : NSObject
/**** Interface ****/
- (id)init;
- (id)initWithAltitudeCompression:(float)altitude;
- (id)initWithAltitudeCompression:(float)altitude orientAroundCoordinate:(NSNumber*)coordinate;

- (void)compress:(GLKVector3[])vectors count:(int)count;

@end
