//
//  FGBrain.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGBrain.h"
#import "FGModel.h"
#import "FGStatusVO.h"
#import "FGLogProxy.h"
#import "FGLoginProxy.h"
#import "FGServerProxy.h"
#import "FGMotionProxy.h"
#import "FGSettingsProxy.h"
#import "FGMessageProxy.h"
#import "FGDateRange.h"
#import "FGSettings.h"
#import "FGConstants.h"
#import "FGTimer.h"


/**** constants and defines ****/
#define BrainDump NSDebugFL


/**** private interface ****/
@interface FGBrain()
{
	BOOL _initializing;
	BOOL _startupState;
	BOOL _resumeState;
	BOOL _startupMessages;
	
	BOOL _sleeping;
	BOOL _retrieving;
	BOOL _loggingIn;
	BOOL _loggingOut;
	BOOL _loginChanged;
	BOOL _retrieveFailed;
	BOOL _messageIntervalElapsed;
	BOOL _locationIntervalElapsed;
	BOOL _locationChanged;
}

/**** private properties ****/
@property (nonatomic, strong) FGTimer *timerMessageInterval;
@property (nonatomic, strong) FGTimer *timerLocationInterval;
@property (nonatomic, strong) FGTimer *timerWelcomeInterval;

/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

- (void)processState;
- (void)resetStates;
- (void)resetStatesIncludingResuming:(BOOL)resume includeRetrieving:(BOOL)retrieve includeLoginChanged:(BOOL)login includeLocationChanged:(BOOL)location;
- (void)setInitalizedComplete;
- (void)setStartStateComplete;
- (void)setStartupMessagesComplete;

- (void)processMessagesInError;

- (void)handleDBOpenSucceeded:(NSNotification*)notification;
- (void)handleSettingsGetComplete:(NSNotification*)notification;
- (void)handleLoggingIn:(NSNotification*)notification;
- (void)handleLoginSucceeded:(NSNotification*)notification;
- (void)handleLoginFailed:(NSNotification*)notification;
- (void)handleLoggingOut:(NSNotification*)notification;
- (void)handleLogoutSucceeded:(NSNotification*)notification;
- (void)handleMessagesRetrieving:(NSNotification*)notification;
- (void)handleMessagesRetrieveSucceeded:(NSNotification*)notification;
- (void)handleMessagesRetrieveFailed:(NSNotification*)notification;
- (void)handleMessagePostSucceeded:(NSNotification*)notification;
- (void)handleMessagePostFailed:(NSNotification*)notification;
- (void)handleLocationChanged:(NSNotification*)notification;
- (void)handleApplicationSuspending:(NSNotification*)notification;
- (void)handleApplicationResuming:(NSNotification*)notification;

- (void)handleLocationTimerTick:(FGTimer*)timer;
- (void)handleMessageTimerTick:(FGTimer*)timer;
- (void)handleWelcomeTimerTick:(FGTimer*)timer;

@end


@implementation FGBrain
static FGBrain *instance;

#pragma mark - Properties
@synthesize timerMessageInterval=_timerMessageInterval;
@synthesize timerLocationInterval=_timerLocationInterval;

- (BOOL)isLoggingIn
{
	return self->_loggingIn;
}
- (BOOL)isLoggingOut
{
	return self->_loggingOut;
}
- (BOOL)isFetchingMessages
{
	return self->_retrieving;
}


#pragma mark - Initialization
+ (void)initialize
{
	NSAssert(instance==nil, @"Should be nil");
	instance=[[FGBrain alloc] init];
}

+ (FGBrain*)instance
{
	NSAssert(instance!=nil, @"Should not be nil");
	return instance;
}

- (id)init
{
	self=[super init];
	self->_initializing=YES;
	self->_startupState=YES;
	
	return self;
}

#pragma mark - Public interface
- (void)startup
{
	[self addObservers];

	// rev up our singleton engines
	[[FGLogProxy instance] startup];
	[[FGModel instance] startup];
	[[FGServerProxy instance] startup];
	[[FGMotionProxy instance] startup];
	// note: starting up location immediately even though we don't use him until everything is settled.
	// The rationale is that location takes a bit to settle down. Let him brew for a bit....until settings
	// are loaded he will use our defaults.
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLocationRetain object:self];
}

- (void)shutdown
{
	// and shut them down, boys
	[[FGServerProxy instance] shutdown];
	[[FGMotionProxy instance] shutdown];
	[[FGModel instance] shutdown];
	[[FGLogProxy instance] shutdown];

	[self removeObservers];
	[self resetStates];
}

- (void)sleep
{
	if(self->_sleeping==NO)
	{
		BrainDump(@"suspending");
		self->_sleeping=YES;
		[self->_timerWelcomeInterval suspend];
		[self processState];
	}
}

- (void)wakeup:(BOOL)reset
{
	if(self->_sleeping)
	{
		BrainDump(@"resuming");
		self->_sleeping=NO;
		[self->_timerWelcomeInterval resume];
		if(reset)
		{
			// don't allow reset if we are in a startup state otherwise we'll end up waiting for a refresh cycle worth of time.
			if(self->_startupState==NO)
			{
				[self resetStatesIncludingResuming:NO includeRetrieving:NO includeLoginChanged:NO includeLocationChanged:NO];
			}
		}
		[self processState];
	}
}

#pragma mark - Private interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	[center addObserver:self selector:@selector(handleDBOpenSucceeded:) name:FGNotificationDBOpenSucceeded object:nil];
	[center addObserver:self selector:@selector(handleSettingsGetComplete:) name:FGNotificationSettingsGetSucceeded object:nil];
	[center addObserver:self selector:@selector(handleSettingsGetComplete:) name:FGNotificationSettingsGetFailed object:nil];
	[center addObserver:self selector:@selector(handleLoggingIn:) name:FGNotificationLoggingIn object:nil];
	[center addObserver:self selector:@selector(handleLoginSucceeded:) name:FGNotificationLoginSucceeded object:nil];
	[center addObserver:self selector:@selector(handleLoginFailed:) name:FGNotificationLoginFailed object:nil];
	[center addObserver:self selector:@selector(handleLoggingOut:) name:FGNotificationLoggingOut object:nil];
	[center addObserver:self selector:@selector(handleLogoutSucceeded:) name:FGNotificationLogoutSucceeded object:nil];
	[center addObserver:self selector:@selector(handleMessagesRetrieving:) name:FGNotificationMessagesGetting object:nil];
	[center addObserver:self selector:@selector(handleMessagesRetrieveSucceeded:) name:FGNotificationMessagesGetSucceeded object:nil];
	[center addObserver:self selector:@selector(handleMessagesRetrieveFailed:) name:FGNotificationMessagesGetFailed object:nil];
	[center addObserver:self selector:@selector(handleMessagePostSucceeded:) name:FGNotificationMessagePostSucceeded object:nil];
	[center addObserver:self selector:@selector(handleMessagePostFailed:) name:FGNotificationMessagePostFailed object:nil];
	[center addObserver:self selector:@selector(handleLocationChanged:) name:FGNotificationLocationChanged object:nil];
	[center addObserver:self selector:@selector(handleApplicationSuspending:) name:FGNotificationApplicationSuspending object:nil];
	[center addObserver:self selector:@selector(handleApplicationResuming:) name:FGNotificationApplicationResuming object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setInitalizedComplete
{
	if(self->_initializing)
	{
		self->_initializing=NO;
		[self processState];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationStartupConfigurationSucceeded object:nil];
	}
}

- (void)setStartStateComplete
{
	if(self->_startupState)
	{
		self->_startupState=NO;
		// reserving room to do stuff if it ever makes sense
	}
}

- (void)setStartupMessagesComplete
{
	if(self->_startupMessages==NO)
	{
		self->_startupMessages=YES;
		if([FGSettings getRetrieveWelcomeMessages])
		{
			// keep requesting welcome messages until they read the message or until the timer elapses.
			self->_timerWelcomeInterval=[FGTimer scheduledTimerWithTimeInterval:FGDefaultWelcomeDuration target:self selector:@selector(handleWelcomeTimerTick:) userInfo:nil repeats:NO];
		}
	}
}


- (void)processState
{
	if([[FGMotionProxy instance] currentLocation])
	{
		// everything is suspended while we are processing
		if((self->_initializing==NO)
		   && (self->_retrieving==NO)
		   && (self->_loggingIn==NO)
		   && (self->_loggingOut==NO))
		{
			// note: follows are conditions under which we always get messages. 
			// If none apply then we check our monitors to make sure they are watching.
			// Note: reason we are retrieving messages on loginChanged is in support of private and group messages. They don't exist
			//	at the moment but should that change we are in good shape to support them.
			if((self->_startupState==YES)
			   || (self->_loginChanged==YES)
			   || (self->_messageIntervalElapsed==YES)
			   || (self->_locationIntervalElapsed==YES))
			{
				if(self->_sleeping==NO)
				{
					[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagesGet object:nil];
				}
			}
			else
			{
				// The following sequence is as follows:
				//	1. resume state: we are resuming after having been suspended which takes precedent (resuses timerMessageInterval timer)
				//	2. refresh interval (self.timerMessageInterval) - if it's not set make sure we set it.
				//	3. location changed - if we have experienced a major shift then startup our fast timer

				// 1. - resuming
				if(self->_resumeState)
				{
					const NSTimeInterval interval=[FGSettings getMessageQueryIntervalForApplicationResume];
					if(([self.timerMessageInterval isActive]==NO)
					   || (interval<[self.timerMessageInterval fireInterval]))
					{
						[self.timerMessageInterval invalidate]; self.timerMessageInterval=nil;
					}
					if(self.timerMessageInterval==nil)
					{
						BrainDump(@"starting resume timer: %f", interval);
						self.timerLocationInterval=[FGTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(handleLocationTimerTick:) userInfo:nil repeats:NO];
					}
				}

				// 2. - refresh interval
				if(self.timerMessageInterval==nil)
				{
					const NSTimeInterval interval=[FGSettings getMessageQueryIntervalMaximum];
					BrainDump(@"starting refresh timer: %f", interval);
					self.timerMessageInterval=[FGTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(handleMessageTimerTick:) userInfo:nil repeats:NO];
				}

				// 3. location changed interval
				if(self->_locationChanged)
				{
					if(self.timerLocationInterval==nil)
					{
						// if our last retrieve failed then wait a maximum before requerying
						const NSTimeInterval interval=(self->_retrieveFailed) ? [FGSettings getMessageQueryIntervalMaximum] : [FGSettings getMessageQueryIntervalMinimum];
						BrainDump(@"starting location timer: %f", interval);
						self.timerLocationInterval=[FGTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(handleLocationTimerTick:) userInfo:nil repeats:NO];
					}
				}
			}
		}
	}
}

- (void)resetStates
{
	[self resetStatesIncludingResuming:YES includeRetrieving:YES includeLoginChanged:YES includeLocationChanged:YES];
}

- (void)resetStatesIncludingResuming:(BOOL)resume includeRetrieving:(BOOL)retrieve includeLoginChanged:(BOOL)login includeLocationChanged:(BOOL)location
{
	// as of the time we are resetting our current state our startup state is complete
	[self setStartStateComplete];
	if(resume)
	{
		self->_resumeState=NO;
	}
	if(retrieve)
	{
		self->_retrieving=NO;
	}
	if(login)
	{
		self->_loginChanged=NO;
	}
	if(location)
	{
		self->_locationChanged=NO;
	}
	self->_retrieveFailed=NO;
	self->_messageIntervalElapsed=NO;
	self->_locationIntervalElapsed=NO;
	[self.timerLocationInterval invalidate]; self.timerLocationInterval=nil;
	[self.timerMessageInterval invalidate]; self.timerMessageInterval=nil;
}

- (void)processMessagesInError
{
	if([FGLoginProxy isLoggedIn])
	{
		FGProfileVO *profile=[FGLoginProxy getLogin];
		NSArray *messages=[FGMessageProxy getMessagesWithStatus:FGMessageStatusError];

		for(FGMessageVO *message in messages)
		{
			if(message.owner==profile)
			{
				[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagePostRetry object:message];
			}
			else
			{
				// this is a strange edge case in which a user logged in as another user, posted a message with an error
				// and then logged in as somebody else.  The message may never go away so we vanquish them.  Sorry, bub.
				[[[FGModel instance] context] deleteObject:message];
			}
		}
	}
}


#pragma mark - Observer handlers
- (void)handleDBOpenSucceeded:(NSNotification*)notification
{
	[FGSettingsProxy installSettings];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationSettingsGet object:nil];
}

- (void)handleSettingsGetComplete:(NSNotification*)notification
{
	// queue up a logout and login as to prevent auth from getting stale
	// note: the reason we turn on our logging in and out flags is so that we wait for completion
	// of both operations before we proceed with message business.
	if([FGLoginProxy logoutIfPossible])
	{
		self->_loggingOut=YES;
	}
	if([FGLoginProxy loginIfPossible])
	{
		self->_loggingIn=YES;
	}

	// at this point we consider ourselves initialized
	[self setInitalizedComplete];
}

- (void)handleLoggingIn:(NSNotification *)notification
{
	self->_loggingIn=YES;
}

- (void)handleLoginSucceeded:(NSNotification*)notification
{
	self->_loggingIn=NO;
	self->_loginChanged=YES;
	// Note about sequencing: it's very possible that we are now posting and getting at the same time. We've got synchronous
	// queuing built into the network proxy such that messages will be commited before the get which is probably the most accurate
	// in terms of the results they will see once the get is processed.
	[self processMessagesInError];
	[self processState];
}

- (void)handleLoginFailed:(NSNotification *)notification
{
	self->_loggingIn=NO;
	[self processState];
}

- (void)handleLoggingOut:(NSNotification *)notification
{
	self->_loggingOut=YES;
}

- (void)handleLogoutSucceeded:(NSNotification*)notification
{
	self->_loggingOut=NO;
	self->_loginChanged=YES;
	[self processState];
}

- (void)handleMessagesRetrieving:(NSNotification*)notification
{
	self->_retrieving=YES;
	[self resetStatesIncludingResuming:YES includeRetrieving:NO includeLoginChanged:YES includeLocationChanged:YES];
}

- (void)handleMessagesRetrieveSucceeded:(NSNotification*)notification
{
	[[FGModel instance] save];

	self->_retrieving=NO;
	[self setStartupMessagesComplete];
	[self processMessagesInError];
	[self processState];
}

- (void)handleMessagesRetrieveFailed:(NSNotification*)notification
{
	// get things started again but in a failed state
	self->_retrieving=NO;
	self->_retrieveFailed=YES;
	[self processState];
}

- (void)handleMessagePostSucceeded:(NSNotification*)notification
{
	[[FGModel instance] save];
}

- (void)handleMessagePostFailed:(NSNotification*)notification
{
	FGStatusVO *status=[notification status];
	
	// first let's make sure we persist the error
	[[FGModel instance] save];
	
	// If it was an auth issue thne try a silent login at which time we will try again to post our messages in error
	if([status.code isEqualToString:FGResponseStatusAuth])
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLogin object:[FGLoginProxy getLogin]];
	}
}

- (void)handleLocationChanged:(NSNotification*)notification
{
	// make sure it's a major shift
	if([[notification userInfo] boolForKey:FGUserKeyLocationMajor])
	{
		self->_locationChanged=YES;
		[self processState];
	}
}

- (void)handleApplicationSuspending:(NSNotification *)notification
{
	// ios has some strange behavior with timers in the background - the appear to honor time elapsed while resting which
	// causes them to fire as soon as we resume (even before our own resume notification). Suspend them and we'll resume them upon waking back up.
	[self.timerLocationInterval suspend];
	[self.timerMessageInterval suspend];
}

- (void)handleApplicationResuming:(NSNotification *)notification
{
	[self.timerLocationInterval resume];
	[self.timerMessageInterval resume];
	if(self->_resumeState==NO)
	{
		// Don't force anything unless we've been in the background for at least a query interval or
		// we have already have a location changes (hoping to get one in the interim if not)
		if(self->_locationChanged)
		{
			self->_resumeState=YES;
		}
		else
		{
			FGDateRange *dateRange=[notification value];
			self->_resumeState=(dateRange.elapsed>=[FGSettings getMessageQueryIntervalMinimum]);
		}
	}
	[self processState];
}

- (void)handleLocationTimerTick:(FGTimer*)timer
{
	self->_locationIntervalElapsed=YES;
	[self processState];
}

- (void)handleMessageTimerTick:(FGTimer*)timer
{
	self->_messageIntervalElapsed=YES;
	[self processState];
}

- (void)handleWelcomeTimerTick:(FGTimer*)timer
{
	self->_timerWelcomeInterval=NULL;
	[FGSettings setRetrieveWelcomeMessages:NO];
}

@end
