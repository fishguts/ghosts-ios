//
//  FGMotionFilterConditioner.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/29/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMotionFilterBase.h"

@class CMDeviceMotion;

@interface FGMotionFilterConditioner : FGMotionFilterBase
/**** Public Interface ****/
- (void)addDeviceMotion:(CMDeviceMotion*)motion withHeading:(double)heading;

@end
