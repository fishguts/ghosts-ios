//
//  UIPinchGestureRecognizer+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/28/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "UIPinchGestureRecognizer+FG.h"

@implementation UIPinchGestureRecognizer (FG)

- (CGFloat)scaleWithFactor:(CGFloat)factor minScale:(CGFloat)minScale maxScale:(CGFloat)maxScale
{
	float scale=1+(self.scale-1)*factor;
	if(scale>maxScale)
	{
		scale=maxScale;
	}
	else if(scale<minScale)
	{
		scale=minScale;
	}
	return scale;
}

@end
