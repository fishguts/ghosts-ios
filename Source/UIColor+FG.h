//
//  UIColor+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 11/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIColor.h>
#import <GLKit/GLKit.h>

@interface UIColor (FG)
/**** class methods ****/
+ (UIColor*)colorWithGLKVector4:(GLKVector4)vector;

/**** initialization ****/
- (id)initWithGLKVector4:(GLKVector4)vector;

@end
