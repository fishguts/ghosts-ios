//
//  GLCOrthoMatrix.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//
// Todo: projection matrix point conversion should be able to be handled generally with matrix transforms, 
//	current projection matrix and viewport matrix.  See GLK matrix operations or poke around.

#import <Foundation/NSObject.h>
#import "GLCTypes.h"


@protocol GLCProjectionMatrixProtocol <NSObject>
/**** properties ****/
@property (nonatomic, readonly) GLfloat left;
@property (nonatomic, readonly) GLfloat right;
@property (nonatomic, readonly) GLfloat top;
@property (nonatomic, readonly) GLfloat bottom;
@property (nonatomic, readonly) GLfloat near;
@property (nonatomic, readonly) GLfloat far;


/**** public interface ****/
- (void)installMatrix;

- (GLfloat)glWidthToPoints:(GLfloat)glWidth depth:(GLfloat)depth;
- (GLfloat)glHeightToPoints:(GLfloat)glHeight depth:(GLfloat)depth;
- (GLfloat)pointWidthToGL:(GLfloat)pointWidth depth:(GLfloat)depth;
- (GLfloat)pointHeightToGL:(GLfloat)pointHeight depth:(GLfloat)depth;

// todo: these guys should be covered by matrix operations
- (GLKVector2)glToPoint:(GLKVector3)coordinate;
- (GLKVector3)pointToGL:(GLKVector2)point depth:(GLfloat)depth;

@end


/**
 * Orthographic projection matrix
 **/
@interface GLCOrthoMatrix : NSObject <GLCProjectionMatrixProtocol>
{
@private
	GLfloat _left;
	GLfloat _right;
	GLfloat _top;
	GLfloat _bottom;
	GLfloat _near;
	GLfloat _far;
}

/**** public interface ****/
- (id)initWithLeft:(GLfloat)left right:(GLfloat)right bottom:(GLfloat)bottom top:(GLfloat)top near:(GLfloat)near far:(GLfloat)far;
/**
 * Uses the aspect ratio of the current view port to determine the height.  Place the origin at the center
 **/
- (id)initByAspectRatioWithWidth:(GLfloat)width near:(GLfloat)near far:(GLfloat)far;
/**
 * Uses the aspect ratio of the current view port to determine the width.  Place the origin at the center
 **/
- (id)initByAspectRatioWithHeight:(GLfloat)height near:(GLfloat)near far:(GLfloat)far;

@end


/**
 * Frustum projection matrix
 **/
@interface GLCFrustumMatrix : NSObject <GLCProjectionMatrixProtocol>
{
@private
	GLfloat _left;
	GLfloat _right;
	GLfloat _top;
	GLfloat _bottom;
	GLfloat _near;
	GLfloat _far;
}

/**** public interface ****/
- (id)initWithLeft:(GLfloat)left right:(GLfloat)right bottom:(GLfloat)bottom top:(GLfloat)top near:(GLfloat)near far:(GLfloat)far;
/**
 * Create a frustum configuration by field of view.  Focal length is termined by near clipping plane
 **/
- (id)initWithFOV:(GLfloat)fov near:(GLfloat)near far:(GLfloat)far;

@end
