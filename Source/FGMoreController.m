//
//  FGMoreController.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMoreController.h"
#import "FGDocumentController.h"
#import "FGProfileVO.h"
#import "FGLoginProxy.h"
#import "FGSettings.h"
#import "FGConstants.h"

/**** static constants ****/
typedef enum : NSInteger
{
	SECTION_ACCOUNT=0,
	SECTION_HELP=1,
	SECTION_LEGAL=2,
	SECTION_DEBUG=3,
	SECTION_COUNT,
} SECTION;

typedef enum : NSInteger
{
	CELL_USER,
	CELL_LOGIN,
	CELL_LOGOUT,
	CELL_REGISTER,
	CELL_SETTINGS,
	CELL_HELP,
	CELL_FEEDBACK,
	CELL_TERMS,
	CELL_PRIVACY,
	CELL_LOG,
	CELL_UNKNOWN,
} CELL;


/**** Private declaration ****/
@interface FGMoreController ()
{
	BOOL _activeViewController;
}

/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

- (void)reloadLoginAffectedRows:(UITableViewRowAnimation)animation;
- (CELL)indexPathToCell:(NSIndexPath*)indexPath;
- (void)processRowSelection:(NSIndexPath*)indexPath;

/**** observer handlers ****/
- (void)handleLoginSucceeded:(NSNotification*)notification;
- (void)handleLogoutSucceeded:(NSNotification*)notification;
@end


@implementation FGMoreController
#pragma mark - UIView lifecycle and friends
- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	// only need to refresh if we are just arriving. Otherwise we are listening.
	if(self->_activeViewController==NO)
	{
		[self reloadLoginAffectedRows:UITableViewRowAnimationNone];
	}
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	if(self->_activeViewController==NO)
	{
		self->_activeViewController=YES;
		[self addObservers];
	}
}

- (void)viewDidDisappear:(BOOL)animated
{
	self->_activeViewController=[self isSelectedViewController];
	if(self->_activeViewController==NO)
	{
		[self removeObservers];
	}
	[super viewDidDisappear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSURL *url;

	[super prepareForSegue:segue sender:sender];
	if([segue.identifier isEqualToString:@"SegueTerms"])
	{
		[[segue destinationViewController] setTitle:@"Terms of Service"];
		url=[NSURL URLWithString:[FGSettings getUrlTermsAndConditions]];
		[[segue destinationViewController] setUrlRemoteRequest:[NSURLRequest requestWithURL:url]];
		url=[[NSBundle mainBundle] URLForResource:@"terms" withExtension:@"html"];
		[[segue destinationViewController] setUrlFailSafeRequest:[NSURLRequest requestWithURL:url]];
	}
	else if([segue.identifier isEqualToString:@"SeguePrivacy"])
	{
		[[segue destinationViewController] setTitle:@"Privacy Policy"];
		url=[NSURL URLWithString:[FGSettings getUrlPrivacyPolicy]];
		[[segue destinationViewController] setUrlRemoteRequest:[NSURLRequest requestWithURL:url]];
		url=[[NSBundle mainBundle] URLForResource:@"privacy" withExtension:@"html"];
		[[segue destinationViewController] setUrlFailSafeRequest:[NSURLRequest requestWithURL:url]];
	}
}


#pragma mark - private interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	[center addObserver:self selector:@selector(handleLoginSucceeded:) name:FGNotificationLoginSucceeded object:nil];
	[center addObserver:self selector:@selector(handleLogoutSucceeded:) name:FGNotificationLogoutSucceeded object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)reloadLoginAffectedRows:(UITableViewRowAnimation)animation
{
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:SECTION_ACCOUNT] withRowAnimation:animation];
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:SECTION_HELP] withRowAnimation:animation];
}

- (CELL)indexPathToCell:(NSIndexPath *)indexPath
{
	if([indexPath section]==SECTION_ACCOUNT)
	{
		if([FGLoginProxy isLoggedIn])
		{
			if([indexPath row]==0)
			{
				return CELL_USER;
			}
			else if([indexPath row]==1)
			{
				return CELL_LOGOUT;
			}
		}
		else
		{
			if([indexPath row]==0)
			{
				return CELL_REGISTER;
			}
			else if([indexPath row]==1)
			{
				return CELL_LOGIN;
			}
		}
	}
	else if([indexPath section]==SECTION_HELP)
	{
		if([indexPath row]==0)
		{
			return CELL_SETTINGS;
		}
		else if([indexPath row]==1)
		{
			return CELL_HELP;
		}
		else
		{
			return CELL_FEEDBACK;
		}
	}
	else if([indexPath section]==SECTION_LEGAL)
	{
		if([indexPath row]==0)
		{
			return CELL_TERMS;
		}
		else
		{
			return CELL_PRIVACY;
		}
	}
	else if([indexPath section]==SECTION_DEBUG)
	{
		return CELL_LOG;
	}

	NSFailWarn(@"Section %d?", [indexPath section]);
	return CELL_UNKNOWN;
}

- (void)processRowSelection:(NSIndexPath*)indexPath
{
	CELL cell=[self indexPathToCell:indexPath];
	if(cell==CELL_LOGIN)
	{
		[self performSegueWithIdentifier:@"SegueLogin" sender:self];
	}
	else if(cell==CELL_LOGOUT)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLogout object:[FGLoginProxy getLogin]];
	}
	else if(cell==CELL_REGISTER)
	{
		[self performSegueWithIdentifier:@"SegueRegister" sender:self];
	}
	else if(cell==CELL_SETTINGS)
	{
		[self performSegueWithIdentifier:@"SegueSettings" sender:self];
	}
	else if(cell==CELL_HELP)
	{
		[self performSegueWithIdentifier:@"SegueHelp" sender:self];
	}
	else if(cell==CELL_FEEDBACK)
	{
		if([FGLoginProxy isLoggedIn])
		{
			[self performSegueWithIdentifier:@"SegueFeedback" sender:self];
		}
	}
	else if(cell==CELL_TERMS)
	{
		[self performSegueWithIdentifier:@"SegueTerms" sender:self];
	}
	else if(cell==CELL_PRIVACY)
	{
		[self performSegueWithIdentifier:@"SeguePrivacy" sender:self];
	}
	else if(cell==CELL_LOG)
	{
		[self performSegueWithIdentifier:@"SegueLog" sender:self];
	}
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#if defined(LOG)
	return SECTION_COUNT;
#else
    return SECTION_COUNT-1;
#endif
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	switch(section)
	{
		case SECTION_ACCOUNT:
		{
			return 2;
		}
		case SECTION_LEGAL:
		{
			return 2;
		}
		case SECTION_HELP:
		{
			return 3;
		}
	}
	NSAssert(section==SECTION_DEBUG, @"Section %d?", section);
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	switch([self indexPathToCell:indexPath])
	{
		case CELL_USER:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Label" forIndexPath:indexPath];
			[cell.textLabel setText:[[FGLoginProxy getLogin] username]];
			break;
		}
		case CELL_LOGIN:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
			[cell.textLabel setText:@"Login"];
			break;
		}
		case CELL_LOGOUT:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
			[cell.textLabel setText:@"Logout"];
			break;
		}
		case CELL_REGISTER:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
			[cell.textLabel setText:@"Register"];
			break;
		}
		case CELL_SETTINGS:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Disclosure" forIndexPath:indexPath];
			[cell.textLabel setText:@"Settings"];
			break;
		}
		case CELL_HELP:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Disclosure" forIndexPath:indexPath];
			[cell.textLabel setText:@"Help"];
			break;
		}
		case CELL_FEEDBACK:
		{
			if([FGLoginProxy isLoggedIn])
			{
				cell=[self.tableView dequeueReusableCellWithIdentifier:@"Disclosure" forIndexPath:indexPath];
			}
			else
			{
				cell=[self.tableView dequeueReusableCellWithIdentifier:@"Disclosure-Disabled" forIndexPath:indexPath];
			}
			[cell.textLabel setText:@"Feedback"];
			break;
		}
		case CELL_TERMS:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Disclosure" forIndexPath:indexPath];
			[cell.textLabel setText:@"Terms of Service"];
			break;
		}
		case CELL_PRIVACY:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Disclosure" forIndexPath:indexPath];
			[cell.textLabel setText:@"Privacy Policy"];
			break;
		}
		case CELL_LOG:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
			[cell.textLabel setText:@"Log"];
			break;
		}
		case CELL_UNKNOWN:
		{
			cell=[self.tableView dequeueReusableCellWithIdentifier:@"Label" forIndexPath:indexPath];
			[cell.textLabel setText:@"Whoops"];
			break;
		}
	}
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self processRowSelection:indexPath];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[self processRowSelection:indexPath];
}


#pragma mark - observer handlers
- (void)handleLoginSucceeded:(NSNotification *)notification
{
	[self reloadLoginAffectedRows:UITableViewRowAnimationAutomatic];
}

- (void)handleLogoutSucceeded:(NSNotification *)notification
{
	[self reloadLoginAffectedRows:UITableViewRowAnimationAutomatic];
}


@end
