//
//  FGProfileVO.h
//  Goggles
//
//  Created by Curtis Elsasser on 11/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGMessageVO;

@interface FGProfileVO : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * imageUrlFull;
@property (nonatomic, retain) NSString * imageUrlThumb;
@property (nonatomic, retain) NSNumber * loggedIn;
@property (nonatomic, retain) NSNumber * login;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *messages;
@end

@interface FGProfileVO (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(FGMessageVO *)value;
- (void)removeMessagesObject:(FGMessageVO *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

@end
