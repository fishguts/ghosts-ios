//
//  UIViewController+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (FG)

- (BOOL)isInContainingController;
- (BOOL)isInNavigationController;
- (BOOL)isInTabBarController;
- (BOOL)isSelectedViewController;
/**
 * If this guy is a navigation controller then it returns the topmost or
 * currently selected view controller. Otherwise it returns 'self'
 **/
- (UIViewController*)activeViewController;

/**
 * Tries to determine whether we are currently presented modally or
 * as a push in a navigation controller and does the right thing.
 **/
- (void)smartDismissViewControllerAnimated:(BOOL)animated;

@end
