//
//  GLCDrawableCircle.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "GLCDrawableCircle.h"
#import "GLCContext.h"
#import <vector>


@interface GLCDrawableCircle2D ()
{
	BOOL _rebuildBuffers;
	std::vector<GLKVector2> _vertexesBorder;
	std::vector<GLKVector2> _vertexesFill;
}

/**** Private Interface ****/
- (void)rebuildBuffers;

@end


@implementation GLCDrawableCircle2D
#pragma mark - Public Properties
@synthesize origin=_origin;
@synthesize radius=_radius;
@synthesize divisions=_divisions;
@synthesize lineWidth=_lineWidth;
@synthesize borderColor=_borderColor;
@synthesize fillColor=_fillColor;
@synthesize isDrawBorder=_isDrawBorder;
@synthesize isDrawFill=_isDrawFill;

- (void)setOrigin:(GLKVector2)origin
{
	if(GLKVector2AllEqualToVector2(self->_origin, origin)==false)
	{
		self->_origin=origin;
		self->_rebuildBuffers=YES;
	}
}

- (void)setDivisions:(GLint)divisions
{
	if(self->_divisions!=divisions)
	{
		self->_divisions=divisions;
		self->_rebuildBuffers=YES;
	}
}

- (void)setRadius:(GLfloat)radius
{
	if(self->_radius!=radius)
	{
		self->_radius=radius;
		self->_rebuildBuffers=YES;
	}
}

- (void)setIsDrawFill:(BOOL)isDrawFill
{
	if(self->_isDrawFill!=isDrawFill)
	{
		self->_isDrawFill=isDrawFill;
		self->_rebuildBuffers=YES;
	}
}

- (void)setIsDrawBorder:(BOOL)isDrawBorder
{
	if(self->_isDrawBorder!=isDrawBorder)
	{
		self->_isDrawBorder=isDrawBorder;
		self->_rebuildBuffers=YES;
	}
}

#pragma mark - Public Interface
- (id)init
{
	// Note: this guy is here to let them know it's okay to have an unitialized instance
	return [super init];
}

- (id)initWithOrigin:(GLKVector2)origin radius:(GLfloat)radius divisions:(GLint)divisions fillColor:(const GLKVector4*)fillColor
{
	return [self initWithOrigin:origin radius:radius divisions:divisions fillColor:fillColor borderColor:nil lineWidth:0];
}

- (id)initWithOrigin:(GLKVector2)origin radius:(GLfloat)radius divisions:(GLint)divisions fillColor:(const GLKVector4*)fillColor borderColor:(const GLKVector4*)borderColor lineWidth:(GLfloat)lineWidth
{
	self=[super init];
	[self setOrigin:origin];
	[self setRadius:radius];
	[self setDivisions:divisions];
	[self setLineWidth:lineWidth];
	if(fillColor!=nil)
	{
		[self setFillColor:*fillColor];
		[self setIsDrawFill:YES];
	}
	if(borderColor!=nil)
	{
		[self setBorderColor:*borderColor];
		[self setIsDrawBorder:YES];
	}
	[self commit];
	
	return self;
}

- (void)commit
{
	if(self->_rebuildBuffers)
	{
		[self rebuildBuffers];
	}
}

- (void)draw:(GLCContext*)context
{
	NSAssertWarn(self->_rebuildBuffers==NO, @"Dirty?");
	NSAssertWarn((self->_isDrawBorder==NO) || (self->_lineWidth>0), @"GL_INVALID_VALUE");

	[context enableClientState:GL_VERTEX_ARRAY];
	[context disableClientState:GL_COLOR_ARRAY];
	if(self->_isDrawFill)
	{
		[context setColor4f:self->_fillColor];
		glVertexPointer(2, GL_FLOAT, 0, self->_vertexesFill.data());
		glDrawArrays(GL_TRIANGLE_FAN, 0, self->_vertexesFill.size());
	}
	
	if(self->_isDrawBorder)
	{
		[context setLineWidth:self->_lineWidth];
		[context setColor4f:self->_borderColor];
		glVertexPointer(2, GL_FLOAT, 0, self->_vertexesBorder.data());
		glDrawArrays(GL_LINE_LOOP, 0, self->_vertexesBorder.size());
	}
}

#pragma mark - Private Interface
- (void)rebuildBuffers
{
	self->_rebuildBuffers=NO;
	self->_vertexesFill.clear();
	self->_vertexesBorder.clear();
	
	if(self->_isDrawBorder || self->_isDrawFill)
	{
		if(self->_isDrawFill)
		{
			// start of the fan is the origin
			self->_vertexesFill.push_back(self->_origin);
		}

		for(int index=0; index<self->_divisions; index++)
		{
			const double radians=(index*M_PI*2)/self->_divisions;
			const GLKVector2 vertex=GLKVector2Make(self->_origin.x+self->_radius*cos(radians),
												   self->_origin.y+self->_radius*sin(radians));
			if(self->_isDrawBorder)
			{
				self->_vertexesBorder.push_back(vertex);
			}
			if(self->_isDrawFill)
			{
				self->_vertexesFill.push_back(vertex);
			}
		}

		if(self->_isDrawFill)
		{
			// close the circle with final fan "blade"
			self->_vertexesFill.push_back(self->_vertexesFill[1]);
		}
	}
}

@end
