//
//  UIViewController+FGMessageView.m
//  Goggles
//
//  Created by Curtis Elsasser on 3/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "UIViewController+MessageEditor.h"
#import "FGMessageVO.h"
#import "FGConstants.h"


@implementation UIViewController (MessageEditor)
- (NSString*)messageSelectionToSegue:(FGMessageVO*)message
{
	return (message.status==FGMessageStatusError)
		? @"SeguePost"
		: @"SegueView";
}

@end
