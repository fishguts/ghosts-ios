//
//  FGMotionFilterThreshold.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/30/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMotionFilterThreshold.h"

@interface FGMotionFilterThreshold()
{
	BOOL _initialized;
	GLKVector3 _lastFiltered;
}

@end

@implementation FGMotionFilterThreshold
- (id)initWithThreshold:(GLfloat)threshold
{
	return [self initWithThresholdX:threshold thresholdY:threshold thresholdZ:threshold];
}

- (id)initWithThresholdX:(GLfloat)x thresholdY:(GLfloat)y thresholdZ:(GLfloat)z
{
	self=[super init];
	self->_thresholdX=x;
	self->_thresholdY=y;
	self->_thresholdZ=z;
	return self;
}

- (void)reset:(BOOL)cascade
{
	self->_initialized=NO;
}

- (void)motionFilter:(FGMotionFilterBase *)filter acceptOrientation:(GLKVector3)orientation
{
	if(self->_initialized==NO)
	{
		self->_initialized=YES;
		self->_lastFiltered=orientation;
		[self.delegate motionFilter:self acceptOrientation:orientation];
	}
	else
	{
		// todo: do filtering
		const GLKVector3 delta=GLKVector3Subtract(orientation, self->_lastFiltered);
		if((fabsf(delta.x)>=self->_thresholdX)
		   || (fabsf(delta.y)>=self->_thresholdY)
		   || (fabsf(delta.z)>=self->_thresholdZ))
		{
			self->_lastFiltered=orientation;
			[self.delegate motionFilter:self acceptOrientation:orientation];
		}
	}
}

@end
