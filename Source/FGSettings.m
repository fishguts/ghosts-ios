//
//  FGConstants.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSettings.h"
#import "FGSettingsVO.h"
#import "FGExpirationVO.h"
#import "NSString+FG.h"


#if !defined(DEBUG) || (DEBUG==0)
	// release
	#define SERVER_REMOTE 1
#else
	#if defined(PRODUCTION)
		#error "DEBUG in production?"
	#endif

	#if TARGET_IPHONE_SIMULATOR
		#define SERVER_REMOTE 0
	#else
		/* Need to figure out how to loop back/proxy traffic to local server if setting to 0 */
		#define SERVER_REMOTE 1
	#endif
#endif


@implementation FGSettings
static FGSettingsVO *settingsVO;
static const NSTimeInterval messageQueryIntervalMinimum=15;
static const NSTimeInterval messageQueryIntervalMaximum=60;
static const double radiusHorizontal=75;
static const double radiusVertical=75;


#pragma mark - Behavior Properties
+ (void)settingsWithVO:(FGSettingsVO*)settings
{
	settingsVO=settings;
}

+ (NSTimeInterval)getMessageQueryIntervalMinimum
{
	return (settingsVO!=nil) ? [settingsVO.queryIntervalMin intValue] : messageQueryIntervalMinimum;
}

+ (NSTimeInterval)getMessageQueryIntervalMaximum
{
	return (settingsVO!=nil) ? [settingsVO.queryIntervalMax intValue] : messageQueryIntervalMaximum;
}

+ (NSTimeInterval)getMessageQueryIntervalForApplicationResume
{
	// allow a brief moment to allow user to get bearings
	return 2.0;
}

+ (double)getRadiusHorizontal
{
	return (settingsVO!=nil) ? [settingsVO.radiusHorizontal doubleValue] : radiusHorizontal;
}

+ (double)getRadiusVertical
{
	return (settingsVO!=nil) ? [settingsVO.radiusVertical doubleValue] : radiusVertical;
}

+ (NSString*)getServerVersion
{
	return (settingsVO!=nil) ? settingsVO.serverVersion : @"Unknown";
}

#pragma mark - Locally managed Properties
+ (BOOL)getShowIntroduction
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"showIntroduction" defaultValue:YES];
}

+ (void)setShowIntroduction:(BOOL)value
{
	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
	[defaults setBool:value forKey:@"showIntroduction"];
	[defaults synchronize];
}

+ (BOOL)getRetrieveWelcomeMessages
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"retrieveWelcomeMessages" defaultValue:YES];
}

+ (void)setRetrieveWelcomeMessages:(BOOL)value
{
	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
	[defaults setBool:value forKey:@"retrieveWelcomeMessages"];
	[defaults synchronize];
}

+ (FGListSortType)getListSortMethod
{
	return (FGListSortType)[[NSUserDefaults standardUserDefaults] integerForKey:@"listSortMethod" defaultValue:FGListSortRank];
}

+ (void)setListSortMethod:(FGListSortType)type
{
	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
	[defaults setInteger:type forKey:@"listSortMethod"];
	[defaults synchronize];
}

+ (NSArray*)getExpirationObjects
{
	return [FGSettings getExpirationObjects:[NSDate date]];
}

+ (NSArray*)getExpirationObjects:(NSDate*)date;
{
	const NSTimeInterval secondsToHour=(60*60)-fmod([date timeIntervalSince1970], 60*60);
	NSDate *dateNextHour=[date dateByAddingTimeInterval:secondsToHour];
	return [NSArray arrayWithObjects:
			[FGExpirationVO expirationWithDuration:0 id:@"None" text:@"None"],
			[FGExpirationVO expirationWithDeadline:dateNextHour id:@"Hour" text:[NSString formatExpirationTime:dateNextHour]],
			[FGExpirationVO expirationWithDuration:60*60*24 id:@"Day" text:@"1 Day"],
			[FGExpirationVO expirationWithDuration:60*60*24*7 id:@"Week" text:@"1 Week"],
			[FGExpirationVO expirationWithDuration:60*60*24*31 id:@"Month" text:@"1 Month"],
			[FGExpirationVO expirationWithDuration:60*60*24*365 id:@"Year" text:@"1 Year"],
			nil];
}

+ (FGExpirationVO*)getExpirationDefault:(NSArray*)expirationObjects;
{
	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
	NSString *expirationId=[defaults stringForKey:@"expirationDefault" defaultValue:@"None"];
	const int index=[expirationObjects indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
		if([[obj id] isEqualToString:expirationId])
		{
			*stop=YES;
			return YES;
		}
		return NO;
	}];
	NSAssertWarn(index!=NSNotFound, @"Default not found");
	return [expirationObjects objectAtIndex:((index!=NSNotFound) ? index : 0)];
	
}

+ (void)setExpirationDefault:(FGExpirationVO*)expiration;
{
	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
	[defaults setValue:expiration.id forKey:@"expirationDefault"];
	[defaults synchronize];
}


#pragma mark - Server communication Properties
+ (NSString*)getServerHost:(BOOL)secure
{
#if SERVER_REMOTE
	// todo: when routing to https the server rejects the request because there is no certificate.
	// For now routing to http.  Change to https once things are sorted out server side.
	#if defined(PRODUCTION)
		return (secure)
			? @"http://goggles.xraymen.com"
			: @"http://goggles.xraymen.com";
	#else
		return (secure)
			? @"http://ghosts.furrylab.com"
			: @"http://ghosts.furrylab.com";
	#endif
#else
	return @"http://localhost:8000";
#endif
}

+ (NSString*)getUrlGetSettings
{
	return [NSString stringWithFormat:@"%@/settings/get/json", [FGSettings getServerHost:NO]];
}
+ (NSString*)getUrlGetMessages
{
	return [NSString stringWithFormat:@"%@/messages/get/json", [FGSettings getServerHost:NO]];
}
+ (NSString*)getUrlPostMessage
{
	return [NSString stringWithFormat:@"%@/messages/put/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlRateMessage
{
	return [NSString stringWithFormat:@"%@/messages/rate/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlFlagMessage
{
	return [NSString stringWithFormat:@"%@/messages/flag/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlDeleteMessage
{
	return [NSString stringWithFormat:@"%@/messages/delete/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlLoginUser
{
	return [NSString stringWithFormat:@"%@/profile/login/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlLogoutUser
{
	return [NSString stringWithFormat:@"%@/profile/logout/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlRegisterUser
{
	return [NSString stringWithFormat:@"%@/profile/register/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlSendFeedback
{
	return [NSString stringWithFormat:@"%@/support/feedback/json", [FGSettings getServerHost:YES]];
}
+ (NSString*)getUrlPrivacyPolicy
{
	return [NSString stringWithFormat:@"%@/static/html/privacy.html", [FGSettings getServerHost:NO]];
}
+ (NSString*)getUrlTermsAndConditions
{
	return [NSString stringWithFormat:@"%@/static/html/terms.html", [FGSettings getServerHost:NO]];
}


@end