//
//  FGGhostProxy.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageProxy.h"
#import "FGModelQueries.h"
#import "FGMessageVO.h"
#import "FGCoordinateGrid.h"
#import "FGConstants.h"


@implementation FGMessageProxy
+ (NSArray*)getMessages
{
	return [FGModelQueries fetchMessages];
}

+ (NSArray*)getMessagesWithStatus:(NSString*)status
{
	return [FGModelQueries fetchMessagesWithStatus:status];
}


+ (FGCoordinateGrid*)getMessagesCoordinateGrid
{
	NSArray *messages=[FGMessageProxy getMessages];
	FGCoordinateGrid *grid=[[FGCoordinateGrid alloc] init];

	for(FGMessageVO *message in messages)
	{
		[grid addCoordinate:CLLocationCoordinate2DMake([message.latitude doubleValue], [message.longitude doubleValue])];
	}

	return grid;
}

+ (void)setMessage:(FGMessageVO*)message status:(NSString*)status
{
	[FGMessageProxy setMessage:message status:status errorText:nil];
}

+ (void)setMessage:(FGMessageVO*)message status:(NSString*)status errorText:(NSString*)error
{
	if(message.status!=status)
	{
		message.status=status;
		message.error=error;
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationDBMessageStatusChanged object:message];
	}
}

@end

