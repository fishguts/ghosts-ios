//
//  main.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FGAppDelegate.h"
int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([FGAppDelegate class]));
	}
}
