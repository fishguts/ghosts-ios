//
//  FGIntroductionController.h
//  Goggles
//
//  Created by Curtis Elsasser on 1/12/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"


@interface FGIntroductionController : UIViewController
/**** Public Interface ****/
@property (nonatomic, weak) id<FGModalDelegate> delegate;

@end
