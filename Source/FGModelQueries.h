//
//  FGModelViews.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGProfileVO;
@class FGProfileVO;
@class FGMessageVO;
@class FGSettingsVO;


@interface FGModelQueries : FGStaticClass

/**** queries ****/
+ (NSArray*)fetchProfiles;
+ (NSArray*)fetchLogins;
+ (FGProfileVO*)fetchLogin;
+ (FGProfileVO*)fetchProfileByName:(NSString*)username;

+ (NSArray*)fetchMessages;
+ (NSArray*)fetchMessagesWithStatus:(NSString*)status;
+ (FGMessageVO*)fetchMessageById:(NSNumber*)id;

+ (NSArray*)fetchSettings;
+ (FGSettingsVO*)fetchSettingByName:(NSString*)name;

@end
