//
//  FGLocationFilter3D.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLocationFilterBase.h"


/**
 * FGLocationFilterMajor - filters out all but major location changes and first/post-reset locations.
 */
@interface FGLocationFilterMajor : FGLocationFilterBase

@end
