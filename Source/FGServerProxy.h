//
//  FGServerProxy.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * Mediates between requests (in the form of notifications) and our network services.
 **/
@interface FGServerProxy : NSObject <FGSingleton>

/**** singleton instance ****/
+ (FGServerProxy*)instance;

@end
