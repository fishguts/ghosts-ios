//
//  FGResponseVO.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStatusVO.h"

@implementation FGStatusVO
@synthesize code;
@synthesize text;

+ (FGStatusVO*)statusFromText:(NSString*)text
{
	return [FGStatusVO statusFromText:text code:@"internal"];
}

+ (FGStatusVO*)statusFromText:(NSString*)text code:(NSString*)code
{
	FGStatusVO *response=[[FGStatusVO alloc] init];
	response.text=text;
	response.code=code;
	return response;
}

- (NSString*)description
{
	return [NSString stringWithFormat:@"code=%@, text=%@", self.code, self.text];
}

- (BOOL)isInternal
{
	return ([self.code isEqualToString:@"internal"]);
}

@end
