//
//  FGLocationFilterAccuracy.m
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/12/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import "FGLocationFilterAccuracy.h"

typedef enum
{
	ACCURACY_PASSED,
	ACCURACY_FAILED,
	ACCURACY_IMPROVED,
} FGAccuracyState;

@interface FGLocationFilterAccuracy ()
{
	CLLocation *_locationLast;
	FGLocationChangeType _changeTypeLast;

	FGAccuracyState _accuracyState;
	NSTimer *_timerWait;
}

@end

@implementation FGLocationFilterAccuracy
#pragma mark - Public Interface
- (id)init
{
	return [self initWithWaitInterval:0.0 HorizontalThreshold:50.0 verticalThreshold:50.0];
}

- (id)initWithThreshold:(CLLocationAccuracy)threshold
{
	return [self initWithWaitInterval:0.0 HorizontalThreshold:threshold verticalThreshold:threshold];
}

- (id)initWithHorizontalThreshold:(CLLocationAccuracy)horizontal verticalThreshold:(CLLocationAccuracy)vertical
{
	return [self initWithWaitInterval:0.0 HorizontalThreshold:horizontal verticalThreshold:vertical];
}

- (id)initWithWaitInterval:(NSTimeInterval)waitInterval Threshold:(CLLocationAccuracy)threshold
{
	return [self initWithWaitInterval:waitInterval HorizontalThreshold:threshold verticalThreshold:threshold];
}

- (id)initWithWaitInterval:(NSTimeInterval)waitInterval HorizontalThreshold:(CLLocationAccuracy)horizontal verticalThreshold:(CLLocationAccuracy)vertical
{
	self=[super init];
	self->_thresholdHorizontal=horizontal;
	self->_thresholdVertical=vertical;
	self->_intervalWait=waitInterval;
	return self;
}

- (void)dispose
{
	[self reset:NO];
	[super dispose];
}

- (void)reset:(BOOL)cascade
{
	[super reset:cascade];
	[self->_timerWait invalidate];
	self->_timerWait=nil;
	self->_locationLast=nil;
}

- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	self->_changeTypeLast=changeType;

	if((self->_locationLast==nil)
	   || ((location.horizontalAccuracy<=self->_thresholdHorizontal)
		   && (location.verticalAccuracy<=self->_thresholdVertical)))
	{
		[self->_timerWait invalidate]; self->_timerWait=nil;
		self->_locationLast=location;
		self->_accuracyState=ACCURACY_PASSED;
		[self.delegate locationFilter:self acceptLocation:location changeType:changeType];
	}
	else if(self->_intervalWait<=0.0)
	{
		// Scenarios:
		//	1. our first tick passed but was wildy inaccurate. See if this guy is better
		if([self attemptLocationImprovement:location])
		{
			[self.delegate locationFilter:self acceptLocation:self->_locationLast changeType:self->_changeTypeLast];
		}
	}
	else
	{
		// Scenarios:
		//	1. this is our first accuracy check failure since our last location
		//	2. horizontal - this location is more accurate than our last failure
		//	3. vertical - this location is more accurate than our last failure

		// check for scenario 1.
		if(self->_accuracyState==ACCURACY_PASSED)
		{
			self->_locationLast=location;
			self->_accuracyState=ACCURACY_FAILED;
		}
		else
		{
			// check for 2 and 3
			[self attemptLocationImprovement:location];
		}

		if(self->_timerWait==nil)
		{
			self->_timerWait=[NSTimer scheduledTimerWithTimeInterval:self->_intervalWait target:self selector:@selector(handleTimerTick:) userInfo:nil repeats:NO];
		}
	}
}

- (BOOL)attemptLocationImprovement:(CLLocation*)location
{
	if(location.horizontalAccuracy<self->_locationLast.horizontalAccuracy)
	{
		if(location.verticalAccuracy>self->_locationLast.verticalAccuracy)
		{
			location=[[CLLocation alloc] initWithCoordinate:location.coordinate
												   altitude:self->_locationLast.altitude
										 horizontalAccuracy:location.horizontalAccuracy
										   verticalAccuracy:self->_locationLast.verticalAccuracy
													 course:location.course
													  speed:location.speed
												  timestamp:location.timestamp];
		}
		self->_locationLast=location;
		self->_accuracyState=ACCURACY_IMPROVED;
		return YES;
	}
	else if(location.verticalAccuracy<self->_locationLast.verticalAccuracy)
	{
		location=[[CLLocation alloc] initWithCoordinate:self->_locationLast.coordinate
											   altitude:location.altitude
									 horizontalAccuracy:self->_locationLast.horizontalAccuracy
									   verticalAccuracy:location.verticalAccuracy
												 course:location.course
												  speed:location.speed
											  timestamp:location.timestamp];
		self->_locationLast=location;
		self->_accuracyState=ACCURACY_IMPROVED;
		return YES;
	}
	return NO;
}

#pragma mark - Observers
- (void)handleTimerTick:(NSTimer*)timer
{
	self->_timerWait=nil;
	// we flag this as passed to force it back through interval processing again and so that it picks
	// up the next location issued otherwise we run the risk of sitting on a location
	self->_accuracyState=ACCURACY_PASSED;
	[self.delegate locationFilter:self acceptLocation:self->_locationLast changeType:self->_changeTypeLast];
}

@end
