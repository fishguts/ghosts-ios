//
//  FGLoginProxy.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/18/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLoginProxy.h"
#import "FGModelQueries.h"
#import "FGMessageVO.h"
#import "FGConstants.h"
#import "NSNumber+FG.h"

@implementation FGLoginProxy
+ (BOOL)isLoggedIn
{
	FGProfileVO *login=[FGLoginProxy getLogin];
	return [NSNumber valueAsBool:login.loggedIn dfault:NO];
}

+ (FGProfileVO*)getLogin
{
	return [FGModelQueries fetchLogin];
}

+ (void)setLogin:(FGProfileVO*)profile
{
	// remove login status from all except our profile
	NSArray *logins=[FGModelQueries fetchLogins];
	for(FGProfileVO *login in logins)
	{
		if([login isEqual:profile]==NO)
		{
			[login setLogin:nil];
			[login setLoggedIn:nil];
		}
	}
	[profile setLogin:[NSNumber numberWithBool:YES]];
	
}

+ (BOOL)loginIfPossible
{
	FGProfileVO *login=[FGLoginProxy getLogin];
	if((login!=nil)
	   && login.loggedIn)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLogin object:login];
		return YES;
	}
	return NO;
}

+ (BOOL)logoutIfPossible
{
	if([FGLoginProxy isLoggedIn])
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLogout object:[FGLoginProxy getLogin]];
		return YES;
	}
	return NO;
}

+ (BOOL)canLoginDeleteMessage:(FGMessageVO*)message
{
	FGProfileVO *login=[FGLoginProxy getLogin];
	// intentionally not looking for loggedIn. We will prompt them.
	if(login)
	{
		if([login isEqual:message.owner])
		{
			return YES;
		}
	}
	return NO;
}


@end
