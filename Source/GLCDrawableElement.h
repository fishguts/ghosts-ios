//
//  GLCDrawableElement.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

@class GLCContext;

@protocol GLCDrawableElement <NSObject>

/**** <#Public Interface#> ****/
- (void)commit;
- (void)draw:(GLCContext*)context;

@end
