//
//  FGMessageTextureView.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageTextureView.h"

static const CGFloat BORDER_WIDTH=1.0;
static const CGFloat BORDER_RADIUS=10.0;


@interface FGMessageTextureView()
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UILabel *labelUser;
@end


@implementation FGMessageTextureView
@synthesize labelMessage;
@synthesize labelUser;

/**** public interface ****/
- (void)setUserText:(NSString*)text
{
	[self.labelUser setText:text];
}

- (void)setMessageText:(NSString*)text
{
	[self.labelMessage setText:text];
	// little trick to get him to vertically align
	[self.labelMessage sizeToFit];
}

- (void)drawRect:(CGRect)rect
{
	CGContextRef context=UIGraphicsGetCurrentContext();
	const CGRect bounds=CGRectInset(self.bounds, BORDER_WIDTH, BORDER_WIDTH);
	UIBezierPath *borderPath=[UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:BORDER_RADIUS];

	[[UIColor whiteColor] setFill];
	[[UIColor darkGrayColor] setStroke];

	CGContextAddPath(context, [borderPath CGPath]);
	CGContextDrawPath(context, kCGPathFillStroke);
}

@end
