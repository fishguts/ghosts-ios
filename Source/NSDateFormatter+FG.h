//
//  NSDateFormatter+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (FG)
/**** public interface ****/
+ (NSString*)stringFromDate:(NSDate*)date withFormat:(NSString*)format;

@end
