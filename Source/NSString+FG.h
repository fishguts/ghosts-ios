//
//  NSString+FGFormat.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGMessageVO;

@interface NSString (FG)
/**** public interface: general ****/
+ (NSString*)formatAlert:(NSNotification*)notification includeTimestamp:(BOOL)includeTimestamp;
+ (NSString*)formatAlert:(NSNotification*)notification includeTimestamp:(BOOL)includeTimestamp suffix:(NSString*)suffix;

/**** public interface: specific ****/
+ (NSString*)formatExpirationTime:(NSDate*)date;
+ (NSString*)formatMessageDate:(NSDate*)date shortStyle:(BOOL)shortStyle;
+ (NSString*)formatMessageUserWithDate:(FGMessageVO*)message shortStyle:(BOOL)shortStyle;
+ (NSString*)formatRatingCount:(NSNumber*)count zeroValue:(NSString*)zero;

@end
