//
//  GLCExt.h
//  OpenGLMixed
//
//  Created by Curtis Elsasser on 10/1/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

@class GLCContext;

@interface GLCExt : NSObject

/**** Public Interface - drawing ****/
+ (void)drawPoint:(GLCContext*)context origin:(GLKVector2)origin radiusInPixels:(GLfloat)radius color:(GLKVector4)color;

@end
