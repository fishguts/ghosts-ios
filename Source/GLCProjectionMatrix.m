//
//  GLCOrthoMatrix.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "GLCProjectionMatrix.h"


@implementation GLCOrthoMatrix
#pragma mark - Properties
- (GLfloat)left			{ return self->_left; }
- (GLfloat)right		{ return self->_right; }
- (GLfloat)top			{ return self->_top; }
- (GLfloat)bottom		{ return self->_bottom; }
- (GLfloat)near			{ return self->_near; }
- (GLfloat)far			{ return self->_far; }

#pragma mark - Public Interface
- (id)initWithLeft:(GLfloat)left right:(GLfloat)right bottom:(GLfloat)bottom top:(GLfloat)top near:(GLfloat)near far:(GLfloat)far
{
	NSAssert(left!=right && top!=bottom && near!=far, @"GL_INVALID_VALUE");
	
	self=[super init];
	self->_left=left;
	self->_right=right;
	self->_top=top;
	self->_bottom=bottom;
	self->_near=near;
	self->_far=far;
	
	return self;
}
	
- (id)initByAspectRatioWithWidth:(GLfloat)width near:(GLfloat)near far:(GLfloat)far
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	const GLfloat clipX=width/2;
	const GLfloat clipY=(clipX*buffer[3])/buffer[2];

	return [self initWithLeft:-clipX right:clipX bottom:-clipY top:clipY near:near far:far];
}

- (id)initByAspectRatioWithHeight:(GLfloat)height near:(GLfloat)near far:(GLfloat)far
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	const GLfloat clipY=height/2;
	const GLfloat clipX=(height*buffer[2])/buffer[3];
	
	return [self initWithLeft:-clipX right:clipX bottom:-clipY top:clipY near:near far:far];
}

- (void)installMatrix
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrthof(self->_left, self->_right, self->_bottom, self->_top, self->_near, self->_far);
}

- (GLfloat)glWidthToPoints:(GLfloat)glWidth depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return buffer[2]*glWidth/(self->_right-self->_left);
}

- (GLfloat)glHeightToPoints:(GLfloat)glHeight depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return buffer[3]*glHeight/(self->_top-self->_bottom);
}

- (GLfloat)pointWidthToGL:(GLfloat)pointWidth depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return (self->_right-self->_left)*pointWidth/buffer[2];
}

- (GLfloat)pointHeightToGL:(GLfloat)pointHeight depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return (self->_top-self->_bottom)*pointHeight/buffer[3];
}


// todo: these guys should be covered by matrix operations
- (GLKVector2)glToPoint:(GLKVector3)coordinate
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return GLKVector2Make(buffer[0]+buffer[2]*coordinate.x/(self->_right-self->_left), 
						  buffer[1]+buffer[3]*coordinate.y/(self->_top-self->_bottom));
}

- (GLKVector3)pointToGL:(GLKVector2)point depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return GLKVector3Make(self->_left+(self->_right-self->_left)*(point.x-buffer[0])/buffer[2],
						  self->_bottom+(self->_top-self->_bottom)*(point.y-buffer[1])/buffer[3],
						  self->_near);
}

@end


@implementation GLCFrustumMatrix
#pragma mark - Properties
- (GLfloat)left			{ return self->_left; }
- (GLfloat)right		{ return self->_right; }
- (GLfloat)top			{ return self->_top; }
- (GLfloat)bottom		{ return self->_bottom; }
- (GLfloat)near			{ return self->_near; }
- (GLfloat)far			{ return self->_far; }


#pragma mark - Public Interface
- (id)initWithLeft:(GLfloat)left right:(GLfloat)right bottom:(GLfloat)bottom top:(GLfloat)top near:(GLfloat)near far:(GLfloat)far
{
	NSAssert(left!=right && top!=bottom && near!=far, @"GL_INVALID_VALUE");
	
	self=[super init];
	self->_left=left;
	self->_right=right;
	self->_top=top;
	self->_bottom=bottom;
	self->_near=near;
	self->_far=far;
	
	return self;
}

- (id)initWithFOV:(GLfloat)fov near:(GLfloat)near far:(GLfloat)far
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	// angle of frustum is defined by how close the near clipping plane is to the origin and how wide/tall it is (draw a picture)
	// thinking of frustum pointing down -Z axis: tan(FOV/2)=X/Z -> X=tan(FOV/2)*Z.
	const GLfloat clipXNear=tanf(GLKMathDegreesToRadians(fov/2))*near;
	const GLfloat clipYNear=clipXNear*(buffer[3]/buffer[2]);
	
	return [self initWithLeft:-clipXNear right:clipXNear bottom:-clipYNear top:clipYNear near:near far:far];
}

- (void)installMatrix
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustumf(self->_left, self->_right, self->_bottom, self->_top, self->_near, self->_far);
}

- (GLfloat)glWidthToPoints:(GLfloat)glWidth depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	// based on tan(angle) -> (self->_right-self->_left)/near = frustumGLWidthAtDepth/depth.
	const GLfloat frustumGLWidthAtDepth=depth*(self->_right-self->_left)/self->_near;
	return buffer[2]*glWidth/frustumGLWidthAtDepth;
}

- (GLfloat)glHeightToPoints:(GLfloat)glHeight depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	// based on tan(angle) -> (self->_top-self->_bottom)/near = frustumGLHeightAtDepth/depth.
	const GLfloat frustumGLHeightAtDepth=depth*(self->_top-self->_bottom)/self->_near;
	return buffer[3]*glHeight/frustumGLHeightAtDepth;
}

- (GLfloat)pointWidthToGL:(GLfloat)pointWidth depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	// based on tan(angle) -> buffer[2]/near = pointWidthAtDepth/depth.
	const GLfloat pointWidthAtDepth=depth*buffer[2]/self->_near;
	return (self->_right-self->_left)*pointWidth/pointWidthAtDepth;
}

- (GLfloat)pointHeightToGL:(GLfloat)pointHeight depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	// based on tan(angle) -> buffer[3]/near = pointHeightAtDepth/depth.
	const GLfloat pointHeightAtDepth=depth*buffer[3]/self->_near;
	return (self->_top-self->_bottom)*pointHeight/pointHeightAtDepth;
}

// todo: these guys should be covered by matrix operations
- (GLKVector2)glToPoint:(GLKVector3)coordinate
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return GLKVector2Make(buffer[0]+buffer[2]*coordinate.x/(self->_right-self->_left), 
						  buffer[1]+buffer[3]*coordinate.y/(self->_top-self->_bottom));
}

- (GLKVector3)pointToGL:(GLKVector2)point depth:(GLfloat)depth
{
	GLfloat buffer[4];
	glGetFloatv(GL_VIEWPORT, buffer);
	return GLKVector3Make(self->_left+(self->_right-self->_left)*(point.x-buffer[0])/buffer[2],
						  self->_bottom+(self->_top-self->_bottom)*(point.y-buffer[1])/buffer[3],
						  depth);
}

@end