//
//  FGMessageController.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/14/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FGMessageVO;

@interface FGMessagePostController : UITableViewController <FGSelectorDelegate, UIScrollViewDelegate>

/**** properties ****/
@property (nonatomic, readonly) NSString *messageText;
@property (nonatomic, weak) id <FGModalDelegate> delegate;

/**
 * For editing messages that failed to post
 **/
@property (nonatomic, strong) FGMessageVO *message;

@end
