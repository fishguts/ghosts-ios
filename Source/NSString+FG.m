//
//  NSString+FGFormat.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSString+FG.h"
#import "FGMessageVO.h"
#import "FGProfileVO.h"
#import "FGConstants.h"


@implementation NSString (FG)
static NSDateFormatter *dateFormatterExpiration;
static NSDateFormatter *dateFormatterMessageShort;
static NSDateFormatter *dateFormatterMessageLong;
static NSDateFormatter *dateFormatterAlerts;

+ (void)initialize
{
	dateFormatterExpiration=[[NSDateFormatter alloc] init];
	dateFormatterAlerts=[[NSDateFormatter alloc] init];
	dateFormatterMessageShort=[[NSDateFormatter alloc] init];
	dateFormatterMessageLong=[[NSDateFormatter alloc] init];

	[dateFormatterExpiration setDateStyle:NSDateFormatterNoStyle];
	[dateFormatterExpiration setTimeStyle:NSDateFormatterShortStyle];

	[dateFormatterMessageShort setDateStyle:NSDateFormatterShortStyle];
	[dateFormatterMessageShort setTimeStyle:NSDateFormatterShortStyle];

	[dateFormatterMessageLong setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatterMessageLong setTimeStyle:NSDateFormatterShortStyle];

	[dateFormatterAlerts setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
	[dateFormatterAlerts setDateFormat:@"yy'-'MM'-'dd' 'HH':'mm':'ss"];
}

#pragma mark - public interface: general
+ (NSString*)formatAlert:(NSNotification*)notification includeTimestamp:(BOOL)includeTimestamp
{
	return [NSString formatAlert:notification includeTimestamp:includeTimestamp suffix:@""];
}

+ (NSString*)formatAlert:(NSNotification*)notification includeTimestamp:(BOOL)includeTimestamp suffix:(NSString*)suffix
{
	NSString *severity;
	NSDate *timestamp=(includeTimestamp) ? [notification timestamp] : nil;

	if([[notification name] isEqualToString:FGNotificationError])
	{
		severity=@"Error: ";
	}
	else if([[notification name] isEqualToString:FGNotificationWarn])
	{
		severity=@"Warn: ";
	}
	else if([[notification name] isEqualToString:FGNotificationInfo])
	{
		severity=@"Info: ";
	}
	else if([[notification name] isEqualToString:FGNotificationDebug])
	{
		severity=@"Debug: ";
	}
	else
	{
		NSAssert(@"Alert type - %@", [notification name]);
	}

	 return (timestamp)
		? [NSString stringWithFormat:@"[%@] %@%@%@", [dateFormatterAlerts stringFromDate:timestamp], severity, [notification text], suffix]
		: [NSString stringWithFormat:@"%@%@%@", severity, [notification text], suffix];
}

+ (NSString*)formatExpirationTime:(NSDate*)date
{
	return [dateFormatterExpiration stringFromDate:date];
}

+ (NSString*)formatMessageDate:(NSDate*)date shortStyle:(BOOL)shortStyle
{
	return (shortStyle)
		? [dateFormatterMessageShort stringFromDate:date]
		: [dateFormatterMessageLong stringFromDate:date];
}

+ (NSString*)formatMessageUserWithDate:(FGMessageVO*)message shortStyle:(BOOL)shortStyle
{
	return [NSString stringWithFormat:@"%@ - %@", message.owner.username, [self formatMessageDate:message.created shortStyle:shortStyle]];
}

+ (NSString*)formatRatingCount:(NSNumber*)count zeroValue:(NSString*)zero
{
	int value=[count intValue];
	if(value>0)
	{
		if(value<1000)
		{
			return [NSString stringWithFormat:@"%d", value];
		}
		else if(value<1000000)
		{
			return [NSString stringWithFormat:@"%.1fk", value/1000.0];
		}
		return [NSString stringWithFormat:@"%.1fM", value/1000000.0];
	}
	return zero;
}

@end
