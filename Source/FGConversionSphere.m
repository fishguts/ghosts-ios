//
//  FGConversionGrid.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/3/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGConversionSphere.h"
#import "CLLocation+FG.h"

@implementation FGConversionSphere

/**** initialization ****/
- (id)init
{
	NSAssert(NO, @"Use initWithCoordinate");
	return nil;
}

- (id)initWithCoordinate:(CLLocation*)center radiusInMeters:(float)radius
{
	self=[super init];
	self->_center=center;
	self->_radiusMeters=radius;
	
	return self;
}

/**** interface ****/
- (BOOL)isCoordinateInGrid:(CLLocation*)location
{
	CLLocationDistance distance=[self->_center distanceIncludingAltitude:location];
	return distance<=self->_radiusMeters;	
}

- (BOOL)isCoordinateInGrid:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude altitude:(CLLocationDistance)altitude
{
	CLLocationDistance distance=[self->_center distanceFromCoordinate:latitude longitude:longitude altitude:altitude];
	return distance<=self->_radiusMeters;	
}

- (GLKVector3)coordinateToVector3:(CLLocation*)location
{
	return GLKVector3Make(0-[self->_center longitudeDeltaInMetersWithCoordinate:location],
						  0-[self->_center altitudeDeltaInMetersWithCoordinate:location],
						  [self->_center latitudeDeltaInMetersWithCoordinate:location]);
}

- (GLKVector3)coordinateToVector3:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude altitude:(CLLocationDistance)altitude
{
	return GLKVector3Make(0-[self->_center longitudeDeltaInMetersWithLongitude:longitude latitude:latitude],
						  0-[self->_center altitudeDeltaInMetersWithAltitude:altitude],
						  [self->_center latitudeDeltaInMetersWithLatitude:latitude]);
}

@end
