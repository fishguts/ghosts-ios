//
//  FLDebug.h
//  OpenGLTexture
//
//  Created by Curtis Elsasser on 9/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//


#ifndef __FLDEBUG_H
#define __FLDEBUG_H

#define NSNoOutput(...) do {} while(0)
// simple assertions with default text
#define NSAssertTrue(condition) NSAssert(condition, @"Should be TRUE")
#define NSAssertFalse(condition) NSAssert(!condition, @"Should be FALSE")
#define NSAssertNil(condition) NSAssert(condition==nil, @"Should be nil")
#define NSAssertNotNil(condition) NSAssert(condition!=nil, @"Should not be nil")


#if DEBUG
	#define NSDump(desc, ...) \
		NSLog(desc, ##__VA_ARGS__)

	#define NSDumpFL(desc, ...) \
		NSLog(@"%s %@", \
			__PRETTY_FUNCTION__, [NSString stringWithFormat:desc, ##__VA_ARGS__])

	#define NSDebug(desc, ...) \
		[[NSNotificationCenter defaultCenter] postNotification:		\
			[NSNotification debugNotificationWithObject:self text:[NSString stringWithFormat:desc, ##__VA_ARGS__]]]	

	#define NSDebugFL(desc, ...) \
		[[NSNotificationCenter defaultCenter] postNotification:		\
			[NSNotification debugNotificationWithObject:self text:[NSString stringWithFormat:@"%s %@",	\
				__PRETTY_FUNCTION__, [NSString stringWithFormat:desc, ##__VA_ARGS__]]]]

	#define NSWarn(desc, ...) \
		[[NSNotificationCenter defaultCenter] postNotification:		\
			[NSNotification warnNotificationWithObject:self text:[NSString stringWithFormat:desc, ##__VA_ARGS__]]]

	#define NSWarnFL(desc, ...) \
		[[NSNotificationCenter defaultCenter] postNotification:		\
			[NSNotification warnNotificationWithObject:self text:[NSString stringWithFormat:@"%s %@",	\
				__PRETTY_FUNCTION__, [NSString stringWithFormat:desc, ##__VA_ARGS__]]]]

	#define NSFailWarn(desc, ...) \
		[[NSNotificationCenter defaultCenter] postNotification:		\
			[NSNotification warnNotificationWithObject:self text:[NSString stringWithFormat:@"FAILED %s %@",	\
			__PRETTY_FUNCTION__, [NSString stringWithFormat:desc, ##__VA_ARGS__]]]];

	#define NSAssertWarn(condition, desc, ...) \
		if (!(condition)) {		\
			[[NSNotificationCenter defaultCenter] postNotification:		\
				[NSNotification warnNotificationWithObject:self text:[NSString stringWithFormat:@"FAILED %s %@",	\
					__PRETTY_FUNCTION__, [NSString stringWithFormat:desc, ##__VA_ARGS__]]]];	\
		}
#else
	#define NSDump(...) NSNoOutput(__VA_ARGS__)
	#define NSDumpFL(...) NSNoOutput(__VA_ARGS__)
	#define NSDebug(...) NSNoOutput(__VA_ARGS__)
	#define NSDebugFL(...) NSNoOutput(__VA_ARGS__)
	#define NSWarn(...) NSNoOutput(__VA_ARGS__)
	#define NSWarnFL(...) NSNoOutput(__VA_ARGS__)
	#define NSFailWarn(...) NSNoOutput(__VA_ARGS__)
	#define NSAssertWarn(...) NSNoOutput(__VA_ARGS__)
#endif

#if VERBOSE
	#define NSVerbose(...) NSDebug(__VA_ARGS__)
	#define NSVerboseFL(...) NSDebugFL(__VA_ARGS__)
#else
	#define NSVerbose(...) NSNoOutput(__VA_ARGS__)
	#define NSVerboseFL(...) NSNoOutput(__VA_ARGS__)
#endif

#endif
