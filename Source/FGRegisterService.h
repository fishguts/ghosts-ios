//
//  FGRegisterService.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/28/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@class FGProfileVO;


@interface FGRegisterService : FGNetworkService
{
	FGProfileVO *_profile;
}

/**** public interface ****/
- (id)initWithProfile:(FGProfileVO*)profile;

@end
