//
//  FGTypes.h
//  Gears
//
//  Created by Curtis Elsasser on 3/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//
#ifndef __FGTYPES_H
#define __FGTYPES_H

/**** forward declarations ****/
@protocol FGSelectorDelegate;


/**** types ****/
typedef enum
{
	FGViewTypeList,
	FGViewTypeMap,
	FGViewTypeCamera,
} FGViewType;

typedef enum
{
	FGListSortRank,
	FGListSortChronological,
} FGListSortType;

typedef struct
{
	float width;
	float height;
} SizeF;

typedef struct
{
	double width;
	double height;
} SizeD;

/**** protocols ****/
@protocol FGDispose <NSObject>
- (void)dispose;
@end

@protocol FGSingleton <NSObject>
- (void)startup;
- (void)shutdown;
@end

@protocol FGModalDelegate <NSObject>
- (void)modalCanceled:(id)modal;
- (void)modalComplete:(id)modal;
@end

@protocol FGSelectorInterface <NSObject>
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) id selection;
@property (nonatomic, weak) id <FGSelectorDelegate> delegate;
@end

@protocol FGSelectorDelegate <NSObject>
- (void)selectorComplete:(id)selector selection:(id)item;
- (void)selectorCanceled:(id)selector;
@end

/*** type factories ****/
static inline SizeF SizeFMake(float width, float height)
{
	SizeF size;
	size.width=width;
	size.height=height;
	return size;
}

static inline SizeD SizeDMake(double width, double height)
{
	SizeD size;
	size.width=width;
	size.height=height;
	return size;
}
#endif
