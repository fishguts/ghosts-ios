//
//  NSUserDefaults+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 1/1/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (FG)
- (NSString*)stringForKey:(NSString*)defaultName defaultValue:(NSString*)defaultValue;
- (BOOL)boolForKey:(NSString*)defaultName defaultValue:(BOOL)defaultValue;
- (NSInteger)integerForKey:(NSString*)defaultName defaultValue:(NSInteger)defaultValue;
@end
