//
//  UIDevice+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 2/1/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "UIDevice+FG.h"

@implementation UIDevice (FG)

- (NSString*)encoded
{
	return [NSString stringWithFormat:@"ios|%@|%@|%@", [self model], [self systemName], [self systemVersion]];
}

@end
