//
//  FGHelpController.h
//  Goggles
//
//  Created by Curtis Elsasser on 1/19/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGHelpController : UITableViewController
/**** Public Properties ****/
@property (nonatomic, weak) id <FGModalDelegate> delegate;

@end
