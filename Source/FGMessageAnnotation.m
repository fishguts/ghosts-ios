//
//  MessageAnnotation.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageAnnotation.h"
#import "FGMessageVO.h"
#import "FGProfileVO.h"
#import "NSString+FG.h"


@implementation FGMessageAnnotation

- (id)initWithMessage:(FGMessageVO *)message
{
	self=[super init];
	self->_message=message;
	return self;
}

- (FGMessageVO*)message
{
	return self->_message;
}

- (NSString*)title
{
	return self->_message.text;
}

- (NSString*)subtitle
{
	return [NSString formatMessageUserWithDate:self->_message shortStyle:NO];
}

- (CLLocationCoordinate2D)coordinate
{
	return CLLocationCoordinate2DMake([self->_message.latitude doubleValue], [self->_message.longitude doubleValue]);
}

- (BOOL)isEqual:(id)object
{
	if([object isKindOfClass:[FGMessageAnnotation class]])
	{
		FGMessageAnnotation *annotation=object;
		if([annotation.title isEqualToString:self.title])
		{
			if([annotation.subtitle isEqualToString:self.subtitle])
			{
				CLLocationCoordinate2D c1=[annotation coordinate];
				CLLocationCoordinate2D c2=[self coordinate];
				if(memcmp(&c1, &c2, sizeof(c1))==0)
				{
					return YES;
				}
			}
		}
	}
	return NO;
}
@end
