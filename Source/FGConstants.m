//
//  FGConstants.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGConstants.h"

#if DEBUG
const NSTimeInterval FGDefaultSplashDuration=1.25;
#else
const NSTimeInterval FGDefaultSplashDuration=1.5;
#endif
const NSTimeInterval FGDefaultWelcomeDuration=60*2;
const int FGMinUserNameLength=4;
const int FGMinPasswordLength=4;
const int FGMinMessageLength=2;
const int FGMinFeedbackLength=6;
const float FGMotionTimerInterval=1.0/20;

const float FGMessageVideoScaleMin=1.0;
const float FGMessageVideoScaleMax=15.0;
const float FGMessageVideoAltitudeCompression=0.20;

const GLKVector4 FGHUDBackgroundColor={ 1.0, 1.0, 1.0, 0.08 };
const GLKVector4 FGHUDBorderColor={ 0.8, 1.8, 1.0, 0.28 };
const GLKVector4 FGHUDCameraColor={ 1.0, 1.0, 0.0, 1.0 };
const GLKVector4 FGHUDMessageColor={ 0, 1.0, 1.0, 1.0 };

const GLKVector4 FGMessageListPostingColor={ 255.0/255.0, 248.0/255.0, 192.0/255.0, 1.0 };
const GLKVector4 FGMessageListErrorColor={ 1.0, 0.0, 0.0, 1.0 };
const GLKVector4 FGMessageListPostedColorUser={ 1.0, 1.0, 1.0, 1.0 };
const GLKVector4 FGMessageListPostedColorWelcome={ 1.0, 1.0, 1.0, 1.0 };
const GLKVector4 FGMessageListPostedColorNotification={ 226.0/255.0, 255.0/255.0, 222.0/255.0, 1.0 };
const GLKVector4 FGMessageListPostedColorAdvertisement={ 255.0/255.0, 246.0/255.0, 218.0/255.0, 1.0 };

const GLKVector4 FGMessageMapCoverageColorFill={ 0.6, 0.40, 1.0, 0.15 };
const GLKVector4 FGMessageMapCoverageColorStroke={ 0.1, 0.45, 1.0, 0.70 };

NSString *const ARC_STRING_YES=@"y";
NSString *const ARC_STRING_NO=@"n";


/**** state notifications ****/
NSString *const FGNotificationStartupConfigurationSucceeded=@"NotificationStartupConfigurationSucceeded";
NSString *const FGNotificationStartupConfigurationFailed=@"NotificationStartupConfigurationFailed";
NSString *const FGNotificationApplicationActive=@"NotificationApplicationActive";
NSString *const FGNotificationApplicationResuming=@"NotificationApplicationResuming";
NSString *const FGNotificationApplicationSuspending=@"NotificationApplicationSuspending";

/**** error notifications ****/
NSString *const FGNotificationUser=@"NotificationUser";
NSString *const FGNotificationDebug=@"NotificationDebug";
NSString *const FGNotificationInfo=@"NotificationInfo";
NSString *const FGNotificationWarn=@"NotificationWarn";
NSString *const FGNotificationError=@"NotificationError";


/**** document notifications ****/
NSString *const FGNotificationDBOpening=@"NotificationDBOpening";
NSString *const FGNotificationDBOpenSucceeded=@"NotificationDBOpenSucceded";
NSString *const FGNotificationDBOpenFailed=@"NotificationDBOpenFailed";
NSString *const FGNotificationDBSaving=@"NotificationDBSaving";
NSString *const FGNotificationDBSaveSucceeded=@"NotificationDBSaveSucceeded";
NSString *const FGNotificationDBSaveFailed=@"NotificationDBSaveFailed";
NSString *const FGNotificationDBCloseSucceeded=@"NotificationDBCloseSucceeded";
NSString *const FGNotificationDBCloseFailed=@"NotificationDBCloseFailed";


/**** model Model update notifications ****/
NSString *const FGNotificationDBMessageStatusChanged=@"NotificationDBMessageStatusChanged";
NSString *const FGNotificationDBMessageUserRatingChanged=@"NotificationDBMessageUserRatingChanged";
NSString *const FGNotificationSettingListSortChanged=@"NotificationSettingListSortChanged";

/**** model and state modification notifications ****/
NSString *const FGNotificationNetworkActivityStarting=@"NotificationNetworkActivityStarting";
NSString *const FGNotificationNetworkActivityEnding=@"NotificationNetworkActivityEnding";
NSString *const FGNotificationNetworkSending=@"NotificationNetworkSending";
NSString *const FGNotificationNetworkSendSucceeded=@"NotificationNetworkSendSucceeded";
NSString *const FGNotificationNetworkSendFailed=@"NotificationNetworkSendFailed";
NSString *const FGNotificationNetworkSendCanceled=@"NotificationNetworkSendCanceled";

NSString *const FGNotificationSettingsGet=@"NotificationSettingsGet";
NSString *const FGNotificationSettingsGetting=@"NotificationSettingsGetting";
NSString *const FGNotificationSettingsGetSucceeded=@"NotificationSettingsGetSucceeded";
NSString *const FGNotificationSettingsGetFailed=@"NotificationSettingsGetFailed";
NSString *const FGNotificationSettingsChanged=@"NotificationSettingsChanged";

NSString *const FGNotificationLogin=@"NotificationLogin";
NSString *const FGNotificationLoggingIn=@"NotificationLoggingIn";
NSString *const FGNotificationLoginSucceeded=@"NotificationLoginSucceeded";
NSString *const FGNotificationLoginFailed=@"NotificationLoginFailed";

NSString *const FGNotificationLogout=@"NotificationLogout";
NSString *const FGNotificationLoggingOut=@"NotificationLoggingOut";
NSString *const FGNotificationLogoutSucceeded=@"NotificationLogoutSucceeded";

NSString *const FGNotificationRegister=@"NotificationRegister";
NSString *const FGNotificationRegistering=@"NotificationRegistering";
NSString *const FGNotificationRegisterSucceeded=@"NotificationRegisterSucceeded";
NSString *const FGNotificationRegisterFailed=@"NotificationRegisterFailed";

NSString *const FGNotificationMessagePost=@"NotificationMessagePost";
NSString *const FGNotificationMessagePostRetry=@"NotificationMessageRetryPost";
NSString *const FGNotificationMessagePosting=@"NotificationMessagePosting";
NSString *const FGNotificationMessagePostSucceeded=@"NotificationMessagePostSucceeded";
NSString *const FGNotificationMessagePostFailed=@"NotificationMessagePostFailed";

NSString *const FGNotificationMessageRate=@"NotificationMessageRate";
NSString *const FGNotificationMessageRating=@"NotificationMessageRating";
NSString *const FGNotificationMessageRateSucceeded=@"NotificationMessageRateSucceeded";
NSString *const FGNotificationMessageRateFailed=@"NotificationMessageRateFailed";

NSString *const FGNotificationMessageFlag=@"NotificationMessageFlag";
NSString *const FGNotificationMessageFlagging=@"NotificationMessageFlagging";
NSString *const FGNotificationMessageFlagSucceeded=@"NotificationMessageFlagSucceeded";
NSString *const FGNotificationMessageFlagFailed=@"NotificationMessageFlagFailed";

NSString *const FGNotificationMessageDelete=@"NotificationMessageDelete";
NSString *const FGNotificationMessageDeleting=@"NotificationMessageDeleting";
NSString *const FGNotificationMessageDeleteSucceeded=@"NotificationMessageDeleteSucceeded";
NSString *const FGNotificationMessageDeleteFailed=@"NotificationMessageDeleteFailed";

NSString *const FGNotificationMessagesGet=@"NotificationMessagesGet";
NSString *const FGNotificationMessagesGetting=@"NotificationMessagesGetting";
NSString *const FGNotificationMessagesGetSucceeded=@"NotificationMessagesGetSucceeded";
NSString *const FGNotificationMessagesGetFailed=@"NotificationMessagesGetFailed";

NSString *const FGNotificationFeedbackPost=@"NotificationFeedbackPost";
NSString *const FGNotificationFeedbackPosting=@"NotificationFeedbackPosting";
NSString *const FGNotificationFeedbackPostSucceeded=@"NotificationFeedbackPostSucceeded";
NSString *const FGNotificationFeedbackPostFailed=@"NotificationFeedbackPostFailed";

NSString *const FGNotificationLocationRetain=@"NotificationLocationRetain";
NSString *const FGNotificationLocationRelease=@"NotificationLocationRelease";
NSString *const FGNotificationLocationChanged=@"NotificationLocationChanged";

NSString *const FGNotificationOrientationRetain=@"NotificationOrientationRetain";
NSString *const FGNotificationOrientationRelease=@"NotificationOrientationRelease";
NSString *const FGNotificationOrientationChanged=@"NotificationOrientationChanged";

#if defined(DEBUG)
/**** test notifications ****/
NSString *const FGNotificationTestMessage=@"NotificationTestMessage";
NSString *const FGNotificationTestStatus=@"NotificationTestStatus";
#endif

/**** notification user dictionary keys ****/
NSString *const FGUserKeyLocationMajor=@"major";
NSString *const FGUserKeyLocationMinor=@"minor";

/***** network statuses ****/
NSString *const FGResponseStatusOkay=@"okay";
NSString *const FGResponseStatusFail=@"fail";
NSString *const FGResponseStatusAuth=@"auth";


/**** message statuses ****/
NSString* const FGMessageStatusPost=@"post";
NSString* const FGMessageStatusPosted=@"posted";
NSString* const FGMessageStatusError=@"error";

/**** message types ****/
NSString *const FGMessageTypeUser=@"user";
NSString *const FGMessageTypeWelcome=@"welcome";
NSString *const FGMessageTypeNotification=@"notification";
NSString *const FGMessageTypeAdvertisement=@"ad";
