//
//  GLXXX.h
//  Ghosts
//
//  Created by Curtis Elsasser on 10/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <GLKit/GLKit.h>

typedef union
{
	struct { GLKVector2 tl, tr, bl, br; };
	GLKVector2 v[4];
} GLCRect2;

typedef union
{
	struct { GLKVector3 tl, tr, bl, br; };
	GLKVector3 v[4];
} GLCRect3;

typedef union
{
	struct { GLKVector4 tl, tr, bl, br; };
	GLKVector4 v[4];
} GLCRect4;

