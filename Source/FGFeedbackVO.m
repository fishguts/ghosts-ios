//
//  FGFeedbackVO.m
//  Goggles
//
//  Created by Curtis Elsasser on 12/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGFeedbackVO.h"
#import "FGProfileVO.h"

@implementation FGFeedbackVO
@synthesize text=_text;
@synthesize owner=_owner;

+ (FGFeedbackVO*)feedbackFromText:(NSString*)text owner:(FGProfileVO*)owner
{
	return [[FGFeedbackVO alloc] initWithText:text owner:owner];
}

- (id)initWithText:(NSString*)text owner:(FGProfileVO*)owner
{
	self=[super init];
	self->_text=text;
	self->_owner=owner;
	return self;
}


@end
