//
//  FGViewController.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/5/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageListController.h"
#import "FGMessageListCell.h"
#import "FGModel.h"
#import "FGBrain.h"
#import "FGMessageVO.h"
#import "FGMessageProxy.h"
#import "FGLoginProxy.h"
#import "FGSettingsProxy.h"
#import "FGConstants.h"
#import "NSString+FG.h"
#import "UIViewController+MessageEditor.h"


/**** Local Types and Defines ****/
#define INCLUDE_RATING	1


@interface FGMessageListController()
{
	BOOL _activeViewController;
	NSString *_pendingSegueId;

	FGListSortType _sortTypeCurrent;
}

/**** properties ****/
@property (nonatomic, strong) NSFetchedResultsController *resultsController;


/**** private workers ****/
- (void)addObservers;
- (void)removeObservers;

- (void)performSegueSequence:(NSString*)segue1 completionSegue:(NSString*)segue2;

- (void)buildFetchRequest;
- (void)updateFetchRequest;
- (void)refreshVisibleCells;
- (void)configureCell:(UITableViewCell*)cell;
- (void)configureCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath;
- (void)updateMessagePostStatus:(FGMessageVO*)message;
- (void)updateMessagePostStatus:(FGMessageVO*)message cell:(UITableViewCell*)cell;

/**** observers ****/
- (void)handleDBIONotification:(NSNotification*)notification;
- (void)handleMessagePosting:(NSNotification*)notification;
- (void)handleMessagePostSucceeded:(NSNotification*)notification;
- (void)handleMessagePostFailed:(NSNotification*)notification;
- (void)handleMessageRateSucceeded:(NSNotification*)notification;
- (void)handleLoginSucceeded:(NSNotification*)notification;
- (void)handleLogoutSucceeded:(NSNotification*)notification;

@end


@implementation FGMessageListController

#pragma mark - Properties
@synthesize resultsController;


#pragma mark - View lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self addObservers];
	// if the model is already opened then get him otherwise wait until he asynchronously is alive.
	if([[FGModel instance] isOpen])
	{
		[self buildFetchRequest];
	}
#if !TARGET_IPHONE_SIMULATOR
	// get rid of our debug refresh button
	[self.navigationItem setLeftBarButtonItem:nil];
#endif
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	// we don't closely monitor when then the sort method has been changed (via) settings. We just
	// make sure that upon being viewed that we are in the correct state.
	[self updateFetchRequest];
}

- (void)viewDidDisappear:(BOOL)animated
{
	// if we have been deselected then that's that
	self->_activeViewController=[self.navigationController isSelectedViewController];
	if(self->_activeViewController)
	{
		[[FGBrain instance] sleep];
	}
	[super viewDidDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	if(self->_activeViewController==NO)
	{
		self->_activeViewController=YES;
	}
	else
	{
		// Note: I had this tied into our modal notification, but some of our views are pushed so that doesn't work.
		// We are resetting the brain which means he'll go back into his wait state which will give us time to perform
		// our subesquent segue if there is one.
		[[FGBrain instance] wakeup:YES];
	}
}

#pragma mark - Private Interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	// db io notification
	[center addObserver:self selector:@selector(handleDBIONotification:) name:FGNotificationDBOpening object:nil];
	[center addObserver:self selector:@selector(handleDBIONotification:) name:FGNotificationDBOpenSucceeded object:nil];
	[center addObserver:self selector:@selector(handleDBIONotification:) name:FGNotificationDBOpenFailed object:nil];

	[center addObserver:self selector:@selector(handleMessagePosting:) name:FGNotificationMessagePosting object:nil];
	[center addObserver:self selector:@selector(handleMessagePostSucceeded:) name:FGNotificationMessagePostSucceeded object:nil];

#if INCLUDE_RATING
	[center addObserver:self selector:@selector(handleMessagePostFailed:) name:FGNotificationMessagePostFailed object:nil];
	[center addObserver:self selector:@selector(handleMessageRateSucceeded:) name:FGNotificationMessageRateSucceeded object:nil];
	[center addObserver:self selector:@selector(handleLoginSucceeded:) name:FGNotificationLoginSucceeded object:nil];
	[center addObserver:self selector:@selector(handleLogoutSucceeded:) name:FGNotificationLogoutSucceeded object:nil];
#endif
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	[super prepareForSegue:segue sender:sender];

	// prevent traffic while we are modal
	id destinationViewController=[segue.destinationViewController activeViewController];
	if([destinationViewController respondsToSelector:@selector(setDelegate:)])
	{
		[destinationViewController setDelegate:self];
	}
	if([destinationViewController respondsToSelector:@selector(setMessage:)])
	{
		if([sender isKindOfClass:[FGMessageVO class]])
		{
			[destinationViewController setMessage:sender];
		}
		else if([sender isKindOfClass:[UITableViewCell class]])
		{
			NSIndexPath *indexPath=[self.tableView	indexPathForCell:sender];
			[destinationViewController setMessage:[self.resultsController objectAtIndexPath:indexPath]];
		}
	}
}

- (void)performSegueSequence:(NSString*)segue1 completionSegue:(NSString*)segue2
{
	self->_pendingSegueId=segue2;
	[self performSegueWithIdentifier:segue1 sender:self];
}


- (void)buildFetchRequest
{
	if(self.resultsController==nil)
	{
		NSError *error;

		FGModel *model=[FGModel instance];
		const FGListSortType sortMethod=[FGSettingsProxy getListSortMethod];
		NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Message"];
		if(sortMethod==FGListSortChronological)
		{
			request.sortDescriptors=[NSArray arrayWithObjects:
									 [NSSortDescriptor sortDescriptorWithKey:@"sortPrefix" ascending:YES],
									 [NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO],
									 nil];
		}
		else
		{
			NSAssertWarn(sortMethod==FGListSortRank, @"Sort Method?");
			request.sortDescriptors=[NSArray arrayWithObjects:
									 [NSSortDescriptor sortDescriptorWithKey:@"sortPrefix" ascending:YES],
									 [NSSortDescriptor sortDescriptorWithKey:@"ratingSum" ascending:NO],
									 [NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO],
									 nil];
		}
		self.resultsController=[[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:model.context sectionNameKeyPath:nil cacheName:nil];
		self.resultsController.delegate=self;
		self->_sortTypeCurrent=sortMethod;

		if([self.resultsController performFetch:&error]==YES)
		{
			// this doesn't seem that it should be needed but view controller does not automatically reload the view.
			[self.tableView reloadData];
		}
		else
		{
			NSString *text=[NSString stringWithFormat:@"Failed to load database: %@", [error localizedDescription]];
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:text]];
		}
	}
}

- (void)updateFetchRequest
{
	if(self.resultsController)
	{
		if(self->_sortTypeCurrent!=[FGSettingsProxy getListSortMethod])
		{
			// do our best to unhook and destroy him
			self.resultsController.delegate=nil;
			self.resultsController=nil;

			[self buildFetchRequest];
		}
	}
}


- (void)refreshVisibleCells
{
	NSArray *cells=[self.tableView visibleCells];
	for(int index=[cells count]-1; index>-1; index--)
	{
		[self configureCell:[cells objectAtIndex:index]];
	}
}

- (void)configureCell:(UITableViewCell*)cell
{
	NSIndexPath *indexPath=[self.tableView indexPathForCell:cell];
	[self configureCell:cell indexPath:indexPath];
}

- (void)configureCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath
{
	FGMessageVO *message=[self.resultsController objectAtIndexPath:indexPath];
	if([cell isKindOfClass:[FGMessageListCell class]])
	{
		FGMessageListCell *cellRate=(FGMessageListCell*)cell;

		[cellRate setTitleText:message.text];
		[cellRate setDetailText:[NSString formatMessageUserWithDate:message shortStyle:NO]];
		if([message.allowFeedback boolValue])
		{
			[cellRate setRatingVisble:YES];
			[cellRate setLikeText:[NSString formatRatingCount:message.ratingLikes zeroValue:@"0"]];
			[cellRate setDislikeText:[NSString formatRatingCount:message.ratingDislikes zeroValue:@"0"]];
			if([FGLoginProxy isLoggedIn])
			{
				[cellRate setRatingEnabled:[message isUserRatingLike] selectDislike:[message isUserRatingDislike]];
			}
			else
			{
				[cellRate setRatingDisabled];
			}
		}
		else
		{
			[cellRate setRatingVisble:NO];
		}
	}
	else
	{
		[cell.textLabel setText:message.text];
		[cell.detailTextLabel setText:[NSString formatMessageUserWithDate:message shortStyle:NO]];
		if(message.imageUrlThumb!=nil)
		{
			[cell.imageView setImage:nil];
			[cell setNeedsLayout];
		}
		else
		{
			[cell.imageView setImage:nil];
		}
	}
	// note: concerning status, see willDisplayCell.
}

- (void)updateMessagePostStatus:(FGMessageVO*)message
{
	NSIndexPath *indexPath=[self.resultsController indexPathForObject:message];
	if(indexPath!=nil)
	{
		UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:indexPath];
		if(cell!=nil)
		{
			[self updateMessagePostStatus:message cell:cell];
		}
	}
}

- (void)updateMessagePostStatus:(FGMessageVO*)message cell:(UITableViewCell *)cell
{
	UIColor *color;
	if([message.status isEqualToString:FGMessageStatusError])
	{
		color=[UIColor colorWithGLKVector4:FGMessageListErrorColor];
	}
	else if([message.status isEqualToString:FGMessageStatusPost])
	{
		color=[UIColor colorWithGLKVector4:FGMessageListPostingColor];
	}
	else
	{
		NSAssert([message.status isEqualToString:FGMessageStatusPosted], @"Unknown status");
		if([message.type isEqualToString:FGMessageTypeUser])
		{
			color=[UIColor colorWithGLKVector4:FGMessageListPostedColorUser];
		}
		else if([message.type isEqualToString:FGMessageTypeWelcome])
		{
			color=[UIColor colorWithGLKVector4:FGMessageListPostedColorWelcome];
		}
		else if([message.type isEqualToString:FGMessageTypeNotification])
		{
			color=[UIColor colorWithGLKVector4:FGMessageListPostedColorNotification];
		}
		else
		{
			NSAssert([message.type isEqualToString:FGMessageTypeAdvertisement], @"Unknown message type");
			color=[UIColor colorWithGLKVector4:FGMessageListPostedColorAdvertisement];
		}
	}
	[UIView animateWithDuration:0.4
						  delay:0.0
						options:UIViewAnimationOptionBeginFromCurrentState
					 animations:^{ [cell setBackgroundColor:color]; }
					 completion:nil];
}


#pragma mark - FGModalDelegate
- (void)modalCanceled:(id)modal
{
	if([super acceptModalOwnership:modal])
	{
		[super modalCanceled:modal];
	}
	else
	{
		[self dismissViewControllerAnimated:YES completion:nil];
		// and reset our lad
		self->_pendingSegueId=nil;
	}
}

- (void)modalComplete:(id)modal
{
	if([super acceptModalOwnership:modal])
	{
		[super modalComplete:modal];
	}
	else
	{
		[modal dismissViewControllerAnimated:YES completion:^(void)
		 {
			 if(self->_pendingSegueId!=nil)
			 {
				 [self performSegueSequence:self->_pendingSegueId completionSegue:nil];
			 }
		 }];
	}
}


#pragma mark - TableView protocol messages
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return MAX(1, [[self.resultsController sections] count]);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    NSInteger rowCount=0;
    if([[self.resultsController sections] count] > 0) 
	{
        id <NSFetchedResultsSectionInfo> sectionInfo=[[self.resultsController sections] objectAtIndex:section];
        rowCount = [sectionInfo numberOfObjects];
    }
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSAssert(indexPath.section==0, @"Fail");

	// note: before mixing cell types, we will need to deal with reorder optimization in which we assume
	// that all cell types are the same (see NSFetchedResultsChangeUpdate)
#if INCLUDE_RATING
	static NSString *cellIdentifier=@"Message-Rate";
#else
	static NSString *cellIdentifier=@"Message-Bare";
#endif

	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	[self configureCell:cell indexPath:indexPath];

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	FGMessageVO *message=[self.resultsController objectAtIndexPath:indexPath];
	[self updateMessagePostStatus:message cell:cell];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	FGMessageVO *message=[self.resultsController objectAtIndexPath:indexPath];
	NSString *segue=[self messageSelectionToSegue:message];
	[self performSegueWithIdentifier:segue sender:message];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	FGMessageVO *message=[self.resultsController objectAtIndexPath:indexPath];
	NSString *segue=[self messageSelectionToSegue:message];
	[self performSegueWithIdentifier:segue sender:message];
}


#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller 
{
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller 
{
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller 
   didChangeObject:(id)anObject 
	   atIndexPath:(NSIndexPath *)indexPath 
	 forChangeType:(NSFetchedResultsChangeType)type 
	  newIndexPath:(NSIndexPath *)newIndexPath
{
	// note: this update is wrapped in a beginUpdates and endUpdates which means all inserts and deletes are deferred which means
	// that when it comes to making updates things get a little tricky. Here is how we stand:
	//	- atIndexPath is where it was at the time we called beginUpdates
	//	- newIndexPath is where it will live at endUpdates or where it lives right now in our fetched controllers data set.
#if DEBUG && 0
	int indexDebug;
	NSArray *messagesSorted=[self.resultsController fetchedObjects];
	NSDebug(@"\ndidChangeObject: object='%@', type=%d, oldIndex=[%@], newIndex=[%@]", 
			[anObject text], type, [indexPath descriptionOfIndexes], [newIndexPath descriptionOfIndexes]);
	for(indexDebug=0; indexDebug<[self.tableView numberOfRowsInSection:0]; indexDebug++)
	{
		UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexDebug inSection:0]];
		NSDebug(@" -cell: %d=%@", indexDebug, [cell.textLabel text]);
	}
	for(indexDebug=0; indexDebug<[messagesSorted count]; indexDebug++)
	{
		NSDebug(@" -sorted: %d=%@", indexDebug, [[messagesSorted objectAtIndex:indexDebug] text]);
	}
#endif
	
	switch(type)
	{
		case NSFetchedResultsChangeInsert:
		{
			[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		}	
		case NSFetchedResultsChangeDelete:
		{
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		}	
		case NSFetchedResultsChangeUpdate:
		{
			// note: both of these work. reload does a swap with the new cell.  Configure updates the existing column.
			// [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			if(newIndexPath!=nil)
			{
				[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] indexPath:newIndexPath];
			}
			else
			{
				[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] indexPath:indexPath];
			}
			break;
		}	
		case NSFetchedResultsChangeMove:
		{
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
		}
	}
}

#pragma mark - Observers handlers
- (IBAction)handlePostMessage:(id)sender 
{
	if([FGLoginProxy isLoggedIn])
	{
		[self performSegueSequence:@"SeguePost" completionSegue:nil];
	}
	else
	{
		[self performSegueSequence:@"SegueLogin" completionSegue:@"SeguePost"];
	}
}

- (void)handleDBIONotification:(NSNotification*)notification
{
	if([[notification name] isEqualToString:FGNotificationDBOpenSucceeded])
	{
		[self buildFetchRequest];
	}
}

- (void)handleMessagePosting:(NSNotification *)notification
{
	[self updateMessagePostStatus:[notification object]];
}

- (void)handleMessagePostSucceeded:(NSNotification *)notification
{
	[self updateMessagePostStatus:[notification object]];
}

- (void)handleMessagePostFailed:(NSNotification *)notification
{
	[self updateMessagePostStatus:[notification object]];
}

- (void)handleMessageRateSucceeded:(NSNotification*)notification
{
	NSIndexPath *indexPath=[self.resultsController indexPathForObject:[notification object]];
	if(indexPath)
	{
		UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:indexPath];
		// possible that he's not visible which is totally valid
		if(cell)
		{
			[self configureCell:cell indexPath:indexPath];
		}
	}
	else
	{
		NSFailWarn(@"No index path?");
	}
}

- (void)handleLoginSucceeded:(NSNotification*)notification
{
	[self refreshVisibleCells];
}

- (void)handleLogoutSucceeded:(NSNotification*)notification
{
	[self refreshVisibleCells];
}


#pragma mark - Action handlers
- (IBAction)handleRefresh:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagesGet object:nil];
}

@end
