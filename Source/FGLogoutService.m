//
//  FGLogutService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLogoutService.h"
#import "FGProfileVO.h"
#import "FGStatusVO.h"
#import "FGModelSerializer.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGLogoutService
#pragma mark - Public Interface
- (id)initWithProfile:(FGProfileVO*)profile
{
	self=[super init];
	self->_profile=profile;
	
	return self;
}

- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLoggingOut object:self->_profile];
	@try 
	{
		NSString *url=[FGSettings getUrlLogoutUser];
		NSData *data=[FGModelSerializer buildLogoutRequest:self->_profile pretty:NO];

		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		[FGModelSerializer processLogoutResponse:data];
		[self completeLogout];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorText=[NSString stringWithFormat:@"Logout failed: %@", [response description]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorText]];
	// note: the call to the backend is a courtesy.  We don't really care if the request succeeds or fails.
	[self completeLogout];
}

- (void)preprocessAuthFailure:(FGStatusVO*)response
{
	// prevent the default logout behavior as we don't want to end up in an endless cycle.
	// We will force a logout regardless of the outcome (see handleFailure)
}


#pragma mark - Private handlers
- (void)completeLogout
{
	// ensure that it's no (will not be if logout request failed)
	[self->_profile setLoggedIn:[NSNumber numberWithBool:NO]];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationLogoutSucceeded object:self->_profile];
}
@end
