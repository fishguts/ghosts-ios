//
//  NSDictionary+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (FG)
/**** Public Interface ****/
- (id)objectForKey:(id)key defaultValue:(id)defaultValue;
- (id)valueForKey:(NSString*)key defaultValue:(id)defaultValue;

/* uses our home brewed ARC_[values] */
- (BOOL)boolForKey:(NSString*)key;

@end

@interface NSMutableDictionary (FG)
- (void)setBool:(BOOL)value forKey:(id)aKey;
@end
