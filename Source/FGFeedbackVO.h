//
//  FGFeedbackVO.h
//  Goggles
//
//  Created by Curtis Elsasser on 12/2/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSObject.h>

@class FGProfileVO;

@interface FGFeedbackVO : NSObject
/**** Properties ****/
@property (nonatomic, copy) NSString *text;
@property (nonatomic, weak) FGProfileVO *owner;

/**** Public interface ****/
+ (FGFeedbackVO*)feedbackFromText:(NSString*)text owner:(FGProfileVO*)owner;

- (id)initWithText:(NSString*)text owner:(FGProfileVO*)owner;

@end
