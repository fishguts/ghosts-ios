//
//  MessageAnnotation.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGMessageVO;

@interface FGMessageAnnotation : NSObject <MKAnnotation>
{
	FGMessageVO *_message;
}

/**** properties ****/
@property (nonatomic, readonly) FGMessageVO *message;

/**** api ****/
- (id)initWithMessage:(FGMessageVO*)message;

@end
