//
//  FGSerialization.h
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGMachine;
@class FGGear;

@interface FGSerializer : FGStaticClass

/**** public interface ****/
+ (NSData*)dataToJSON:(id)object pretty:(BOOL)pretty;
+ (id)jsonToData:(NSData*)json;

@end

@interface FGSerializeUtils : FGStaticClass

/**** safe write values ****/
+ (void)addValue:(id)value toDictionary:(NSMutableDictionary*)dictionary withKey:(NSString*)key;
+ (void)addValue:(id)value toArray:(NSMutableArray*)array;

+ (NSString*)dateToString:(NSDate*)date;
+ (NSDate*)stringToDate:(NSString*)text;

@end