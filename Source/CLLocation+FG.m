//
//  CLLocation+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "CLLocation+FG.h"

/**** constants ****/
static const CLLocationDistance WORLD_CIRCUMFERENCE_METERS=40075.017*1000;


@implementation CLLocation (FG)
+ (CLLocation*)clone:(CLLocation*)location withCoordinate:(CLLocationCoordinate2D)coordinate altitude:(CLLocationDistance)altitude
{
	return [[CLLocation alloc] initWithCoordinate:coordinate altitude:altitude
							   horizontalAccuracy:[location horizontalAccuracy]
								 verticalAccuracy:[location verticalAccuracy]
										   course:[location course]
											speed:[location speed]
										timestamp:[location timestamp]];
}

+ (CLLocationDegrees)metersToLatitudeDelta:(CLLocationDistance)meters
{	
	// fraction of a degree: 360*(view.radius/WORLD_CIRCUMFERENCE_METERS)
	return (360*meters)/WORLD_CIRCUMFERENCE_METERS;
}

+ (CLLocationDegrees)metersToLongitudeDelta:(CLLocationDistance)meters atLatitude:(CLLocationDegrees)latitude
{
	// keep it from going to 0 for our polar pals
	const CLLocationDegrees latitudeBounded=MAX(-89.9, MIN(89.9, latitude));
	const CLLocationDistance latitudeCircumference=cos(M_PI*latitudeBounded/180)*WORLD_CIRCUMFERENCE_METERS;
	// fraction of a degree: 360*(view.radius/latitude_circumference)
	return (360*meters)/latitudeCircumference;
}


- (CLLocationDistance)distanceFromCoordinate:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude altitude:(CLLocationDistance)altitude
{
	const double dalt=[self altitudeDeltaInMetersWithAltitude:altitude];
	const double dlon=[self longitudeDeltaInMetersWithLongitude:longitude latitude:latitude];
	const double dlat=[self latitudeDeltaInMetersWithLatitude:latitude];
	
	return sqrt(dalt*dalt + dlon*dlon + dlat*dlat);
}

- (CLLocationDistance)distanceIncludingAltitude:(const CLLocation*)subtrahend
{
	const double dalt=[self altitudeDeltaInMetersWithCoordinate:subtrahend];
	const double dlon=[self longitudeDeltaInMetersWithCoordinate:subtrahend];
	const double dlat=[self latitudeDeltaInMetersWithCoordinate:subtrahend];
	
	return sqrt(dalt*dalt + dlon*dlon + dlat*dlat);
}


- (CLLocationDistance)latitudeDeltaInMetersWithLatitude:(CLLocationDegrees)latitudeSubtrahend
{
	const CLLocationDistance delta=self.coordinate.latitude-latitudeSubtrahend;
	// better accuracy than: delta/360 * WORLD_CIRCUMFERENCE_METERS
	return (delta*WORLD_CIRCUMFERENCE_METERS)/360;
}

- (CLLocationDistance)latitudeDeltaInMetersWithCoordinate:(const CLLocation*)subtrahend
{
	return [self latitudeDeltaInMetersWithLatitude:subtrahend.coordinate.latitude];
}


- (CLLocationDistance)longitudeDeltaInMetersWithLongitude:(CLLocationDegrees)longitudeSubtrahend latitude:(CLLocationDegrees)latitudeSubtrahend
{
	const CLLocationDistance delta=self.coordinate.longitude-longitudeSubtrahend;
	// kind of breaks down here - assuming for our purposes that they are close.
	const CLLocationDegrees latitudeMidpoint=self.coordinate.latitude-(self.coordinate.latitude-latitudeSubtrahend)/2;
	const CLLocationDegrees latitudeBounded=MAX(-89.9, MIN(89.9, latitudeMidpoint));
	const CLLocationDistance latitudeCircumference=cos(M_PI*latitudeBounded/180)*WORLD_CIRCUMFERENCE_METERS;
	return (delta*latitudeCircumference)/360;
}

- (CLLocationDistance)longitudeDeltaInMetersWithCoordinate:(const CLLocation*)subtrahend
{
	return [self longitudeDeltaInMetersWithLongitude:subtrahend.coordinate.longitude latitude:subtrahend.coordinate.latitude];
}


- (CLLocationDistance)altitudeDeltaInMetersWithAltitude:(CLLocationDegrees)altitudeSubtrahend
{
	return self.altitude-altitudeSubtrahend;
}
- (CLLocationDistance)altitudeDeltaInMetersWithCoordinate:(const CLLocation*)subtrahend
{
	return self.altitude-subtrahend.altitude;
}

- (NSString*)descriptionShort
{
	return [NSString stringWithFormat:@"<%.7f, %.7f, %.1f> +/-%.0fmh, +/-%.0fmv", 
			self.coordinate.latitude, 
			self.coordinate.longitude, 
			self.altitude, 
			self.horizontalAccuracy, 
			self.verticalAccuracy];
}

@end
