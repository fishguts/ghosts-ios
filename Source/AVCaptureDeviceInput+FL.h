//
//  AVCaptureDeviceInput+FL.h
//  TestVideo
//
//  Created by Curtis Elsasser on 8/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVCaptureDeviceInput (FL)
/**** public API ****/
+ (AVCaptureDeviceInput *)defaultDeviceInputWithMediaType:(NSString *)mediaType;
@end
