//
//  FGMessageSerializer.m
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGModelSerializer.h"
#import "FGModel.h"
#import "FGFeedbackVO.h"
#import "FGProfileVO.h"
#import "FGStatusVO.h"
#import "FGMessageVO.h"
#import "FGSettingsVO.h"
#import "FGModelQueries.h"
#import "FGSerializer.h"
#import "FGConstants.h"
#import "UIDevice+FG.h"


@interface FGModelSerializer()
/**** private workers *****/
+ (NSDictionary*)messageToDictionary:(FGMessageVO*)message;
+ (NSDictionary*)profileToDictionary:(FGProfileVO*)profile;

+ (FGMessageVO*)dictionaryToMessage:(NSDictionary*)data;
+ (void)dictionaryToMessage:(NSDictionary*)data message:(FGMessageVO*)message;

+ (FGProfileVO*)dictionaryToProfile:(NSDictionary*)data;
+ (void)dictionaryToProfile:(NSDictionary*)datas profile:(FGProfileVO*)profile;

+ (FGSettingsVO*)dictionaryToSetting:(NSDictionary*)data;

@end

@implementation FGModelSerializer

#pragma mark - Builders
+ (NSData*)buildGetSettingsRequest:(BOOL)pretty
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	NSString *version=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
	[data setValue:version forKey:@"version"];
	return [FGSerializer dataToJSON:data pretty:pretty];
}

+ (NSData*)buildLoginRequest:(FGProfileVO*)profile pretty:(BOOL)pretty
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	[data setValue:profile.username forKey:@"username"];
	[data setValue:profile.password forKey:@"password"];
	return [FGSerializer dataToJSON:data pretty:pretty];
}

+ (NSData*)buildLogoutRequest:(FGProfileVO*)profile pretty:(BOOL)pretty
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	[data setValue:profile.username forKey:@"username"];
	return [FGSerializer dataToJSON:data pretty:pretty];
}

+ (NSData*)buildRegisterRequest:(FGProfileVO*)profile pretty:(BOOL)pretty
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	[data setValue:profile.username forKey:@"username"];
	[data setValue:profile.email forKey:@"email"];
	[data setValue:profile.password forKey:@"password"];
	return [FGSerializer dataToJSON:data pretty:pretty];
}

+ (NSData*)buildSendFeedbackRequest:(FGFeedbackVO*)feedback pretty:(BOOL)pretty
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	NSString *device=[[UIDevice currentDevice] encoded];
	
	[data setValue:feedback.text forKey:@"text"];
	[data setValue:feedback.owner.username forKey:@"ownerId"];
	[data setValue:device forKey:@"device"];
	return [FGSerializer dataToJSON:data pretty:pretty];
}

+ (NSData*)buildGetMessagesRequest:(CLLocation*)location welcomeState:(BOOL)welcomeState pretty:(BOOL)pretty
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	[data setValue:@"radial" forKey:@"view"];
	[data setValue:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:@"latitude"];
	[data setValue:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"longitude"];
	[data setValue:[NSNumber numberWithDouble:location.altitude] forKey:@"altitude"];
	if(welcomeState)
	{
		[data setValue:[NSNumber numberWithBool:YES] forKey:@"welcome"];
	}
	return [FGSerializer dataToJSON:data pretty:pretty];
}

+ (NSData*)buildPostMessageRequest:(FGMessageVO*)message pretty:(BOOL)pretty
{
	NSData *jsonData;
	@try
	{
		NSDictionary *data=[FGModelSerializer messageToDictionary:message];
		jsonData=[FGSerializer dataToJSON:data pretty:pretty];
	}
	@catch(NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to build post message request: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return jsonData;
}

+ (NSData*)buildPostMessagesRequest:(NSArray*)messages pretty:(BOOL)pretty
{
	NSData *jsonData;
	@try 
	{
		NSMutableArray *data=[NSMutableArray array];
		for(FGMessageVO *message in messages)
		{
			[data addObject:[FGModelSerializer messageToDictionary:message]];
		}
		jsonData=[FGSerializer dataToJSON:data pretty:pretty];
	}
	@catch(NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to build post message request: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return jsonData;
}

+ (NSData*)buildRateMessageRequest:(FGMessageVO*)message rating:(NSNumber*)rating pretty:(BOOL)pretty
{
	NSData *jsonData;
	@try
	{
		NSMutableDictionary *data=[NSMutableDictionary dictionary];
		[data setValue:message.id forKey:@"messageId"];
		[data setValue:rating forKey:@"rating"];
		jsonData=[FGSerializer dataToJSON:data pretty:pretty];
	}
	@catch(NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to build rate message request: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return jsonData;
}

+ (NSData*)buildFlagMessageRequest:(FGMessageVO*)message pretty:(BOOL)pretty
{
	NSData *jsonData;
	@try
	{
		NSMutableDictionary *data=[NSMutableDictionary dictionary];
		[data setValue:message.id forKey:@"messageId"];
		jsonData=[FGSerializer dataToJSON:data pretty:pretty];
	}
	@catch(NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to build flag message request: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return jsonData;
}

+ (NSData*)buildDeleteMessageRequest:(FGMessageVO*)message pretty:(BOOL)pretty
{
	NSData *jsonData;
	@try
	{
		NSMutableDictionary *data=[NSMutableDictionary dictionary];
		[data setValue:message.id forKey:@"messageId"];
		jsonData=[FGSerializer dataToJSON:data pretty:pretty];
	}
	@catch(NSException *exception)
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to build delete message request: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}

	return jsonData;
}



#pragma mark - Parsers
+ (FGStatusVO*)parseResponseStatus:(NSData*)jsonData
{
	FGStatusVO *result;
	@try 
	{
		NSDictionary *dataMap=[FGSerializer jsonToData:jsonData];
		NSDictionary *response=[dataMap valueForKey:@"status"];

		result=[FGStatusVO statusFromText:[response valueForKey:@"text"] code:[response valueForKey:@"code"]];
	}
	@catch (NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to convert JSON to response: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return result;
}

+ (void)processLoginResponse:(NSData*)jsonData intoProfile:(FGProfileVO *)profile
{
	@try
	{
		NSDictionary *dataMap=[FGSerializer jsonToData:jsonData];
		NSDictionary *data=[dataMap valueForKey:@"profile"];

		[FGModelSerializer dictionaryToProfile:data profile:profile];
		[profile setLoggedIn:[NSNumber numberWithBool:YES]];
	}
	@catch (NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to convert JSON to response: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
}

+ (void)processLogoutResponse:(NSData*)jsonData
{
	// we don't really care what they said.  Just let them out
	FGProfileVO *login=[FGModelQueries fetchLogin];
	[login setLoggedIn:[NSNumber numberWithBool:NO]];
}

+ (void)processRegisterResponse:(NSData*)jsonData
{
	// todo: is there anything we care about in the response?
}


+ (NSArray*)parseSettingsResponse:(NSData*)jsonData
{
	NSMutableArray *results=[NSMutableArray array];
	@try 
	{
		NSDictionary *dataMap=[FGSerializer jsonToData:jsonData];
		NSArray *settings=[dataMap valueForKey:@"settings"];
		NSDictionary *server=[dataMap valueForKey:@"server"];
		
		for(NSDictionary *settingData in settings)
		{
			FGSettingsVO *setting=[FGModelSerializer dictionaryToSetting:settingData];
			// squeeze this guy into our settings. Should we ever really support multiple setting views then may revisit.
			setting.serverVersion=[server valueForKey:@"version"];
			[results addObject:setting];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to parse settings response: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return results;
}

+ (void)parsePostResponse:(NSData*)jsonData intoMessage:(FGMessageVO*)message
{
	@try 
	{
		NSDictionary *dataMap=[FGSerializer jsonToData:jsonData];
		NSArray *profiles=[dataMap valueForKey:@"profiles"];
		NSArray *messages=[dataMap valueForKey:@"messages"];
		
		if([messages count]!=1)
		{
			@throw [NSException exceptionWithName:@"FormatException" reason:@"Improper format" userInfo:nil];
		}
		// build up our profiles first as messages will be referencing them
		for(NSDictionary *profileData in profiles)
		{
			[FGModelSerializer dictionaryToProfile:profileData];
		}
		[FGModelSerializer dictionaryToMessage:messages.lastObject message:message];
		message.status=FGMessageStatusPosted;
	}
	@catch (NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to parse message response: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
}

+ (NSArray*)parseMessagesResponse:(NSData*)jsonData
{
	NSMutableArray *results=[NSMutableArray array];
	@try 
	{
		NSDictionary *dataMap=[FGSerializer jsonToData:jsonData];
		NSArray *profiles=[dataMap valueForKey:@"profiles"];
		NSArray *messages=[dataMap valueForKey:@"messages"];

		// build up our profiles first as messages will be referencing them
		for(NSDictionary *profileData in profiles)
		{
			[FGModelSerializer dictionaryToProfile:profileData];
		}
		for(NSDictionary *messageData in messages)
		{
			FGMessageVO *message=[FGModelSerializer dictionaryToMessage:messageData];
			message.status=FGMessageStatusPosted;
			[results addObject:message];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to parse message response: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		@throw exception;
	}
	
	return results;
}

#pragma mark - Private Interface
+ (NSDictionary*)messageToDictionary:(FGMessageVO*)message
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	
	[FGSerializeUtils addValue:message.id toDictionary:data withKey:@"id"];
	[FGSerializeUtils addValue:message.text toDictionary:data withKey:@"text"];
	[FGSerializeUtils addValue:message.imageUrlThumb toDictionary:data withKey:@"imageUrlThumb"];
	[FGSerializeUtils addValue:message.imageUrlFull toDictionary:data withKey:@"imageUrlFull"];
	[FGSerializeUtils addValue:message.latitude toDictionary:data withKey:@"latitude"];
	[FGSerializeUtils addValue:message.longitude toDictionary:data withKey:@"longitude"];
	[FGSerializeUtils addValue:message.altitude toDictionary:data withKey:@"altitude"];
	[FGSerializeUtils addValue:message.created toDictionary:data withKey:@"created"];
	if(message.expiration!=nil)
	{
		// note: api is in seconds (so that date is always in reference to their create time stamp)
		NSNumber *expiration=[NSNumber numberWithDouble:[message.expiration timeIntervalSinceDate:message.created]];
		[FGSerializeUtils addValue:expiration toDictionary:data withKey:@"expiration"];
	}
	if(message.owner!=nil)
	{
		[FGSerializeUtils addValue:message.owner.username toDictionary:data withKey:@"ownerId"];
	}
	// note: we don't include rating nor flagged information as we never issue it. We just consume it.
	
	return data;
}

+ (NSDictionary*)profileToDictionary:(FGProfileVO*)profile
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	
	[FGSerializeUtils addValue:profile.username toDictionary:data withKey:@"username"];
	[FGSerializeUtils addValue:profile.imageUrlThumb toDictionary:data withKey:@"imageUrlThumb"];
	[FGSerializeUtils addValue:profile.imageUrlFull toDictionary:data withKey:@"imageUrlFull"];
	[FGSerializeUtils addValue:profile.created toDictionary:data withKey:@"created"];
	
	return data;
}

+ (FGMessageVO*)dictionaryToMessage:(NSDictionary*)data
{
	// first see whether it already exists
	FGMessageVO *message=[FGModelQueries fetchMessageById:[data valueForKey:@"id"]];
	if(message==nil)
	{
		NSManagedObjectContext *context=[[FGModel instance] context];
		NSEntityDescription *entity=[NSEntityDescription entityForName:@"Message" inManagedObjectContext:context];
		message=[[FGMessageVO alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
	}
	[FGModelSerializer dictionaryToMessage:data message:message];

	return message;
}

+ (void)dictionaryToMessage:(NSDictionary*)data message:(FGMessageVO*)message
{
	message.id=[data valueForKey:@"id"];
	message.type=[data valueForKey:@"type"];
	message.text=[data valueForKey:@"text"];
	message.imageUrlThumb=[data valueForKey:@"imageUrlThumb"];
	message.imageUrlFull=[data valueForKey:@"imageUrlFull"];
	message.latitude=[data valueForKey:@"latitude"];
	message.longitude=[data valueForKey:@"longitude"];
	message.altitude=[data valueForKey:@"altitude"];
	message.ratingLikes=[data valueForKey:@"ratingLikes"];
	message.ratingDislikes=[data valueForKey:@"ratingDislikes"];
	message.ratingSum=[NSNumber numberWithInt:([message.ratingLikes intValue]-[message.ratingDislikes intValue])];
	message.ratingUser=[data valueForKey:@"ratingUser"];
	message.flaggedUser=[data valueForKey:@"flaggedUser"];
	message.created=[FGSerializeUtils stringToDate:[data valueForKey:@"created"]];
	message.expiration=[FGSerializeUtils stringToDate:[data valueForKey:@"expiration"]];
	message.allowFeedback=[data valueForKey:@"supportsFeedback" defaultValue:[NSNumber staticBoolYES]];

	// handy way of testing ratings and layout
#if DEBUG && 0
	message.ratingLikes=[NSNumber numberWithInt:rand()/(rand()/20+1)];
	message.ratingDislikes=[NSNumber numberWithInt:rand()/(rand()/20+1)];
	message.ratingSum=[NSNumber numberWithInt:([message.ratingLikes intValue]-[message.ratingDislikes intValue])];
#endif

	if(message.owner==nil)
	{
		[[FGModelQueries fetchProfileByName:[data valueForKey:@"ownerId"]] addMessagesObject:message];
	}
	NSAssert(message.owner!=nil, @"Owner?");
}

+ (FGProfileVO*)dictionaryToProfile:(NSDictionary*)data
{
	// first see whether our chap already exists
	FGProfileVO *profile=[FGModelQueries fetchProfileByName:[data valueForKey:@"username"]];
	if(profile==nil)
	{
		profile=[NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:[[FGModel instance] context]];
	}
	[FGModelSerializer dictionaryToProfile:data profile:profile];

	return profile;
}

+ (void)dictionaryToProfile:(NSDictionary*)data profile:(FGProfileVO*)profile
{
	profile.username=[data valueForKey:@"username"];
	profile.imageUrlThumb=[data valueForKey:@"imageUrlThumb"];
	profile.imageUrlFull=[data valueForKey:@"imageUrlFull"];
	profile.created=[FGSerializeUtils stringToDate:[data valueForKey:@"created"]];
}

+ (FGSettingsVO*)dictionaryToSetting:(NSDictionary *)data
{
	FGSettingsVO *setting=[FGModelQueries fetchSettingByName:[data valueForKey:@"name"]];
	if(setting==nil)
	{
		setting=[NSEntityDescription insertNewObjectForEntityForName:@"Settings" inManagedObjectContext:[[FGModel instance] context]];
		setting.name=[data valueForKey:@"name"];
	}
	setting.queryIntervalMin=[data valueForKey:@"queryIntervalMin"];
	setting.queryIntervalMax=[data valueForKey:@"queryIntervalMax"];
	setting.radiusHorizontal=[data valueForKey:@"radiusHorizontal"];
	setting.radiusVertical=[data valueForKey:@"radiusVertical"];
	setting.messagesPerQuery=[data valueForKey:@"messagesPerQueryMax"];
	
	return setting;
}

@end
