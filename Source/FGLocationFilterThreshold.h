//
//  FGLocationFilterThreshold.h
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/12/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import "FGLocationFilterBase.h"

/**** types ****/
typedef enum
{
	FGLocationFilterThresholdMeasure,
	FGLocationFilterThresholdFilter,
} FGLocationFilterThresholdMethod;


/**** classes ****/
@interface FGLocationFilterThreshold : FGLocationFilterBase
/**** Properties ****/
@property (nonatomic, readonly) FGLocationFilterThresholdMethod filterMethod;
@property (nonatomic) CLLocationDegrees thresholdHorizontal;
@property (nonatomic) CLLocationDegrees thresholdVertical;
@property (nonatomic) BOOL filterOutDuplicates;

/**** Interface ****/
- (id)initWithMethod:(FGLocationFilterThresholdMethod)filterMethod horizontalThreshold:(CLLocationDegrees)thresholdH;
- (id)initWithMethod:(FGLocationFilterThresholdMethod)filterMethod horizontalThreshold:(CLLocationDegrees)thresholdH verticalThreshold:(CLLocationDegrees)thresholdV;

@end
