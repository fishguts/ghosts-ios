//
//  FGTimer.h
//  Goggles
//
//  Created by Curtis Elsasser on 1/13/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSTimer.h>

/**
 * Suspendable timer
 */
@interface FGTimer : NSObject
{
	NSTimeInterval _interval;
	id _target;
	SEL _selector;
	id _userInfo;
	BOOL _repeats;

	NSTimer *_timer;
	NSDate *_startTime;
	NSTimeInterval _remaining;
	BOOL _valid;
}

/**** Allocation ****/
+ (FGTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval target:(id)target selector:(SEL)selector userInfo:(id)userInfo repeats:(BOOL)repeats;

/**** Public Propertie ****/
- (NSTimeInterval)timeInterval;
- (id)userInfo;
- (NSDate *)fireDate;
- (NSTimeInterval)fireInterval;

/**** Public Interface ****/
- (void)invalidate;
- (BOOL)isValid;
- (BOOL)isActive;

- (void)suspend;
- (void)resume;
- (void)fire;


@end
