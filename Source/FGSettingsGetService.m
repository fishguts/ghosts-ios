//
//  FGGetSettingsService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSettingsGetService.h"
#import "FGModelSerializer.h"
#import "FGModelQueries.h"
#import "FGStatusVO.h"
#import "FGSettings.h"
#import "FGSettingsProxy.h"
#import "FGConstants.h"


@implementation FGSettingsGetService

#pragma mark - Public Interface
- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationSettingsGetting object:nil];
	@try 
	{
		NSString *url=[FGSettings getUrlGetSettings];
		NSData *data=[FGModelSerializer buildGetSettingsRequest:NO];
		
		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		NSArray *settingsNew=[FGModelSerializer parseSettingsResponse:data];

		// have proxy update our environment
		[FGSettingsProxy installSettings];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationSettingsGetSucceeded object:settingsNew];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to get settings failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Get settings failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationSettingsGetFailed object:nil text:errorTextFriendly status:response]];
}

@end
