//
//  FGLocationFilterAnimate.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLocationFilterBase.h"


/**
 * FGLocationFilterAnimate - He animatates location changes if configured to do so.
 */
@interface FGLocationFilterAnimate : FGLocationFilterBase

/**** Properties ****/
@property (nonatomic, readonly) int animationIntervals;
@property (nonatomic, readonly) double animationDuration;
@property (nonatomic, readonly) double animationInterval;

/**** Public Interface ****/
- (id)init;
- (id)initWithAnimationIntervals:(int)intervals overDuration:(double)duration;

/**** Protected Interface ****/
/**
 * Animation changes will happen automatically if animation is turned on.  Any location at any time
 * can be manually triggered with this method
 */
- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType withAnimation:(BOOL)animate;

@end
