//
//  FGExpirationVO.h
//  Goggles
//
//  Created by Curtis Elsasser on 4/5/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGExpirationVO : NSObject
/**** Public Properties ****/
@property (strong, readonly) NSString *id;
@property (strong, readonly) NSString *text;

/**** Construction ****/
+ (FGExpirationVO*)expirationWithDuration:(NSTimeInterval)totalSeconds id:(NSString*)id text:(NSString*)text;
+ (FGExpirationVO*)expirationWithDeadline:(NSDate*)deadline id:(NSString*)id text:(NSString*)text;

/**** Public Interface ****/
- (NSDate*)getExpirationDate;
- (NSDate*)getExpirationDate:(NSDate*)fromTime;


@end
