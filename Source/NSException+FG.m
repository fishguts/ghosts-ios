//
//  NSException+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSException+FG.h"

@implementation NSException (FG)

+ (NSException*)abstractViolation:(NSString*)method
{
	return [NSException exceptionWithName:@"AbstractException" reason:@"Must implement" userInfo:nil];
}

@end
