//
//  FGMessageMapController.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/13/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageMapController.h"
#import "FGMessageAnnotation.h"
#import "FGBrain.h"
#import "FGProfileVO.h"
#import "FGMessageVO.h"
#import "FGLoginProxy.h"
#import "FGMotionProxy.h"
#import "FGMessageProxy.h"
#import "FGCoordinateGrid.h"
#import "FGGeometry.h"
#import "FGSession.h"
#import "FGSettings.h"
#import "FGConstants.h"
#import "CLLocation+FG.h"
#import "UIColor+FG.h"
#import "UIViewController+MessageEditor.h"


/**** Local Types and Constants ****/
typedef enum
{
	FGRegionOwnerApp,
	FGRegionOwnerUser,
} FGRegionOwner;

/**
 * DEFAULT_MAP_SCALE - at our zoomed level, the map is a very tight fit. Give us some room so that the fringes don't fall off the map 
 *	and to accomodate the user annotation (roughly 12 points)
 */
static const double DEFAULT_MAP_SCALE=1.1;
/**
 * MAXIMUM_MAP_SCALE - this just makes sure we don't try to stretch too far...for example if somebody should suspend, move and resume
 */
static const double MAXIMUM_MAP_SCALE=4.0;
/**
 * MAP_SCALE_INCREMENT - amount to scale map by when we need more room (multiplies perfect fit by this value)
 */
static const double MAP_SCALE_INCREMENT=1.25;


/**** FGMessageMapController ****/
@interface FGMessageMapController() <MKMapViewDelegate>
{
	BOOL _activeViewController;
	NSString *_pendingSegueId;

	MKCircle *_coverageOverlay;
	FGRegionOwner _regionOwner;
}

/**** properties ****/
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonOrient;

/**** private workers ****/
- (void)addObservers;
- (void)removeObservers;

- (void)performSegueSequence:(NSString*)segue1 completionSegue:(NSString*)segue2;

- (SizeD)calculateMapSizeInMeters:(double)scale;
- (FGCoordinateGrid*)buildCoordinateGrid;

- (void)updateOverlays;
- (void)updateAnnotations;
- (void)updateMapLocationWithAnimation:(BOOL)animated;
- (void)updateMapLocationWithAnimation:(BOOL)animated center:(CLLocationCoordinate2D)center perimeterMultiplier:(double)scale;
- (void)updateMessagePostStatus:(FGMessageVO*)message;
- (void)updateMessagePostStatus:(FGMessageVO*)message annotationView:(MKPinAnnotationView*)view;

/**** observer handlers ****/
- (void)handleLocationChanged:(NSNotification*)notification;
- (void)handleMessageGetSucceeded:(NSNotification*)notification;

- (void)handleMessagePost:(NSNotification*)notification;
- (void)handleMessagePosting:(NSNotification*)notification;
- (void)handleMessagePostSucceeded:(NSNotification*)notification;
- (void)handleMessagePostFailed:(NSNotification*)notification;

@end


@implementation FGMessageMapController
#pragma mark - Public/Private Properties

- (void)setRegionOwner:(FGRegionOwner)regionOwner
{
	self->_regionOwner=regionOwner;
	[self.buttonOrient setHidden:(regionOwner==FGRegionOwnerApp)];
}

#pragma mark - View lifecycle
- (void)viewDidUnload 
{
	[self setMapView:nil];
	[self setButtonOrient:nil];
	[super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if(self->_activeViewController==NO)
	{
		self->_activeViewController=YES;
		[self setRegionOwner:FGRegionOwnerApp];
		[self updateMapLocationWithAnimation:NO];
		[self updateAnnotations];
		[self updateOverlays];
		[self addObservers];
	}
	else
	{
		[[FGBrain instance] wakeup:YES];
	}
}

- (void)viewDidDisappear:(BOOL)animated
{
	self->_activeViewController=[self.navigationController isSelectedViewController];
	if(self->_activeViewController)
	{
		[[FGBrain instance] sleep];
	}
	else
	{
		[self removeObservers];
	}
	[super viewDidDisappear:animated];
}

#pragma mark - Private Interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	[center addObserver:self selector:@selector(handleLocationChanged:) name:FGNotificationLocationChanged object:nil];
	[center addObserver:self selector:@selector(handleMessageGetSucceeded:) name:FGNotificationMessagesGetSucceeded object:nil];
	[center addObserver:self selector:@selector(handleMessagePost:) name:FGNotificationMessagePost object:nil];
	[center addObserver:self selector:@selector(handleMessagePosting:) name:FGNotificationMessagePosting object:nil];
	[center addObserver:self selector:@selector(handleMessagePostSucceeded:) name:FGNotificationMessagePostSucceeded object:nil];
	[center addObserver:self selector:@selector(handleMessagePostFailed:) name:FGNotificationMessagePostFailed object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	[super prepareForSegue:segue sender:sender];

	// prevent traffic while we are modal
	id destinationViewController=[segue.destinationViewController activeViewController];
	if([destinationViewController respondsToSelector:@selector(setDelegate:)])
	{
		[destinationViewController setDelegate:self];
	}
	if([destinationViewController respondsToSelector:@selector(setMessage:)])
	{
		if([sender isKindOfClass:[FGMessageVO class]])
		{
			[destinationViewController setMessage:sender];
		}
	}
}

- (void)performSegueSequence:(NSString*)segue1 completionSegue:(NSString*)segue2
{
	self->_pendingSegueId=segue2;
	[self performSegueWithIdentifier:segue1 sender:self];
}

/**
 * @param scale - scale is a positive scale. It scales the amount that is visible in the mapview 
 *	and inversely scales the size of the contents of the map
 */
- (SizeD)calculateMapSizeInMeters:(double)scale
{
	const double aspectRatio=self.mapView.bounds.size.width/self.mapView.bounds.size.height;
	const CLLocationDistance diameterInMeters=([FGSettings getRadiusHorizontal]*2)*scale;
	return SizeDMake((aspectRatio>1) ? diameterInMeters*aspectRatio : diameterInMeters,
					 (aspectRatio<1) ? diameterInMeters/aspectRatio : diameterInMeters);
}

- (FGCoordinateGrid*)buildCoordinateGrid
{
	FGCoordinateGrid *grid=[FGMessageProxy getMessagesCoordinateGrid];
	CLLocation *location=[[FGSession instance] locationLastRetrieve];
	if(location)
	{
		// make sure our coverage perimeter is visible
		const CLLocationCoordinate2D coordinate=location.coordinate;
		const CLLocationDegrees radiusDegreesLatitude=[CLLocation metersToLatitudeDelta:[FGSettings getRadiusHorizontal]];
		const CLLocationDegrees radiusDegreesLongitude=[CLLocation metersToLongitudeDelta:[FGSettings getRadiusHorizontal] atLatitude:coordinate.latitude];
		[grid addCoordinate:CLLocationCoordinate2DMake(coordinate.latitude+radiusDegreesLatitude, coordinate.longitude)];
		[grid addCoordinate:CLLocationCoordinate2DMake(coordinate.latitude-radiusDegreesLatitude, coordinate.longitude)];
		[grid addCoordinate:CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude+radiusDegreesLongitude)];
		[grid addCoordinate:CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude-radiusDegreesLongitude)];
	}

	return grid;
}

- (void)updateMapLocationWithAnimation:(BOOL)animated
{
	// if the user owns the scaling and panning then don't do a thing
	if(self->_regionOwner==FGRegionOwnerApp)
	{
		CLLocation *location=[[FGMotionProxy instance] currentLocation];
		if(location!=nil)
		{
			FGCoordinateGrid *grid=[self buildCoordinateGrid];

			// the possible are possible:
			//	1. There are no messages
			//	2. User is somewhere in the midst of our collection of messages
			//	3. User is not in the midst but all can be contained in a mapview
			//	4. Messages plus user are not within our normal bounds but we are willing to bend
			//	5. User is somewhere totally different
			if([grid isEmpty])
			{
				[self updateMapLocationWithAnimation:animated center:location.coordinate perimeterMultiplier:DEFAULT_MAP_SCALE];
			}
			else if([grid contains:location.coordinate])
			{
				const CLLocationCoordinate2D center=CLLocationCoordinate2DMake([grid verticalCenter], [grid horizontalCenter]);
				[self updateMapLocationWithAnimation:animated center:center perimeterMultiplier:DEFAULT_MAP_SCALE];
			}
			else
			{
				// add the phone's location to our grid and see whether we've got a reasonable grid
				[grid addCoordinate:location.coordinate];

				const SizeD defaultMapSizeInMeters=[self calculateMapSizeInMeters:1.0];
				const CLLocationDegrees defaultMapHeightInDegrees=[CLLocation metersToLatitudeDelta:defaultMapSizeInMeters.height];
				const CLLocationDegrees defaultMapWidthInDegrees=[CLLocation metersToLongitudeDelta:defaultMapSizeInMeters.width atLatitude:location.coordinate.latitude];
				const double scale=MAX(grid.width/defaultMapWidthInDegrees, grid.height/defaultMapHeightInDegrees)*MAP_SCALE_INCREMENT;

				if(scale<MAXIMUM_MAP_SCALE)
				{
					const CLLocationCoordinate2D center=CLLocationCoordinate2DMake([grid verticalCenter], [grid horizontalCenter]);
					[self updateMapLocationWithAnimation:animated center:center perimeterMultiplier:MAX(scale, DEFAULT_MAP_SCALE)];
				}
				else
				{
					[self updateMapLocationWithAnimation:animated center:location.coordinate perimeterMultiplier:DEFAULT_MAP_SCALE];
				}
			}
		}
	}
}

- (void)updateMapLocationWithAnimation:(BOOL)animated center:(CLLocationCoordinate2D)center perimeterMultiplier:(double)scale
{
	const SizeD sizeInMeters=[self calculateMapSizeInMeters:scale];
	const MKCoordinateSpan span=MKCoordinateSpanMake([CLLocation metersToLatitudeDelta:sizeInMeters.height],
													 [CLLocation metersToLongitudeDelta:sizeInMeters.width atLatitude:center.latitude]);
	const MKCoordinateRegion region=MKCoordinateRegionMake(center, span);

	// disengage ourselves while we update the region so that our handler doesn't pick it up
	[self.mapView setDelegate:nil];
	{
		[self.mapView setRegion:region animated:animated];
	}
	[self.mapView setDelegate:self];
}

- (void)updateAnnotations
{
	NSArray *messages=[FGMessageProxy getMessages];
	NSMutableArray *annotationsNew=[NSMutableArray arrayWithCapacity:[messages count]];
	
	// create annotations
	for(FGMessageVO *message in messages)
	{
		[annotationsNew addObject:[[FGMessageAnnotation alloc] initWithMessage:message]];
	}
	
	// cleaner replace - if the annotation hasn't changed then don't replace it
	for(int indexOld=[[self.mapView annotations] count]-1; indexOld>-1; indexOld--)
	{
		id<MKAnnotation> annotationOld=[[self.mapView annotations] objectAtIndex:indexOld];
		if([annotationOld isKindOfClass:[FGMessageAnnotation class]])
		{
			// note: indexOfObject uses isEqual
			int indexNew=[annotationsNew indexOfObject:annotationOld];
			if(indexNew!=NSNotFound)
			{
				[annotationsNew removeObjectAtIndex:indexNew];
				// in that this annotation is not being added our associated view will not update by default.
				[self updateMessagePostStatus:[(FGMessageAnnotation*)annotationOld message]
							   annotationView:(MKPinAnnotationView*)[self.mapView viewForAnnotation:annotationOld]];
			}
			else
			{
				[self.mapView removeAnnotation:annotationOld];
			}
		}
	}
	[self.mapView addAnnotations:annotationsNew];
}

- (void)updateOverlays
{
	CLLocation *location=[[FGSession instance] locationLastRetrieve];
	MKCircle *overlayOld=self->_coverageOverlay; self->_coverageOverlay=nil;

	// add new
	if(location!=nil)
	{
		self->_coverageOverlay=[MKCircle circleWithCenterCoordinate:location.coordinate radius:[FGSettings getRadiusHorizontal]];
		[self.mapView addOverlay:self->_coverageOverlay];
	}

	// get rid of old
	if(overlayOld!=nil)
	{
		[self.mapView removeOverlay:overlayOld];
	}
}

- (void)updateMessagePostStatus:(FGMessageVO*)message
{
	NSUInteger index=[[self.mapView annotations] indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
		if([obj isKindOfClass:[FGMessageAnnotation class]])
		{
			if([(FGMessageAnnotation*)obj message]==message)
			{
				*stop=YES;
			}
		}
		return *stop;
	}];
	if(index!=NSNotFound)
	{
		id<MKAnnotation> annotation=[[self.mapView annotations] objectAtIndex:index];
		MKPinAnnotationView *view=(MKPinAnnotationView*)[self.mapView viewForAnnotation:annotation];
		[self updateMessagePostStatus:message annotationView:view];
	}
}

/** Updates the view with current status of message.  Is okay if view is nil. **/
- (void)updateMessagePostStatus:(FGMessageVO*)message annotationView:(MKPinAnnotationView*)view
{
	if([message.status isEqualToString:FGMessageStatusError])
	{
		[view setPinColor:MKPinAnnotationColorRed];
	}
	else if([message.status isEqualToString:FGMessageStatusPost])
	{
		[view setPinColor:MKPinAnnotationColorPurple];
	}
	else
	{
		NSAssert([message.status isEqualToString:FGMessageStatusPosted], @"Unknown status");
		[view setPinColor:MKPinAnnotationColorGreen];
	}
}

#pragma mark - FGModalDelegate
- (void)modalCanceled:(id)modal
{
	[self dismissViewControllerAnimated:YES completion:nil];
	// and reset our lad
	self->_pendingSegueId=nil;
}

- (void)modalComplete:(id)modal
{
	[modal dismissViewControllerAnimated:YES completion:^(void)
	 {
		 if(self->_pendingSegueId!=nil)
		 {
			 [self performSegueSequence:self->_pendingSegueId completionSegue:nil];
		 }
	 }];
}

#pragma mark - MKMapViewDelegate messages
- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
	MKAnnotationView *view=nil;
	
	if([annotation isKindOfClass:[FGMessageAnnotation class]])
	{
		view=[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"FGStandard"];
		if(view==nil)
		{
			view=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"FGStandard"];
			[view setCanShowCallout:YES];
			[view setRightCalloutAccessoryView:[UIButton buttonWithType:UIButtonTypeDetailDisclosure]];
		}
		[self updateMessagePostStatus:[(FGMessageAnnotation*)annotation message] annotationView:(MKPinAnnotationView*)view];
		[view setAnnotation:annotation];
	}
	
	return view;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
	if([overlay isKindOfClass:[MKCircle class]])
	{
		MKCircleView *view=[[MKCircleView alloc] initWithCircle:(MKCircle*)overlay];
		[view setFillColor:[UIColor colorWithGLKVector4:FGMessageMapCoverageColorFill]];
		[view setStrokeColor:[UIColor colorWithGLKVector4:FGMessageMapCoverageColorStroke]];
		[view setLineWidth:4.0];

		return view;
	}

	NSFailWarn(@"What type?");
	return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
	if([view.annotation isKindOfClass:[FGMessageAnnotation class]])
	{
		FGMessageAnnotation *annotation=(FGMessageAnnotation*)view.annotation;
		NSString *segue=[self messageSelectionToSegue:annotation.message];
		[self performSegueWithIdentifier:segue sender:annotation.message];
	}
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
	// any time we set the region we disengage the delegate so that we can distinguish
	// user activity from our own.
	[self setRegionOwner:FGRegionOwnerUser];
}

#pragma mark - Action handlers
- (IBAction)handlePostMessage:(id)sender 
{
	if([FGLoginProxy isLoggedIn])
	{
		[self performSegueSequence:@"SeguePost" completionSegue:nil];
	}
	else
	{
		[self performSegueSequence:@"SegueLogin" completionSegue:@"SeguePost"];
	}
}

- (IBAction)handleUpdateOrientation:(id)sender
{
	[self setRegionOwner:FGRegionOwnerApp];
	[self updateMapLocationWithAnimation:YES];
}

#pragma mark - Notification handlers
- (void)handleLocationChanged:(NSNotification*)notification
{
	// make sure it's a major shift. Minor shifts are updated by the mapview.
	if([[notification userInfo] valueForKey:FGUserKeyLocationMajor])
	{
		[self updateMapLocationWithAnimation:YES];
	}
	else
	{
		// if the user's location is no longer on the map then update location.
		CLLocation *location=[notification object];
		// 6/2013 - changing to use point's location as we can more easily create a pixel boundary
		// (was slipping under edges and mappoints are difficult to translate).
		const CGPoint locationPoint=[self.mapView convertCoordinate:location.coordinate toPointToView:self.mapView];
		const CGRect locationBounds=CGRectMakeOutset(locationPoint.x, locationPoint.y, 10.0, 10.0);

		if(CGRectContainsRect(self.mapView.bounds, locationBounds)==NO)
		{
			[self updateMapLocationWithAnimation:YES];
		}
	}
}

- (void)handleMessageGetSucceeded:(NSNotification*)notification
{
	[self updateAnnotations];
	[self updateMapLocationWithAnimation:YES];
	[self updateOverlays];
}

- (void)handleMessagePost:(NSNotification*)notification
{
	[self.mapView addAnnotation:[[FGMessageAnnotation alloc] initWithMessage:[notification object]]];
}

- (void)handleMessagePosting:(NSNotification*)notification
{
	[self updateMessagePostStatus:[notification object]];
}

- (void)handleMessagePostSucceeded:(NSNotification*)notification
{
	[self updateMessagePostStatus:[notification object]];
}

- (void)handleMessagePostFailed:(NSNotification*)notification
{
	[self updateMessagePostStatus:[notification object]];
}

@end
