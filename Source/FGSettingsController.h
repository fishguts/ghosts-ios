//
//  FGSettingsController.h
//  Goggles
//
//  Created by Curtis Elsasser on 6/7/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"

@interface FGSettingsController : UITableViewController
/**** Public Properties ****/
@property (nonatomic, weak) id <FGModalDelegate> delegate;

@end
