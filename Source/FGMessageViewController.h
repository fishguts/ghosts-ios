//
//  FGMessageViewController.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FGMessageVO;


@interface FGMessageViewController : UIViewController
/**** properties ****/
@property (nonatomic, strong) FGMessageVO *message;


@end
