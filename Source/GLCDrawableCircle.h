//
//  GLCDrawableCircle.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "GLCDrawableElement.h"
#import <GLKit/GLKit.h>


@interface GLCDrawableCircle2D : NSObject<GLCDrawableElement>
/**** Public Properties ****/
@property (nonatomic) GLKVector2 origin;
@property (nonatomic) GLfloat radius;
@property (nonatomic) GLint divisions;
@property (nonatomic) GLfloat lineWidth;
@property (nonatomic) GLKVector4 borderColor;
@property (nonatomic) GLKVector4 fillColor;
@property (nonatomic) BOOL isDrawFill;
@property (nonatomic) BOOL isDrawBorder;

/**** Public Interface ****/
- (id)init;
- (id)initWithOrigin:(GLKVector2)origin radius:(GLfloat)radius divisions:(GLint)divisions fillColor:(const GLKVector4*)fillColor;
- (id)initWithOrigin:(GLKVector2)origin radius:(GLfloat)radius divisions:(GLint)divisions fillColor:(const GLKVector4*)fillColor borderColor:(const GLKVector4*)borderColor lineWidth:(GLfloat)lineWidth;

@end
