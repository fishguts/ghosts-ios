//
//  UIViewController+FGMessageView.h
//  Goggles
//
//  Created by Curtis Elsasser on 3/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FGMessageVO;

@interface UIViewController (MessageEditor)
/**** Public Interface ****/

/**
 * Decides whether the action with this message should be view
 * or edit (if in error). I was filtering on the current user
 * but am simplifying thinking that phones are single user and
 * that dead messages will not be able to be removed should a user
 * loose access to his account.
 */
- (NSString*)messageSelectionToSegue:(FGMessageVO*)message;

@end
