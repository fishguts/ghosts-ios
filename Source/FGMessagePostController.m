//
//  FGMessageController.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/14/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessagePostController.h"
#import "FGMessageVO.h"
#import "FGExpirationVO.h"
#import "FGModelFactory.h"
#import "FGSettings.h"
#import "FGConstants.h"


@interface FGMessagePostController()
{
	NSArray *_expirationObjects;
	FGExpirationVO *_expiration;
}
@property (weak, nonatomic) IBOutlet UITextView *textMessage;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellExpiration;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellError;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonCancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonPost;

/**** private interface ****/
- (void)messageToUI;
- (void)updateExpirationText;

/**** actions and observer handlers ****/
- (IBAction)handlePost:(id)sender;
- (IBAction)handleCancel:(id)sender;

@end

@implementation FGMessagePostController
@synthesize delegate;
@synthesize textMessage;
@synthesize buttonCancel;
@synthesize buttonPost;
@synthesize message=_message;

#pragma mark - Properties
- (NSString*)messageText
{
	return [self.textMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];	
}

- (void)setMessage:(FGMessageVO *)message
{
	self->_message=message;
	if(self->_message!=nil)
	{
		[self messageToUI];
		// tableview scroll problems only happen with pre-existing text
		self.tableView.delegate=self;
	}
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
	self->_expirationObjects=[FGSettings getExpirationObjects];
	self->_expiration=[FGSettings getExpirationDefault:self->_expirationObjects];

    [super viewDidLoad];

	if(self->_message!=nil)
	{
		[self messageToUI];
	}
	else
	{
		[self.textMessage becomeFirstResponder];
	}
	[self updateExpirationText];
}

- (void)viewDidUnload
{
	[self setButtonCancel:nil];
	[self setButtonPost:nil];
	[self setTextMessage:nil];
	[self setCellExpiration:nil];
	[self setCellExpiration:nil];
	[self setCellError:nil];
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([[segue identifier] isEqualToString:@"SegueExpiration"])
	{
		[segue.destinationViewController setDelegate:self];
		[segue.destinationViewController setTitle:@"Expiration"];
		[segue.destinationViewController setSelection:self->_expiration];
		[segue.destinationViewController setItems:self->_expirationObjects];
	}
}


#pragma mark - Private Interface
- (void)messageToUI
{
	[self.textMessage setText:self->_message.text];
	[[self.cellError textLabel] setText:self->_message.error];

	// update post button state through common channels
	[self textViewDidChange:self.textMessage];
}

- (void)updateExpirationText
{
	[self.cellExpiration.detailTextLabel setText:self->_expiration.text];
}


#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
	NSString *trimmed=[self messageText];
	[self.buttonPost setEnabled:[trimmed length]>=FGMinMessageLength];
}


#pragma mark - UITableViewDelegate and UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	if([self->_message.error length]>0)
	{
		return 3;
	}
	return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// we've got plenty of realestate when creating a new message to keep keyboard visible.
	// It's not the case when we are allowing a user to modify a message in error.
	if(self->_message)
	{
		[self.textMessage resignFirstResponder];
	}
	[self performSegueWithIdentifier:@"SegueExpiration" sender:nil];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[self tableView:tableView didSelectRowAtIndexPath:indexPath];
}


#pragma mark - FGSelectorDelegate
- (void)selectorComplete:(id)selector selection:(id)value
{
	self->_expiration=value;
	[FGSettings setExpirationDefault:value];

	[self updateExpirationText];
	[self smartDismissViewControllerAnimated:YES];
}

- (void)selectorCanceled:(id)selector
{
	[self smartDismissViewControllerAnimated:YES];
}


#pragma mark - UIScrollViewDelegate
/**
 * There is a framework bug that causes our view to scroll upon startup
 * We "snuff" them until any user interaction at which point we disengage.
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	if((scrollView==self.tableView)
	   && (scrollView.delegate==self))
	{
		scrollView.delegate=nil;
	}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if(scrollView==self.tableView)
	{
		[scrollView setContentOffset:CGPointMake(0, 0)];
	}
}

#pragma mark - Observers
- (IBAction)handlePost:(id)sender
{
	@try
	{
		FGMessageVO *message;
		if(self->_message!=nil)
		{
			message=self->_message;
			[FGModelFactory updateMessage:message text:[self messageText] expiration:self->_expiration];
		}
		else
		{
			message=[FGModelFactory createMessage:[self messageText] expiration:self->_expiration];
		}

		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessagePost object:message];
		[self.delegate modalComplete:self];
	}
	@catch (NSException *exception) 
	{
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:[exception description]]];
	}
}

- (IBAction)handleCancel:(id)sender 
{
	if(self.delegate)
	{
		[self.delegate modalCanceled:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (IBAction)handleDelete:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageDelete object:self.message];
	[self.delegate modalComplete:self];
}

@end
