//
//  FGStaticTableViewController.h
//  Goggles
//
//  Created by Curtis Elsasser on 1/2/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Adds support for dismissing.  That's it
 */
@interface FGStaticTableViewController : UITableViewController
/**** Public Properties ****/
@property (nonatomic, weak) id <FGModalDelegate> delegate;

@end
