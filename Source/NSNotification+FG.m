//
//  NSNotification+FGFactory.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSNotification+FG.h"
#import "FGConstants.h"


@implementation NSNotification (FG)
#pragma mark - Initializers
+ (id)notificationWithName:(NSString *)name object:(id)object text:(NSString*)text
{
	return [NSNotification notificationWithName:name object:object userInfo:[NSDictionary dictionaryWithObject:text forKey:@"text"]];
}

+ (id)notificationWithName:(NSString *)name object:(id)object text:(NSString*)text status:(FGStatusVO*)status
{
	NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
	[dictionary setValue:text forKey:@"text"];
	[dictionary setValue:status forKey:@"status"];
	return [NSNotification notificationWithName:name object:object userInfo:dictionary];
}

+ (id)notificationWithName:(NSString *)name object:(id)object value:(id)value text:(NSString*)text status:(FGStatusVO*)status
{
	NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
	[dictionary setValue:text forKey:@"text"];
	[dictionary setValue:status forKey:@"status"];
	[dictionary setValue:value forKey:@"value"];
	return [NSNotification notificationWithName:name object:object userInfo:dictionary];
}

+ (id)notificationWithName:(NSString*)name object:(id)object title:(NSString*)title message:(NSString*)message
{
	NSMutableDictionary *dictionary=[NSMutableDictionary dictionary];
	[dictionary setValue:title forKey:@"title"];
	[dictionary setValue:message forKey:@"message"];
	return [NSNotification notificationWithName:name object:object userInfo:dictionary];
}

+ (id)notificationWithName:(NSString*)name object:(id)object value:(id)value
{
	return [NSNotification notificationWithName:name object:object userInfo:[NSDictionary dictionaryWithObject:value forKey:@"value"]];
}

+ (id)debugNotificationWithObject:(id)object text:(NSString*)text
{
	NSMutableDictionary *userInfo=[NSMutableDictionary dictionary];
	[userInfo setValue:text forKey:@"text"];
	[userInfo setValue:[NSDate date] forKey:@"timestamp"];
	return [NSNotification notificationWithName:FGNotificationDebug object:object userInfo:userInfo];
}

+ (id)infoNotificationWithObject:(id)object text:(NSString*)text
{
	NSMutableDictionary *userInfo=[NSMutableDictionary dictionary];
	[userInfo setValue:text forKey:@"text"];
	[userInfo setValue:[NSDate date] forKey:@"timestamp"];
	return [NSNotification notificationWithName:FGNotificationInfo object:object userInfo:userInfo];
}

+ (id)warnNotificationWithObject:(id)object text:(NSString*)text
{
	NSMutableDictionary *userInfo=[NSMutableDictionary dictionary];
	[userInfo setValue:text forKey:@"text"];
	[userInfo setValue:[NSDate date] forKey:@"timestamp"];
	return [NSNotification notificationWithName:FGNotificationWarn object:object userInfo:userInfo];
}

+ (id)errorNotificationWithObject:(id)object text:(NSString*)text
{
	NSMutableDictionary *userInfo=[NSMutableDictionary dictionary];
	[userInfo setValue:text forKey:@"text"];
	[userInfo setValue:[NSDate date] forKey:@"timestamp"];
	return [NSNotification notificationWithName:FGNotificationError object:object userInfo:userInfo];
}

#pragma mark - Property like messages
- (NSString*)text
{
	return [[self userInfo] valueForKey:@"text"];
}

- (NSString*)title
{
	return [[self userInfo] valueForKey:@"title"];
}

- (NSString*)message
{
	return [[self userInfo] valueForKey:@"message"];
}

- (FGStatusVO*)status
{
	return [[self userInfo] valueForKey:@"status"];
}

- (NSDate*)timestamp
{
	return [[self userInfo] valueForKey:@"timestamp"];
}

- (id)value
{
	return [[self userInfo] valueForKey:@"value"];
}

@end
