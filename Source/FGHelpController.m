//
//  FGHelpController.m
//  Goggles
//
//  Created by Curtis Elsasser on 1/19/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGHelpController.h"
#import "FGSettings.h"


@interface FGHelpController ()
@property (weak, nonatomic) IBOutlet UITableViewCell *cellClientVersion;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellServerVersion;
@property (weak, nonatomic) IBOutlet UITextView *textViewWebAddress;

@end


@implementation FGHelpController
#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self updateVersions];

	// seems to be a little bug in framework - links are recognized but for some
	// reason this one is not.  I tried everything I could imagine and then ran
	// into some forum posts which corroborated my experience. Enough time wasted.
	[self.textViewWebAddress setDataDetectorTypes:UIDataDetectorTypeLink];
	[self.textViewWebAddress setText:@"http://xraymen.com"];
}

- (void)viewDidUnload
{
	[self setCellClientVersion:nil];
	[self setCellServerVersion:nil];
	[self setTextViewWebAddress:nil];
	[super viewDidUnload];
}

#pragma mark - Private entourage
- (void)updateVersions
{
	NSObject *versionClient=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];

	[[self.cellClientVersion detailTextLabel] setText:[versionClient description]];
	[[self.cellServerVersion detailTextLabel] setText:[FGSettings getServerVersion]];
}

#pragma mark - Observer handlers
- (IBAction)handleDone:(id)sender
{
	if(self.delegate)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

@end
