//
//  FGConstants.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

/**** application constants ****/
extern const NSTimeInterval FGDefaultSplashDuration;
extern const NSTimeInterval FGDefaultWelcomeDuration;
extern const int FGMinUserNameLength;
extern const int FGMinPasswordLength;
extern const int FGMinMessageLength;
extern const int FGMinFeedbackLength;
extern const float FGMotionTimerInterval;

extern const GLKVector4 FGMessageListPostingColor;
extern const GLKVector4 FGMessageListErrorColor;
extern const GLKVector4 FGMessageListPostedColorUser;
extern const GLKVector4 FGMessageListPostedColorWelcome;
extern const GLKVector4 FGMessageListPostedColorNotification;
extern const GLKVector4 FGMessageListPostedColorAdvertisement;

extern const GLKVector4 FGMessageMapCoverageColorFill;
extern const GLKVector4 FGMessageMapCoverageColorStroke;

extern const float FGMessageVideoScaleMin;
extern const float FGMessageVideoScaleMax;
extern const float FGMessageVideoAltitudeCompression;

extern const GLKVector4 FGHUDBackgroundColor;
extern const GLKVector4 FGHUDBorderColor;
extern const GLKVector4 FGHUDCameraColor;
extern const GLKVector4 FGHUDMessageColor;

/**** arc versions of constants/flags ****/
extern NSString *const ARC_STRING_YES;
extern NSString *const ARC_STRING_NO;


/**** notifications names ****/
extern NSString *const FGNotificationStartupConfigurationSucceeded;
extern NSString *const FGNotificationStartupConfigurationFailed;
extern NSString *const FGNotificationApplicationActive;
extern NSString *const FGNotificationApplicationResuming;
extern NSString *const FGNotificationApplicationSuspending;

/**** various notifications ****/
extern NSString *const FGNotificationUser;
extern NSString *const FGNotificationDebug;
extern NSString *const FGNotificationInfo;
extern NSString *const FGNotificationWarn;
extern NSString *const FGNotificationError;

/**** model IO notifications ****/
extern NSString *const FGNotificationDBOpening;
extern NSString *const FGNotificationDBOpenSucceeded;
extern NSString *const FGNotificationDBOpenFailed;
extern NSString *const FGNotificationDBSaving;
extern NSString *const FGNotificationDBSaveSucceeded;
extern NSString *const FGNotificationDBSaveFailed;
extern NSString *const FGNotificationDBCloseSucceeded;
extern NSString *const FGNotificationDBCloseFailed;

/**** model Model and environment update notifications ****/
extern NSString *const FGNotificationDBMessageStatusChanged;
extern NSString *const FGNotificationDBMessageUserRatingChanged;
extern NSString *const FGNotificationSettingListSortChanged;

extern NSString *const FGNotificationNetworkActivityStarting;
extern NSString *const FGNotificationNetworkActivityEnding;
extern NSString *const FGNotificationNetworkSending;
extern NSString *const FGNotificationNetworkSendSucceeded;
extern NSString *const FGNotificationNetworkSendFailed;
extern NSString *const FGNotificationNetworkSendCanceled;

extern NSString *const FGNotificationSettingsGet;
extern NSString *const FGNotificationSettingsGetting;
extern NSString *const FGNotificationSettingsGetSucceeded;
extern NSString *const FGNotificationSettingsGetFailed;
extern NSString *const FGNotificationSettingsChanged;

extern NSString *const FGNotificationLogin;
extern NSString *const FGNotificationLoggingIn;
extern NSString *const FGNotificationLoginSucceeded;
extern NSString *const FGNotificationLoginFailed;

extern NSString *const FGNotificationLogout;
extern NSString *const FGNotificationLoggingOut;
extern NSString *const FGNotificationLogoutSucceeded;

extern NSString *const FGNotificationRegister;
extern NSString *const FGNotificationRegistering;
extern NSString *const FGNotificationRegisterSucceeded;
extern NSString *const FGNotificationRegisterFailed;

extern NSString *const FGNotificationMessagePost;
extern NSString *const FGNotificationMessagePostRetry;
extern NSString *const FGNotificationMessagePosting;
extern NSString *const FGNotificationMessagePostSucceeded;
extern NSString *const FGNotificationMessagePostFailed;

extern NSString *const FGNotificationMessageRate;
extern NSString *const FGNotificationMessageRating;
extern NSString *const FGNotificationMessageRateSucceeded;
extern NSString *const FGNotificationMessageRateFailed;

extern NSString *const FGNotificationMessageFlag;
extern NSString *const FGNotificationMessageFlagging;
extern NSString *const FGNotificationMessageFlagSucceeded;
extern NSString *const FGNotificationMessageFlagFailed;

extern NSString *const FGNotificationMessagesGet;
extern NSString *const FGNotificationMessagesGetting;
extern NSString *const FGNotificationMessagesGetSucceeded;
extern NSString *const FGNotificationMessagesGetFailed;

extern NSString *const FGNotificationMessageDelete;
extern NSString *const FGNotificationMessageDeleting;
extern NSString *const FGNotificationMessageDeleteSucceeded;
extern NSString *const FGNotificationMessageDeleteFailed;

extern NSString *const FGNotificationFeedbackPost;
extern NSString *const FGNotificationFeedbackPosting;
extern NSString *const FGNotificationFeedbackPostSucceeded;
extern NSString *const FGNotificationFeedbackPostFailed;

extern NSString *const FGNotificationLocationRetain;
extern NSString *const FGNotificationLocationRelease;
extern NSString *const FGNotificationLocationChanged;

extern NSString *const FGNotificationOrientationRetain;
extern NSString *const FGNotificationOrientationRelease;
extern NSString *const FGNotificationOrientationChanged;

#if defined(DEBUG)
extern NSString *const FGNotificationTestMessage;
extern NSString *const FGNotificationTestStatus;
#endif


/**** notification user dictionary keys ****/
extern NSString *const FGUserKeyLocationMajor;
extern NSString *const FGUserKeyLocationMinor;


/***** network statuses ****/
extern NSString *const FGResponseStatusOkay;
extern NSString *const FGResponseStatusFail;
extern NSString *const FGResponseStatusAuth;


/***** message statuses ****/
extern NSString *const FGMessageStatusPost;
extern NSString *const FGMessageStatusPosted;
extern NSString *const FGMessageStatusError;

/**** message types ****/
extern NSString *const FGMessageTypeUser;
extern NSString *const FGMessageTypeWelcome;
extern NSString *const FGMessageTypeNotification;
extern NSString *const FGMessageTypeAdvertisement;
