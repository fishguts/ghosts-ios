//
//  FGAppDelegate.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGAppDelegate.h"
#import "FGBrain.h"
#import "FGDateRange.h"
#import "FGConstants.h"


@interface FGAppDelegate()
{
	int _networkActivityCount;
	NSDate *_timestampSuspended;
}

/**** private workers ****/
- (void)startup;
- (void)shutdown;
- (void)addObservers;
- (void)removeObservers;

/**** observers ****/
- (void)handleUserNotification:(NSNotification*)notification;
- (void)handleErrorNotification:(NSNotification*)notification;
- (void)handleNetworkActivityStarting:(NSNotification*)notification;
- (void)handleNetworkActivityEnding:(NSNotification*)notification;
@end


@implementation FGAppDelegate
@synthesize window = _window;

- (id)init
{
	NSLog(@"Version=%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]);
	
	self=[super init];
#if defined(TEST)
	#if !defined(DEBUG)
		#error "Tests turned on in non-DEBUG"
	#endif
#else
	[self startup];
#endif

	return self;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	NSDebugFL(@"Background");
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationApplicationSuspending object:self];
	self->_timestampSuspended=[NSDate date];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	NSDebugFL((self->_timestampSuspended==nil) ? @"Active" : @"Resuming");
	if(self->_timestampSuspended==nil)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationApplicationActive object:self];
	}
	else
	{
		FGDateRange *range=[FGDateRange dateRangeWithFrom:self->_timestampSuspended andTo:[NSDate date]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationApplicationResuming object:self value:range]];
	}
}

#pragma mark - Private workers
- (void)startup
{
	[self addObservers];
	[[FGBrain instance] startup];
}

- (void)shutdown
{
	[[FGBrain instance] shutdown];
	[self removeObservers];
}

- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	// we are the notification and error mediator. LogProxy handles the rest
	[center addObserver:self selector:@selector(handleUserNotification:) name:FGNotificationUser object:nil];
	[center addObserver:self selector:@selector(handleErrorNotification:) name:FGNotificationError object:nil];
	
	// network activity
	[center addObserver:self selector:@selector(handleNetworkActivityStarting:) name:FGNotificationNetworkActivityStarting object:nil];
	[center addObserver:self selector:@selector(handleNetworkActivityStarting:) name:FGNotificationNetworkSending object:nil];
	[center addObserver:self selector:@selector(handleNetworkActivityEnding:) name:FGNotificationNetworkActivityEnding object:nil];
	[center addObserver:self selector:@selector(handleNetworkActivityEnding:) name:FGNotificationNetworkSendSucceeded object:nil];
	[center addObserver:self selector:@selector(handleNetworkActivityEnding:) name:FGNotificationNetworkSendFailed object:nil];
	[center addObserver:self selector:@selector(handleNetworkActivityEnding:) name:FGNotificationNetworkSendCanceled object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - UIApplicationDelegate
 - (void)applicationWillTerminate:(UIApplication *)application
{
	[self shutdown];
}
 

#pragma mark - Observers
- (void)handleUserNotification:(NSNotification*)notification
{
	UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:[notification title] message:[notification message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
}

- (void)handleErrorNotification:(NSNotification*)notification
{
	UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:[notification text] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
}

 - (void)handleNetworkActivityStarting:(NSNotification*)notification
{
	self->_networkActivityCount++;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

 - (void)handleNetworkActivityEnding:(NSNotification*)notification
{
	NSAssert(self->_networkActivityCount>0, @"activity count %d", self->_networkActivityCount-1);
	if(--self->_networkActivityCount==0)
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
}

@end
