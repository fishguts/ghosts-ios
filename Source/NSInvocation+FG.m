//
//  NSInvocation+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSInvocation+FG.h"


@implementation NSInvocation (FG)

+ (NSInvocation*)invocationForNotificationPost:(NSNotificationCenter*)center notification:(NSNotification*)notification
{
	SEL selector=@selector(postNotification:);
	NSInvocation *invocation=[NSInvocation invocationWithMethodSignature:[center methodSignatureForSelector:selector]];

	// retain otherwise they suffer at the mercy of ARC (note that their pointers are saved and not managed arguments).
	[invocation retainArguments];
	[invocation setTarget:center];
	[invocation setSelector:selector];
	[invocation setArgument:&notification atIndex:2];
	
	return invocation;
}

@end
