//
//  FGLogProxy.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * Once started up this guy will listen for status notifications (error, warn, info, debug) and write them to our log.
 * He's smart - he will only track our log if there is a reason.  For example, in DEBUG mode where it can be observed.
 **/
@interface FGLogProxy : NSObject <FGSingleton>
{
	NSMutableArray *_alerts;
	NSURL *_fileUrl;
}

/**** singleton ****/
+ (FGLogProxy*)instance;

/**** public interface ****/
- (NSArray*)getAlerts;
- (void)clearAlerts;
- (void)saveAlerts;

@end
