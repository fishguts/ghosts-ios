//
//  FGGetMessagesService.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@class CLLocation;

@interface FGMessagesGetService : FGNetworkService
{
	CLLocation *_location;
	BOOL _replaceMessages;
	BOOL _welcomeState;
}

/**** public interface ****/
- (id)initWithLocation:(CLLocation*)location replaceMessages:(BOOL)replace welcomeState:(BOOL)welcome;

@end
