//
//  FGLocationFilterThreshold.m
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/12/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import "FGLocationFilterThreshold.h"

/**** Local Types and Defines ****/
#define Dump NSNoOutput


@interface FGLocationFilterThreshold ()
{
	CLLocation *_locationLastFiltered;
	CLLocation *_locationLastReceived;
}

@end

@implementation FGLocationFilterThreshold

#pragma mark - Public Interface
- (id)initWithMethod:(FGLocationFilterThresholdMethod)filterMethod horizontalThreshold:(CLLocationDegrees)thresholdH
{
	return [self initWithMethod:filterMethod horizontalThreshold:thresholdH verticalThreshold:DBL_MAX];
}

- (id)initWithMethod:(FGLocationFilterThresholdMethod)filterMethod horizontalThreshold:(CLLocationDegrees)thresholdH verticalThreshold:(CLLocationDegrees)thresholdV
{
	self=[super init];
	self->_filterMethod=filterMethod;
	self->_thresholdHorizontal=thresholdH;
	self->_thresholdVertical=thresholdV;
	self->_filterOutDuplicates=YES;
	return self;
}

- (void)reset:(BOOL)cascade
{
	[super reset:cascade];
	self->_locationLastFiltered=nil;
	self->_locationLastReceived=nil;
}

- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	const FGLocationChangeType measuredChangeType=[self measureChangeType:location];
	if(measuredChangeType!=FGLocationChangeTypeNone)
	{
		if(self->_filterMethod==FGLocationFilterThresholdMeasure)
		{
			[self.delegate locationFilter:self acceptLocation:location changeType:measuredChangeType];
		}
		else
		{
			NSAssert(self->_filterMethod==FGLocationFilterThresholdFilter, @"Method?");
			if(measuredChangeType==FGLocationChangeTypeMajor)
			{
				// note: we don't change the original type - we are only a filter
				[self.delegate locationFilter:self acceptLocation:location changeType:changeType];
			}
		}
	}
}

#pragma mark - Private Interface
- (FGLocationChangeType)measureChangeType:(CLLocation *)location
{
	FGLocationChangeType result=FGLocationChangeTypeMinor;
	if(self->_locationLastFiltered==nil)
	{
		self->_locationLastFiltered=location;
		result=FGLocationChangeTypeMajor;
	}
	else
	{
		// horizontal threshold
		const CLLocationDegrees deltaFilteredHorizontal=[self->_locationLastFiltered distanceFromLocation:location];
		if(deltaFilteredHorizontal>=self->_thresholdHorizontal)
		{
			Dump(@"[FGLocationFilterThreshold] horizontal exceeded=%f", deltaFilteredHorizontal);
			self->_locationLastFiltered=location;
			result=FGLocationChangeTypeMajor;
		}
		else
		{
			// vertical threshold
			const CLLocationDegrees deltaFilteredVertical=fabs(location.altitude-self->_locationLastFiltered.altitude);
			if(deltaFilteredVertical>=self->_thresholdVertical)
			{
				Dump(@"[FGLocationFilterThreshold] vertical exceeded=%f", deltaFilteredVertical);
				self->_locationLastFiltered=location;
				result=FGLocationChangeTypeMajor;
			}
			else
			{
				Dump(@"[FGLocationFilterThreshold] horizontal/vertical within=%f, %f", deltaFilteredHorizontal, deltaFilteredVertical);
				if(self->_filterOutDuplicates)
				{
					const CLLocationDegrees deltaPreviousVertical=fabs(location.altitude-self->_locationLastReceived.altitude);
					if(deltaPreviousVertical<=FLT_EPSILON)
					{
						const CLLocationDegrees deltaPreviousHorizontal=[self->_locationLastReceived distanceFromLocation:location];
						if(deltaPreviousHorizontal<=FLT_EPSILON)
						{
							result=FGLocationChangeTypeNone;
						}
					}
				}
			}
		}
	}
	self->_locationLastReceived=location;
	return result;
}



@end
