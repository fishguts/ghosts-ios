//
//  FGSettingsVO.h
//  Goggles
//
//  Created by Curtis Elsasser on 11/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FGSettingsVO : NSManagedObject

@property (nonatomic, retain) NSNumber * messagesPerQuery;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * queryIntervalMax;
@property (nonatomic, retain) NSNumber * queryIntervalMin;
@property (nonatomic, retain) NSNumber * radiusHorizontal;
@property (nonatomic, retain) NSNumber * radiusVertical;
@property (nonatomic, retain) NSString * serverVersion;

@end
