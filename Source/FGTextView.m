//
//  FGTextView.m
//  GraphicsPlayground
//
//  Created by Curtis Elsasser on 12/8/12.
//
//

#import "FGTextView.h"
#import <QuartzCore/QuartzCore.h>

@interface FGTextView ()
/**** Private Interface ****/
- (void)setDefaults;
@end


@implementation FGTextView
#pragma mark - Public Properties
- (UIColor*)borderColor
{
	return [UIColor colorWithCGColor:self.layer.borderColor];
}
- (void)setBorderColor:(UIColor *)color
{
	self.layer.borderColor=[color CGColor];
}

- (CGFloat)borderWidth
{
	return self.layer.borderWidth;
}
- (void)setBorderWidth:(CGFloat)width
{
	self.layer.borderWidth=width;
}

#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
	[self setDefaults];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self=[super initWithCoder:aDecoder];
	[self setDefaults];
	return self;
}

#pragma mark - Private Interface
- (void)setDefaults
{
	// note: background color must already be set at the layer level
	// because it is being set correctly.
    self.layer.cornerRadius=10.0;
	self.layer.masksToBounds=YES;
	// 1.1 vs. 1.0 - a bit of bolder border
	[self setBorderWidth:1.1];
	[self setBorderColor:[UIColor lightGrayColor]];
}

@end
