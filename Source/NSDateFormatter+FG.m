//
//  NSDateFormatter+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSDateFormatter+FG.h"

@implementation NSDateFormatter (FG)
+ (NSString*)stringFromDate:(NSDate*)date withFormat:(NSString*)format
{
	NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
	[formatter setDateFormat:format];
	
	return [formatter stringFromDate:date];
}

@end
