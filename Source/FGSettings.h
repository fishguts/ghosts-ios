//
//  FGDefaults.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#include "FGTypes.h"

@class FGSettingsVO;
@class FGExpirationVO;


@interface FGSettings : NSObject 
/**** Behavior Properties ****/
+ (void)settingsWithVO:(FGSettingsVO*)settings;

+ (NSTimeInterval)getMessageQueryIntervalMinimum;
+ (NSTimeInterval)getMessageQueryIntervalMaximum;
+ (NSTimeInterval)getMessageQueryIntervalForApplicationResume;
+ (double)getRadiusHorizontal;
+ (double)getRadiusVertical;
+ (NSString*)getServerVersion;

/**** Locally managed Properties ****/
+ (BOOL)getShowIntroduction;
+ (void)setShowIntroduction:(BOOL)value;

+ (BOOL)getRetrieveWelcomeMessages;
+ (void)setRetrieveWelcomeMessages:(BOOL)value;

+ (FGListSortType)getListSortMethod;
+ (void)setListSortMethod:(FGListSortType)type;

+ (NSArray*)getExpirationObjects;
+ (NSArray*)getExpirationObjects:(NSDate*)date;
+ (FGExpirationVO*)getExpirationDefault:(NSArray*)expirationObjects;
+ (void)setExpirationDefault:(FGExpirationVO*)expiration;

/**** Server communication Properties ****/
+ (NSString*)getServerHost:(BOOL)secure;
+ (NSString*)getUrlGetSettings;
+ (NSString*)getUrlGetMessages;
+ (NSString*)getUrlPostMessage;
+ (NSString*)getUrlRateMessage;
+ (NSString*)getUrlFlagMessage;
+ (NSString*)getUrlDeleteMessage;
+ (NSString*)getUrlLoginUser;
+ (NSString*)getUrlLogoutUser;
+ (NSString*)getUrlRegisterUser;
+ (NSString*)getUrlSendFeedback;
+ (NSString*)getUrlPrivacyPolicy;
+ (NSString*)getUrlTermsAndConditions;
@end
