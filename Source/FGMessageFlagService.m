//
//  FGMessageFlagService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageFlagService.h"
#import "FGStatusVO.h"
#import "FGMessageVO.h"
#import "FGModelSerializer.h"
#import "FGMessageProxy.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGMessageFlagService
#pragma mark - Public Interface
- (id)initWithMessage:(FGMessageVO *)message
{
	self=[super init];
	self->_message=message;
	
	return self;
}

- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageFlagging object:self->_message];
	@try 
	{
		NSString *url=[FGSettings getUrlFlagMessage];
		NSData *data=[FGModelSerializer buildFlagMessageRequest:self->_message pretty:NO];
		
		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		NSArray *messages=[FGModelSerializer parseMessagesResponse:data];
		if([messages count]==1)
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageFlagSucceeded object:[messages objectAtIndex:0]];
		}
		else
		{
			[self handleFailure:[FGStatusVO statusFromText:@"Improperly formatted response"]];
		}
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to flag message failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Flag message failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationMessageFlagFailed object:self->_message text:errorTextFriendly status:response]];
}



@end
