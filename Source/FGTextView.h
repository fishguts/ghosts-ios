//
//  FGTextView.h
//  GraphicsPlayground
//
//  Created by Curtis Elsasser on 12/8/12.
//
//

#import <UIKit/UIKit.h>

@interface FGTextView : UITextView
/**** Public Properties ****/
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic) CGFloat borderWidth;

@end
