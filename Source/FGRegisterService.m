//
//  FGRegisterService.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/28/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGRegisterService.h"
#import "FGStatusVO.h"
#import "FGModelSerializer.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGRegisterService
#pragma mark - Public Interface
- (id)initWithProfile:(FGProfileVO *)profile
{
	self=[super init];
	self->_profile=profile;
	
	return self;
}

- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationRegistering object:self->_profile];
	@try
	{
		NSString *url=[FGSettings getUrlRegisterUser];
		NSData *data=[FGModelSerializer buildRegisterRequest:self->_profile pretty:NO];
		
		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception)
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected Handlers
- (void)handleSuccess:(NSData*)data
{
	@try
	{
		[FGModelSerializer processRegisterResponse:data];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationRegisterSucceeded object:self->_profile];
	}
	@catch (NSException *exception)
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to register failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Register failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationRegisterFailed object:self->_profile text:errorTextFriendly status:response]];
}

@end
