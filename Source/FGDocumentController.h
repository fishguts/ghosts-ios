//
//  FGDocumentController.h
//  Legal
//
//  Created by Curtis Elsasser on 12/3/12.
//  Copyright (c) 2012 Curtis Elsasser. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Presents a "remote" document (url request) with a fail safe backup.  The idea
 * is to present an updatable remote version if more recent (exists) otherwise
 * to resort to safe local version
 **/
@interface FGDocumentController : UIViewController <UIWebViewDelegate>
/**** Public Interface ****/
@property (nonatomic, strong) NSURLRequest *urlRemoteRequest;
@property (nonatomic, strong) NSURLRequest *urlFailSafeRequest;
@property (nonatomic, weak) id<FGModalDelegate> delegate;


@end
