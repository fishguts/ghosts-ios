//
//  FGLocationFilterAccuracy.h
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/12/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "FGLocationFilterBase.h"


/**
 * Filters on accuracy.  There are two types of filtering:
 *	1. Threshold - allows first location, improvements and those below threshold through.
 *	2. Timer/Threshold - allows first location plus the best of each timed interval
 **/
@interface FGLocationFilterAccuracy : FGLocationFilterBase
/**** Properties ****/
@property (nonatomic) CLLocationAccuracy thresholdHorizontal;
@property (nonatomic) CLLocationAccuracy thresholdVertical;
@property (nonatomic) NSTimeInterval intervalWait;

/**** Initialization Interface ****/
- (id)init;
- (id)initWithThreshold:(CLLocationAccuracy)threshold;
- (id)initWithHorizontalThreshold:(CLLocationAccuracy)horizontal verticalThreshold:(CLLocationAccuracy)vertical;
- (id)initWithWaitInterval:(NSTimeInterval)waitInterval Threshold:(CLLocationAccuracy)threshold;
- (id)initWithWaitInterval:(NSTimeInterval)waitInterval HorizontalThreshold:(CLLocationAccuracy)horizontal verticalThreshold:(CLLocationAccuracy)vertical;

@end
