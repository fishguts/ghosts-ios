//
//  FGGhostProxy.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"
#import "FGMessageVO.h"

@class FGCoordinateGrid;

@interface FGMessageProxy : FGStaticClass
/**** <#Public Interface#> ****/
+ (NSArray*)getMessages;
+ (NSArray*)getMessagesWithStatus:(NSString*)status;
+ (FGCoordinateGrid*)getMessagesCoordinateGrid;

/* if different from the current status, sets the messages status and dispatches notification */
+ (void)setMessage:(FGMessageVO*)message status:(NSString*)status;
+ (void)setMessage:(FGMessageVO*)message status:(NSString*)status errorText:(NSString*)error;

@end

