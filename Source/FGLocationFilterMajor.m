//
//  FGLocationFilterMajor.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLocationFilterMajor.h"

@interface FGLocationFilterMajor ()
{
	CLLocation *_locationLastFiltered;
}

@end

@implementation FGLocationFilterMajor

#pragma mark - Public Interface
- (void)reset:(BOOL)cascade
{
	[super reset:cascade];
	self->_locationLastFiltered=nil;
}

- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	if((self->_locationLastFiltered==nil)
	   || (changeType==FGLocationChangeTypeMajor))
	{
		self->_locationLastFiltered=location;
		[self.delegate locationFilter:self acceptLocation:location changeType:changeType];
	}
}

@end
