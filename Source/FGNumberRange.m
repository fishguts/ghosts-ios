//
//  FGNumberRange.m
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/11/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import "FGNumberRange.h"

@implementation FGNumberRange
#pragma mark - Properties
- (NSNumber*)min
{
	return self->_min;
}
- (NSNumber*)max
{
	return self->_max;
}

#pragma mark - Public Interface
- (void)addNumber:(NSNumber*)value
{
	if(self->_min==nil)
	{
		NSAssertNil(self->_max);
		self->_min=value;
		self->_max=value;
	}
	else if([self->_min compare:value]==NSOrderedDescending)
	{
		self->_min=value;
	}
	else if([self->_max compare:value]==NSOrderedAscending)
	{
		self->_max=value;
	}
}

- (void)addInt:(int)value
{
	[self addNumber:[NSNumber numberWithInt:value]];
}

- (void)addDouble:(double)value
{
	[self addNumber:[NSNumber numberWithDouble:value]];
}


#pragma mark - Double methods
- (double)getDoubleRange
{
	const double min=[self->_min doubleValue];
	const double max=[self->_max doubleValue];
	return max-min;
}

- (double)getDoubleMid
{
	const double min=[self->_min doubleValue];
	const double max=[self->_max doubleValue];
	return min+(max-min)/2;
}

- (double)getDoubleOffsetFromMin:(double)value
{
	const double min=[self->_min doubleValue];
	return value-min;
}

- (double)getDoubleOffsetFromMax:(double)value
{
	const double max=[self->_max doubleValue];
	return value-max;
}

- (double)getDoubleRatioFromMin:(double)value
{
	const double min=[self->_min doubleValue];
	const double max=[self->_max doubleValue];
	const double delta=max-min;

	NSAssertWarn(delta>0, @"Ratio not valid when no delta");
	return (delta!=0.0) ? (value-min)/delta : 0.0;
}

- (double)getDoubleRatioFromMax:(double)value
{
	return 1.0-[self getDoubleRatioFromMin:value];
}

@end