//
//  FGLoginProxy.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/18/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"
#import "FGProfileVO.h"

@class FGMessageVO;

@interface FGLoginProxy : FGStaticClass
/**** Public Interface ****/

/* Is somebody currently logged in? */
+ (BOOL)isLoggedIn;

/* Get the current login */
+ (FGProfileVO*)getLogin;

/* Set the current login profile (different from logged in) */
+ (void)setLogin:(FGProfileVO*)profile;

/* Looks in DB for a default. If found this guy initiates a login and returns YES. Otherwise returns NO */
+ (BOOL)loginIfPossible;

/* If a current login exists, this guy initiates a logout and returns YES. Otherwise returns NO */
+ (BOOL)logoutIfPossible;


+ (BOOL)canLoginDeleteMessage:(FGMessageVO*)message;

@end
