//
//  FGTableViewSelectionController.h
//  Goggles
//
//  Created by Curtis Elsasser on 12/8/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * Assumes an array if objects that have a 'text' property.
 **/
@interface FGListSelectionController : UITableViewController <FGSelectorInterface>
/**** Public Properties ****/
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) id selection;
@property (nonatomic, weak) id <FGSelectorDelegate> delegate;

@end
