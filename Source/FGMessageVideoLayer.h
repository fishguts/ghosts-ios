//
//  FGMessageVideoLayer.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/3/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FGConversionSphere.h"

@class FGVector3;
@class FGMessageVO;


@interface FGMessageVideoLayer : CAEAGLLayer <FGDispose>
/**** Public Properties ****/
@property (nonatomic, strong) NSArray *messages;
@property (nonatomic, readonly) CLLocation *location;
@property (nonatomic, strong) FGVector3 *orientation;
@property (nonatomic) GLfloat scale;

/* Sets the location.  The idea behind rebuilding the sphere is to make sure our calculations do not get wildly off.
 * As we know latitude calculations are based on the current longitude. Our sphere uses the current latitude.  For the
 * better part people will not be moving, nonetheless it can happen and this is our way of staying on top of it.
 */
- (void)setLocation:(CLLocation *)location rebuildSphere:(BOOL)rebuild;

/**** Public Interface ****/
- (id)initWithFrame:(CGRect)frame;

- (FGMessageVO*)hitTestForMessage:(CGPoint)point;

#if DEBUG
- (void)dump;
#endif

@end
