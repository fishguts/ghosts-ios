//
//  FGMotionFilterBase.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/29/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSObject.h>
#import <GLKit/GLKVector3.h>
#import "FGTypes.h"

@class FGMotionFilterBase;

/**** protocols ****/
@protocol FGMotionFilterDelegate <NSObject>
- (void)motionFilter:(FGMotionFilterBase*)filter acceptOrientation:(GLKVector3)orientation;
@end



/**
 * FGMotionFilterBase - base class for all location filters.
 */
@interface FGMotionFilterBase : NSObject <FGDispose, FGMotionFilterDelegate>
/**** Properties ****/
@property (nonatomic, strong) id<FGMotionFilterDelegate> delegate;
@property (nonatomic, readonly) FGMotionFilterBase *nextFilter;
@property (nonatomic, readonly) FGMotionFilterBase *lastFilter;

/**** Interface ****/
+ (FGMotionFilterBase*)createFilterChainWithDelegates:(NSArray*)delegates;

- (void)addOrientation:(GLKVector3)orientation;
- (void)reset:(BOOL)cascade;


@end
