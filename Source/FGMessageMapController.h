//
//  FGMessageMapController.h
//  Goggles
//
//  Created by Curtis Elsasser on 8/13/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface FGMessageMapController : UIViewController <FGModalDelegate, MKMapViewDelegate>

@end
