//
//  FGSerialization.m
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSerializer.h"


@implementation FGSerializer

+ (NSData*)dataToJSON:(id)object pretty:(BOOL)pretty
{
	NSData *json;
	NSError *error;
	@try 
	{
		NSJSONWritingOptions options=(pretty) ? NSJSONWritingPrettyPrinted : 0;
		json=[NSJSONSerialization dataWithJSONObject:object options:options error:&error];
		if(json==nil)
		{
			@throw [NSException exceptionWithName:@"SerializationException" reason:[error localizedDescription] userInfo:nil];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *text=[NSString stringWithFormat:@"Failed to serialize object to JSON: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:text]];
		// let 'em know
		@throw exception;
	}
	return json;
}

+ (id)jsonToData:(NSData*)json
{
	id data;
	NSError *error;
	@try 
	{
		data=[NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:&error];
		if(data==nil)
		{
			@throw [NSException exceptionWithName:@"SerializationException" reason:[error localizedDescription] userInfo:nil];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *text=[NSString stringWithFormat:@"Failed to serialize object from JSON: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:text]];
		// let 'em know
		@throw exception;
	}
	return data;

}

@end

@implementation FGSerializeUtils
/*** local globals ****/
static NSDateFormatter *dateFormatter;

+ (void)initialize
{
	dateFormatter=[[NSDateFormatter alloc] init];
	[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
	[dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss"];
}

+ (void)addValue:(id)value toDictionary:(NSMutableDictionary *)dictionary withKey:(NSString*)key
{
	if(value!=nil)
	{
		if([value isKindOfClass:[NSDate class]])
		{
			value=[FGSerializeUtils dateToString:value];
		}
		[dictionary setObject:value forKey:key];
	}
}

+ (void)addValue:(id)value toArray:(NSMutableArray *)array
{
	if(value!=nil)
	{
		if([value isKindOfClass:[NSDate class]])
		{
			value=[FGSerializeUtils dateToString:value];
		}
		[array addObject:value];
	}
}

+ (NSString*)dateToString:(NSDate*)date
{
	return [dateFormatter stringFromDate:date];
}

+ (NSDate*)stringToDate:(NSString*)text
{
	return [dateFormatter dateFromString:text];
}

@end