//
//  FGTimer.m
//  Goggles
//
//  Created by Curtis Elsasser on 1/13/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTimer.h"

@implementation FGTimer
#pragma mark - Instantiation
+ (FGTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval target:(id)target selector:(SEL)selector userInfo:(id)userInfo repeats:(BOOL)repeats
{
	FGTimer *timer=[[FGTimer alloc] init];
	timer->_interval=interval;
	timer->_target=target;
	timer->_selector=selector;
	timer->_userInfo=userInfo;
	timer->_repeats=repeats;
	timer->_valid=YES;

	[timer startTickCycle];

	return timer;
}

#pragma mark - Public Propertie
- (NSTimeInterval)timeInterval
{
	return self->_interval;
}
- (id)userInfo
{
	return self->_userInfo;
}

- (NSDate *)fireDate
{
	NSAssertWarn([self isActive], @"Timer needs to be active");
	return [self->_timer fireDate];
}

- (NSTimeInterval)fireInterval
{
	NSDate *fire=[self fireDate];
	return [fire timeIntervalSinceDate:[NSDate date]];
}

#pragma mark - Public Interface
- (void)invalidate
{
	[self->_timer invalidate];
	self->_valid=NO;
	self->_timer=nil;
}

- (BOOL)isValid
{
	return self->_valid;
}

- (BOOL)isActive
{
	return [self->_timer isValid];
}

- (void)suspend
{
	if(self->_timer)
	{
		[self->_timer invalidate]; self->_timer=nil;
		self->_remaining-=[[NSDate date] timeIntervalSinceDate:self->_startTime];
		NSAssertWarn(self->_remaining>=0, @"Has timer fired - %f?", self->_remaining);
	}
}

- (void)resume
{
	NSAssertWarn(self->_valid, @"Resuming when invalid?");
	if(self->_valid
	   && (self->_timer==nil))
	{
		self->_startTime=[NSDate date];
		self->_timer=[NSTimer scheduledTimerWithTimeInterval:MAX(0, self->_remaining) target:self selector:@selector(handleTimerTick:) userInfo:nil repeats:self->_repeats];
	}
}

- (void)fire
{
	NSAssertWarn(self->_valid, @"Still firing?");
	if(self->_valid)
	{
		// do validation stuff so that we cannot re-enter
		if(self->_repeats==NO)
		{
			// as per documentation - once fired (for non repeating timers), this is it!
			[self invalidate];
		}

		// not sure why, but ARC issues a warning on this guy
		#pragma clang diagnostic push
		{
			#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
			[self->_target performSelector:self->_selector withObject:self];
		}
		#pragma clang diagnostic pop

		if(self->_repeats && self->_valid)
		{
			[self startTickCycle];
		}
	}
}

#pragma mark - Private Interface
- (void)startTickCycle
{
	// make sure it's not ticking
	[self->_timer invalidate];

	// start full cycle
	self->_remaining=self->_interval;
	self->_startTime=[NSDate date];
	self->_timer=[NSTimer scheduledTimerWithTimeInterval:self->_remaining target:self selector:@selector(handleTimerTick:) userInfo:nil repeats:self->_repeats];
}

#pragma mark - Observer handlers
- (void)handleTimerTick:(NSTimer*)timer
{
	[self fire];
}

@end
