//
//  FGMessageDeleteService.m
//  Goggles
//
//  Created by Curtis Elsasser on 3/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageDeleteService.h"
#import "FGModel.h"
#import "FGModelSerializer.h"
#import "FGMessageVO.h"
#import "FGStatusVO.h"
#import "FGSettings.h"
#import "FGConstants.h"


@interface FGMessageDeleteService()
/**** Private Interface ****/
- (void)performDeleteLocal;
- (void)performDeleteRemote;

@end

@implementation FGMessageDeleteService
#pragma mark - Public Interface
- (id)initWithMessage:(FGMessageVO *)message
{
	self=[super init];
	self->_message=message;

	return self;
}

- (void)send
{
	// note: message is considered alive and fine until it's successfully deleted on the server hence no status change
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageDeleting object:self->_message];
	if(self->_message.id)
	{
		[self performDeleteRemote];
	}
	else
	{
		[self performDeleteLocal];
	}
}


#pragma mark - Private interface
- (void)performDeleteLocal
{
	// "simulate" these guys so as to trigger the universe that we are done
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSending object:self];
	@try
	{
		[[[FGModel instance] context] deleteObject:self->_message];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageDeleteSucceeded object:self->_message];
	}
	@catch (NSException *exception)
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkSendSucceeded object:self];
}

- (void)performDeleteRemote
{
	@try
	{
		NSString *url=[FGSettings getUrlDeleteMessage];
		NSData *data=[FGModelSerializer buildDeleteMessageRequest:self->_message pretty:NO];

		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception)
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try
	{
		// remove him from our model
		[[[FGModel instance] context] deleteObject:self->_message];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageDeleteSucceeded object:self->_message];
	}
	@catch (NSException *exception)
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to delete message failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Delete message failed: %@", [response description]];

	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationMessageDeleteFailed object:self->_message text:errorTextFriendly status:response]];
}

@end
