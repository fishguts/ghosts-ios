//
//  UIColor+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 11/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "UIColor+FG.h"

@implementation UIColor (FG)

+ (UIColor*)colorWithGLKVector4:(GLKVector4)vector
{
	return [[UIColor alloc] initWithGLKVector4:vector];
}

- (id)initWithGLKVector4:(GLKVector4)vector
{
	self=[self initWithRed:vector.r green:vector.g blue:vector.b alpha:vector.a];
	return self;
}

@end
