//
//  FGStaticTableViewController.m
//  Goggles
//
//  Created by Curtis Elsasser on 1/2/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticTableViewController.h"

@implementation FGStaticTableViewController

- (IBAction)handleDone:(id)sender
{
	if(self.delegate)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

@end
