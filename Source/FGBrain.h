//
//  FGBrain.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FGBrain : NSObject <FGSingleton>
/**** singleton ****/
+ (FGBrain*)instance;


/**** properties ****/
@property (nonatomic, readonly) BOOL isLoggingIn;
@property (nonatomic, readonly) BOOL isLoggingOut;
@property (nonatomic, readonly) BOOL isFetchingMessages;


/**** public interface ****/
/* suspends all initiation of message based internet traffic */
- (void)sleep;

/**
 * resumes and processes pending state of internet traffic
 * @param reset: YES to cancel all pending requests and restart message monitor cycle
 */
- (void)wakeup:(BOOL)reset;

@end
