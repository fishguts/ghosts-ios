//
//  FGResponseVO.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGStatusVO : NSObject
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *text;

+ (FGStatusVO*)statusFromText:(NSString*)text;
+ (FGStatusVO*)statusFromText:(NSString*)text code:(NSString*)code;

- (BOOL)isInternal;
@end
