//
//  FGMotionTypes.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <CoreLocation/CLLocation.h>
#import "FGLocationFilterBase.h"


@implementation FGLocationFilterBase
@synthesize delegate=_delegate;

- (FGLocationFilterBase*)nextFilter
{
	if([self.delegate isKindOfClass:[FGLocationFilterBase class]])
	{
		return (FGLocationFilterBase*)self.delegate;
	}
	return nil;
}

- (FGLocationFilterBase*)lastFilter
{
	FGLocationFilterBase *next=[self nextFilter];
	return (next) ? [next lastFilter] : self;
}


#pragma mark - Public Interface
+ (FGLocationFilterBase*)createFilterChainWithDelegates:(NSArray*)delegates
{
	for(int index=1; index<[delegates count]; index++)
	{
		[[delegates objectAtIndex:index-1] setDelegate:[delegates objectAtIndex:index]];
	}
	return [delegates objectAtIndex:0];
}

- (void)dispose
{
	[[self nextFilter] dispose];
}

- (void)reset:(BOOL)cascade
{
	if(cascade)
	{
		[[self nextFilter] reset:cascade];
	}
}

- (void)addLocation:(CLLocation*)location changeType:(FGLocationChangeType)changeType
{
	[self locationFilter:self acceptLocation:location changeType:changeType];
}


#pragma mark - FGMotionFilterDelegate
- (void)locationFilter:(FGLocationFilterBase*)filter acceptLocation:(CLLocation*)location changeType:(FGLocationChangeType)changeType
{
	NSWarnFL(@"Base implementation being called");
	NSAssertWarn(self.delegate!=nil, @"Delegate nil");

	[self.delegate locationFilter:self acceptLocation:location changeType:changeType];
}

@end


