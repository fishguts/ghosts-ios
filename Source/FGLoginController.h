//
//  FGAccountController.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/14/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FGLoginController : UIViewController
/**** properties ****/
@property (nonatomic, weak) id <FGModalDelegate> delegate;

@end
