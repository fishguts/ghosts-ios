//
//  FGModelViews.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGModelQueries.h"
#import "FGModel.h"
#import "FGProfileVO.h"
#import "FGConstants.h"


@interface FGModelQueries()
+ (void)throwFetchException:(NSString*)text;
@end


@implementation FGModelQueries

#pragma mark - Public interface

+ (NSArray*)fetchProfiles
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Profile"];
	NSArray *results=[context executeFetchRequest:request error:&error];
	
	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch profiles failed: %@", [error localizedDescription]]];
	}
	
	return results;
}

+ (NSArray*)fetchLogins
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Profile"];
	[request setPredicate:[NSPredicate predicateWithFormat:@"login==TRUE"]];
	NSArray *results=[context executeFetchRequest:request error:&error];

	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch logins failed: %@", [error localizedDescription]]];
	}

	return results;
}

+ (FGProfileVO*)fetchLogin
{
	NSArray *results=[FGModelQueries fetchLogins];
	NSAssert([results count]<=1, @"Should only be one profile flagged as login");
	return ([results count]==1) ? [results objectAtIndex:0] : nil;
}

+ (FGProfileVO*)fetchProfileByName:(NSString*)username
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Profile"];
	[request setPredicate:[NSPredicate predicateWithFormat:@"username==%@", username]];
	NSArray *results=[context executeFetchRequest:request error:&error];
	
	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch profile failed: %@", [error localizedDescription]]];
	}
	
	NSAssert([results count]<=1, @"Should only be one profile per name: '%@'", username);
	return ([results count]==1) ? [results objectAtIndex:0] : nil;
}

+ (NSArray*)fetchMessages
{
	return [FGModelQueries fetchMessagesWithStatus:nil];
}

+ (NSArray*)fetchMessagesWithStatus:(NSString*)status
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Message"];
	if(status!=nil)
	{
		[request setPredicate:[NSPredicate predicateWithFormat:@"status==%@", status]];
	}
	NSArray *results=[context executeFetchRequest:request error:&error];
	
	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch messages failed: %@", [error localizedDescription]]];
	}
	
	return results;
}

+ (FGMessageVO*)fetchMessageById:(NSNumber*)id
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Message"];
	[request setPredicate:[NSPredicate predicateWithFormat:@"id==%@", id]];
	NSArray *results=[context executeFetchRequest:request error:&error];
	
	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch message failed: %@", [error localizedDescription]]];
	}
	
	NSAssert([results count]<=1, @"Should only be one message: '%@'", id);
	return ([results count]==1) ? [results objectAtIndex:0] : nil;
}


+ (NSArray*)fetchSettings
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Settings"];
	NSArray *results=[context executeFetchRequest:request error:&error];
	
	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch settings failed: %@", [error localizedDescription]]];
	}
	
	return results;
}

+ (FGSettingsVO*)fetchSettingByName:(NSString*)name
{
	NSError *error;
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"Settings"];
	[request setPredicate:[NSPredicate predicateWithFormat:@"name==%@", name]];
	NSArray *results=[context executeFetchRequest:request error:&error];
	
	if(error!=nil)
	{
		[FGModelQueries throwFetchException:[NSString stringWithFormat:@"Attempt to fetch setting failed: %@", [error localizedDescription]]];
	}
	
	NSAssert([results count]<=1, @"Should only be one setting: '%@'", name);
	return ([results count]==1) ? [results objectAtIndex:0] : nil;
}


#pragma mark - Private interface
+ (void)throwFetchException:(NSString*)text
{
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:text]];
	@throw [NSException exceptionWithName:@"FetchException" reason:text userInfo:nil];
}

@end
