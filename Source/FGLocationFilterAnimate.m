//
//  FGLocationFilterAnimate.m
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/10/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import <Foundation/NSTimer.h>
#import "FGLocationFilterAnimate.h"


@interface FGLocationFilterAnimate ()
{
	FGLocationChangeType _changeTypeLastTarget;
	CLLocation *_locationLastTarget;
	CLLocation *_locationLastAnimation;
	CLLocation *_locationAnimationBegin;
	CLLocation *_locationAnimationEnd;
	NSTimer *_timerAnimation;
	double _timerTick;
}

/**** Private Interface ****/
- (void)handleAnimationTimerTick:(NSTimer*)timer;

@end


@implementation FGLocationFilterAnimate
#pragma mark - Properties
@synthesize animationIntervals=_animationIntervals;
@synthesize animationDuration=_animationDuration;

- (double)animationInterval
{
	return self->_animationDuration/(double)self->_animationIntervals;
}

#pragma mark - Public Interface
- (id)init
{
	return [self initWithAnimationIntervals:15 overDuration:1.0];
}

- (id)initWithAnimationIntervals:(int)intervals overDuration:(double)duration
{
	NSAssert(intervals>0, @"Interval count?");
	NSAssert(duration>0, @"Duration?");

	self=[super init];
	self->_animationIntervals=intervals;
	self->_animationDuration=duration;
	return self;
}

- (void)dispose
{
	[self reset:NO];
	[super dispose];
}

- (void)reset:(BOOL)cascade
{
	[super reset:cascade];
	[self->_timerAnimation invalidate];
	self->_timerAnimation=nil;
	self->_locationLastTarget=nil;
	self->_locationLastAnimation=nil;
	self->_locationAnimationBegin=nil;
	self->_locationAnimationEnd=nil;
}

- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	[self locationFilter:self acceptLocation:location changeType:changeType withAnimation:YES];
}

- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType withAnimation:(BOOL)animate
{
	// reset an existing animation first
	[self->_timerAnimation invalidate];
	self->_timerAnimation=nil;

	if((self->_locationLastTarget==nil)
	   || (animate==NO))
	{
		self->_locationLastAnimation=nil;
		self->_locationLastTarget=location;
		self->_changeTypeLastTarget=changeType;
		[self.delegate locationFilter:self acceptLocation:location changeType:changeType];
	}
	else
	{
		// essentially we are picking up from wherever we left off.
		if(self->_locationLastAnimation)
		{
			self->_locationAnimationBegin=self->_locationLastAnimation;
		}
		else
		{
			self->_locationAnimationBegin=self->_locationLastTarget;
		}
		self->_locationAnimationEnd=location;
		self->_locationLastTarget=location;
		self->_changeTypeLastTarget=changeType;

		self->_timerTick=0.0;
		self->_timerAnimation=[NSTimer scheduledTimerWithTimeInterval:self.animationInterval target:self selector:@selector(handleAnimationTimerTick:) userInfo:nil repeats:YES];
	}
}

#pragma mark - Observer handlers
- (void)handleAnimationTimerTick:(NSTimer*)timer
{
	self->_timerTick++;

	const double ratio=self->_timerTick/self->_animationIntervals;
	const double timeElapsed=ratio*self->_animationDuration;
	const CLLocationCoordinate2D beginCoordinate=[self->_locationAnimationBegin coordinate];
	const CLLocationDegrees beginAltitude=[self->_locationAnimationBegin altitude];
	const CLLocationDegrees beginAccuracyHorizontal=[self->_locationAnimationBegin horizontalAccuracy];
	const CLLocationDegrees beginAccuracyVertical=[self->_locationAnimationBegin horizontalAccuracy];
	const CLLocationCoordinate2D endCoordinate=[self->_locationAnimationEnd coordinate];
	const CLLocationDegrees endAltitude=[self->_locationAnimationEnd altitude];
	const CLLocationDegrees endAccuracyHorizontal=[self->_locationAnimationEnd horizontalAccuracy];
	const CLLocationDegrees endAccuracyVertical=[self->_locationAnimationEnd horizontalAccuracy];

	// linear interpolation. todo: might be nice with ease in and out
	const CLLocationDegrees latitude=beginCoordinate.latitude+(endCoordinate.latitude-beginCoordinate.latitude)*ratio;
	const CLLocationDegrees longitude=beginCoordinate.longitude+(endCoordinate.longitude-beginCoordinate.longitude)*ratio;
	const CLLocationDegrees altitude=beginAltitude+(endAltitude-beginAltitude)*ratio;
	const CLLocationDegrees accuracyHorizontal=beginAccuracyHorizontal+(endAccuracyHorizontal-beginAccuracyHorizontal)*ratio;
	const CLLocationDegrees accuracyVertical=beginAccuracyVertical+(endAccuracyVertical-beginAccuracyVertical)*ratio;
	self->_locationLastAnimation=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(latitude, longitude)
															   altitude:altitude
													 horizontalAccuracy:accuracyHorizontal
													   verticalAccuracy:accuracyVertical
															  timestamp:[NSDate dateWithTimeInterval:timeElapsed sinceDate:self->_locationLastTarget.timestamp]];

	// if this is an intermediate animation then we assume it's a minor change.
	if(self->_timerTick<self->_animationIntervals)
	{
		[self.delegate locationFilter:self acceptLocation:self->_locationLastAnimation changeType:FGLocationChangeTypeInterpolate];
	}
	else
	{
		[self.delegate locationFilter:self acceptLocation:self->_locationLastAnimation changeType:self->_changeTypeLastTarget];
		// wrap it up if our time is done
		[self->_timerAnimation invalidate];
		self->_timerAnimation=nil;
	}
}
@end
