//
//  FGSettingsVO.m
//  Goggles
//
//  Created by Curtis Elsasser on 11/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSettingsVO.h"


@implementation FGSettingsVO

@dynamic messagesPerQuery;
@dynamic name;
@dynamic queryIntervalMax;
@dynamic queryIntervalMin;
@dynamic radiusHorizontal;
@dynamic radiusVertical;
@dynamic serverVersion;

@end
