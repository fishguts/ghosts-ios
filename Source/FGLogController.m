//
//  FGLogController.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGLogController.h"
#import "FGLogProxy.h"
#import "NSString+FG.h"

#if defined(LOG)

@interface FGLogController()
/**** properties ****/
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonDone;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSend;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSave;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonClear;

/**** private interface ****/
- (void)updateAlerts;

@end

@implementation FGLogController
@synthesize textView;
@synthesize buttonDone;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self updateAlerts];
}

- (void)viewDidUnload
{
	[self setTextView:nil];
	[self setButtonSend:nil];
	[self setButtonSave:nil];
	[self setButtonClear:nil];
    [super viewDidUnload];
}

#pragma mark - Private interface
- (void)updateAlerts
{
	NSArray *alerts=[[FGLogProxy instance] getAlerts];
	NSMutableString *buffer=[[NSMutableString alloc] init];;
	
	 for(NSNotification *alert in alerts)
	 {
		 [buffer appendString:[NSString formatAlert:alert includeTimestamp:YES suffix:@"\n\n"]];
	 }
	 [self.textView setText:buffer];
}

- (void)sendAlerts
{
    MFMailComposeViewController *composer=[[MFMailComposeViewController alloc] init];
	NSMutableString *buffer=[[NSMutableString alloc] init];

	// build body
	[buffer appendString:@"Problem?\n\n"];
	[buffer appendString:[self.textView text]];

	// configure and send
	[composer setMailComposeDelegate:self];
	[composer setTitle:@"Send Log"];
    [composer setSubject:@"Goggles debug log"];
    [composer setToRecipients:[NSArray arrayWithObjects:@"curt.elsasser@gmail.com", nil]];
    [composer setMessageBody:buffer isHTML:NO];
    [self presentModalViewController:composer animated:YES];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Observers
- (IBAction)handleDone:(id)sender
{
	// note: attempting to keep debug stuff consolidated therefor not relying on external actions to manage us.
	[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)handleSend:(id)sender
{
	[self sendAlerts];
}

- (IBAction)handleSave:(id)sender
{
	[[FGLogProxy instance] saveAlerts];
	[sender setEnabled:NO];
}

- (IBAction)handleClear:(id)sender
{
	[[FGLogProxy instance] clearAlerts];
	[self updateAlerts];
	// that's it for this round
	[sender setEnabled:NO];
	[self.buttonSend setEnabled:NO];
	[self.buttonSave setEnabled:NO];
}

@end

#endif
