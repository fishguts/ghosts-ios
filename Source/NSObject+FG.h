//
//  NSObject+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 12/10/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (FG)
/**** Public Interface ****/

/**
 * Takes into account that obj1 or obj2 may be nil. If neither
 * then it passes on down to [NSObject isEqual]
 **/
+ (BOOL)isEqual:(NSObject*)obj1 obj2:(NSObject*)obj2;

@end
