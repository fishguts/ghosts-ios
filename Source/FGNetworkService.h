//
//  FGNetworkService.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGStatusVO;


@interface FGNetworkService : NSObject

/**** public interface ****/
- (void)send;
- (void)cancel;

/**** protected interface ****/
- (void)sendRequestToURL:(NSString*)url;
- (void)postRequestToURL:(NSString*)url data:(NSData*)json;

/**** protected abstract handlers ****/
- (void)handleSuccess:(NSData*)data;
- (void)handleFailure:(FGStatusVO*)response;
- (void)preprocessAuthFailure:(FGStatusVO*)response;

@end
