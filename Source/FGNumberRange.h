//
//  FGNumberRange.h
//  FilterLocation
//
//  Created by Curtis Elsasser on 5/11/13.
//  Copyright (c) 2013 Xraymen Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGNumberRange : NSObject
{
	NSNumber *_min;
	NSNumber *_max;
}

@property (nonatomic, readonly) NSNumber *min;
@property (nonatomic, readonly) NSNumber *max;

- (void)addNumber:(NSNumber*)value;
- (void)addInt:(int)value;
- (void)addDouble:(double)value;

- (double)getDoubleRange;
- (double)getDoubleMid;
- (double)getDoubleOffsetFromMin:(double)value;
- (double)getDoubleOffsetFromMax:(double)value;
- (double)getDoubleRatioFromMin:(double)value;
- (double)getDoubleRatioFromMax:(double)value;

@end


