//
//  FGFeedbackViewController.h
//  Goggles
//
//  Created by Curtis Elsasser on 12/1/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGFeedbackController : UIViewController

/**** properties ****/
@property (nonatomic, readonly) NSString *feedbackText;
@property (nonatomic, weak) id <FGModalDelegate> delegate;


@end
