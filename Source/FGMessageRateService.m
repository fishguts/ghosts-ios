//
//  FGMessageRateService.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageRateService.h"
#import "FGStatusVO.h"
#import "FGMessageVO.h"
#import "FGModelSerializer.h"
#import "FGMessageProxy.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGMessageRateService
#pragma mark - Public Interface
- (id)initWithMessage:(FGMessageVO *)message rating:(NSNumber *)rating
{
	self=[super init];
	self->_message=message;
	self->_rating=rating;
	
	return self;
}

- (void)send
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageRating object:self->_message];
	@try 
	{
		NSString *url=[FGSettings getUrlRateMessage];
		NSData *data=[FGModelSerializer buildRateMessageRequest:self->_message rating:self->_rating pretty:NO];
		
		[self postRequestToURL:url data:data];
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

#pragma mark - Protected handlers
- (void)handleSuccess:(NSData*)data
{
	@try 
	{
		NSArray *messages=[FGModelSerializer parseMessagesResponse:data];
		if([messages count]==1)
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationMessageRateSucceeded object:[messages objectAtIndex:0]];
		}
		else
		{
			[self handleFailure:[FGStatusVO statusFromText:@"Improperly formatted response"]];
		}
	}
	@catch (NSException *exception) 
	{
		[self handleFailure:[FGStatusVO statusFromText:[exception description]]];
	}
}

- (void)handleFailure:(FGStatusVO*)response
{
	NSString *errorTextFriendly=[NSString stringWithFormat:@"Attempt to rate message failed: %@", response.text];
	NSString *errorTextInternal=[NSString stringWithFormat:@"Rate message failed: %@", [response description]];
	
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:errorTextInternal]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationMessageRateFailed object:self->_message value:self->_rating text:errorTextFriendly status:response]];
}


@end
