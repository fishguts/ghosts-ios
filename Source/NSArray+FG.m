//
//  NSArray+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSArray+FG.h"

@implementation NSArray (FG)

- (id)firstObject
{
	return ([self count]>0) ? [self objectAtIndex:0] : nil;
}

@end
