//
//  FGTableViewSelectionController.m
//  Goggles
//
//  Created by Curtis Elsasser on 12/8/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGListSelectionController.h"

@interface FGListSelectionController ()

@end

@implementation FGListSelectionController
#pragma mark - Public Properties
- (void)setItems:(NSArray *)items
{
	self->_items=[items copy];
}

- (void)setSelection:(id)selection
{
	if([NSObject isEqual:selection obj2:self->_selection]==NO)
	{
		self->_selection=selection;
		// cheapy little way to update all of our checks...
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:NO];
	}
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.navigationItem setTitle:[self title]];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self->_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	id item=[self->_items objectAtIndex:[indexPath row]];
	NSString *itemTitle=[item text];
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Selectable" forIndexPath:indexPath];

	[cell.textLabel setText:itemTitle];
	if([NSObject isEqual:item obj2:self->_selection])
	{
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	}
	else
	{
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	}
	
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.selection=[self->_items objectAtIndex:[indexPath row]];
}


#pragma mark - Observers
- (IBAction)handleDone:(id)sender
{
	if(self.delegate)
	{
		[self.delegate selectorComplete:self selection:self->_selection];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

- (IBAction)handleCancel:(id)sender
{
	if(self.delegate)
	{
		[self.delegate selectorCanceled:self];
	}
	else
	{
		[self smartDismissViewControllerAnimated:YES];
	}
}

@end
