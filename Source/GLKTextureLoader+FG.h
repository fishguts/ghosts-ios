//
//  GLKTextureLoader+FG.h
//  OpenGLTexture
//
//  Created by Curtis Elsasser on 9/14/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface GLKTextureLoader (FG)
/**** public interface ****/
+ (GLKTextureInfo*)textureWithView:(UIView*)view generateMipmap:(BOOL)mipmap;
+ (GLKTextureInfo*)textureWithView:(UIView*)view generateMipmap:(BOOL)mipmap error:(NSError**)error;

@end
