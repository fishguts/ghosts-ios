//
//  NSIndexPath+FG.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/28/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (FG)

/**** public interface ****/
- (NSString*)descriptionOfIndexes;
@end
