//
//  FGUserRegistrationController.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGRegistrationController : UIViewController
/**** properties ****/
@property (nonatomic, weak) id <FGModalDelegate> delegate;

@end
