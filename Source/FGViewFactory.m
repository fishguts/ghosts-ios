//
//  FGViewFactory.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGViewFactory.h"
#import "FGMessageTextureView.h"
#import "FGMessageVO.h"
#import "NSString+FG.h"


@implementation FGViewFactory
/***** public interface ****/
+ (UIView*)createMessageTextureView:(FGMessageVO*)message
{
	NSArray *views=[[NSBundle mainBundle] loadNibNamed:@"FGMessageTextureView" owner:nil options:nil];
	FGMessageTextureView *view=[views objectAtIndex:0];
	
	[view setMessageText:message.text];
	[view setUserText:[NSString formatMessageUserWithDate:message shortStyle:NO]];
	
	return view;
}

@end
