//
//  FGCoordinateGrid.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGCoordinateGrid.h"
#import <vector>

@interface FGCoordinateGrid()
{
	std::vector<CLLocationCoordinate2D> _coordinates;
	CLLocationDegrees _left;
	CLLocationDegrees _right;
	CLLocationDegrees _top;
	CLLocationDegrees _bottom;
}

@end

@implementation FGCoordinateGrid
#pragma mark - Public Properties
- (CLLocationDegrees)left
{
	return self->_left;
}
- (CLLocationDegrees)right
{
	return self->_right;
}
- (CLLocationDegrees)top
{
	return self->_top;
}
- (CLLocationDegrees)bottom
{
	return self->_bottom;
}
- (CLLocationDegrees)width
{
	return self->_right-self->_left;
}
- (CLLocationDegrees)height
{
	return self->_top-self->_bottom;
}
- (CLLocationDegrees)horizontalCenter
{
	return self->_left+(self.width/2);
}
- (CLLocationDegrees)verticalCenter
{
	return self->_bottom+(self.height/2);
}

#pragma mark - Public Interface
- (void)reset
{
	self->_coordinates.clear();
	self->_left=0;
	self->_right=0;
	self->_top=0;
	self->_bottom=0;
}

- (BOOL)isEmpty
{
	return self->_coordinates.empty();
}

- (void)addCoordinate:(CLLocationCoordinate2D)coordinate
{
	self->_coordinates.push_back(coordinate);
	if(self->_coordinates.size()==1)
	{
		self->_left=coordinate.longitude;
		self->_right=coordinate.longitude;
		self->_top=coordinate.latitude;
		self->_bottom=coordinate.latitude;
	}
	else
	{
		// note: this algorithm doesn't work well once we cross the latitudate and longitude divides: 180->-180, 90, -90
		// but those are going to be extremely rare cases and nearly non-issues for our small grids.
		if(coordinate.longitude<self->_left)
		{
			self->_left=coordinate.longitude;
		}
		if(coordinate.longitude>self->_right)
		{
			self->_right=coordinate.longitude;
		}
		if(coordinate.latitude<self->_bottom)
		{
			self->_bottom=coordinate.latitude;
		}
		if(coordinate.latitude>self->_top)
		{
			self->_top=coordinate.latitude;
		}
	}
}

- (BOOL)contains:(CLLocationCoordinate2D)coordinate
{
	if(self->_coordinates.size()>0)
	{
		return ((coordinate.longitude>=self->_left)
				&& (coordinate.longitude<=self->_right)
				&& (coordinate.latitude<=self->_top)
				&& (coordinate.latitude>=self->_bottom));
	}
	return NO;
}


@end
