//
//  FGMessageDeleteService.h
//  Goggles
//
//  Created by Curtis Elsasser on 3/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@class FGMessageVO;

/**
 * Really a dummy service. All is handled on this side
 **/
@interface FGMessageDeleteService : FGNetworkService
{
	FGMessageVO *_message;
}

/**** public interface ****/
- (id)initWithMessage:(FGMessageVO*)message;


@end
