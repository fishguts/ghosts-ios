//
//  GLCContext.h
//  Goggles
//
//  Created by Curtis Elsasser on 10/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <OpenGLES/EAGLDrawable.h>
#import "GLCProjectionMatrix.h"
#import "FGTypes.h"

/**
 * This guy is a portable mini-framework for a gl core.  Instead of designing a general purpose framework, this guy (and friends) is meant
 * to be copied and adapted for whatever environment he goes to.
 * Notes:
 *	- projectionMatrix: he's public for fast access and he's yours to manage.  See GLCProjectionMatrix.h
 *
**/
@interface GLCContext : NSObject <FGDispose>
{
@public
	id<GLCProjectionMatrixProtocol> projectionMatrix;
}

/**** Initialization ****/
- (id)initWithEAGLDrawable:(id<EAGLDrawable>)drawable width:(GLfloat)width height:(GLfloat)height;
- (id)initWithEAGLDrawable:(id<EAGLDrawable>)drawable width:(GLfloat)width height:(GLfloat)height includeDepthBuffer:(BOOL)depthBuffer;

/**** Render buffer preparation and presentation ****/
- (void)prepareRenderbuffer:(GLKVector4)clearColor;
- (BOOL)presentRenderbuffer:(NSUInteger)target;

/**** projection matrix management ****/
- (void)installProjectionMatrix:(id<GLCProjectionMatrixProtocol>)matrix;
- (void)pushProjectionMatrix:(id<GLCProjectionMatrixProtocol>)matrix;
- (void)popProjectionMatrix;

/**** client state management ****/
- (void)pushClientState;
- (void)popClientState;
- (void)enableState:(GLenum)state;
- (void)disableState:(GLenum)state;
- (void)enableClientState:(GLenum)state;
- (void)disableClientState:(GLenum)state;

- (void)setColor4f:(GLKVector4)color;
- (void)setPointSize:(GLfloat)size;
- (void)setLineWidth:(GLfloat)width;

@end
