//
//  FGDateRange.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/10/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSDate.h>

/**
 * FGDateRange - manages a from and to date and members for managing
 * them and measuring the time between them.
 */
@interface FGDateRange : NSObject
/**** Public Properties ****/
@property (nonatomic, readonly) NSTimeInterval elapsed;
@property (nonatomic, strong) NSDate *dateFrom;
@property (nonatomic, strong) NSDate *dateTo;

/**** Public Interface ****/
+ (FGDateRange*)dateRange;
+ (FGDateRange*)dateRangeWithFrom:(NSDate*)from;
+ (FGDateRange*)dateRangeWithFrom:(NSDate*)from andTo:(NSDate*)to;

- (void)swapDatesAndSetTo:(NSDate*)to;
- (NSTimeInterval)elapsed:(NSDate*)dateTo;

@end
