//
//  FGMessageViewController.m
//  Goggles
//
//  Created by Curtis Elsasser on 7/29/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageViewController.h"
#import "FGLoginProxy.h"
#import "FGMessageVO.h"
#import "FGConstants.h"
#import "NSString+FG.h"
#import "NSNotification+FG.h"


@interface FGMessageViewController()
{
	BOOL _flagMessageIsDeleteMessage;
	NSInvocation *_pendingInvocation;
}

/**** properties ****/
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonLike;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonDislike;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonFlag;

/**** private interface *****/
- (void)addObservers;
- (void)removeObservers;

- (void)feedbackToUI;

/**** handlers ****/
- (void)handleLoginSucceeded:(NSNotification*)notification;
- (void)handleLogoutSucceeded:(NSNotification*)notification;
- (void)handleRating:(NSNotification*)notification;
- (void)handleRateSucceeded:(NSNotification*)notification;
- (void)handleRateFailed:(NSNotification*)notification;
- (void)handleFlagging:(NSNotification*)notification;
- (void)handleFlagSucceeded:(NSNotification*)notification;
- (void)handleFlagFailed:(NSNotification*)notification;
- (void)handleDeleting:(NSNotification*)notification;
- (void)handleDeleteSucceeded:(NSNotification*)notification;
- (void)handleDeleteFailed:(NSNotification*)notification;

- (IBAction)handleLike:(id)sender;
- (IBAction)handleDislike:(id)sender;
- (IBAction)handleFlag:(id)sender;

@end


@implementation FGMessageViewController
#pragma mark - Properties
- (void)setMessage:(FGMessageVO *)message
{
	self->_message=message;
	self->_flagMessageIsDeleteMessage=[FGLoginProxy canLoginDeleteMessage:message];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self addObservers];
	[self feedbackToUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
	if([self isInNavigationController]==NO)
	{
		[self removeObservers];
	}
	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
	[self setButtonLike:nil];
	[self setButtonDislike:nil];
	[self setButtonFlag:nil];
	[self setTableView:nil];
    [super viewDidUnload];
}


#pragma mark - private interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	[center addObserver:self selector:@selector(handleRating:) name:FGNotificationMessageRating object:nil];
	[center addObserver:self selector:@selector(handleRateSucceeded:) name:FGNotificationMessageRateSucceeded object:nil];
	[center addObserver:self selector:@selector(handleRateFailed:) name:FGNotificationMessageRateFailed object:nil];
	[center addObserver:self selector:@selector(handleFlagging:) name:FGNotificationMessageFlagging object:nil];
	[center addObserver:self selector:@selector(handleFlagSucceeded:) name:FGNotificationMessageFlagSucceeded object:nil];
	[center addObserver:self selector:@selector(handleFlagFailed:) name:FGNotificationMessageFlagFailed object:nil];
	[center addObserver:self selector:@selector(handleDeleting:) name:FGNotificationMessageDeleting object:nil];
	[center addObserver:self selector:@selector(handleDeleteSucceeded:) name:FGNotificationMessageDeleteSucceeded object:nil];
	[center addObserver:self selector:@selector(handleDeleteFailed:) name:FGNotificationMessageDeleteFailed object:nil];
	[center addObserver:self selector:@selector(handleLoginSucceeded:) name:FGNotificationLoginSucceeded object:nil];
	[center addObserver:self selector:@selector(handleLogoutSucceeded:) name:FGNotificationLogoutSucceeded object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)feedbackToUI
{
	NSString *text;
	const BOOL allowFeedback=[self->_message.allowFeedback boolValue];

	// update text
	if((allowFeedback==NO)
	   || (text=[NSString formatRatingCount:self->_message.ratingLikes zeroValue:nil])==nil)
	{
		[self.buttonLike setTitle:@"Like"];
	}
	else
	{
		[self.buttonLike setTitle:[NSString stringWithFormat:@"Like %@", text]];
	}
	if((allowFeedback==NO)
	   || (text=[NSString formatRatingCount:self->_message.ratingDislikes zeroValue:nil])==nil)
	{
		[self.buttonDislike setTitle:@"Dislike"];
	}
	else
	{
		[self.buttonDislike setTitle:[NSString stringWithFormat:@"Dislike %@", text]];
	}

	// update enabled states
	if(allowFeedback)
	{
		[self.buttonLike setEnabled:([self.message isUserRatingLike]==NO)];
		[self.buttonDislike setEnabled:([self.message isUserRatingDislike]==NO)];
		if(self->_flagMessageIsDeleteMessage)
		{
			[self.buttonFlag setTitle:@"Delete"];
			[self.buttonFlag setEnabled:YES];
		}
		else
		{
			[self.buttonFlag setEnabled:([self.message.flaggedUser boolValue]==NO)];
		}
	}
	else
	{
		[self.buttonLike setEnabled:NO];
		[self.buttonDislike setEnabled:NO];
		[self.buttonFlag setEnabled:NO];
	}
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSAssert(section<2, @"?");
	return (section==0) ? 1 : 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	UITextView *textView;

	if(indexPath.section==0)
	{
		cell=[self.tableView dequeueReusableCellWithIdentifier:@"Message" forIndexPath:indexPath];
		textView=[[cell.contentView subviews] objectAtIndex:0];
		[textView setText:self->_message.text];
		[textView setFrame:CGRectMake(0, 0, textView.bounds.size.width, cell.bounds.size.height-2)];
	}
	else
	{
		cell=[self.tableView dequeueReusableCellWithIdentifier:@"Information" forIndexPath:indexPath];
		if(indexPath.row==0)
		{
			[cell.textLabel setText:self->_message.owner.username];
			[cell.detailTextLabel setText:[NSString formatMessageDate:self->_message.created shortStyle:NO]];
		}
		else
		{
			NSAssert(indexPath.row==1, @"?");
			[cell.textLabel setText:@"Expiration"];
			if(self->_message.expiration)
			{
				[cell.detailTextLabel setText:[NSString formatMessageDate:self->_message.expiration shortStyle:NO]];
			}
			else
			{
				[cell.detailTextLabel setText:@"None"];
			}
		}
	}
	return cell;
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	const CGFloat defaultRowHeight=[self.tableView rowHeight];
	CGFloat rowHeight=defaultRowHeight;
	
	if(indexPath.section==0)
	{
		UITableViewCell *cell=[self.tableView dequeueReusableCellWithIdentifier:@"Message"];
		UITextView *textView=[[cell.contentView subviews] objectAtIndex:0];

		// configure the text view as if he were about to hit the stage. We determine
		// from his desired content size whether we are going to beef up his shorts or not.
		[textView setText:self.message.text];
		if(textView.contentSize.height>(cell.bounds.size.height+2))
		{
			rowHeight=MIN(textView.contentSize.height+2, self.tableView.bounds.size.height-defaultRowHeight*2-15*3);
		}
		else
		{
			rowHeight=cell.bounds.size.height;
		}
	}
	return rowHeight;
}


#pragma mark - observer handlers
- (void)handleLoginSucceeded:(NSNotification*)notification
{
	// if there is a pending operation then invoke it.
	[self->_pendingInvocation invoke];
	self->_pendingInvocation=nil;
}

- (void)handleLogoutSucceeded:(NSNotification*)notification
{
	// we are okay and ready should they log back in.
}


- (void)handleRating:(NSNotification *)notification
{
	// prevent duplicates. We'll re-enable upon success or failure
	[self.buttonLike setEnabled:NO];
	[self.buttonDislike setEnabled:NO];
}

- (void)handleRateSucceeded:(NSNotification*)notification
{
	[self feedbackToUI];
}

- (void)handleRateFailed:(NSNotification*)notification
{
	[self feedbackToUI];
	// repost as an error so that it's modally displayed
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:[notification object] text:[notification text]]];
}


- (void)handleFlagging:(NSNotification *)notification
{
	[self.buttonFlag setEnabled:NO];
}

- (void)handleFlagSucceeded:(NSNotification*)notification
{
	[self feedbackToUI];
}

- (void)handleFlagFailed:(NSNotification*)notification
{
	[self feedbackToUI];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:[notification object] text:[notification text]]];
}


- (void)handleDeleting:(NSNotification *)notification
{
	[self.buttonFlag setEnabled:NO];
}

- (void)handleDeleteSucceeded:(NSNotification*)notification
{
	[self smartDismissViewControllerAnimated:YES];
}

- (void)handleDeleteFailed:(NSNotification*)notification
{
	[self feedbackToUI];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:[notification object] text:[notification text]]];
}


#pragma mark - IBAction observers
- (IBAction)handleLike:(id)sender
{
	NSNotification *notification=[NSNotification notificationWithName:FGNotificationMessageRate object:self.message value:[NSNumber numberWithInt:1]];
	self->_pendingInvocation=[NSInvocation invocationForNotificationPost:[NSNotificationCenter defaultCenter] notification:notification];

	if([FGLoginProxy isLoggedIn])
	{
		[self->_pendingInvocation invoke];
		self->_pendingInvocation=nil;
	}
	else
	{
		[self performSegueWithIdentifier:@"SegueLogin" sender:self];
	}
}

- (IBAction)handleDislike:(id)sender
{
	NSNotification *notification=[NSNotification notificationWithName:FGNotificationMessageRate object:self.message value:[NSNumber numberWithInt:-1]];
	self->_pendingInvocation=[NSInvocation invocationForNotificationPost:[NSNotificationCenter defaultCenter] notification:notification];

	if([FGLoginProxy isLoggedIn])
	{
		[self->_pendingInvocation invoke];
		self->_pendingInvocation=nil;
	}
	else
	{
		
		[self performSegueWithIdentifier:@"SegueLogin" sender:self];
	}
}

- (IBAction)handleFlag:(id)sender
{
	NSNotification *notification=(self->_flagMessageIsDeleteMessage)
		? [NSNotification notificationWithName:FGNotificationMessageDelete object:self.message]
		: [NSNotification notificationWithName:FGNotificationMessageFlag object:self.message];
	self->_pendingInvocation=[NSInvocation invocationForNotificationPost:[NSNotificationCenter defaultCenter] notification:notification];
	
	if([FGLoginProxy isLoggedIn])
	{
		[self->_pendingInvocation invoke];
		self->_pendingInvocation=nil;
	}
	else
	{
		[self performSegueWithIdentifier:@"SegueLogin" sender:self];
	}
}


@end
