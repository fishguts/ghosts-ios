//
//  FGGeometry.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/24/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#ifndef Goggles_FGGeometry_h
#define Goggles_FGGeometry_h

#include <CoreGraphics/CGGeometry.h>

static inline CGFloat CGRectLeft(CGRect r)		{ return r.origin.x; }
static inline CGFloat CGRectTop(CGRect r)		{ return r.origin.y; }
static inline CGFloat CGRectRight(CGRect r)		{ return r.origin.x+r.size.width; }
static inline CGFloat CGRectBottom(CGRect r)	{ return r.origin.y+r.size.height; }

static inline CGRect CGRectMakeOutset(CGFloat x, CGFloat y, CGFloat dx, CGFloat dy)
{
	return CGRectMake(x-dx, y-dy, dx*2, dy*2);
}


#endif
