//
//  GLCExt.m
//  OpenGLMixed
//
//  Created by Curtis Elsasser on 10/1/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "GLCExt.h"
#import "GLCContext.h"


@implementation GLCExt

#pragma mark - Public interface
+ (void)drawPoint:(GLCContext*)context origin:(GLKVector2)origin radiusInPixels:(GLfloat)radius color:(GLKVector4)color
{
	[context enableClientState:GL_VERTEX_ARRAY];
	[context disableClientState:GL_COLOR_ARRAY];
	[context enableState:GL_POINT_SMOOTH];
	[context setPointSize:(radius*2)];
	[context setColor4f:color];

	glVertexPointer(2, GL_FLOAT, 0, &origin);
	glDrawArrays(GL_POINTS, 0, 1);
}

@end
