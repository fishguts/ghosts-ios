//
//  FGUserRegistrationController.m
//  Goggles
//
//  Created by Curtis Elsasser on 10/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGRegistrationController.h"
#import "FGProfileVO.h"
#import "FGModelFactory.h"
#import "FGConstants.h"

@interface FGRegistrationController ()
/**** properties ****/
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *password;

@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textEmail;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonRegister;

/**** private interface ****/
- (void)addObservers;
- (void)removeObservers;

- (void)updateEnabledStates;

/**** handlers ****/
- (IBAction)handleTextChanged:(id)sender;
- (IBAction)handleRegister:(id)sender;
- (IBAction)handleCancel:(id)sender;
- (void)handleRegisterSucceeded:(NSNotification*)notification;
- (void)handleRegisterFailed:(NSNotification*)notification;

@end

@implementation FGRegistrationController
#pragma mark - Properties
@synthesize delegate;
@synthesize textName;
@synthesize textPassword;
@synthesize buttonRegister;

- (NSString*)name
{
	return [self.textName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString*)email
{
	return [self.textEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString*)password
{
	return [self.textPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self addObservers];
	[self.textName becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[self removeObservers];
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
	[self setTextName:nil];
	[self setTextPassword:nil];
	[self setButtonRegister:nil];
	[self setTextEmail:nil];
    [super viewDidUnload];
}

#pragma mark - Private interface
- (void)addObservers
{
	NSNotificationCenter *notificationCenter=[NSNotificationCenter defaultCenter];
	
	[notificationCenter addObserver:self selector:@selector(handleRegisterSucceeded:) name:FGNotificationRegisterSucceeded object:nil];
	[notificationCenter addObserver:self selector:@selector(handleRegisterFailed:) name:FGNotificationRegisterFailed object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateEnabledStates
{
	BOOL ready=([self.name length]>=FGMinUserNameLength) && ([self.password length]>=FGMinPasswordLength);
	
	[self.buttonRegister setEnabled:ready];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if(textField==self.textName)
	{
		[self.textEmail becomeFirstResponder];
	}
	else if(textField==self.textEmail)
	{
		[self.textPassword becomeFirstResponder];
	}
	else if(textField==self.textPassword)
	{
		if([self.buttonRegister isEnabled])
		{
			[self handleRegister:textField];
		}
	}
	return NO;
}

#pragma mark - Observer handlers
- (IBAction)handleTextChanged:(id)sender
{
	[self updateEnabledStates];
}

- (IBAction)handleRegister:(id)sender
{
	FGProfileVO *profile=[FGModelFactory createRegistration:[self name] email:[self email] password:[self password]];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationRegister object:profile];
}

- (IBAction)handleCancel:(id)sender
{
	if(self.delegate!=nil)
	{
		[self.delegate modalCanceled:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)handleRegisterSucceeded:(NSNotification*)notification
{
	FGProfileVO *profile=[notification object];
	NSString *message=[NSString stringWithFormat:@"An activation email has been sent to %@", profile.email];
	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Success" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	
	[alert show];
	if(self.delegate!=nil)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)handleRegisterFailed:(NSNotification*)notification
{
	// repost as an error so that it's modally displayed
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:[notification object] text:[notification text]]];
}

@end
