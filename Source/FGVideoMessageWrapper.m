//
//  FGVideoMessageWrapper.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGVideoMessageWrapper.h"

@implementation FGVideoMessageWrapper

/**** properties ****/
@synthesize vector=_vector;

- (FGMessageVO*)message
{
	return self->_message;
}

- (GLKTextureInfo*)texture
{
	return self->_texture;
}

/**** interface ****/
- (id)initWithMessage:(FGMessageVO *)message vector:(GLKVector3)vector texture:(GLKTextureInfo*)texture
{
	self=[super init];
	self->_message=message;
	self->_vector=vector;
	self->_texture=texture;
	
	return self;
}
@end
