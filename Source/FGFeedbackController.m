//
//  FGFeedbackViewController.m
//  Goggles
//
//  Created by Curtis Elsasser on 12/1/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGFeedbackController.h"
#import "FGConstants.h"
#import "FGFeedbackVO.h"
#import "FGLoginProxy.h"


@interface FGFeedbackController ()
/**** properties ****/
@property (weak, nonatomic) IBOutlet UITextView *textViewFeedback;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonCancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSend;

/**** observer handlers ****/
- (void)handleSendFeedbackSucceeded:(NSNotification*)notification;
- (void)handleSendFeedbackFailed:(NSNotification*)notification;
@end

@implementation FGFeedbackController

#pragma mark - Properties
- (NSString*)feedbackText
{
	return [self.textViewFeedback.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self addObservers];
	[self.textViewFeedback becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[self removeObservers];
	[super viewDidDisappear:animated];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
	NSString *trimmed=[self feedbackText];
	[self.buttonSend setEnabled:[trimmed length]>=FGMinFeedbackLength];
}

#pragma mark - Private Interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	[center addObserver:self selector:@selector(handleSendFeedbackSucceeded:) name:FGNotificationFeedbackPostSucceeded object:nil];
	[center addObserver:self selector:@selector(handleSendFeedbackFailed:) name:FGNotificationFeedbackPostFailed object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Private observer handlers
- (IBAction)handleSend:(id)sender
{
	FGFeedbackVO *feedback=[FGFeedbackVO feedbackFromText:[self feedbackText] owner:[FGLoginProxy getLogin]];
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationFeedbackPost object:feedback];
}

- (IBAction)handleCancel:(id)sender
{
	if(self.delegate)
	{
		[self.delegate modalCanceled:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)handleSendFeedbackSucceeded:(NSNotification*)notification
{
	if(self.delegate)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)handleSendFeedbackFailed:(NSNotification*)notification
{
	// repost as error so that it gets modally displayed
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:[notification object] text:[notification text]]];
}

@end
