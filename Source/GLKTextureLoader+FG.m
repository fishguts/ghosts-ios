//
//  GLKTextureLoader+FG.m
//  OpenGLTexture
//
//  Created by Curtis Elsasser on 9/14/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "GLKTextureLoader+FG.h"


@implementation GLKTextureLoader(FG)

+ (GLKTextureInfo*)textureWithView:(UIView*)view generateMipmap:(BOOL)mipmap
{
	NSError *error;
	GLKTextureInfo *texture=[GLKTextureLoader textureWithView:view generateMipmap:mipmap error:&error];
	if(texture==nil)
	{
		NSString *text=[NSString stringWithFormat:@"Failed to create texture: %@", [error description]];
		@throw [NSException exceptionWithName:@"TextureException" reason:text userInfo:nil];
	}
	return texture;
}

+ (GLKTextureInfo*)textureWithView:(UIView*)view generateMipmap:(BOOL)mipmap error:(NSError**)error
{
	GLKTextureInfo *texture;
	UIGraphicsBeginImageContext(view.bounds.size);
	{
		[view.layer renderInContext:UIGraphicsGetCurrentContext()];
		UIImage *image=UIGraphicsGetImageFromCurrentImageContext();
		NSDictionary *options=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], GLKTextureLoaderOriginBottomLeft, 
							   [NSNumber numberWithBool:mipmap], GLKTextureLoaderGenerateMipmaps, nil];
		texture=[GLKTextureLoader textureWithCGImage:[image CGImage] options:options error:error];
		if(texture==nil)
		{
			NSString *text=[NSString stringWithFormat:@"Failed to create texture: %@", [*error description]];
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:text]];
		}
		else
		{
			// NSLog(@"texture: size=(%d, %d), bitDepth=%d", [texture width], [texture height], (int)CGImageGetBitsPerPixel([image CGImage]));
			NSAssertWarn((mipmap==NO) || (remainderf(log2f(image.size.width), 1.0)==0), @"Width should be power of 2: %f", image.size.width);
			NSAssertWarn((mipmap==NO) || (remainderf(log2f(image.size.height), 1.0)==0), @"Height should be power of 2: %f", image.size.height);
		}
	}
	UIGraphicsEndImageContext();
	return texture;
}

@end
