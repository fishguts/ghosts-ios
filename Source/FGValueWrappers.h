//
//  FGValueWrappers.h
//  Gears
//
//  Created by Curtis Elsasser on 6/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGPoint : NSObject
{
@public
	CGPoint value;
}
- (id)initWithValue:(CGPoint)object;

@end


@interface FGSize : NSObject
{
@public
	CGSize value;
}
- (id)initWithValue:(CGSize)object;

@end


@interface FGRect : NSObject
{
@public
	CGRect value;
}
- (id)initWithValue:(CGRect)object;

@end


@interface FGVector3 : NSObject
{
@public
	GLKVector3 value;
}
- (id)initWithValue:(GLKVector3)object;

@end
