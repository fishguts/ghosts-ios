//
//  UIViewController+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "UIViewController+FG.h"

@implementation UIViewController (FG)

- (BOOL)isInContainingController
{
	return [self isInNavigationController] || [self isInTabBarController];
}

- (BOOL)isInNavigationController
{
	if(self.navigationController==nil)
	{
		return NO;
	}
	// not very future friendly: https://discussions.apple.com/thread/1660161?threadID=1660161
	if([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
	{
		return NO;
	}
	return YES;
}

- (BOOL)isInTabBarController
{
	if(self.tabBarController==nil)
	{
		return NO;
	}
	if([self.tabBarController.viewControllers indexOfObject:self]==NSNotFound)
	{
		return NO;
	}
	return YES;
}

- (BOOL)isSelectedViewController
{
	if(self.tabBarController!=nil)
	{
		return self.tabBarController.selectedViewController==self;
	}
	if(self.navigationController!=nil)
	{
		return [self.navigationController.viewControllers lastObject]==self;
	}
	return NO;
}

- (UIViewController*)activeViewController
{
	if([self isKindOfClass:[UINavigationController class]])
	{
		if([(UINavigationController*)self topViewController])
		{
			return [[(UINavigationController*)self topViewController] activeViewController];
		}
	}
	else if([self isKindOfClass:[UITabBarController class]])
	{
		if([(UITabBarController*)self selectedViewController])
		{
			return [[(UITabBarController*)self selectedViewController] activeViewController];
		}
	}
	return self;
}

- (void)smartDismissViewControllerAnimated:(BOOL)animated
{
	if(self.navigationController)
	{
		if([[self.navigationController viewControllers] count]>1)
		{
			[self.navigationController popViewControllerAnimated:animated];
		}
		else
		{
			// there are no more view controllers on the navigation stack.  Assume they are popping the navigation controller
			[self.presentingViewController dismissViewControllerAnimated:animated completion:nil];
		}
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:animated completion:nil];
	}
}

@end
