//
//  NSDictionary+FG.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSDictionary+FG.h"
#import "FGConstants.h"

@implementation NSDictionary (FG)

- (id)objectForKey:(id)key defaultValue:(id)defaultValue
{
	id result=[self objectForKey:key];
	return (result!=nil) ? result : defaultValue;
}

- (id)valueForKey:(NSString*)key defaultValue:(id)defaultValue
{
	id result=[self valueForKey:key];
	return (result!=nil) ? result : defaultValue;
}

- (BOOL)boolForKey:(NSString*)key
{
	return [self valueForKey:key]==ARC_STRING_YES;
}

@end


@implementation NSMutableDictionary (FG)
- (void)setBool:(BOOL)value forKey:(id)aKey
{
	NSObject *arcValue=(value) ? ARC_STRING_YES : ARC_STRING_NO;
	[self setObject:arcValue forKey:aKey];
}
@end
