//
//  FGVideoMessageWrapper.h
//  Goggles
//
//  Created by Curtis Elsasser on 9/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

@class FGMessageVO;

@interface FGVideoMessageWrapper : NSObject
{
	FGMessageVO *_message;
	GLKTextureInfo *_texture;
}

/**** properties ****/
@property (nonatomic, readonly) FGMessageVO *message;
@property (nonatomic, readonly) GLKTextureInfo *texture;
@property (nonatomic) GLKVector3 vector;

/**** interface ****/
- (id)initWithMessage:(FGMessageVO*)message vector:(GLKVector3)vector texture:(GLKTextureInfo*)texture;
@end
