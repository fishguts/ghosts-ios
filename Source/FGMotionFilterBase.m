//
//  FGMotionFilterBase.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/29/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMotionFilterBase.h"

@implementation FGMotionFilterBase
@synthesize delegate=_delegate;

- (FGMotionFilterBase*)nextFilter
{
	if([self.delegate isKindOfClass:[FGMotionFilterBase class]])
	{
		return (FGMotionFilterBase*)self.delegate;
	}
	return nil;
}

- (FGMotionFilterBase*)lastFilter
{
	FGMotionFilterBase *next=[self nextFilter];
	return (next) ? [next lastFilter] : self;
}

#pragma mark - Public Interface
+ (FGMotionFilterBase*)createFilterChainWithDelegates:(NSArray*)delegates
{
	for(int index=1; index<[delegates count]; index++)
	{
		[[delegates objectAtIndex:index-1] setDelegate:[delegates objectAtIndex:index]];
	}
	return [delegates objectAtIndex:0];
}

- (void)dispose
{
	[[self nextFilter] dispose];
}

- (void)reset:(BOOL)cascade
{
	if(cascade)
	{
		[[self nextFilter] reset:cascade];
	}
}

- (void)addOrientation:(GLKVector3)orientation
{
	[self motionFilter:self acceptOrientation:orientation];
}

- (void)motionFilter:(FGMotionFilterBase*)filter acceptOrientation:(GLKVector3)orientation
{
	NSWarnFL(@"Base implementation being called");
	NSAssertWarn(self.delegate!=nil, @"Delegate nil");

	[self.delegate motionFilter:self acceptOrientation:orientation];
}


@end
