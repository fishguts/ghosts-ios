//
//  FGReferenceCount.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/16/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGReferenceCount.h"

@implementation FGReferenceCount
- (int)count
{
	return self->_count;
}

- (int)addReference
{
	return ++self->_count;
}

- (int)releaseReference
{
	NSAssertWarn(self->_count>0, @"Reference count bad: %d", self->_count-1);
	return --self->_count;
}

@end
