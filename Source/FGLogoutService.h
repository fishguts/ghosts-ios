//
//  FGLogutService.h
//  Goggles
//
//  Created by Curtis Elsasser on 7/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNetworkService.h"

@class FGProfileVO;


@interface FGLogoutService : FGNetworkService
{
	FGProfileVO *_profile;
}

/**** public interface ****/
- (id)initWithProfile:(FGProfileVO*)profile;

@end
