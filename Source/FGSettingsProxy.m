//
//  FGSettingsProxy.m
//  Goggles
//
//  Created by Curtis Elsasser on 8/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSettingsProxy.h"
#import "FGModelQueries.h"
#import "FGSettings.h"
#import "FGConstants.h"


@implementation FGSettingsProxy

+ (void)installSettings
{
	FGSettingsVO *settings=[FGModelQueries fetchSettingByName:@"radial"];
	// can add diff checking if it's ever warranted. Don't care at the moment....just going through procedure.
	if(settings!=nil)
	{
		[FGSettings settingsWithVO:settings];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationSettingsChanged object:settings];
	}
}

+ (FGListSortType)getListSortMethod
{
	return [FGSettings getListSortMethod];
}

+ (void)setListSortMethod:(FGListSortType)sortMethod
{
	if([FGSettings getListSortMethod]!=sortMethod)
	{
		[FGSettings setListSortMethod:sortMethod];
		[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationSettingListSortChanged object:[NSNumber numberWithInteger:sortMethod]];
	}
}

@end
