//
//  FGMessageVO.m
//  Goggles
//
//  Created by Curtis Elsasser on 1/14/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMessageVO.h"
#import "FGProfileVO.h"
#import "FGConstants.h"


@implementation FGMessageVO
#pragma mark - Properties
@dynamic allowFeedback;
@dynamic altitude;
@dynamic color;
@dynamic created;
@dynamic expiration;
@dynamic flaggedUser;
@dynamic id;
@dynamic imageUrlFull;
@dynamic imageUrlThumb;
@dynamic latitude;
@dynamic longitude;
@dynamic ratingDislikes;
@dynamic ratingLikes;
@dynamic ratingSum;
@dynamic ratingUser;
@dynamic text;
@dynamic type;
@dynamic sortPrefix;
@dynamic owner;
@dynamic status;
@dynamic error;


#pragma mark - Public Interface
- (BOOL)isAllowFeedback
{
	return [self.allowFeedback boolValue];
}

- (BOOL)isUserRatingLike
{
	return [NSNumber isEqualNumber:self.ratingUser toNumber:[NSNumber numberWithInt:1]];
}

- (BOOL)isUserRatingDislike
{
	return [NSNumber isEqualNumber:self.ratingUser toNumber:[NSNumber numberWithInt:-1]];
}

#pragma mark - Overrides
- (void)didChangeValueForKey:(NSString *)key
{
	[super didChangeValueForKey:key];
	if([key isEqualToString:@"type"]
	   || [key isEqualToString:@"status"])
	{
		[self updateSortPrefix];
	}
}

#pragma mark - Private Interface
- (void)updateSortPrefix
{
	// [arnie voice] wait until we've got all of the information
	if(([self.type length]>0)
	   && ([self.status length]>0))
	{
		// format: [type]|[status]
		NSMutableString *value=[[NSMutableString alloc] initWithCapacity:2];

		// major sort - type
		if([self.type isEqualToString:FGMessageTypeAdvertisement])
		{
			[value setString:@"a"];
		}
		else if([self.type isEqualToString:FGMessageTypeNotification])
		{
			[value setString:@"b"];
		}
		else if([self.type isEqualToString:FGMessageTypeWelcome])
		{
			[value setString:@"c"];
		}
		else
		{
			[value setString:@"d"];
		}

		// minor sort - status
		if([self.status isEqualToString:FGMessageStatusPost])
		{
			[value appendString:@"a"];
		}
		else if([self.status isEqualToString:FGMessageStatusError])
		{
			[value appendString:@"b"];
		}
		else
		{
			[value appendString:@"c"];
		}

		// update
		self.sortPrefix=value;
	}
}

@end
