//
//  FGDateRange.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/10/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDateRange.h"

@implementation FGDateRange
@synthesize dateFrom=_dateFrom;
@synthesize dateTo=_dateTo;

- (NSTimeInterval)elapsed
{
	NSAssertWarn((self->_dateFrom && self->_dateTo), @"dateFrom and dateTo should be set");
	return (self->_dateFrom && self->_dateTo)
	   ? [self->_dateTo timeIntervalSinceDate:self->_dateFrom]
		: 0;
}

+ (FGDateRange*)dateRange
{
	return [FGDateRange dateRangeWithFrom:nil andTo:nil];
}

+ (FGDateRange*)dateRangeWithFrom:(NSDate*)from
{
	return [FGDateRange dateRangeWithFrom:from andTo:nil];
}

+ (FGDateRange*)dateRangeWithFrom:(NSDate*)from andTo:(NSDate*)to
{
	FGDateRange *range=[[FGDateRange alloc] init];
	range.dateFrom=from;
	range.dateTo=to;
	return range;
}

- (void)swapDatesAndSetTo:(NSDate *)to
{
	self->_dateFrom=self->_dateTo;
	self->_dateTo=to;
}

- (NSTimeInterval)elapsed:(NSDate*)dateTo
{
	return [dateTo timeIntervalSinceDate:self->_dateFrom];
}


@end
