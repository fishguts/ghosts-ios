//
//  FGMotionTypes.h
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/NSObject.h>
#import "FGTypes.h"

@class CLLocation;
@class FGLocationFilterBase;

/**** types ****/
typedef enum
{
	FGLocationChangeTypeUndetermined,
	FGLocationChangeTypeNone,
	FGLocationChangeTypeMinor,
	FGLocationChangeTypeMajor,
	FGLocationChangeTypeInterpolate,
} FGLocationChangeType;


/**** protocols ****/
@protocol FGLocationFilterDelegate <NSObject>
- (void)locationFilter:(FGLocationFilterBase*)filter acceptLocation:(CLLocation*)location changeType:(FGLocationChangeType)changeType;
@end


/**** classes ****/

/**
 * FGLocationFilterBase - base class for all location filters.
 * All filters implement FGLocationFilterBase so that they may be chained together: delegate -> delegate, etc. 
 */
@interface FGLocationFilterBase : NSObject <FGDispose, FGLocationFilterDelegate>
/**** Properties ****/
@property (nonatomic, strong) id<FGLocationFilterDelegate> delegate;
@property (nonatomic, readonly) FGLocationFilterBase *nextFilter;
@property (nonatomic, readonly) FGLocationFilterBase *lastFilter;

/**** Interface ****/
+ (FGLocationFilterBase*)createFilterChainWithDelegates:(NSArray*)delegates;

- (void)addLocation:(CLLocation*)location changeType:(FGLocationChangeType)changeType;
- (void)reset:(BOOL)cascade;

@end


