//
//  FGDocumentController.m
//  Legal
//
//  Created by Curtis Elsasser on 12/3/12.
//  Copyright (c) 2012 Curtis Elsasser. All rights reserved.
//

#import "FGDocumentController.h"
#import "FGConstants.h"


@interface FGDocumentController()
{
	NSURLRequest *_activeRequest;
}

/**** Private Properties ****/
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationTitle;

/**** Private Interface ****/
- (void)loadNextURLRequest;
- (void)loadURLRequestWithStatusCodeMonitoring:(NSURLRequest*)request;

@end


@implementation FGDocumentController
#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self loadNextURLRequest];
	[self.navigationTitle setTitle:[self title]];
}

#pragma mark - UIWebViewDelegate members
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	NSWarnFL(@"Load %@ failed: %@", self->_activeRequest, [error localizedDescription]);
	[self loadNextURLRequest];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkActivityStarting object:self];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkActivityEnding object:self];
}

#pragma mark - Private Interface
- (void)loadNextURLRequest
{
	BOOL monitorStatusCode=FALSE;
	
	// determine what our next course of action is:
	//	1st time - remote request if !nil
	//	2nd time - fail safe if !nil
	if(self->_activeRequest==nil)
	{
		self->_activeRequest=(self.urlRemoteRequest!=nil) ? self.urlRemoteRequest : self.urlFailSafeRequest;
		monitorStatusCode=(self.urlRemoteRequest!=nil) && (self.urlFailSafeRequest!=nil);
	}
	else if(self->_activeRequest==self.urlRemoteRequest)
	{
		self->_activeRequest=self.urlFailSafeRequest;
	}
	else
	{
		self->_activeRequest=nil;
	}

	// present what we've got otherwise error.
	if(self->_activeRequest!=nil)
	{
		if(monitorStatusCode)
		{
			[self loadURLRequestWithStatusCodeMonitoring:self->_activeRequest];
		}
		else
		{
			[self.webView loadRequest:self->_activeRequest];
		}
	}
	else
	{
		NSString *errorText=[NSString stringWithFormat:@"Unable to load %@ page", [self title]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:errorText]];
	}
}

- (void)loadURLRequestWithStatusCodeMonitoring:(NSURLRequest*)request
{
	[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkActivityStarting object:self];
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^()
	{
		NSError *error;
		NSURLResponse *response;

		// We do our own custom status code monitoring via NSURLConnection because webview does not give us access to it.
		NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        dispatch_sync(dispatch_get_main_queue(), ^()
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:FGNotificationNetworkActivityEnding object:self];
			if(error!=nil)
			{
				NSWarnFL(@"Load %@ failed: %@", [request URL], [error localizedDescription]);
				[self loadNextURLRequest];
			}
			else
			{
				NSInteger statusCode=[response isKindOfClass:[NSHTTPURLResponse class]]
					? [(NSHTTPURLResponse*)response statusCode]
					: 200;
				if(statusCode<400)
				{
					[self.webView loadData:data MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:[request URL]];
				}
				else
				{
					NSWarnFL(@"Load %@ returned %d", [request URL], statusCode);
					[self loadNextURLRequest];
				}
			}
        });
	});
}



#pragma mark - Private observers
- (IBAction)handleDone:(id)sender
{
	if(self.delegate!=nil)
	{
		[self.delegate modalComplete:self];
	}
	else
	{
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

@end
