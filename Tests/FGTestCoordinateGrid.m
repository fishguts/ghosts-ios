//
//  FGTestCoordinateGrid.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/3/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGCoordinateGrid.h"


@interface FGTestCoordinateGrid : SenTestCase

@end


@implementation FGTestCoordinateGrid
- (void)testCoordinateGridInsert
{
	FGCoordinateGrid *grid=[[FGCoordinateGrid alloc] init];
	[grid addCoordinate:CLLocationCoordinate2DMake(1, 1)];
	STAssertTrue(grid.left==1, @"Left not equal?");
	STAssertTrue(grid.right==1, @"Right not equal?");
	STAssertTrue(grid.top==1, @"Top not equal?");
	STAssertTrue(grid.bottom==1, @"Bottom not equal?");
	STAssertTrue(grid.width==0, @"Width?");
	STAssertTrue(grid.height==0, @"Height?");
}

- (void)testCoordinateGridContains
{
	FGCoordinateGrid *grid=[[FGCoordinateGrid alloc] init];
	[grid addCoordinate:CLLocationCoordinate2DMake(1, 1)];
	[grid addCoordinate:CLLocationCoordinate2DMake(-1, -1)];
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(0, 0)], @"Doesn't contain?");
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(-1, 0)], @"Doesn't contain?");
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(1, 0)], @"Doesn't contain?");
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(0, -1)], @"Doesn't contain?");
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(0, 1)], @"Doesn't contain?");
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(1, 1)], @"Doesn't contain?");
	STAssertTrue([grid contains:CLLocationCoordinate2DMake(-1, -1)], @"Doesn't contain?");
	STAssertFalse([grid contains:CLLocationCoordinate2DMake(-2, 0)], @"Contains?");
	STAssertFalse([grid contains:CLLocationCoordinate2DMake(2, 0)], @"Contains?");
	STAssertFalse([grid contains:CLLocationCoordinate2DMake(0, -2)], @"Contains?");
	STAssertFalse([grid contains:CLLocationCoordinate2DMake(0, -2)], @"Contains?");
	[grid reset];
	STAssertFalse([grid contains:CLLocationCoordinate2DMake(0, 0)], @"Contains?");
}

- (void)testCoordinateGridReset
{
	FGCoordinateGrid *grid=[[FGCoordinateGrid alloc] init];
	[grid addCoordinate:CLLocationCoordinate2DMake(1, 1)];
	[grid addCoordinate:CLLocationCoordinate2DMake(-1, -1)];
	[grid reset];
	STAssertTrue(grid.left==0, @"Left not equal?");
	STAssertTrue(grid.right==0, @"Right not equal?");
	STAssertTrue(grid.top==0, @"Top not equal?");
	STAssertTrue(grid.bottom==0, @"Bottom not equal?");
	STAssertTrue(grid.width==0, @"Width?");
	STAssertTrue(grid.height==0, @"Height?");
}

- (void)testCoordinateGridLatitude
{
	FGCoordinateGrid *grid=[[FGCoordinateGrid alloc] init];
	[grid addCoordinate:CLLocationCoordinate2DMake(1, 0)];
	[grid addCoordinate:CLLocationCoordinate2DMake(-1, 0)];
	STAssertTrue(grid.top==1, @"Top %f?", grid.top);
	STAssertTrue(grid.bottom==-1, @"Bottom %f?", grid.bottom);
	STAssertTrue(grid.width==0, @"Width %f?", grid.width);
	STAssertTrue(grid.height==2, @"Height %f?", grid.height);
	STAssertTrue(grid.verticalCenter==0, @"VCenter %f?", grid.verticalCenter);
	STAssertTrue(grid.horizontalCenter==0, @"HCenter %f?", grid.horizontalCenter);
	[grid addCoordinate:CLLocationCoordinate2DMake(2, 0)];
	STAssertTrue(grid.top==2, @"Top %f?", grid.top);
	STAssertTrue(grid.bottom==-1, @"Bottom %f?", grid.bottom);
	STAssertTrue(grid.width==0, @"Width %f?", grid.width);
	STAssertTrue(grid.height==3, @"Height %f?", grid.height);
	STAssertTrue(grid.verticalCenter==0.5, @"VCenter %f?", grid.verticalCenter);
	STAssertTrue(grid.horizontalCenter==0, @"HCenter %f?", grid.horizontalCenter);
	[grid addCoordinate:CLLocationCoordinate2DMake(-2, 0)];
	STAssertTrue(grid.top==2, @"Top %f?", grid.top);
	STAssertTrue(grid.bottom==-2, @"Bottom %f?", grid.bottom);
	STAssertTrue(grid.width==0, @"Width %f?", grid.width);
	STAssertTrue(grid.height==4, @"Height %f?", grid.height);
	STAssertTrue(grid.verticalCenter==0, @"VCenter %f?", grid.verticalCenter);
	STAssertTrue(grid.horizontalCenter==0, @"HCenter %f?", grid.horizontalCenter);
}

- (void)testCoordinateGridLongitude
{
	FGCoordinateGrid *grid=[[FGCoordinateGrid alloc] init];
	[grid addCoordinate:CLLocationCoordinate2DMake(0, 1)];
	[grid addCoordinate:CLLocationCoordinate2DMake(0, -1)];
	STAssertTrue(grid.left==-1, @"Left %f?", grid.left);
	STAssertTrue(grid.right==1, @"Right %f?", grid.right);
	STAssertTrue(grid.width==2, @"Width %f?", grid.width);
	STAssertTrue(grid.height==0, @"Height %f?", grid.height);
	STAssertTrue(grid.horizontalCenter==0, @"HCenter %f?", grid.horizontalCenter);
	STAssertTrue(grid.verticalCenter==0, @"VCenter %f?", grid.verticalCenter);
	[grid addCoordinate:CLLocationCoordinate2DMake(0, -2)];
	STAssertTrue(grid.left==-2, @"Left %f?", grid.left);
	STAssertTrue(grid.right==1, @"Right %f?", grid.right);
	STAssertTrue(grid.width==3, @"Width %f?", grid.width);
	STAssertTrue(grid.height==0, @"Height %f?", grid.height);
	STAssertTrue(grid.horizontalCenter==-0.5, @"HCenter %f?", grid.horizontalCenter);
	STAssertTrue(grid.verticalCenter==0, @"VCenter %f?", grid.verticalCenter);
	[grid addCoordinate:CLLocationCoordinate2DMake(0, 2)];
	STAssertTrue(grid.left==-2, @"Left %f?", grid.left);
	STAssertTrue(grid.right==2, @"Right %f?", grid.right);
	STAssertTrue(grid.width==4, @"Width %f?", grid.width);
	STAssertTrue(grid.height==0, @"Height %f?", grid.height);
	STAssertTrue(grid.horizontalCenter==0, @"HCenter %f?", grid.horizontalCenter);
	STAssertTrue(grid.verticalCenter==0, @"VCenter %f?", grid.verticalCenter);
}

@end
