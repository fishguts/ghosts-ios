//
//  FGTestTimer.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/4/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestAsyncBase.h"
#import "FGTimer.h"


@interface FGTestTimer : FGTestAsyncBase
{
	FGTimer *_timer;
	FGTimer *_slice;
	int _ticks;
}

@end


@implementation FGTestTimer

- (void)testSuspend
{
	self->_timer=[FGTimer scheduledTimerWithTimeInterval:5.1 target:self selector:@selector(handleTick:) userInfo:nil repeats:NO];
	STAssertTrue([self->_timer isValid], @"Should be valid");
	STAssertTrue([self->_timer isActive], @"Should be active");
	// making sure we cover a full cycle: suspend->sleep->resume->sleep->suspend....
	for(int index=0; index<2; index++)
	{
		[self->_timer suspend];
		sleep(1);
		[self->_timer resume];
		STAssertNotNil([self->_timer fireDate], @"Fire date nil?");
		STAssertEqualsWithAccuracy([self->_timer fireInterval], 5.1-index, 0.01, @"Interval!=%f", 5.1-index);
		STAssertEqualsWithAccuracy([[self->_timer fireDate] timeIntervalSinceDate:[NSDate date]], [self->_timer fireInterval], 0.01, @"Fire date!=Interval");
		// test one round with resume behavior
		sleep(1);
	}
	[self->_timer invalidate]; self->_timer=nil;
}

- (void)testAbuse
{
	self->_timer=[FGTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(handleTick:) userInfo:nil repeats:YES];
	for(int index=0; index<2; index++)
	{
		[self->_timer suspend];
		STAssertTrue([self->_timer isValid], @"Should be valid");
		STAssertFalse([self->_timer isActive], @"Should not be active");
	}
	for(int index=0; index<2; index++)
	{
		[self->_timer resume];
		STAssertTrue([self->_timer isValid], @"Should be valid");
		STAssertTrue([self->_timer isActive], @"Should be active");
	}
	for(int index=0; index<2; index++)
	{
		[self->_timer invalidate];
		STAssertFalse([self->_timer isValid], @"Should not be valid");
		STAssertFalse([self->_timer isActive], @"Should not be active");
	}
	[self->_timer resume];
	STAssertFalse([self->_timer isValid], @"Should not be valid");
	STAssertFalse([self->_timer isActive], @"Should not be active");
	self->_timer=nil;
}

- (void)testActive
{
	self->_timer=[FGTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(handleTick:) userInfo:nil repeats:NO];
	STAssertTrue([self->_timer isValid], @"Should be valid");
	STAssertTrue([self->_timer isActive], @"Should be active");
	[self->_timer suspend];
	STAssertTrue([self->_timer isValid], @"Should be valid");
	STAssertFalse([self->_timer isActive], @"Should not be active");
	[self->_timer resume];
	STAssertTrue([self->_timer isValid], @"Should be valid");
	STAssertTrue([self->_timer isActive], @"Should be active");
	[self->_timer invalidate];
	STAssertFalse([self->_timer isValid], @"Should not be valid");
	STAssertFalse([self->_timer isActive], @"Should not be active");
	self->_timer=nil;
}

- (void)testRepeat
{
	self->_ticks=0;
	self->_timer=[FGTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(handleTick:) userInfo:nil repeats:YES];
	[super spinCurrentLoop];
}

#pragma mark - handlers
- (void)handleTick:(FGTimer*)timer
{
	[self->_slice invalidate]; self->_slice=nil;

	STAssertTrue([self->_timer isEqual:timer], @"Not current timer?");
	STAssertTrue([[self->_timer fireDate] timeIntervalSinceDate:[NSDate date]]<=0, @"Fire date?");
	STAssertEqualsWithAccuracy([[self->_timer fireDate] timeIntervalSinceDate:[NSDate date]], [self->_timer fireInterval], 0.01, @"Fire date!=Interval");
	if(++self->_ticks==10)
	{
		[self->_timer invalidate]; self->_timer=nil;
		[super exitCurrentLoop];
	}
	else
	{
		// check him out mid-tick
		self->_slice=[FGTimer scheduledTimerWithTimeInterval:[self->_timer timeInterval]/2 target:self selector:@selector(handleSlice:) userInfo:nil repeats:NO];
	}
}

- (void)handleSlice:(FGTimer*)timer
{
	STAssertTrue([[self->_timer fireDate] compare:[NSDate date]]==NSOrderedDescending, @"Fire date less than current?");
	STAssertEqualsWithAccuracy([self->_timer timeInterval]/2, [self->_timer fireInterval], 0.05, @"Fire date!=Interval");
	STAssertEqualsWithAccuracy([[self->_timer fireDate] timeIntervalSinceDate:[NSDate date]], [self->_timer fireInterval], 0.01, @"Fire date!=Interval");
}

@end
