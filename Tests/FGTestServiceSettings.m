//
//  FGTestServiceSettings.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestAsyncBase.h"
#import "FGSettingsGetService.h"
#import "FGSettingsVO.h"
#import "FGConstants.h"


@interface FGTestServiceSettings : FGTestAsyncBase
{
	FGSettingsGetService *_service;
}

@end

@implementation FGTestServiceSettings
- (void)setUp
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleGetSettingsSucceeded:) name:FGNotificationSettingsGetSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleGetSettingsFailed:) name:FGNotificationSettingsGetFailed object:nil];
}

- (void)tearDown
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)testService
{
	self->_service=[[FGSettingsGetService alloc] init];
	[self->_service send];
	[super spinCurrentLoop];
}

#pragma mark - observer handlers
- (void)handleGetSettingsSucceeded:(NSNotification*)notification
{
	NSArray *settings=[notification object];
	FGSettingsVO *setting=[settings objectAtIndex:0];

	STAssertTrue(([settings count]==1), @"count?");
	STAssertNotNil(setting.name, @"nil?");
	STAssertNotNil(setting.messagesPerQuery, @"nil?");
	STAssertNotNil(setting.queryIntervalMax, @"nil?");
	STAssertNotNil(setting.queryIntervalMin, @"nil?");
	STAssertNotNil(setting.radiusHorizontal, @"nil?");
	STAssertNotNil(setting.radiusVertical, @"nil?");
	STAssertTrue([setting.name isEqualToString:@"radial"], @"name?");

	[super exitCurrentLoop];
}

- (void)handleGetSettingsFailed:(NSNotification*)notification
{
	STAssertTrue(NO, @"failed?");

	[super exitCurrentLoop];
}
@end
