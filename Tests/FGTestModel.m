//
//  FGTestModel.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestAsyncBase.h"
#import "FGModel.h"
#import "FGConstants.h"

typedef enum
{
	TEST_START,
	TEST_OPEN,
	TEST_SAVE,
	TEST_CLOSE,
	TEST_END,
} TEST_TYPE;

@interface FGTestModel : FGTestAsyncBase
{
	TEST_TYPE _testType;
}

@end

@implementation FGTestModel
#pragma mark - configuration
- (void)setUp
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBOpenSucceeded:) name:FGNotificationDBOpenSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBOpenFailed:) name:FGNotificationDBOpenFailed object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBSaveSucceeded:) name:FGNotificationDBSaveSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBSaveFailed:) name:FGNotificationDBSaveFailed object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBCloseSucceeded:) name:FGNotificationDBCloseSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBCloseFailed:) name:FGNotificationDBCloseFailed object:nil];
}

- (void)tearDown
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Tests
- (void)testModel
{
	self->_testType=TEST_START;
	[self runNextTest];
	[super spinCurrentLoop];
}

#pragma mark - Private Interface
- (void)runNextTest
{
	switch(++self->_testType)
	{
		case TEST_START:
		case TEST_OPEN:
			STAssertFalse([[FGModel instance] isOpen], @"Model open?");
			[[FGModel instance] open];
			break;
		case TEST_SAVE:
			STAssertTrue([[FGModel instance] isOpen], @"Model not open?");
			[[FGModel instance] save];
			break;
		case TEST_CLOSE:
			STAssertTrue([[FGModel instance] isOpen], @"Model not open?");
			[[FGModel instance] close];
			break;
		case TEST_END:
			STAssertFalse([[FGModel instance] isOpen], @"Model open?");
			[super exitCurrentLoop];
			break;
	}
}

- (void)handleDBOpenSucceeded:(NSNotification*)notification
{
	[self runNextTest];
}

- (void)handleDBOpenFailed:(NSNotification*)notification
{
	STFail(@"DB open failed");
	[super exitCurrentLoop];
}

- (void)handleDBCloseSucceeded:(NSNotification*)notification
{
	[self runNextTest];
}

- (void)handleDBCloseFailed:(NSNotification*)notification
{
	STFail(@"DB close failed");
	[super exitCurrentLoop];
}


- (void)handleDBSaveSucceeded:(NSNotification*)notification
{
	[self runNextTest];
}

- (void)handleDBSaveFailed:(NSNotification*)notification
{
	STFail(@"DB save failed");
	[super exitCurrentLoop];
}

@end
