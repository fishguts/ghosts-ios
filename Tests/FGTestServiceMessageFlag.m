//
//  FGTestServiceMessageFlag.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGMessageVO.h"


@interface FGTestServiceMessageFlag : FGTestServiceBase

@end


@implementation FGTestServiceMessageFlag
- (void)testMessageFlag
{
	[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
	[super spinCurrentLoop];
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	[super postMessage:[FGModelFactory createMessage:@"testing" expiration:nil]];
}

- (void)handleMessagePosted:(FGMessageVO *)message
{
	[super flagMessage:message];
}

- (void)handleMessageFlagged:(FGMessageVO *)message
{
	STAssertTrue([message.flaggedUser isEqualToNumber:[NSNumber numberWithInt:1]], @"flagged?");
	[super deleteMessage:message];
}

- (void)handleMessageDeleted:(FGMessageVO *)message
{
	[super exitCurrentLoop];
}

@end
