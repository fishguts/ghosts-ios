//
//  FGTestServiceLoginFailure.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGProfileVO.h"

typedef enum
{
	TEST_START=0,
	TEST_BAD_USER,
	TEST_BAD_PASSWORD,
	TEST_INVALID_USER,
	TEST_INVALID_PASSWORD,
	TEST_INACTIVE_USER,
	TEST_DONE,
} TEST_TYPE;

@interface FGTestServiceLoginFailure : FGTestServiceBase
{
	TEST_TYPE _testType;
}

@end

@implementation FGTestServiceLoginFailure
- (void)testService
{
	self->_testType=TEST_START;

	[self runNextTest];
	[super spinCurrentLoop];
}

- (void)runNextTest
{
	switch(++self->_testType)
	{
		case TEST_BAD_USER:
		{
			[super loginUser:[FGModelFactory createLogin:@"nonesuch" password:@"password"]];
			break;
		}
		case TEST_BAD_PASSWORD:
		{
			[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"bad-password"]];
			break;
		}
		case TEST_INVALID_USER:
		{
			[super loginUser:[FGModelFactory createLogin:@"" password:@"password"]];
			break;
		}
		case TEST_INVALID_PASSWORD:
		{
			[super loginUser:[FGModelFactory createLogin:@"testcase" password:@""]];
			break;
		}
		case TEST_INACTIVE_USER:
		{
			[super loginUser:[FGModelFactory createLogin:@"root" password:@"password"]];
			break;
		}
		default:
		{
			[super exitCurrentLoop];
		}
	}
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	STAssertTrue(NO, @"Logged In?");
	[super exitCurrentLoop];
}

- (void)handleLoginFailed:(NSNotification *)notification
{
	[self runNextTest];
}

@end
