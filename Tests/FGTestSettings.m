//
//  FGTestSettings.m
//  Goggles
//
//  Created by Curtis Elsasser on 4/5/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGSettings.h"
#import "FGSettingsProxy.h"
#import "FGTypes.h"

@interface FGTestSettings : SenTestCase

@end


@implementation FGTestSettings
- (void)testShowIntroduction
{
	const BOOL original=[FGSettings getShowIntroduction];
	{
		[FGSettings setShowIntroduction:YES];
		STAssertTrue([FGSettings getShowIntroduction], @"should be yes");
		[FGSettings setShowIntroduction:NO];
		STAssertFalse([FGSettings getShowIntroduction], @"should be no");
	}
	[FGSettings setShowIntroduction:original];
}

- (void)testRetrieveWelcomeMessages
{
	const BOOL original=[FGSettings getRetrieveWelcomeMessages];
	{
		[FGSettings setRetrieveWelcomeMessages:YES];
		STAssertTrue([FGSettings getRetrieveWelcomeMessages], @"should be yes");
		[FGSettings setRetrieveWelcomeMessages:NO];
		STAssertFalse([FGSettings getRetrieveWelcomeMessages], @"should be no");
	}
	[FGSettings setRetrieveWelcomeMessages:original];
}

- (void)testExpiration
{
	NSArray *expirations=[FGSettings getExpirationObjects];
	FGExpirationVO *original=[FGSettings getExpirationDefault:expirations];
	{
		for(FGExpirationVO *expiration in expirations)
		{
			[FGSettings setExpirationDefault:expiration];
			STAssertTrue(expiration==[FGSettings getExpirationDefault:expirations], @"default?");
		}
	}
	[FGSettings setExpirationDefault:original];
}

- (void)testListSort
{
	FGListSortType original=[FGSettings getListSortMethod];
	{
		[FGSettings setListSortMethod:FGListSortChronological];
		STAssertEquals([FGSettings getListSortMethod], FGListSortChronological, @"Chronological?");
		[FGSettings setListSortMethod:FGListSortRank];
		STAssertEquals([FGSettings getListSortMethod], FGListSortRank, @"Rank?");
	}
	[FGSettings setListSortMethod:original];
}


- (void)testListSortProxy
{
	FGListSortType original=[FGSettingsProxy getListSortMethod];
	{
		[FGSettingsProxy setListSortMethod:FGListSortChronological];
		STAssertEquals([FGSettingsProxy getListSortMethod], FGListSortChronological, @"Chronological?");
		[FGSettingsProxy setListSortMethod:FGListSortRank];
		STAssertEquals([FGSettingsProxy getListSortMethod], FGListSortRank, @"Rank?");
	}
	[FGSettingsProxy setListSortMethod:original];
}


@end

