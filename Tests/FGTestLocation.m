//
//  FGTestLocation.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "CLLocation+FG.h"

@interface FGTestLocation : SenTestCase

@end


@implementation FGTestLocation

- (void)testPoles
{
	STAssertTrue([CLLocation metersToLongitudeDelta:1 atLatitude:-90]>0, @"Should not go to 0");
	STAssertTrue([CLLocation metersToLongitudeDelta:1 atLatitude:90]>0, @"Should not go to 0");
}

@end
