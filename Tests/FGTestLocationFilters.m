//
//  FGTestLocationFilters.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "FGTestAsyncBase.h"
#import "FGLocationFilterBase.h"
#import "FGLocationFilterAnimate.h"
#import "FGLocationFilterAccuracy.h"
#import "FGLocationFilterMajor.h"
#import "FGLocationFilterThreshold.h"


@interface FGTestLocationFilters : FGTestAsyncBase <FGLocationFilterDelegate>
{
	CLLocation *_locationReceived;
	FGLocationChangeType _changeTypeReceived;
}
@end


@implementation FGTestLocationFilters

- (void)testLocationFilterBase
{
	FGLocationFilterBase *filter=[[FGLocationFilterBase alloc] init];
	filter.delegate=self;

	for(int index=0; index<2; index++)
	{
		CLLocation *location=[[CLLocation alloc] initWithLatitude:0.0 longitude:index*0.0];
		FGLocationChangeType changeType=(index%2) ? FGLocationChangeTypeMinor : FGLocationChangeTypeMajor;

		[filter addLocation:location changeType:changeType];
		STAssertEqualObjects(location, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, changeType, @"Locations types not equal?");
	}

	// reset
	self->_locationReceived=nil;
	self->_changeTypeReceived=FGLocationChangeTypeNone;
}

- (void)testLocationFilterThresholdMeasure
{
	CLLocation *location1, *location2;
	FGLocationFilterThreshold *filter=[[FGLocationFilterThreshold alloc] initWithMethod:FGLocationFilterThresholdMeasure horizontalThreshold:100.0 verticalThreshold:100.0];
	filter.delegate=self;

	// repeat same location
	for(int index=0; index<2; index++)
	{
		// test same coordinate sequence: location1|location2 with filter out duplicates ON
		filter.filterOutDuplicates=YES;
		location1=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Location1?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMajor, @"First/Major?");
		location2=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location2 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Should be filtered out");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMajor, @"First/Major?");

		// repeat same sequence: location1|location2 with filter out duplicates OFF
		filter.filterOutDuplicates=NO;
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Location1?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMinor, @"Repeat type?");
		[filter addLocation:location2 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location2, self->_locationReceived, @"Should not be filtered out");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMinor, @"Repeat type?");
		
		// test minor coordinate
		location1=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.0001, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMinor, @"Minor coordinate?");

		// test major coordinate
		location1=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMajor, @"Major coordinate?");

		// test minor altitude
		location1=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index+50 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMinor, @"Minor altitude?");

		// test major altitude
		location1=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index+150 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location1, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMajor, @"Major altitude?");

		// test reset with same coordinate
		[filter reset:YES];
		location1=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index+150 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location1 changeType:FGLocationChangeTypeUndetermined];
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMajor, @"Reset?");
	}

	// reset
	self->_locationReceived=nil;
	self->_changeTypeReceived=FGLocationChangeTypeNone;
}

- (void)testLocationFilterThresholdFilter
{
	FGLocationFilterThreshold *filter=[[FGLocationFilterThreshold alloc] initWithMethod:FGLocationFilterThresholdFilter horizontalThreshold:100.0 verticalThreshold:100.0];
	filter.delegate=self;

	// repeat same location
	for(int index=0; index<2; index++)
	{
		CLLocation *location=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeUndetermined, @"Same as sent?");

		// repeat same location
		self->_locationReceived=nil;
		[filter addLocation:location changeType:FGLocationChangeTypeUndetermined];
		STAssertNil(self->_locationReceived, @"Not filtered?");

		// test minor coordinate
		location=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.0001, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location changeType:FGLocationChangeTypeUndetermined];
		STAssertNil(self->_locationReceived, @"Not filtered?");

		// test major coordinate
		location=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location changeType:FGLocationChangeTypeMajor];
		STAssertEqualObjects(location, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMajor, @"Same as sent?");

		// test minor altitude
		self->_locationReceived=nil;
		location=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index+50 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location changeType:FGLocationChangeTypeUndetermined];
		STAssertNil(self->_locationReceived, @"Not filtered?");

		// test major altitude
		location=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index+150 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location changeType:FGLocationChangeTypeMinor];
		STAssertEqualObjects(location, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeMinor, @"Same as sent?");

		// test reset with same coordinate
		[filter reset:YES];
		location=[[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(index+0.001, index) altitude:index+150 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:nil];
		[filter addLocation:location changeType:FGLocationChangeTypeUndetermined];
		STAssertEqualObjects(location, self->_locationReceived, @"Locations not equal?");
		STAssertEquals(self->_changeTypeReceived, FGLocationChangeTypeUndetermined, @"Reset?");
	}

	// reset
	self->_locationReceived=nil;
	self->_changeTypeReceived=FGLocationChangeTypeNone;
}

- (void)testLocationFilterChain
{
	FGLocationFilterBase *filter1=[[FGLocationFilterBase alloc] init];
	FGLocationFilterBase *filter2=[[FGLocationFilterBase alloc] init];
	FGLocationFilterBase *filter3=[[FGLocationFilterBase alloc] init];
	FGLocationFilterBase *head=[FGLocationFilterBase createFilterChainWithDelegates:[NSArray arrayWithObjects:filter1, filter2, filter3, self, nil]];

	STAssertEqualObjects(head, filter1, @"Head?");
	STAssertEqualObjects(filter1.delegate, filter2, @"Delegate 1?");
	STAssertEqualObjects(filter2.delegate, filter3, @"Delegate 2?");
	STAssertEqualObjects(filter3.delegate, self, @"Delegate 3?");
}


#pragma mark - FGLocationFilterDelegate 
- (void)locationFilter:(FGLocationFilterBase *)filter acceptLocation:(CLLocation *)location changeType:(FGLocationChangeType)changeType
{
	self->_locationReceived=location;
	self->_changeTypeReceived=changeType;
}

@end
