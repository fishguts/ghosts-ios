//
//  FGAsyncBase.h
//  Goggles
//
//  Created by Curtis Elsasser on 3/2/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

@interface FGTestAsyncBase : SenTestCase
{
	dispatch_semaphore_t _semaphore;
}
/**** Protected Interface ****/
- (void)spinCurrentLoop;
- (void)exitCurrentLoop;

@end
