//
//  FGTestLoggedInBase.m
//  Goggles
//
//  Created by Curtis Elsasser on 3/31/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGLoginService.h"
#import "FGLogoutService.h"
#import "FGMessagePostService.h"
#import "FGMessageDeleteService.h"
#import "FGMessageRateService.h"
#import "FGMessageFlagService.h"
#import "FGModelFactory.h"
#import "FGModelOperations.h"
#import "FGProfileVO.h"
#import "FGMessageVO.h"
#import "FGConstants.h"

@interface FGTestServiceBase()
{
	FGLoginService *_serviceLogin;
	FGLogoutService *_serviceLogout;
	FGMessagePostService *_servicePost;
	FGMessageDeleteService *_serviceDelete;
	FGMessageFlagService *_serviceFlag;
	FGMessageRateService *_serviceRate;
}

@end

@implementation FGTestServiceBase
@synthesize profile=_profile;
@synthesize message=_message;


- (void)setUp
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLoginSucceeded:) name:FGNotificationLoginSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLoginFailed:) name:FGNotificationLoginFailed object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLogoutSucceeded:) name:FGNotificationLogoutSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessagePostSucceeded:) name:FGNotificationMessagePostSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessagePostFailed:) name:FGNotificationMessagePostFailed object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageDeleteSucceeded:) name:FGNotificationMessageDeleteSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageDeleteFailed:) name:FGNotificationMessageDeleteFailed object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageFlagSucceeded:) name:FGNotificationMessageFlagSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageFlagFailed:) name:FGNotificationMessageFlagFailed object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageRateSucceeded:) name:FGNotificationMessageRateSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageRateFailed:) name:FGNotificationMessageRateFailed object:nil];
}

- (void)tearDown
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	// clean up whatever message we made
	[FGModelOperations clearDatabase];
}

- (void)loginUser:(FGProfileVO*)profile
{
	self->_profile=profile;
	self->_serviceLogin=[[FGLoginService alloc] initWithProfile:profile];
	[self->_serviceLogin send];
}

- (void)logoutUser:(FGProfileVO*)profile
{
	self->_profile=profile;
	self->_serviceLogout=[[FGLogoutService alloc] initWithProfile:profile];
	[self->_serviceLogout send];
}

- (void)postMessage:(FGMessageVO*)message
{
	self->_message=message;
	self->_servicePost=[[FGMessagePostService alloc] initWithMessage:message];
	[self->_servicePost send];
}

- (void)deleteMessage:(FGMessageVO*)message
{
	self->_message=message;
	self->_serviceDelete=[[FGMessageDeleteService alloc] initWithMessage:message];
	[self->_serviceDelete send];
}

- (void)flagMessage:(FGMessageVO*)message
{
	self->_message=message;
	self->_serviceFlag=[[FGMessageFlagService alloc] initWithMessage:message];
	[self->_serviceFlag send];
}

- (void)rateMessage:(FGMessageVO*)message rating:(int)rating
{
	self->_message=message;
	self->_serviceRate=[[FGMessageRateService alloc] initWithMessage:message rating:[NSNumber numberWithInt:rating]];
	[self->_serviceRate send];
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO*)profile
{
	NSAssert(NO, @"Override");
}

- (void)handleLoggedOut:(FGProfileVO*)profile
{
	NSAssert(NO, @"Override");
}

- (void)handleMessagePosted:(FGMessageVO*)message
{
	NSAssert(NO, @"Override");
}

- (void)handleMessageDeleted:(FGMessageVO *)message
{
	NSAssert(NO, @"Override");
}

- (void)handleMessageRated:(FGMessageVO*)message
{
	NSAssert(NO, @"Override");
}

- (void)handleMessageFlagged:(FGMessageVO*)message
{
	NSAssert(NO, @"Override");
}


#pragma mark - observer handlers
- (void)handleLoginSucceeded:(NSNotification*)notification
{
	STAssertTrue([[notification object] isEqual:self->_profile], @"login?");
	[self handleLoggedIn:self->_profile];
}

- (void)handleLoginFailed:(NSNotification*)notification
{
	STAssertTrue(NO, @"Login Failed?");
	[super exitCurrentLoop];
}

- (void)handleLogoutSucceeded:(NSNotification*)notification
{
	STAssertTrue([[notification object] isEqual:self->_profile], @"logout?");
	[self handleLoggedOut:self->_profile];
}

- (void)handleMessagePostSucceeded:(NSNotification*)notification
{
	STAssertTrue([[notification object] isEqual:self->_message], @"post?");
	STAssertTrue([self->_message.owner isEqual:self->_profile], @"post owner?");
	[self handleMessagePosted:self->_message];
}

- (void)handleMessagePostFailed:(NSNotification*)notification
{
	STAssertTrue(NO, @"Message Post Failed?");
	[super exitCurrentLoop];
}

- (void)handleMessageDeleteSucceeded:(NSNotification*)notification
{
	STAssertTrue([[notification object] isEqual:self->_message], @"delete?");
	STAssertTrue([self->_message.owner isEqual:self->_profile], @"delete owner?");
	STAssertTrue([self->_message isDeleted], @"still exists?");
	[self handleMessageDeleted:self->_message];
}

- (void)handleMessageDeleteFailed:(NSNotification*)notification
{
	STAssertTrue(NO, @"message delete failed?");
	[super exitCurrentLoop];
}

- (void)handleMessageFlagSucceeded:(NSNotification*)notification
{
	STAssertTrue([[notification object] isEqual:self->_message], @"flag?");
	STAssertTrue([self->_message.owner isEqual:self->_profile], @"flag owner?");
	[self handleMessageFlagged:self->_message];
}

- (void)handleMessageFlagFailed:(NSNotification*)notification
{
	STAssertTrue(NO, @"message flag failed?");
	[super exitCurrentLoop];
}

- (void)handleMessageRateSucceeded:(NSNotification*)notification
{
	STAssertTrue([[notification object] isEqual:self->_message], @"rate?");
	STAssertTrue([self->_message.owner isEqual:self->_profile], @"rate owner?");
	[self handleMessageRated:self->_message];
}

- (void)handleMessageRateFailed:(NSNotification*)notification
{
	STAssertTrue(NO, @"message rate failed?");
	[super exitCurrentLoop];
}

@end
