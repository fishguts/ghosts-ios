//
//  FGTestFactories.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGModelQueries.h"
#import "FGModelOperations.h"
#import "FGMessageVO.h"
#import "FGProfileVO.h"
#import "FGExpirationVO.h"
#import "FGSettings.h"


@interface FGTestFactories : FGTestServiceBase

@end

@implementation FGTestFactories
#pragma mark - Public Interface
- (void)testMessageFactory
{
	// we need a login for messages
	[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
	[super spinCurrentLoop];
}

- (void)testLoginFactory
{
	[FGModelOperations clearDatabase];
	STAssertTrue([[FGModelQueries fetchLogins] count]==0, @"Should be no profiles");

	FGProfileVO *profile=[FGModelFactory createLogin:@"dummy" password:@"password"];
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy"] isEqual:profile], @"Profile should exist");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile], @"Login should exist");
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([profile.username isEqualToString:@"dummy"], @"username?");
	STAssertTrue([profile.password isEqualToString:@"password"], @"password?");
	STAssertTrue([profile.login isEqual:[NSNumber numberWithBool:YES]], @"login?");
	STAssertNil(profile.loggedIn, @"loggedIn?");
}

- (void)testRegistrationFactory
{
	[FGModelOperations clearDatabase];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==0, @"Should be no profiles");

	FGProfileVO *profile=[FGModelFactory createRegistration:@"dummy" email:@"dummy@domain.com" password:@"password"];
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy"] isEqual:profile], @"Profile should exist");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([profile.username isEqualToString:@"dummy"], @"username?");
	STAssertTrue([profile.password isEqualToString:@"password"], @"password?");
	STAssertTrue([profile.email isEqualToString:@"dummy@domain.com"], @"email?");
	STAssertTrue([profile.login isEqual:[NSNumber numberWithBool:YES]], @"login?");
	STAssertNil(profile.loggedIn, @"loggedIn?");
}

- (void)testLoginReuse
{
	[FGModelOperations clearDatabase];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==0, @"Should be no profiles");

	FGProfileVO *profile1=[FGModelFactory createLogin:@"dummy1" password:@"password"];
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile1], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy1"] isEqual:profile1], @"Profile should exist");
	STAssertTrue([profile1.login isEqual:[NSNumber numberWithBool:YES]], @"login?");

	FGProfileVO *profile2=[FGModelFactory createLogin:@"dummy1" password:@"password"];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==1, @"Should be only 1 profile");
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should still be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile2], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy1"] isEqual:profile2], @"Profile should still exist");
	STAssertTrue([profile2.login isEqual:[NSNumber numberWithBool:YES]], @"login?");
}

- (void)testRegistrationReuse
{
	[FGModelOperations clearDatabase];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==0, @"Should be no profiles");

	FGProfileVO *profile1=[FGModelFactory createRegistration:@"dummy1" email:@"dummy1@domain.com" password:@"password"];
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile1], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy1"] isEqual:profile1], @"Profile should exist");
	STAssertTrue([profile1.login isEqual:[NSNumber numberWithBool:YES]], @"login?");

	FGProfileVO *profile2=[FGModelFactory createRegistration:@"dummy1" email:@"dummy1@domain.com" password:@"password"];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==1, @"Should be only 1 profile");
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should still be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile2], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy1"] isEqual:profile2], @"Profile should still exist");
	STAssertTrue([profile2.login isEqual:[NSNumber numberWithBool:YES]], @"login?");
}

- (void)testLoginSwitch
{
	[FGModelOperations clearDatabase];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==0, @"Should be no profiles");

	FGProfileVO *profile1=[FGModelFactory createLogin:@"dummy1" password:@"password"];
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile1], @"Login should be profile1");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy1"] isEqual:profile1], @"Profile should exist");
	STAssertTrue([profile1.login isEqual:[NSNumber numberWithBool:YES]], @"login?");

	FGProfileVO *profile2=[FGModelFactory createLogin:@"dummy2" password:@"password"];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==2, @"Should 2 profiles");
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile2], @"Login should be profile2");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy2"] isEqual:profile2], @"Profile should exist");
	STAssertNil(profile1.login, @"login profile1?");
	STAssertTrue([profile2.login isEqual:[NSNumber numberWithBool:YES]], @"login profile2?");
}

- (void)testRegistrationSwitch
{
	[FGModelOperations clearDatabase];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==0, @"Should be no profiles");

	FGProfileVO *profile1=[FGModelFactory createRegistration:@"dummy1" email:@"dummy1@domain.com" password:@"password"];
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile1], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy1"] isEqual:profile1], @"Profile should exist");
	STAssertTrue([profile1.login isEqual:[NSNumber numberWithBool:YES]], @"login?");
	STAssertTrue([profile1.email isEqualToString:@"dummy1@domain.com"], @"email?");

	FGProfileVO *profile2=[FGModelFactory createRegistration:@"dummy2" email:@"dummy2@domain.com" password:@"password"];
	STAssertTrue([[FGModelQueries fetchProfiles] count]==2, @"Should be 2 profiles");
	STAssertTrue([[FGModelQueries fetchLogins] count]==1, @"Should still be only 1 login");
	STAssertTrue([[FGModelQueries fetchLogin] isEqual:profile2], @"Login should be same");
	STAssertTrue([[FGModelQueries fetchProfileByName:@"dummy2"] isEqual:profile2], @"Profile should exist");
	STAssertTrue([profile2.login isEqual:[NSNumber numberWithBool:YES]], @"login?");
	STAssertTrue([profile2.email isEqualToString:@"dummy2@domain.com"], @"email?");
}


#pragma mark - Private Interface
- (void)internalTestMessage
{
	FGMessageVO *message=[FGModelFactory createMessage:@"dummy" expiration:nil];
	STAssertTrue([message.text isEqualToString:@"dummy"], @"message?");
	STAssertTrue([message.created compare:[NSDate date]]<=0, @"created?");
	STAssertTrue([message.owner isEqual:super.profile], @"owner?");
	STAssertNil(message.expiration, @"expiration?");
}

- (void)internalTestMessageExpiration
{
	NSArray *expirations=[FGSettings getExpirationObjects];
	for(FGExpirationVO *expiration in expirations)
	{
		FGMessageVO *message=[FGModelFactory createMessage:@"dummy" expiration:expiration];
		STAssertTrue([message.text isEqualToString:@"dummy"], @"message?");
		STAssertTrue([message.created compare:[NSDate date]]<=0, @"created?");
		STAssertTrue([message.owner isEqual:super.profile], @"owner?");
		if([expiration.id isEqualToString:@"None"])
		{
			STAssertNil(message.expiration, @"expiration?");
		}
		else if([expiration.id isEqualToString:@"Hour"])
		{
			STAssertTrue([message.expiration timeIntervalSinceDate:message.created]>0, @"hour?");
			STAssertTrue([message.expiration timeIntervalSinceDate:message.created]<=(60*60), @"hour?");
		}
		else if([expiration.id isEqualToString:@"Day"])
		{
			STAssertTrue([message.expiration timeIntervalSinceDate:message.created]==(60*60*24), @"day?");
		}
		else if([expiration.id isEqualToString:@"Week"])
		{
			STAssertTrue([message.expiration timeIntervalSinceDate:message.created]==(60*60*24*7), @"week?");
		}
		else if([expiration.id isEqualToString:@"Month"])
		{
			STAssertTrue([message.expiration timeIntervalSinceDate:message.created]==(60*60*24*31), @"month?");
		}
		else if([expiration.id isEqualToString:@"Year"])
		{
			STAssertTrue([message.expiration timeIntervalSinceDate:message.created]==(60*60*24*365), @"year?");
		}
	}
}

- (void)internalTestEndOfHourExpiration
{
	NSCalendar *calendar=[NSCalendar currentCalendar];
	NSDateComponents *components=[[NSDateComponents alloc] init];

	// create an arbitrary date as a reference
	[components setYear:2000];
	[components setMonth:1];
	[components setDay:1];
	[components setSecond:0];
	for(int hour=0; hour<=24; hour++)
	{
		[components setHour:hour];
		for(int minute=0; minute<=60; minute++)
		{
			[components setMinute:minute];
			NSDate *date=[calendar dateFromComponents:components];
			NSArray *expirations=[FGSettings getExpirationObjects:date];
			FGExpirationVO *expiration=[expirations objectAtIndex:[expirations indexOfObjectPassingTest:^BOOL(FGExpirationVO *obj, NSUInteger idx, BOOL *stop) {
				return (*stop=[[obj id] isEqualToString:@"Hour"]);
			}]];
			NSDate *expirationDate=[expiration getExpirationDate];
			const NSTimeInterval interval=(minute%60) ? 60*60-60*minute : 60*60;
			STAssertTrue([expirationDate timeIntervalSinceDate:date]==interval, [NSString stringWithFormat:@"%f!=%f?", [expirationDate timeIntervalSinceDate:date], interval]);
		}
	}
}

#pragma mark - observers
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	[self internalTestMessage];
	[self internalTestMessageExpiration];
	[self internalTestEndOfHourExpiration];
	[super exitCurrentLoop];
}
@end
