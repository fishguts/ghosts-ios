//
//  FGTestCategories.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "NSNumber+FG.h"
#import "NSObject+FG.h"


@interface FGTestCategories : SenTestCase

@end


@implementation FGTestCategories

- (void)testNumberEquality
{
	STAssertFalse([NSNumber isEqualNumber:[NSNumber numberWithInt:1] toNumber:nil], @"Equal?");
	STAssertFalse([NSNumber isEqualNumber:nil toNumber:[NSNumber numberWithInt:1]], @"Equal?");
	STAssertFalse([NSNumber isEqualNumber:[NSNumber numberWithInt:1] toNumber:[NSNumber numberWithInt:2]], @"Equal?");
	STAssertTrue([NSNumber isEqualNumber:[NSNumber numberWithInt:1] toNumber:[NSNumber numberWithInt:1]], @"Not equal?");
}

- (void)testObjectEquality
{
	STAssertFalse([NSObject isEqual:[[NSObject alloc] init] obj2:nil], @"Equal?");
	STAssertFalse([NSObject isEqual:nil obj2:[[NSObject alloc] init]], @"Equal?");
	STAssertFalse([NSObject isEqual:[NSNumber numberWithInt:1] obj2:[NSNumber numberWithInt:2]], @"Equal?");
	STAssertTrue([NSObject isEqual:[NSNumber numberWithInt:1] obj2:[NSNumber numberWithInt:1]], @"Not equal?");
}

@end
