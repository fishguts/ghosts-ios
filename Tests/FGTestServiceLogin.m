//
//  FGTestServiceLogin.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGProfileVO.h"


@interface FGTestServiceLogin : FGTestServiceBase

@end

@implementation FGTestServiceLogin
- (void)testService
{
	[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
	[super spinCurrentLoop];
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	STAssertNil(profile.email, @"Shouldn't exchange email");
	STAssertTrue([profile.username isEqualToString:@"testcase"], @"username?");
	[super logoutUser:profile];
}

- (void)handleLoggedOut:(FGProfileVO *)profile
{
	STAssertTrue([profile.username isEqualToString:@"testcase"], @"username?");
	[super exitCurrentLoop];
}

@end
