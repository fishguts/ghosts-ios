//
//  FGTestNotifications.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGLogProxy.h"
#import "FGStatusVO.h"
#import "FGConstants.h"
#import "NSNotification+FG.h"


@interface FGTestNotifications : SenTestCase
{
	BOOL _notified;
	NSString *_expectedType;
	NSString *_expectedText;
	NSString *_expectedTitle;
	NSString *_expectedMessage;
	FGStatusVO *_expectedStatus;
}

@end

@implementation FGTestNotifications

- (void)setUp
{
	[super setUp];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationDebug object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationInfo object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationWarn object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAlertNotification:) name:FGNotificationError object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleGeneralNotification:) name:FGNotificationTestMessage object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleGeneralNotification:) name:FGNotificationTestStatus object:nil];
}

- (void)tearDown
{
	[super tearDown];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)testAlertNotifications
{
	self->_expectedType=FGNotificationDebug;
	self->_expectedText=@"Debug notification";
	self->_notified=NO;
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification debugNotificationWithObject:self text:self->_expectedText]];
	STAssertTrue(self->_notified, @"Notification?");

	self->_expectedType=FGNotificationInfo;
	self->_expectedText=@"Information notification";
	self->_notified=NO;
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification infoNotificationWithObject:self text:self->_expectedText]];
	STAssertTrue(self->_notified, @"Notification?");

	self->_expectedType=FGNotificationWarn;
	self->_expectedText=@"Warn notification";
	self->_notified=NO;
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:self->_expectedText]];
	STAssertTrue(self->_notified, @"Notification?");

	self->_expectedType=FGNotificationError;
	self->_expectedText=@"Error notification";
	self->_notified=NO;
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:self->_expectedText]];
	STAssertTrue(self->_notified, @"Notification?");
}

- (void)testMessageNotifications
{
	self->_expectedType=FGNotificationTestMessage;
	self->_expectedText=nil;
	self->_expectedMessage=@"message";
	self->_expectedTitle=@"title";
	self->_expectedStatus=nil;
	self->_notified=NO;
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationTestMessage object:self title:self->_expectedTitle message:self->_expectedMessage]];
	STAssertTrue(self->_notified, @"Notification?");

	self->_expectedType=FGNotificationTestStatus;
	self->_expectedText=@"status";
	self->_expectedMessage=nil;
	self->_expectedTitle=nil;
	self->_expectedStatus=[FGStatusVO statusFromText:@"text" code:@"code"];
	self->_notified=NO;
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:FGNotificationTestStatus object:self text:self->_expectedText status:self->_expectedStatus]];
	STAssertTrue(self->_notified, @"Notification?");
}

- (void)testValueEmbedding
{
	NSNotification *notification;

	notification=[NSNotification notificationWithName:@"dummy" object:self text:@"test"];
	STAssertEqualObjects([notification name], @"dummy", @"Name?");
	STAssertEqualObjects([notification text], @"test", @"Text?");
	STAssertNil([notification timestamp], @"Timestamp not nil?");

	notification=[NSNotification notificationWithName:@"dummy" object:self text:@"test" status:[FGStatusVO statusFromText:@"status" code:@"code"]];
	STAssertEqualObjects([notification name], @"dummy", @"Name?");
	STAssertEqualObjects([notification text], @"test", @"Text?");
	STAssertEqualObjects([[notification status] text], @"status", @"Status text?");
	STAssertEqualObjects([[notification status] code], @"code", @"Status code?");
	STAssertNil([notification timestamp], @"Timestamp not nil?");

	notification=[NSNotification notificationWithName:@"dummy" object:self value:@"value" text:@"test" status:[FGStatusVO statusFromText:@"status" code:@"code"]];
	STAssertEqualObjects([notification name], @"dummy", @"Name?");
	STAssertEqualObjects([notification text], @"test", @"Text?");
	STAssertEqualObjects([notification value], @"value", @"Value?");
	STAssertEqualObjects([[notification status] text], @"status", @"Status text?");
	STAssertEqualObjects([[notification status] code], @"code", @"Status code?");
	STAssertNil([notification timestamp], @"Timestamp not nil?");

	notification=[NSNotification notificationWithName:@"dummy" object:self title:@"title" message:@"message"];
	STAssertEqualObjects([notification name], @"dummy", @"Name?");
	STAssertEqualObjects([notification title], @"title", @"Title?");
	STAssertNil([notification timestamp], @"Timestamp not nil?");

	notification=[NSNotification notificationWithName:@"dummy" object:self value:@"value"];
	STAssertEqualObjects([notification name], @"dummy", @"Name?");
	STAssertEqualObjects([notification value], @"value", @"Value?");
	STAssertNil([notification timestamp], @"Timestamp not nil?");
}

#pragma mark - Private Observers
- (void)handleAlertNotification:(NSNotification*)notification
{
	STAssertFalse(self->_notified, @"Notified?");
	STAssertTrue([NSObject isEqual:self->_expectedType obj2:[notification name]], @"Type not equal");
	STAssertTrue([NSObject isEqual:self->_expectedText obj2:[notification text]], @"Text not equal");
	STAssertNotNil([notification timestamp], @"Timestamp?");
	STAssertTrue([[notification timestamp] compare:[NSDate date]]==NSOrderedAscending, @"Timestamp value?");
	STAssertNil(self->_expectedMessage, @"Message not nil");
	STAssertNil(self->_expectedTitle, @"Title not nil");
	STAssertNil(self->_expectedStatus, @"Status not nil");
	self->_notified=YES;
}

- (void)handleGeneralNotification:(NSNotification*)notification
{
	STAssertFalse(self->_notified, @"Notified?");
	STAssertTrue([NSObject isEqual:self->_expectedType obj2:[notification name]], @"Type not equal");
	STAssertTrue([NSObject isEqual:self->_expectedText obj2:[notification text]], @"Text not equal");
	STAssertTrue([NSObject isEqual:self->_expectedMessage obj2:[notification message]], @"Message not equal");
	STAssertTrue([NSObject isEqual:self->_expectedTitle obj2:[notification title]], @"Titles not equal");
	STAssertTrue([NSObject isEqual:self->_expectedStatus obj2:[notification status]], @"Status not equal");
	self->_notified=YES;
}
@end
