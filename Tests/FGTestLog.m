//
//  FGTestLog.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGLogProxy.h"
#import "NSNotification+FG.h"


@interface FGTestLog : SenTestCase

@end

@implementation FGTestLog
- (void)setUp
{
	[super setUp];
	[[FGLogProxy instance] startup];
}

- (void)tearDown
{
	[super tearDown];
	[[FGLogProxy instance] shutdown];
}

- (void)testLog
{
	FGLogProxy *proxy=[FGLogProxy instance];

	STAssertTrue([[proxy getAlerts] count]==0, @"Count?");

	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification debugNotificationWithObject:self text:@"Debug notification"]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification infoNotificationWithObject:self text:@"Information notification"]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:self text:@"Warn notification"]];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Error notification"]];
	STAssertTrue([[proxy getAlerts] count]==4, @"Count?");

	// not testing much here. Just make sure it doesn't blow up.
	[[FGLogProxy instance] saveAlerts];

	[proxy clearAlerts];
	STAssertTrue([[proxy getAlerts] count]==0, @"Count?");

}

@end
