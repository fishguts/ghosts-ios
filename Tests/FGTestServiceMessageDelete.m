//
//  FGTestServiceMessageDelete.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGModelQueries.h"
#import "FGMessageVO.h"


@interface FGTestServiceMessageDelete : FGTestServiceBase

@end


@implementation FGTestServiceMessageDelete
- (void)testMessageDelete
{
	[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
	[super spinCurrentLoop];
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	[super postMessage:[FGModelFactory createMessage:@"testing" expiration:nil]];
}

- (void)handleMessagePosted:(FGMessageVO *)message
{
	[super deleteMessage:message];
}

- (void)handleMessageDeleted:(FGMessageVO *)message
{
	// base class covers tests
	[super exitCurrentLoop];
}

@end
