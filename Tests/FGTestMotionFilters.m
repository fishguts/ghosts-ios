//
//  FGTestMotionFilters.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/31/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGMotionFilterBase.h"
#import "FGMotionFilterThreshold.h"


@interface FGTestMotionFilters : SenTestCase <FGMotionFilterDelegate>
{
	GLKVector3 _orientationReceived;
	BOOL _orientationUpdated;
}

@end


@implementation FGTestMotionFilters

- (void)testBase
{
	GLKVector3 vector;
	FGMotionFilterBase *filter=[[FGMotionFilterBase alloc] init];
	filter.delegate=self;

	self->_orientationUpdated=NO;
	vector=GLKVector3Make(1.0, 2.0, 3.0);
	[filter addOrientation:vector];
	STAssertTrue(self->_orientationUpdated, @"Not updated?");
	STAssertTrue(GLKVector3AllEqualToVector3(self->_orientationReceived, vector), @"Not equal?");
}

- (void)testThresholdFilter
{
	GLKVector3 vector1;
	FGMotionFilterThreshold *filter=[[FGMotionFilterThreshold alloc] initWithThreshold:1.0];
	filter.delegate=self;

	self->_orientationUpdated=NO;
	vector1=GLKVector3Make(0, 0, 0);
	[filter addOrientation:vector1];
	STAssertTrue(self->_orientationUpdated, @"Not updated?");
	STAssertTrue(GLKVector3AllEqualToVector3(self->_orientationReceived, vector1), @"Not equal?");

	self->_orientationUpdated=NO;
	[filter addOrientation:GLKVector3Make(0.5, 0, 0)];
	STAssertFalse(self->_orientationUpdated, @"Updated?");
	[filter addOrientation:GLKVector3Make(0, 0.5, 0)];
	STAssertFalse(self->_orientationUpdated, @"Updated?");
	[filter addOrientation:GLKVector3Make(0, 0, 0.5)];
	STAssertFalse(self->_orientationUpdated, @"Updated?");

	self->_orientationUpdated=NO;
	vector1=GLKVector3Make(1.0, 0, 0);
	[filter addOrientation:vector1];
	STAssertTrue(self->_orientationUpdated, @"Not updated?");
	STAssertTrue(GLKVector3AllEqualToVector3(self->_orientationReceived, vector1), @"Not equal?");

	self->_orientationUpdated=NO;
	vector1=GLKVector3Make(0, 1.0, 0);
	[filter addOrientation:vector1];
	STAssertTrue(self->_orientationUpdated, @"Not updated?");
	STAssertTrue(GLKVector3AllEqualToVector3(self->_orientationReceived, vector1), @"Not equal?");

	self->_orientationUpdated=NO;
	vector1=GLKVector3Make(0, 0, 1.0);
	[filter addOrientation:vector1];
	STAssertTrue(self->_orientationUpdated, @"Not updated?");
	STAssertTrue(GLKVector3AllEqualToVector3(self->_orientationReceived, vector1), @"Not equal?");

	// make sure it's holding on to last update
	self->_orientationUpdated=NO;
	[filter addOrientation:GLKVector3Make(0, 0, 1.5)];
	STAssertFalse(self->_orientationUpdated, @"Updated?");
}

#pragma mark - FGMotionFilterDelegate
- (void)motionFilter:(FGMotionFilterBase *)filter acceptOrientation:(GLKVector3)orientation
{
	self->_orientationUpdated=YES;
	self->_orientationReceived=orientation;
}

@end
