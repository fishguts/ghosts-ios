//
//  FGAsyncBase.m
//  Goggles
//
//  Created by Curtis Elsasser on 3/2/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestAsyncBase.h"

@implementation FGTestAsyncBase

- (void)spinCurrentLoop
{
	self->_semaphore=dispatch_semaphore_create(0);
    while(dispatch_semaphore_wait(self->_semaphore, DISPATCH_TIME_NOW))
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:1]];
	}
	dispatch_release(self->_semaphore);
}

- (void)exitCurrentLoop
{
	dispatch_semaphore_signal(self->_semaphore);
}
@end
