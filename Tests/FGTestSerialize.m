//
//  FGTestSerialize.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGSerializer.h"
#import "FGModelSerializer.h"
#import "FGModelFactory.h"
#import "FGMessageVO.h"
#import "FGProfileVO.h"
#import "FGFeedbackVO.h"
#import "UIDevice+FG.h"


@interface FGTestSerialize : SenTestCase

@end

@implementation FGTestSerialize
- (void)testDateEncoding
{
	// we toss away millis so keep it on a second boundary
	for(NSTimeInterval offset=0; offset<24*60*60; offset+=30*60)
	{
		NSDate *dateSource=[NSDate dateWithTimeIntervalSince1970:offset];
		NSString *dateEncoded=[FGSerializeUtils dateToString:dateSource];
		NSDate *dateDecoded=[FGSerializeUtils stringToDate:dateEncoded];

		STAssertTrue([dateSource isEqualToDate:dateDecoded], @"dates not the same?");
	}
}

- (void)testLoginEncoding
{
	FGProfileVO *profile=[FGModelFactory createLogin:@"bones" password:@"jones"];
	NSData *encoded=[FGModelSerializer buildLoginRequest:profile pretty:NO];
	NSDictionary *decoded=[FGSerializer jsonToData:encoded];

	STAssertEqualObjects([decoded valueForKey:@"username"], @"bones", @"username?");
	STAssertEqualObjects([decoded valueForKey:@"password"], @"jones", @"password?");
}

- (void)testLogoutEncoding
{
	FGProfileVO *profile=[FGModelFactory createLogin:@"bones" password:@"jones"];
	NSData *encoded=[FGModelSerializer buildLogoutRequest:profile pretty:NO];
	NSDictionary *decoded=[FGSerializer jsonToData:encoded];

	STAssertEqualObjects([decoded valueForKey:@"username"], @"bones", @"username?");
}

- (void)testRegisterEncoding
{
	FGProfileVO *profile=[FGModelFactory createRegistration:@"bones" email:@"bones@jones.com" password:@"jones"];
	NSData *encoded=[FGModelSerializer buildRegisterRequest:profile pretty:NO];
	NSDictionary *decoded=[FGSerializer jsonToData:encoded];

	STAssertEqualObjects([decoded valueForKey:@"username"], @"bones", @"username?");
	STAssertEqualObjects([decoded valueForKey:@"password"], @"jones", @"password?");
	STAssertEqualObjects([decoded valueForKey:@"email"], @"bones@jones.com", @"email?");
}

- (void)testFeedbackEncoding
{
	NSString *text=@"Marvel thought he invented invented";
	NSString *device=[[UIDevice currentDevice] encoded];
	
	FGProfileVO *profile=[FGModelFactory createLogin:@"bones" password:@"jones"];
	FGFeedbackVO *feedback=[FGFeedbackVO feedbackFromText:text owner:profile];
	NSData *encoded=[FGModelSerializer buildSendFeedbackRequest:feedback pretty:NO];
	NSDictionary *decoded=[FGSerializer jsonToData:encoded];

	STAssertEqualObjects([decoded valueForKey:@"ownerId"], @"bones", @"username?");
	STAssertEqualObjects([decoded valueForKey:@"text"], text, @"feedback?");
	STAssertEqualObjects([decoded valueForKey:@"device"], device, @"device?");
}

@end
