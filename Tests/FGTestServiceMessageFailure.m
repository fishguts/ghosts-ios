//
//  FGTestServiceMessageFailure.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGMessageVO.h"
#import "FGConstants.h"

typedef enum
{
	TEST_START=0,
	TEST_LOGIN,
	TEST_SHORT_MESSAGE,
	TEST_LONG_MESSAGE,
	TEST_BAD_RATE,
	TEST_BAD_FLAG,
	TEST_BAD_DELETE,
	TEST_DONE,
} TEST_TYPE;

@interface FGTestServiceMessageFailure : FGTestServiceBase
{
	TEST_TYPE _testType;
}

@end

@implementation FGTestServiceMessageFailure
- (void)testService
{
	self->_testType=TEST_START;

	[self runNextTest];
	[super spinCurrentLoop];
}

- (void)runNextTest
{
	switch(++self->_testType)
	{
		case TEST_LOGIN:
		{
			[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
			break;
		}
		case TEST_SHORT_MESSAGE:
		{
			[super postMessage:[FGModelFactory createMessage:[FGTestServiceMessageFailure createDummyText:FGMinMessageLength-1] expiration:nil]];
			break;
		}
		case TEST_LONG_MESSAGE:
		{
			[super postMessage:[FGModelFactory createMessage:[FGTestServiceMessageFailure createDummyText:2500] expiration:nil]];
			break;
		}
		case TEST_BAD_RATE:
		{
			// attempt to rate a message that doesn't exist
			[super rateMessage:[FGModelFactory createMessage:@"Fail Rate" expiration:nil] rating:1];
			break;
		}
		case TEST_BAD_FLAG:
		{
			// attempt to flag a message that doesn't exist
			[super flagMessage:[FGModelFactory createMessage:@"Fail Flag" expiration:nil]];
			break;
		}
		case TEST_BAD_DELETE:
		{
			// attempt to delete a message with a non-existant id
			FGMessageVO *message=[FGModelFactory createMessage:@"Fail Delete" expiration:nil];
			message.id=[NSNumber numberWithInt:-1];
			[super deleteMessage:message];
			break;
		}
		default:
		{
			[super exitCurrentLoop];
		}
	}
}

+ (NSString*)createDummyText:(int)length
{
	NSMutableString *buffer=[[NSMutableString alloc] init];
	for(int index=0; index<length; index++)
	{
		[buffer appendFormat:@"%d", index%10];
	}
	return buffer;
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	[self runNextTest];
}

- (void)handleMessagePostFailed:(NSNotification *)notification
{
	STAssertEqualObjects(self.message.sortPrefix, @"db", @"User|Error?");
	[self runNextTest];
}

- (void)handleMessageFlagFailed:(NSNotification *)notification
{
	[self runNextTest];
}

- (void)handleMessageRateFailed:(NSNotification *)notification
{
	[self runNextTest];
}

- (void)handleMessageDeleteFailed:(NSNotification *)notification
{
	[self runNextTest];
}
@end
