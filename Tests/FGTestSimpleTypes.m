//
//  FGTestSimpleTypes.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/10/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGDateRange.h"
#import "FGNumberRange.h"
#import "FGReferenceCount.h"

@interface FGTestSimpleTypes : SenTestCase

@end


@implementation FGTestSimpleTypes
- (void)testDateRange
{
	FGDateRange *range;
	NSDate *date1=[NSDate date];
	sleep(1);
	NSDate *date2=[NSDate date];

	range=[FGDateRange dateRange];
	STAssertNil(range.dateFrom, @"From not nil?");
	STAssertNil(range.dateTo, @"To not nil?");
	STAssertEquals(range.elapsed, (NSTimeInterval)0, @"interval not 0?");

	range=[FGDateRange dateRangeWithFrom:date1];
	STAssertNotNil(range.dateFrom, @"From nil?");
	STAssertNil(range.dateTo, @"To not nil?");
	STAssertEquals(range.elapsed, (NSTimeInterval)0, @"interval not 0?");

	range=[FGDateRange dateRangeWithFrom:date1 andTo:date2];
	STAssertNotNil(range.dateFrom, @"From nil?");
	STAssertNotNil(range.dateTo, @"To nil?");
	STAssertTrue(range.elapsed>0.9, @"interval?");
	[range swapDatesAndSetTo:date2];
	STAssertEquals(range.elapsed, (NSTimeInterval)0, @"interval not 0?");
	[range swapDatesAndSetTo:date1];
	STAssertTrue(range.elapsed<0.9, @"interval?");
}

- (void)testNumberRange
{
	FGNumberRange *range=[[FGNumberRange alloc] init];

	STAssertNil(range.min, @"Min not nil?");
	STAssertNil(range.max, @"Max not nil?");

	// test max & min
	[range addInt:1];
	STAssertEquals([range.min intValue], (int)1, @"Min?");
	STAssertEquals([range.max intValue], (int)1, @"Max?");
	[range addInt:-1];
	STAssertEquals([range.min intValue], (int)-1, @"Min?");
	STAssertEquals([range.max intValue], (int)1, @"Max?");
	[range addDouble:-1.5];
	STAssertEquals([range.min doubleValue], (double)-1.5, @"Min?");
	STAssertEquals([range.max doubleValue], (double)1.0, @"Max?");
	[range addNumber:[NSNumber numberWithLong:2]];
	STAssertEquals([range.min doubleValue], (double)-1.5, @"Min?");
	STAssertEquals([range.max doubleValue], (double)2.0, @"Max?");

	// test offsets
	range=[[FGNumberRange alloc] init];
	[range addDouble:0.0];
	[range addDouble:1.0];
	STAssertEquals([range getDoubleRange], 1.0, @"Range");
	STAssertEquals([range getDoubleMid], 0.5, @"Middle");
	STAssertEquals([range getDoubleOffsetFromMin:0], 0.0, @"Offset?");
	STAssertEquals([range getDoubleOffsetFromMin:0.1], 0.1, @"Offset?");
	STAssertEquals([range getDoubleOffsetFromMax:0], -1.0, @"Offset?");
	STAssertEquals([range getDoubleOffsetFromMax:0.1], -0.9, @"Offset?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMin:0.0], 0.0, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMin:0.5], 0.5, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMin:1.0], 1.0, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMax:0.0], 1.0, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMax:0.5], 0.5, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMax:1.0], 0.0, FLT_EPSILON, @"Ratio?");

	// test with non zero from
	range=[[FGNumberRange alloc] init];
	[range addDouble:10.0];
	[range addDouble:15.0];
	STAssertEquals([range getDoubleRange], 5.0, @"Range");
	STAssertEquals([range getDoubleMid], 12.5, @"Middle");
	STAssertEquals([range getDoubleOffsetFromMin:10.0], 0.0, @"Offset?");
	STAssertEquals([range getDoubleOffsetFromMin:11.0], 1.0, @"Offset?");
	STAssertEquals([range getDoubleOffsetFromMax:15.0], 0.0, @"Offset?");
	STAssertEquals([range getDoubleOffsetFromMax:14.0], -1.0, @"Offset?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMin:10.0], 0.0, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMin:12.5], 0.5, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMin:15.0], 1.0, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMax:10.0], 1.0, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMax:12.5], 0.5, FLT_EPSILON, @"Ratio?");
	STAssertEqualsWithAccuracy([range getDoubleRatioFromMax:15.0], 0.0, FLT_EPSILON, @"Ratio?");
}

- (void)testReferenceCount
{
	FGReferenceCount *reference=[[FGReferenceCount alloc] init];

	STAssertEquals([reference count], 0, @"Not 0?");
	[reference addReference];
	STAssertEquals([reference count], 1, @"Not 1?");
	[reference addReference];
	STAssertEquals([reference count], 2, @"Not 2?");
	[reference releaseReference];
	STAssertEquals([reference count], 1, @"Not 1?");
	[reference releaseReference];
	STAssertEquals([reference count], 0, @"Not 0?");
	[reference releaseReference];
	STAssertEquals([reference count], -1, @"Not -1?");
}


@end
