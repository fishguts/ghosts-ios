//
//  FGTestLoggedInBase.h
//  Goggles
//
//  Created by Curtis Elsasser on 3/31/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestAsyncBase.h"

@class FGProfileVO;
@class FGMessageVO;

@interface FGTestServiceBase : FGTestAsyncBase

@property (nonatomic, strong) FGProfileVO *profile;
@property (nonatomic, strong) FGMessageVO *message;

/**** Public Interface ****/
- (void)loginUser:(FGProfileVO*)profile;
- (void)logoutUser:(FGProfileVO*)profile;
- (void)postMessage:(FGMessageVO*)message;
- (void)deleteMessage:(FGMessageVO*)message;
- (void)flagMessage:(FGMessageVO*)message;
- (void)rateMessage:(FGMessageVO*)message rating:(int)rating;

/**** Protected Interface: success handlers ****/
- (void)handleLoggedIn:(FGProfileVO*)profile;
- (void)handleLoggedOut:(FGProfileVO*)profile;
- (void)handleMessagePosted:(FGMessageVO*)message;
- (void)handleMessageDeleted:(FGMessageVO*)message;
- (void)handleMessageRated:(FGMessageVO*)message;
- (void)handleMessageFlagged:(FGMessageVO*)message;

/**** Protected Interface: failure handlers - defaults assert failure ****/
- (void)handleLoginFailed:(NSNotification*)notification;
- (void)handleMessagePostFailed:(NSNotification*)notification;
- (void)handleMessageDeleteFailed:(NSNotification*)notification;
- (void)handleMessageFlagSucceeded:(NSNotification*)notification;
- (void)handleMessageFlagFailed:(NSNotification*)notification;
- (void)handleMessageRateFailed:(NSNotification*)notification;


@end
