//
//  FGTestLocationProcessors.m
//  Goggles
//
//  Created by Curtis Elsasser on 5/6/13.
//  Copyright (c) 2013 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "FGLocationCompression.h"


@interface FGTestLocationProcessors : SenTestCase

@end


@implementation FGTestLocationProcessors

- (void)testLocationCompression
{
	FGLocationCompression *compression;

	// test unity
	{
		GLKVector3 vectors[]={ GLKVector3Make(0, 0, 0), GLKVector3Make(0, 10, 0) };
		compression=[[FGLocationCompression alloc] initWithAltitudeCompression:1.0];

		[compression compress:vectors count:2];
		STAssertEquals(vectors[0].x, (float)0, @"X[0]?");
		STAssertEquals(vectors[0].y, (float)0, @"Y[0]?");
		STAssertEquals(vectors[0].z, (float)0, @"Z[0]?");
		STAssertEquals(vectors[1].x, (float)0, @"X[1]?");
		STAssertEquals(vectors[1].y, (float)10.0, @"Y[1]?");
		STAssertEquals(vectors[1].z, (float)0, @"Z[1]?");
	}

	// test orientation
	{
		GLKVector3 vectors[]={ GLKVector3Make(0, 5.0, 0), GLKVector3Make(0, 10, 0) };
		compression=[[FGLocationCompression alloc] initWithAltitudeCompression:1.0 orientAroundCoordinate:[NSNumber numberWithFloat:100.0]];

		[compression compress:vectors count:2];
		STAssertEquals(vectors[0].x, (float)0, @"X[0]?");
		STAssertEquals(vectors[0].y, (float)97.5, @"Y[0]?");
		STAssertEquals(vectors[0].z, (float)0, @"Z[0]?");
		STAssertEquals(vectors[1].x, (float)0, @"X[1]?");
		STAssertEquals(vectors[1].y, (float)102.5, @"Y[1]?");
		STAssertEquals(vectors[1].z, (float)0, @"Z[1]?");
	}
	
	// test compression
	{
		GLKVector3 vectors[]={ GLKVector3Make(0, -30.0, 0), GLKVector3Make(0, 0, 0), GLKVector3Make(0, 30.0, 0) };
		compression=[[FGLocationCompression alloc] initWithAltitudeCompression:1.0/3.0];

		[compression compress:vectors count:3];
		STAssertEquals(vectors[0].x, (float)0, @"X[0]?");
		STAssertEquals(vectors[0].y, (float)-10.0, @"Y[0]?");
		STAssertEquals(vectors[0].z, (float)0, @"Z[0]?");
		STAssertEquals(vectors[1].x, (float)0, @"X[1]?");
		STAssertEquals(vectors[1].y, (float)0, @"Y[1]?");
		STAssertEquals(vectors[1].z, (float)0, @"Z[1]?");
		STAssertEquals(vectors[2].x, (float)0, @"X[2]?");
		STAssertEquals(vectors[2].y, (float)10.0, @"Y[2]?");
		STAssertEquals(vectors[2].z, (float)0, @"Z[2]?");
	}
	
	// test compression 2
	{
		GLKVector3 vectors[]={ GLKVector3Make(0, -30.0, 0), GLKVector3Make(0, 10.0, 0) };
		compression=[[FGLocationCompression alloc] initWithAltitudeCompression:0.25];

		[compression compress:vectors count:2];
		STAssertEquals(vectors[0].x, (float)0, @"X[0]?");
		STAssertEquals(vectors[0].y, (float)-15.0, @"Y[0]?");
		STAssertEquals(vectors[0].z, (float)0, @"Z[0]?");
		STAssertEquals(vectors[1].x, (float)0, @"X[1]?");
		STAssertEquals(vectors[1].y, (float)-5.0, @"Y[1]?");
		STAssertEquals(vectors[1].z, (float)0, @"Z[1]?");
	}
	
	// test compression + orientation
	{
		GLKVector3 vectors[]={ GLKVector3Make(0, -30.0, 0), GLKVector3Make(0, 10.0, 0) };
		compression=[[FGLocationCompression alloc] initWithAltitudeCompression:0.25 orientAroundCoordinate:[NSNumber numberWithFloat:0.0]];

		[compression compress:vectors count:2];
		STAssertEquals(vectors[0].x, (float)0, @"X[0]?");
		STAssertEquals(vectors[0].y, (float)-5.0, @"Y[0]?");
		STAssertEquals(vectors[0].z, (float)0, @"Z[0]?");
		STAssertEquals(vectors[1].x, (float)0, @"X[1]?");
		STAssertEquals(vectors[1].y, (float)5.0, @"Y[1]?");
		STAssertEquals(vectors[1].z, (float)0, @"Z[1]?");
	}
	
}
@end
