//
//  FGTestContainers.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "NSArray+FG.h"
#import "NSDictionary+FG.h"

@interface FGTestContainers : SenTestCase

@end


@implementation FGTestContainers

- (void)testArrayFirst
{
	NSArray *array;

	array=[NSArray array];
	STAssertNil([array firstObject], @"Not nil?");

	array=[NSArray arrayWithObjects:@"One", @"Two", nil];
	STAssertEqualObjects([array firstObject], @"One", @"First?");
}

- (void)testDictionaryDefaults
{
	NSDictionary *map;
	NSString *keyText=@"key";
	NSObject *keyObject=[NSNumber numberWithInt:1];

	map=[NSDictionary dictionary];
	STAssertEqualObjects([map valueForKey:keyText defaultValue:@"default"], @"default", @"Not equal?");
	STAssertEqualObjects([map objectForKey:keyObject defaultValue:@"default"], @"default", @"Not equal?");

	map=[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1", @"2", nil]
									forKeys:[NSArray arrayWithObjects:keyText, keyObject, nil]];
	STAssertEqualObjects([map valueForKey:keyText defaultValue:@"default"], @"1", @"Not equal?");
	STAssertEqualObjects([map objectForKey:keyObject defaultValue:@"default"], @"2", @"Not equal?");
}

- (void)testDictionaryWrite
{
	NSMutableDictionary *map;

	map=[[NSMutableDictionary alloc] init];

	[map setBool:YES forKey:@"key"];
	STAssertEquals([map boolForKey:@"key"], YES, @"NO?");

	[map setBool:NO forKey:@"key"];
	STAssertEquals([map boolForKey:@"key"], NO, @"YES?");
}

@end
