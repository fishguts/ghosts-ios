//
//  FGTestServiceMessagePost.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGMessageVO.h"
#import "FGProfileVO.h"
#import "FGExpirationVO.h"
#import "FGSettings.h"

typedef enum
{
	TEST_START=0,
	TEST_LOGIN,
	TEST_NORMAL,
	TEST_EXPIRE_HOUR,
	TEST_EXPIRE_DAY,
	TEST_EXPIRE_WEEK,
	TEST_EXPIRE_MONTH,
	TEST_EXPIRE_YEAR,
	TEST_EXPIRED,
	TEST_SORT_PREFIX,
	TEST_END,
} TEST_TYPE;

@interface FGTestServiceMessagePost : FGTestServiceBase
{
	TEST_TYPE _testType;
	NSArray *_expirations;
}
@end


@implementation FGTestServiceMessagePost
- (void)testMessagePost
{
	self->_expirations=[FGSettings getExpirationObjects];
	self->_testType=TEST_START;

	[self runNextTest];
	[super spinCurrentLoop];
}

- (void)runNextTest
{
	switch(++self->_testType)
	{
		case TEST_START: // adding start to keep compiler from complaining
		case TEST_LOGIN:
		{
			[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
			break;
		}
		case TEST_NORMAL:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:nil]];
			break;
		}
		case TEST_EXPIRE_HOUR:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:[self expirationForId:@"Hour"]]];
			break;
		}
		case TEST_EXPIRE_DAY:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:[self expirationForId:@"Day"]]];
			break;
		}
		case TEST_EXPIRE_WEEK:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:[self expirationForId:@"Week"]]];
			break;
		}
		case TEST_EXPIRE_MONTH:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:[self expirationForId:@"Month"]]];
			break;
		}
		case TEST_EXPIRE_YEAR:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:[self expirationForId:@"Year"]]];
			break;
		}
		case TEST_EXPIRED:
		{
			FGExpirationVO *expiration=[FGExpirationVO expirationWithDeadline:[[NSDate date] dateByAddingTimeInterval:-1] id:@"dummy" text:@"expired"];
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:expiration]];
			break;
		}
		case TEST_SORT_PREFIX:
		{
			FGMessageVO *message=[FGModelFactory createMessage:@"testing" expiration:nil];
			STAssertEqualObjects(message.sortPrefix, @"da", @"User|Post?");
			[super postMessage:message];
			break;
		}
		case TEST_END:
		{
			[super exitCurrentLoop];
			break;
		}
	}
}

#pragma mark - Utilities
- (FGExpirationVO*)expirationForId:(NSString*)id
{
	NSUInteger index=[self->_expirations indexOfObjectPassingTest:^BOOL(FGExpirationVO *obj, NSUInteger idx, BOOL *stop) {
		if([id isEqualToString:[obj id]])
		{
			*stop=YES;
			return TRUE;
		}
		return FALSE;
	}];
	STAssertTrue(index!=NSNotFound, @"Expiration id not found");
	return [self->_expirations objectAtIndex:index];
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	[self runNextTest];
}

- (void)handleMessagePosted:(FGMessageVO *)message
{
	STAssertTrue([message.text isEqualToString:@"testing"], @"text?");
	STAssertTrue([message.owner isEqual:[super profile]], @"user?");
	STAssertEqualObjects(message.sortPrefix, @"dc", @"User|Post?");
	switch(self->_testType)
	{
		case TEST_NORMAL:
		{
			STAssertNil(message.expiration, @"expiration?");
			break;
		}
		case TEST_EXPIRE_HOUR:
		{
			FGExpirationVO *expiration=[self expirationForId:@"Hour"];
			// note: this guy was acting strangely even though dates appeared the same. Make sure seconds are aligned and if so we are good.
			STAssertTrue([message.expiration timeIntervalSince1970]==[[expiration getExpirationDate:message.created] timeIntervalSince1970], @"dates?");
			break;
		}
		case TEST_EXPIRE_DAY:
		{
			FGExpirationVO *expiration=[self expirationForId:@"Day"];
			STAssertTrue([message.expiration isEqualToDate:[expiration getExpirationDate:message.created]], @"dates?");
			break;
		}
		case TEST_EXPIRE_WEEK:
		{
			FGExpirationVO *expiration=[self expirationForId:@"Week"];
			STAssertTrue([message.expiration isEqualToDate:[expiration getExpirationDate:message.created]], @"dates?");
			break;
		}
		case TEST_EXPIRE_MONTH:
		{
			FGExpirationVO *expiration=[self expirationForId:@"Month"];
			STAssertTrue([message.expiration isEqualToDate:[expiration getExpirationDate:message.created]], @"dates?");
			break;
		}
		case TEST_EXPIRE_YEAR:
		{
			FGExpirationVO *expiration=[self expirationForId:@"Year"];
			STAssertTrue([message.expiration isEqualToDate:[expiration getExpirationDate:message.created]], @"dates?");
			break;
		}
		case TEST_EXPIRED:
		{
			STAssertTrue([message.expiration compare:message.created]==NSOrderedAscending, @"dates?");
			break;
		}
	}
	[super deleteMessage:message];
}

- (void)handleMessageDeleted:(FGMessageVO *)message
{
	[self runNextTest];
}

@end
