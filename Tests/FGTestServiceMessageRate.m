//
//  FGTestServiceMessageRate.m
//  Goggles
//
//  Created by Curtis Elsasser on 9/23/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTestServiceBase.h"
#import "FGModelFactory.h"
#import "FGModelQueries.h"
#import "FGMessageVO.h"

typedef enum
{
	TEST_START=0,
	TEST_LOGIN,
	TEST_POST,
	TEST_LIKE,
	TEST_DISLIKE,
	TEST_DELETE,
	TEST_DONE
} TEST_TYPE;

@interface FGTestServiceMessageRate : FGTestServiceBase
{
	TEST_TYPE _testType;
}
@end


@implementation FGTestServiceMessageRate
- (void)testMessageRate
{
	self->_testType=TEST_START;
	[self runNextTest];
	[super spinCurrentLoop];
}

- (void)runNextTest
{
	switch(++self->_testType)
	{
		case TEST_START:
		case TEST_LOGIN:
		{
			[super loginUser:[FGModelFactory createLogin:@"testcase" password:@"password"]];
			break;
		}
		case TEST_POST:
		{
			[super postMessage:[FGModelFactory createMessage:@"testing" expiration:nil]];
			break;
		}
		case TEST_LIKE:
		{
			[super rateMessage:super.message rating:1];
			break;
		}
		case TEST_DISLIKE:
		{
			[super rateMessage:super.message rating:-1];
			break;
		}
		case TEST_DELETE:
		{
			[super deleteMessage:super.message];
			break;
		}
		case TEST_DONE:
		{
			[super exitCurrentLoop];
			break;
		}
	}
}

#pragma mark - Protected Interface
- (void)handleLoggedIn:(FGProfileVO *)profile
{
	[self runNextTest];
}

- (void)handleMessagePosted:(FGMessageVO *)message
{
	[self runNextTest];
}

- (void)handleMessageRated:(FGMessageVO *)message
{
	if(self->_testType==TEST_LIKE)
	{
		STAssertTrue([message.ratingLikes isEqualToNumber:[NSNumber numberWithInt:1]], @"Likes?");
		STAssertTrue([message.ratingDislikes isEqualToNumber:[NSNumber numberWithInt:0]], @"Dislikes?");
		STAssertTrue([message.ratingSum isEqualToNumber:[NSNumber numberWithInt:1]], @"Sum?");
		STAssertTrue([message.ratingUser isEqualToNumber:[NSNumber numberWithInt:1]], @"User?");
		STAssertTrue([message isUserRatingLike], @"Like method?");
		STAssertFalse([message isUserRatingDislike], @"Dislike method?");
	}
	else
	{
		STAssertTrue(self->_testType==TEST_DISLIKE, @"Test?");
		STAssertTrue([message.ratingLikes isEqualToNumber:[NSNumber numberWithInt:0]], @"Likes?");
		STAssertTrue([message.ratingDislikes isEqualToNumber:[NSNumber numberWithInt:1]], @"Dislikes?");
		STAssertTrue([message.ratingSum isEqualToNumber:[NSNumber numberWithInt:-1]], @"Sum?");
		STAssertTrue([message.ratingUser isEqualToNumber:[NSNumber numberWithInt:-1]], @"User?");
		STAssertTrue([message isUserRatingDislike], @"Dislike method?");
		STAssertFalse([message isUserRatingLike], @"Like method?");
	}
	[self runNextTest];
}

- (void)handleMessageDeleted:(FGMessageVO *)message
{
	[self runNextTest];
}

@end
